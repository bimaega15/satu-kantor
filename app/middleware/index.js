const jwt = require("jsonwebtoken");
const { userMappingHelper } = require("../helper");
require("dotenv").config();

const protect = async (req, res, next) => {
  let token;
  if (
    req.headers.authorization &&
    req.headers.authorization.startsWith("Bearer")
  ) {
    try {
      token = req.headers.authorization.split(" ")[1];
      const decoded = jwt.verify(token, process.env.JWT_SECRET);
      req.user = decoded;

      next();
    } catch (error) {
      res.status(401).json({
        status: 401,
        message: "User access ditolak",
        result: error,
      });
    }
  }

  if (!token) {
    res.status(401).json({
      status: 401,
      message: "User access ditolak",
    });
  }
};

const webProtect = async (req, res, next) => {
  const auth = req.session.token;
  let token;
  if (auth) {
    try {
      const urlCurrent = req._parsedUrl.pathname;
      token = auth;
      const decoded = jwt.verify(token, process.env.JWT_SECRET);
      req.user = decoded;

      // res locals
      let user = decoded.data;
      const getMapping = await userMappingHelper.getJoinDataMapping(
        user.jenis_mapping,
        null,
        user.id_user_mapping
      );
      let name_profile = null;
      if (user.jenis_mapping == "client") {
        name_profile = getMapping.client.nama_client;
      }

      res.locals = { ...decoded.data, name_profile: name_profile };

      req.session.currentUrl = urlCurrent;
      next();
    } catch (error) {
      console.log("get error midleware", error.message);

      req.flash("error", "Authorization akses kehalaman admin");
      res.redirect("/login");
      next();
    }
  }

  if (!token) {
    req.flash("error", "Silahkan login terlebih dahulu");
    res.redirect("/login");
  }
};

const userGuide = async (req, res, next) => {
  const auth = req.session.token;
  if (auth) {
    req.is_login = true;
  }
  next();
};

module.exports = {
  protect,
  webProtect,
  userGuide,
};
