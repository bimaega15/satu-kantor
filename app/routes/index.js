const { body } = require("express-validator");
const express = require("express");
const router = express.Router();

const loginController = require("../controller/LoginController");
const registerController = require("../controller/RegisterController");
const settingPlaystoreController = require("../controller/PrivacyTerms/SettingPlaystoreController");

// login
router
  .route("/login")
  .get(loginController.index)
  .post(
    body("username").notEmpty().withMessage("Username wajib diisi"),
    body("password_hash").notEmpty().withMessage("Password wajib diisi"),
    loginController.store
  );

// register
router.route("/register").get(registerController.index);

router.route("/").get(loginController.index);
router.route("/logout").get(loginController.logout);

router.route("/policy").get(settingPlaystoreController.index);
router.route("/terms").get(settingPlaystoreController.terms);

module.exports = router;
