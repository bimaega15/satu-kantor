const express = require("express");
const router = express.Router();
const { body } = require("express-validator");
const formidable = require("formidable");

const laporanKehadiranController = require("../../controller/ModuleLaporan/KehadiranController");

// kehadiran
router.route("/kehadiran").get(laporanKehadiranController.index);
router.route("/kehadiran/:id_pegawai_jadwal/detail").get(laporanKehadiranController.detail);

module.exports = router;
