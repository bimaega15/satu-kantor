const express = require("express");
const router = express.Router();
const { body } = require("express-validator");
const formidable = require("formidable");

const {
  pegawaiHelper,
  userHelper,
  userMappingHelper,
} = require("../../helper/index");
const {} = require("../../middleware/index");

const dashboardController = require("../../controller/ModuleMaster/DashboardController");
const clientController = require("../../controller/ModuleMaster/ClientController");
const jadwalController = require("../../controller/ModuleMaster/JadwalController");
const dashboardClientController = require("../../controller/ModuleMaster/DashboardClientController");
const cabangController = require("../../controller/ModuleMaster/CabangController");
const unitController = require("../../controller/ModuleMaster/UnitController");
const pegawaiController = require("../../controller/ModuleMaster/PegawaiController");
const jabatanController = require("../../controller/ModuleMaster/JabatanController");
const jenisAbsensiController = require("../../controller/ModuleMaster/JenisAbsensiController");
const jenisCutiController = require("../../controller/ModuleMaster/JenisCutiController");
const jenisLemburController = require("../../controller/ModuleMaster/JenisLemburController");
const komponenGajiController = require("../../controller/ModuleMaster/KomponenGajiController");
const langgananController = require("../../controller/ModuleMaster/LanggananController");
const tugasController = require("../../controller/ModuleMaster/TugasController");
const lokasiTugasController = require("../../controller/ModuleMaster/LokasiTugasController");
const penugasanController = require("../../controller/ModuleMaster/PenugasanController");

const formDataImport = (req, res, next) => {
  const form = formidable({ multiples: true });
  form.parse(req, (err, fields, files) => {
    req.body = fields;
    req.body.importData = files;
    if (err) {
      next(err);
    }
    next();
  });
};

// dashboard
router.route("/dashboard").get(dashboardController.index);

// client
router
  .route("/client")
  .get(clientController.index)
  .post(
    body("nama_client")
      .notEmpty()
      .withMessage("Nama client wajib diisi")
      .trim(),
    body("alamat").notEmpty().withMessage("Alamat wajib disii").trim(),
    body("kontak")
      .notEmpty()
      .withMessage("Kontak wajib diisi")
      .trim()
      .isNumeric()
      .withMessage("Wajib berupa angka"),
    body("username").notEmpty().withMessage("Username wajib diisi").trim(),
    body("username").custom(async (value, meta) => {
      const { page, id_client } = meta.req.body;
      if (page == "add") {
        let check = await userHelper.checkIdentitas("username", page, value);
        if (check > 0) {
          return Promise.reject("Username ini sudah digunakan");
        }
      }
      if (page == "edit") {
        const getMapping = await userMappingHelper.UserMapping.findOne({
          where: {
            id_mapping: id_client,
          },
        });
        const getUsers = await userHelper.Users.findOne({
          where: {
            id: getMapping.id_user,
          },
        });
        let check = await userHelper.checkIdentitas(
          "username",
          page,
          value,
          getUsers.id
        );

        if (check > 0) {
          return Promise.reject("Username ini sudah digunakan");
        }
      }
    }),
    body("password_hash").custom(async (value, meta) => {
      const { page, confirm_password_hash } = meta.req.body;
      if (page == "add") {
        if (value == null) {
          return Promise.reject("Password wajib diisi");
        }

        if (value != null && confirm_password_hash != null) {
          if (value != confirm_password_hash) {
            return Promise.reject(
              "Password tidak sama dengan confirm password"
            );
          }
        }
      }
      if (page == "edit") {
        if (value != null && confirm_password_hash != null) {
          if (value != confirm_password_hash) {
            return Promise.reject(
              "Password tidak sama dengan confirm password"
            );
          }
        }
      }
    }),
    body("email")
      .notEmpty()
      .withMessage("Email wajib diisi")
      .trim()
      .isEmail()
      .withMessage("Email tidak valid"),
    clientController.store
  );
router.route("/client/:id_client/edit").get(clientController.edit);
router.route("/client/:id_client/detail").get(clientController.detail);
router.route("/client/:id_client/delete").get(clientController.deleteData);

// client
router.route("/dashboardClient").get(dashboardClientController.index);

// jadwal
router
  .route("/jadwal")
  .get(jadwalController.index)
  .post(
    body("waktu_masuk")
      .notEmpty()
      .withMessage("Waktu masuk wajib diisi")
      .trim(),
    body("waktu_keluar").notEmpty().withMessage("Waktu keluar disii").trim(),
    body("warna").notEmpty().withMessage("Warna wajib diisi").trim(),
    body("jenis").notEmpty().withMessage("Jenis wajib diisi").trim(),
    jadwalController.store
  );
router.route("/jadwal/:id_jadwal/edit").get(jadwalController.edit);
router.route("/jadwal/:id_jadwal/delete").get(jadwalController.deleteData);
router.route("/jadwal/import").post(
  formDataImport,
  body("importData").custom(async (value, meta) => {
    const fileImport = meta.req.body.importData;
    const { importData } = fileImport;

    if (importData.size > 0) {
      const imageType = importData.type;
      const mimeType = [
        "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
      ];
      const checkType = mimeType.includes(imageType);
      if (!checkType) {
        return Promise.reject(
          "Format file tidak didukung, format yang didukung yaitu: " +
            mimeType.join(",")
        );
      }
    } else {
      return Promise.reject("File wajib di upload");
    }
  }),
  jadwalController.importData
);

// cabang
router
  .route("/cabang")
  .get(cabangController.index)
  .post(
    body("nama_cabang")
      .notEmpty()
      .withMessage("Nama cabang wajib diisi")
      .trim(),
    body("longitude").notEmpty().withMessage("Longitude wajib diisi").trim(),
    body("latitude").notEmpty().withMessage("Latitude wajib diisi").trim(),
    body("alamat").notEmpty().withMessage("Alamat wajib disii").trim(),
    body("kontak")
      .notEmpty()
      .withMessage("Kontak wajib diisi")
      .trim()
      .isNumeric()
      .withMessage("Kontak wajib berupa angka"),
    cabangController.store
  );
router.route("/cabang/:id_cabang/edit").get(cabangController.edit);
router.route("/cabang/:id_cabang/delete").get(cabangController.deleteData);
router.route("/cabang/import").post(
  formDataImport,
  body("importData").custom(async (value, meta) => {
    const fileImport = meta.req.body.importData;
    const { importData } = fileImport;

    if (importData.size > 0) {
      const imageType = importData.type;
      const mimeType = [
        "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
      ];
      const checkType = mimeType.includes(imageType);
      if (!checkType) {
        return Promise.reject(
          "Format file tidak didukung, format yang didukung yaitu: " +
            mimeType.join(",")
        );
      }
    } else {
      return Promise.reject("File wajib di upload");
    }
  }),
  cabangController.importData
);

// unit
router
  .route("/unit")
  .get(unitController.index)
  .post(
    body("id_cabang").notEmpty().withMessage("Cabang wajib diisi").trim(),
    body("nama_unit").notEmpty().withMessage("Unit wajib diisi").trim(),
    unitController.store
  );
router.route("/unit/:id_unit/edit").get(unitController.edit);
router.route("/unit/:id_unit/delete").get(unitController.deleteData);
router.route("/unit/:id_unit/detail").get(unitController.detail);
router.route("/unit/getFindInUnit").get(unitController.getFindInUnit);
router.route("/unit/getFindAllUnit").get(unitController.getFindAllUnit);
router.route("/unit/import").post(
  formDataImport,
  body("importData").custom(async (value, meta) => {
    const fileImport = meta.req.body.importData;
    const { importData } = fileImport;

    if (importData.size > 0) {
      const imageType = importData.type;
      const mimeType = [
        "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
      ];
      const checkType = mimeType.includes(imageType);
      if (!checkType) {
        return Promise.reject(
          "Format file tidak didukung, format yang didukung yaitu: " +
            mimeType.join(",")
        );
      }
    } else {
      return Promise.reject("File wajib di upload");
    }
  }),
  unitController.importData
);

const formData = (req, res, next) => {
  const form = formidable({ multiples: true });
  form.parse(req, (err, fields, files) => {
    req.body = fields;
    req.body.gambar = files;
    if (err) {
      next(err);
    }
    next();
  });
};

// pegawai
router
  .route("/pegawai")
  .get(pegawaiController.index)
  .post(
    formData,
    body("username").notEmpty().withMessage("Username wajib diisi").trim(),
    body("username").custom(async (value, meta) => {
      const { page, id_pegawai } = meta.req.body;
      if (page == "add") {
        let check = await userHelper.checkIdentitas("username", page, value);
        if (check > 0) {
          return Promise.reject("Username ini sudah digunakan");
        }
      }
      if (page == "edit") {
        const getMapping = await userMappingHelper.UserMapping.findOne({
          where: {
            id_mapping: id_pegawai,
          },
        });
        const getUsers = await userHelper.Users.findOne({
          where: {
            id: getMapping.id_user,
          },
        });
        let check = await userHelper.checkIdentitas(
          "username",
          page,
          value,
          getUsers.id
        );

        if (check > 0) {
          return Promise.reject("Username ini sudah digunakan");
        }
      }
    }),
    body("password_hash").custom(async (value, meta) => {
      const { page, confirm_password_hash } = meta.req.body;
      if (page == "add") {
        if (value == null) {
          return Promise.reject("Password wajib diisi");
        }

        if (value != null && confirm_password_hash != null) {
          if (value != confirm_password_hash) {
            return Promise.reject(
              "Password tidak sama dengan confirm password"
            );
          }
        }
      }
      if (page == "edit") {
        if (value != null && confirm_password_hash != null) {
          if (value != confirm_password_hash) {
            return Promise.reject(
              "Password tidak sama dengan confirm password"
            );
          }
        }
      }
    }),
    body("email")
      .notEmpty()
      .withMessage("Email wajib diisi")
      .trim()
      .isEmail()
      .withMessage("Email tidak valid"),

    body("id_absensi")
      .notEmpty()
      .withMessage("Id absensi tidak boleh kosong")
      .trim(),
    body("jenis_identitas")
      .notEmpty()
      .withMessage("Jenis identitas tidak boleh kosong")
      .trim(),
    body("no_identitas")
      .notEmpty()
      .withMessage("No. identitas tidak boleh kosong")
      .trim()
      .isLength({ max: 20 })
      .withMessage("Jenis identitas maksimal 20 karakter"),
    body("no_identitas").custom(async (value, meta) => {
      const { page, id_pegawai } = meta.req.body;
      if (page == "add") {
        let check = await pegawaiHelper.checkIdentitas(
          "no_identitas",
          page,
          value
        );
        if (check > 0) {
          return Promise.reject("No identitas ini sudah digunakan");
        }
      }
      if (page == "edit") {
        let check = await pegawaiHelper.checkIdentitas(
          "no_identitas",
          page,
          value,
          id_pegawai
        );
        if (check > 0) {
          return Promise.reject("No identitas ini sudah digunakan");
        }
      }
    }),
    body("nama_lengkap")
      .notEmpty()
      .withMessage("Nama lengkap tidak boleh kosong")
      .trim(),
    body("jenis_identitas")
      .notEmpty()
      .withMessage("Jenis identitas tidak boleh kosong")
      .isLength({ max: 10 })
      .withMessage("Jenis identitas maksimal 10 karakter"),
    body("jenis_kelamin")
      .notEmpty()
      .withMessage("Jenis kelamin tidak boleh kosong"),
    body("tempat_lahir")
      .notEmpty()
      .withMessage("Tempat lahir tidak boleh kosong"),
    body("tanggal_lahir")
      .notEmpty()
      .withMessage("Tanggal lahir tidak boleh kosong"),
    body("status_perkawinan")
      .notEmpty()
      .withMessage("Status perkawinan tidak boleh kosong"),
    body("agama").notEmpty().withMessage("Agama tidak boleh kosong"),
    body("pendidikan").notEmpty().withMessage("Pendidikan tidak boleh kosong"),
    body("alamat_domisili")
      .notEmpty()
      .withMessage("Alamat domisili tidak boleh kosong"),
    body("alamat_ktp").notEmpty().withMessage("Alamat KTP tidak boleh kosong"),
    body("no_kontak1")
      .notEmpty()
      .withMessage("No. kontak tidak boleh kosong")
      .isInt()
      .withMessage("No. kontak wajib angka")
      .isLength({ max: 15 })
      .withMessage("No. kontak maksimal 15 karakter"),
    body("email")
      .notEmpty()
      .withMessage("Email tidak boleh kosong")
      .isEmail()
      .withMessage("Email tidak valid"),
    body("no_pegawai")
      .notEmpty()
      .withMessage("No. pegawai tidak boleh kosong")
      .isLength({ max: 25 })
      .withMessage("Jenis identitas maksimal 25 karakter"),
    body("no_pegawai").custom(async (value, meta) => {
      const { page, id_pegawai } = meta.req.body;
      if (page == "add") {
        let check = await pegawaiHelper.checkIdentitas(
          "no_pegawai",
          page,
          value
        );
        if (check > 0) {
          return Promise.reject("No. pegawai ini sudah digunakan");
        }
      }
      if (page == "edit") {
        let check = await pegawaiHelper.checkIdentitas(
          "no_pegawai",
          page,
          value,
          id_pegawai
        );
        if (check > 0) {
          return Promise.reject("No. pegawai ini sudah digunakan");
        }
      }
    }),
    body("tanggal_masuk")
      .notEmpty()
      .withMessage("Tanggal masuk tidak boleh kosong"),
    body("gambar").custom(async (value, meta) => {
      const fileGambar = meta.req.body.gambar;
      const { gambar } = fileGambar;
      if (gambar != undefined) {
        if (gambar.size > 0) {
          const imageType = gambar.type;
          const size = gambar.size / 1000000;
          const mimeType = [
            "image/jpg",
            "image/jpeg",
            "image/gif",
            "image/png",
            "image/svg+xml",
          ];
          const byte = 14000000 / 1000000;
          const checkType = mimeType.includes(imageType);
          if (!checkType) {
            return Promise.reject(
              "Format file tidak didukung, format yang didukung yaitu: " +
                mimeType.join(",")
            );
          }
          if (size > byte) {
            return Promise.reject("Ukuran gambar lebih dari 14 mb ");
          }
        }
      }
    }),

    // body("nama_bank_rekening").notEmpty().withMessage("Nama bank wajib diisi"),
    // body("nomor_rekening")
    //   .notEmpty()
    //   .withMessage("No. rekening wajib diisi")
    //   .isInt()
    //   .withMessage("No. rekening wajib berupa angka"),
    // body("pemilik_rekening")
    //   .notEmpty()
    //   .withMessage("Pemilik rekening wajib diisi"),

    // body("str_izin").notEmpty().withMessage("STR Izin wajib diisi"),
    // body("jenis_izin").notEmpty().withMessage("Jenis Izin wajib diisi"),
    // body("nomor_izin").notEmpty().withMessage("Nomor Izin wajib diisi"),
    // body("tanggal_kadaluarsa_izin")
    //   .notEmpty()
    //   .withMessage("Tanggal kadaluarsa wajib diisi"),

    body("id_cabang").notEmpty().withMessage("Cabang wajib diisi"),
    body("id_unit").notEmpty().withMessage("Unit wajib diisi"),
    body("id_jabatan").notEmpty().withMessage("Jabatan wajib diisi"),
    body("jenis_kontrak").notEmpty().withMessage("Jenis kontrak wajib diisi"),
    body("tanggal_mulai").notEmpty().withMessage("Tanggal mulai wajib diisi"),
    // body("tanggal_selesai")
    //   .notEmpty()
    //   .withMessage("Tanggal selesai wajib diisi"),
    body("id_komponen_gaji").custom(async (value, meta) => {
      const { nominal } = meta.req.body;
      let id_komponen_gaji = value;
      // if (id_komponen_gaji == null) {
      //   return Promise.reject("Komponen gaji wajib diisi");
      // }

      let nominal_gaji = nominal;
      id_komponen_gaji = id_komponen_gaji.split(",");
      let error_id = false;
      if (id_komponen_gaji == "" || id_komponen_gaji == null) {
        error_id = true;
      }
      // if (error_id) {
      //   return Promise.reject("Komponen gaji wajib diisi");
      // }

      nominal_gaji = nominal_gaji.split(",");
      let error = false;
      nominal_gaji.map((v, i) => {
        if ((v == "" || v == 0) && id_komponen_gaji[0] != "") {
          error = true;
        }
      });
      if (error) {
        return Promise.reject("Nominal gaji yang dipilih wajib diisi");
      }
    }),
    pegawaiController.store
  );
router.route("/pegawai/:id_pegawai/edit").get(pegawaiController.edit);
router
  .route("/pegawai/:id_unit/unitPegawai")
  .get(pegawaiController.unitPegawai);
router
  .route("/pegawai/:id_cabang/cabangPegawai")
  .get(pegawaiController.cabangPegawai);
router.route("/pegawai/:id_pegawai/detail").get(pegawaiController.detail);
router.route("/pegawai/:id_pegawai/delete").get(pegawaiController.deleteData);
router.route("/pegawai/import").post(
  formDataImport,
  body("importData").custom(async (value, meta) => {
    const fileImport = meta.req.body.importData;
    const { importData } = fileImport;

    if (importData.size > 0) {
      const imageType = importData.type;
      const mimeType = [
        "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
      ];
      const checkType = mimeType.includes(imageType);
      if (!checkType) {
        return Promise.reject(
          "Format file tidak didukung, format yang didukung yaitu: " +
            mimeType.join(",")
        );
      }
    } else {
      return Promise.reject("File wajib di upload");
    }
  }),
  pegawaiController.importData
);

// jabatan
router
  .route("/jabatan")
  .get(jabatanController.index)
  .post(
    body("id_unit").notEmpty().withMessage("Unit wajib diisi").trim(),
    body("nama_jabatan").notEmpty().withMessage("Jabatan wajib diisi").trim(),
    jabatanController.store
  );
router.route("/jabatan/:id_jabatan/edit").get(jabatanController.edit);
router.route("/jabatan/:id_jabatan/detail").get(jabatanController.detail);
router.route("/jabatan/:id_jabatan/delete").get(jabatanController.deleteData);
router
  .route("/jabatan/getFindInJabatan")
  .get(jabatanController.getFindInJabatan);
router
  .route("/jabatan/getFindAllJabatan")
  .get(jabatanController.getFindAllJabatan);
router.route("/jabatan/import").post(
  formDataImport,
  body("importData").custom(async (value, meta) => {
    const fileImport = meta.req.body.importData;
    const { importData } = fileImport;

    if (importData.size > 0) {
      const imageType = importData.type;
      const mimeType = [
        "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
      ];
      const checkType = mimeType.includes(imageType);
      if (!checkType) {
        return Promise.reject(
          "Format file tidak didukung, format yang didukung yaitu: " +
            mimeType.join(",")
        );
      }
    } else {
      return Promise.reject("File wajib di upload");
    }
  }),
  jabatanController.importData
);

// jenisAbsensi
router
  .route("/jenisAbsensi")
  .get(jenisAbsensiController.index)
  .post(
    body("jenis_absensi")
      .notEmpty()
      .withMessage("Jenis absensi wajib diisi")
      .trim(),
    jenisAbsensiController.store
  );
router
  .route("/jenisAbsensi/:id_jenis_absensi/edit")
  .get(jenisAbsensiController.edit);
router
  .route("/jenisAbsensi/:id_jenis_absensi/delete")
  .get(jenisAbsensiController.deleteData);
router.route("/jenisAbsensi/import").post(
  formDataImport,
  body("importData").custom(async (value, meta) => {
    const fileImport = meta.req.body.importData;
    const { importData } = fileImport;

    if (importData.size > 0) {
      const imageType = importData.type;
      const mimeType = [
        "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
      ];
      const checkType = mimeType.includes(imageType);
      if (!checkType) {
        return Promise.reject(
          "Format file tidak didukung, format yang didukung yaitu: " +
            mimeType.join(",")
        );
      }
    } else {
      return Promise.reject("File wajib di upload");
    }
  }),
  jenisAbsensiController.importData
);

// jenisCuti
router
  .route("/jenisCuti")
  .get(jenisCutiController.index)
  .post(
    body("jenis_cuti").notEmpty().withMessage("Jenis cuti wajib diisi").trim(),
    body("saldo").notEmpty().withMessage("Saldo wajib diisi").trim(),
    body("saldo").isInt().withMessage("Saldo harus merupakan angka"),
    jenisCutiController.store
  );
router.route("/jenisCuti/:id_jenis_cuti/edit").get(jenisCutiController.edit);
router
  .route("/jenisCuti/:id_jenis_cuti/delete")
  .get(jenisCutiController.deleteData);
router.route("/jenisCuti/import").post(
  formDataImport,
  body("importData").custom(async (value, meta) => {
    const fileImport = meta.req.body.importData;
    const { importData } = fileImport;

    if (importData.size > 0) {
      const imageType = importData.type;
      const mimeType = [
        "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
      ];
      const checkType = mimeType.includes(imageType);
      if (!checkType) {
        return Promise.reject(
          "Format file tidak didukung, format yang didukung yaitu: " +
            mimeType.join(",")
        );
      }
    } else {
      return Promise.reject("File wajib di upload");
    }
  }),
  jenisCutiController.importData
);

// jenisLembur
router
  .route("/jenisLembur")
  .get(jenisLemburController.index)
  .post(
    body("jenis_lembur")
      .notEmpty()
      .withMessage("Jenis lembur wajib diisi")
      .trim(),
    body("jenis_jadwal")
      .notEmpty()
      .withMessage("Jenis jadwal wajib diisi")
      .trim(),
    body("jenis_perhitungan")
      .notEmpty()
      .withMessage("Jenis perhitungan wajib diisi")
      .trim(),
    body("formula").custom(async (value, meta) => {
      let dataFormula = JSON.parse(value);
      const { jenis_perhitungan } = meta.req.body;
      if (dataFormula.length == 0) {
        return Promise.reject("Formula wajib diisi");
      }

      if (jenis_perhitungan == "jam") {
        let error = false;
        dataFormula.map((v1, i1) => {
          Object.keys(v1).map((ov1, oi1) => {
            let jam1 = ov1.split("_");
            jam1 = jam1[1];
            dataFormula.map((v2, i2) => {
              Object.keys(v2).map((ov2, oi2) => {
                let jam2 = ov2.split("_");
                jam2 = jam2[1];

                if (jam1 < jam2) {
                  let nominal1 = v1[ov1];
                  let nominal2 = v2[ov2];
                  if (parseFloat(nominal1) > parseFloat(nominal2)) {
                    error = true;
                  }
                }
              });
            });
          });
        });

        if (error) {
          return Promise.reject(
            "Perhatikan kembali waktu lembur dengan nominal lembur"
          );
        }
      }
    }),
    jenisLemburController.store
  );
router
  .route("/jenisLembur/:id_jenis_lembur/edit")
  .get(jenisLemburController.edit);
router
  .route("/jenisLembur/:id_jenis_lembur/delete")
  .get(jenisLemburController.deleteData);
router.route("/jenisLembur/import").post(
  formDataImport,
  body("importData").custom(async (value, meta) => {
    const fileImport = meta.req.body.importData;
    const { importData } = fileImport;

    if (importData.size > 0) {
      const imageType = importData.type;
      const mimeType = [
        "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
      ];
      const checkType = mimeType.includes(imageType);
      if (!checkType) {
        return Promise.reject(
          "Format file tidak didukung, format yang didukung yaitu: " +
            mimeType.join(",")
        );
      }
    } else {
      return Promise.reject("File wajib di upload");
    }
  }),
  jenisLemburController.importData
);

// komponenGaji
router
  .route("/komponenGaji")
  .get(komponenGajiController.index)
  .post(
    body("komponen_gaji")
      .notEmpty()
      .withMessage("Komponen gaji wajib diisi")
      .trim(),
    body("jenis").notEmpty().withMessage("Jenis wajib diisi").trim(),
    body("jenis")
      .isLength({
        max: 5,
      })
      .withMessage("Jenis harus 5 karakter")
      .trim(),
    body("kelompok_komponen")
      .notEmpty()
      .withMessage("Kelompok komponen wajib diisi")
      .trim()
      .isLength({
        max: 50,
      })
      .withMessage("Kelompok komponen harus 50 karakter"),
    body("komponen").notEmpty().withMessage("Formula wajib diisi").trim(),
    komponenGajiController.store
  );
router
  .route("/komponenGaji/:id_komponen_gaji/edit")
  .get(komponenGajiController.edit);
router
  .route("/komponenGaji/:id_komponen_gaji/delete")
  .get(komponenGajiController.deleteData);
router.route("/komponenGaji/import").post(
  formDataImport,
  body("importData").custom(async (value, meta) => {
    const fileImport = meta.req.body.importData;
    const { importData } = fileImport;

    if (importData.size > 0) {
      const imageType = importData.type;
      const mimeType = [
        "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
      ];
      const checkType = mimeType.includes(imageType);
      if (!checkType) {
        return Promise.reject(
          "Format file tidak didukung, format yang didukung yaitu: " +
            mimeType.join(",")
        );
      }
    } else {
      return Promise.reject("File wajib di upload");
    }
  }),
  komponenGajiController.importData
);

// langganan
router
  .route("/langganan")
  .get(langgananController.index)
  .post(
    body("waktu_mulai")
      .notEmpty()
      .withMessage("Waktu mulai wajib diisi")
      .trim(),
    body("waktu_selesai")
      .notEmpty()
      .withMessage("Waktu selesai wajib diisi")
      .trim(),
    body("id_client").notEmpty().withMessage("Client wajib diisi").trim(),
    langgananController.store
  );
router.route("/langganan/:id_langganan/edit").get(langgananController.edit);
router
  .route("/langganan/:id_langganan/delete")
  .get(langgananController.deleteData);

// tugas
router
  .route("/tugas")
  .get(tugasController.index)
  .post(
    body("id_cabang").notEmpty().withMessage("Cabang wajib diisi").trim(),
    body("nama_tugas").notEmpty().withMessage("Nama tugas wajib diisi").trim(),
    tugasController.store
  );
router.route("/tugas/:id_tugas/edit").get(tugasController.edit);
router.route("/tugas/:id_cabang/getCabang").get(tugasController.getCabang);
router.route("/tugas/:id_tugas/delete").get(tugasController.deleteData);

// lokasiTugas
router
  .route("/lokasiTugas")
  .get(lokasiTugasController.index)
  .post(
    body("id_cabang").notEmpty().withMessage("Cabang wajib diisi").trim(),
    body("nama_lokasi")
      .notEmpty()
      .withMessage("Nama lokasi wajib diisi")
      .trim(),
    body("longitude").notEmpty().withMessage("Longitude wajib diisi").trim(),
    body("latitude").notEmpty().withMessage("Latitude wajib diisi").trim(),
    lokasiTugasController.store
  );
router
  .route("/lokasiTugas/:id_lokasi_tugas/edit")
  .get(lokasiTugasController.edit);
router
  .route("/lokasiTugas/:id_cabang/getCabang")
  .get(lokasiTugasController.getCabang);
router
  .route("/lokasiTugas/:id_lokasi_tugas/delete")
  .get(lokasiTugasController.deleteData);

module.exports = router;
