const { Op } = require("sequelize");
const { JenisAbsensi } = require("../model/index");

const getJenisAbsensi = async (
  limit = null,
  offset = null,
  search = null,
  client_id = null,
  filter = {}
) => {
  let whereObj = {};
  whereObj.queryLike = null;
  whereObj.filter = null;

  // filter
  if (filter.jenis_absensi != "" && filter.jenis_absensi != null) {
    whereObj.filter = {
      ...whereObj.filter,
      jenis_absensi: {
        [Op.iLike]: "%" + filter.jenis_absensi + "%",
      },
    };
  }

  if (filter.is_aktif != "" && filter.is_aktif != null) {
    whereObj.filter = {
      ...whereObj.filter,
      is_aktif: filter.is_aktif,
    };
  }

  if (whereObj.filter != null) {
    whereObj.filter = {
      [Op.and]: whereObj.filter,
    };
  }

  let jenis_absensi = "";
  if (search != null && search != "") {
    whereObj.queryLike = {
      [Op.or]: {
        jenis_absensi: {
          [Op.iLike]: "%" + search + "%",
        },
      },
    };
  }

  // execute
  let pushWhere = [];
  pushWhere.push({
    id_client: client_id,
  });
  if (search != null && search != "") {
    pushWhere.push(whereObj.queryLike);
  }

  if (whereObj.filter != null) {
    pushWhere.push(whereObj.filter);
  }

  jenis_absensi = await JenisAbsensi.findAll({
    where: pushWhere,
    order: [["id_jenis_absensi", "asc"]],
    limit: limit,
    offset: offset,
  });

  if (limit == null && offset == null) {
    jenis_absensi = await JenisAbsensi.findAll({
      where: pushWhere,
      order: [["id_jenis_absensi", "asc"]],
    });
  }
  return jenis_absensi;
};

module.exports = {
  JenisAbsensi,
  getJenisAbsensi,
};
