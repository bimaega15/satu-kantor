const { Op } = require("sequelize");
const { KomponenGaji } = require("../model/index");

const getKomponenGaji = async (
  limit = null,
  offset = null,
  search = null,
  client_id = null,
  filter = {}
) => {
  let whereObj = {};
  whereObj.queryLike = null;
  whereObj.filter = null;

  // filter
  if (filter.komponen_gaji != "" && filter.komponen_gaji != null) {
    whereObj.filter = {
      ...whereObj.filter,
      komponen_gaji: {
        [Op.iLike]: "%" + filter.komponen_gaji + "%",
      },
    };
  }
  if (filter.jenis != "" && filter.jenis != null) {
    whereObj.filter = {
      ...whereObj.filter,
      jenis: filter.jenis,
    };
  }
  if (filter.kelompok_komponen != "" && filter.kelompok_komponen != null) {
    whereObj.filter = {
      ...whereObj.filter,
      kelompok_komponen: filter.kelompok_komponen,
    };
  }
  if (filter.komponen != "" && filter.komponen != null) {
    whereObj.filter = {
      ...whereObj.filter,
      komponen: filter.komponen,
    };
  }
  if (filter.is_aktif != "" && filter.is_aktif != null) {
    whereObj.filter = {
      ...whereObj.filter,
      is_aktif: filter.is_aktif,
    };
  }

  if (whereObj.filter != null) {
    whereObj.filter = {
      [Op.and]: whereObj.filter,
    };
  }

  let komponen_gaji = "";
  if (search != null && search != "") {
    whereObj.queryLike = {
      [Op.or]: {
        komponen_gaji: {
          [Op.iLike]: "%" + search + "%",
        },
        jenis: {
          [Op.iLike]: "%" + search + "%",
        },
        kelompok_komponen: {
          [Op.iLike]: "%" + search + "%",
        },
        komponen: {
          [Op.iLike]: "%" + search + "%",
        },
      },
    };
  }

  // execute
  let pushWhere = [];
  pushWhere.push({
    id_client: client_id,
  });
  if (search != null && search != "") {
    pushWhere.push(whereObj.queryLike);
  }

  if (whereObj.filter != null) {
    pushWhere.push(whereObj.filter);
  }

  komponen_gaji = await KomponenGaji.findAll({
    where: pushWhere,
    order: [["id_komponen_gaji", "asc"]],
    limit: limit,
    offset: offset,
  });

  if (limit == null && offset == null) {
    komponen_gaji = await KomponenGaji.findAll({
      where: pushWhere,
      order: [["id_komponen_gaji", "asc"]],
    });
  }
  return komponen_gaji;
};

module.exports = { KomponenGaji, getKomponenGaji };
