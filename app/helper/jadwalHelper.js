const { Op } = require("sequelize");
const { Jadwal } = require("../model/index");

const getJadwal = async (
  limit = null,
  offset = null,
  search = null,
  client_id = null,
  filter = {}
) => {
  let whereObj = {};
  whereObj.queryLike = null;
  whereObj.filter = null;

  // filter
  if (filter.waktu_masuk != "" && filter.waktu_masuk != null) {
    whereObj.filter = {
      ...whereObj.filter,
      waktu_masuk: filter.waktu_masuk + ":00",
    };
  }
  if (filter.waktu_keluar != "" && filter.waktu_keluar != null) {
    whereObj.filter = {
      ...whereObj.filter,
      waktu_keluar: filter.waktu_keluar + ":00",
    };
  }
  if (filter.is_aktif != "" && filter.is_aktif != null) {
    whereObj.filter = {
      ...whereObj.filter,
      is_aktif: filter.is_aktif,
    };
  }
  if (filter.jenis != "" && filter.jenis != null) {
    whereObj.filter = {
      ...whereObj.filter,
      jenis: filter.jenis,
    };
  }

  if (whereObj.filter != null) {
    whereObj.filter = {
      [Op.and]: whereObj.filter,
    };
  }

  let jadwal = "";
  if (search != null && search != "") {
    limitWhere = true;
    whereObj.queryLike = {
      [Op.or]: {
        warna: {
          [Op.iLike]: "%" + search + "%",
        },
        jenis: {
          [Op.iLike]: "%" + search + "%",
        },
      },
    };
  }

  // execute
  let pushWhere = [];
  pushWhere.push({
    id_client: client_id,
  });
  if (search != null && search != "") {
    pushWhere.push(whereObj.queryLike);
  }

  if (whereObj.filter != null) {
    pushWhere.push(whereObj.filter);
  }

  jadwal = await Jadwal.findAll({
    where: pushWhere,
    order: [["id_jadwal", "asc"]],
    limit: limit,
    offset: offset,
  });

  if (limit == null && offset == null) {
    jadwal = await Jadwal.findAll({
      where: pushWhere,
      order: [["id_jadwal", "asc"]],
    });
  }

  return jadwal;
};

const getAllJadwal = async (client_id = null) => {
  let jadwal = "";
  if (client_id != null) {
    jadwal = await Jadwal.findAll({
      attributes: [
        "id_jadwal",
        "waktu_masuk",
        "waktu_keluar",
        "is_aktif",
        "warna",
        "jenis",
      ],
      where: {
        id_client: client_id,
      },
      order: [["id_jadwal", "asc"]],
    });
  }
  return jadwal;
};

const jadwalById = async (id_jadwal = null) => {
  let jadwal = "";
  if (id_jadwal != null) {
    jadwal = await Jadwal.findOne({
      where: {
        id_jadwal: id_jadwal,
      },
      order: [["id_jadwal", "asc"]],
    });
  }
  return jadwal;
};
module.exports = {
  getJadwal,
  Jadwal,
  getAllJadwal,
  jadwalById,
};
