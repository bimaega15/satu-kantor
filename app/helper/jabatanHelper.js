const { Op } = require("sequelize");
const { Jabatan, Unit, Cabang } = require("../model/index");

const getJabatan = async (
  limit = null,
  offset = null,
  search = null,
  client_id = null,
  filter = {}
) => {
  let whereObj = {};
  whereObj.queryLike = null;
  whereObj.filter = null;

  // filter
  if (filter.nama_cabang != "" && filter.nama_cabang != null) {
    whereObj.filter = {
      ...whereObj.filter,
      "$unit.cabang.nama_cabang$": {
        [Op.iLike]: "%" + filter.nama_cabang + "%",
      },
    };
  }
  if (filter.nama_unit != "" && filter.nama_unit != null) {
    whereObj.filter = {
      ...whereObj.filter,
      "$unit.nama_unit$": {
        [Op.iLike]: "%" + filter.nama_unit + "%",
      },
    };
  }
  if (filter.nama_jabatan != "" && filter.nama_jabatan != null) {
    whereObj.filter = {
      ...whereObj.filter,
      nama_jabatan: {
        [Op.iLike]: "%" + filter.nama_jabatan + "%",
      },
    };
  }
  if (filter.is_aktif != "" && filter.is_aktif != null) {
    whereObj.filter = {
      ...whereObj.filter,
      is_aktif: filter.is_aktif,
    };
  }

  if (whereObj.filter != null) {
    whereObj.filter = {
      [Op.and]: whereObj.filter,
    };
  }

  let jabatan = "";
  // condition
  if (search != null && search != "") {
    whereObj.queryLike = {
      [Op.or]: {
        nama_jabatan: {
          [Op.iLike]: "%" + search + "%",
        },
        "$unit.nama_unit$": {
          [Op.iLike]: "%" + search + "%",
        },
        "$unit.cabang.nama_cabang$": {
          [Op.iLike]: "%" + search + "%",
        },
      },
    };
  }

  // execute
  let pushWhere = [];
  pushWhere.push({
    id_client: client_id,
  });

  if (search != null && search != "") {
    pushWhere.push(whereObj.queryLike);
  }

  if (whereObj.filter != null) {
    pushWhere.push(whereObj.filter);
  }

  let include = [
    {
      model: Unit,
      include: [
        {
          model: Cabang,
        },
      ],
    },
  ];
  jabatan = await Jabatan.findAll({
    order: [["id_jabatan", "asc"]],
    where: pushWhere,
    limit: limit,
    offset: offset,
    include: include,
  });

  if (limit == null && offset == null) {
    jabatan = await Jabatan.findAll({
      order: [["id_jabatan", "asc"]],
      where: pushWhere,
      include: include,
    });
  }

  return jabatan;
};

const getJabatanId = async (jabatan_id) => {
  let jabatan = "";
  if (jabatan_id[0] != "" && jabatan_id[0] != null) {
    jabatan = await Jabatan.findAll({
      attributes: ["nama_jabatan"],
      where: {
        id_jabatan: {
          [Op.in]: jabatan_id,
        },
      },
      order: [["id_jabatan", "asc"]],
    });
  }

  return jabatan;
};

const getJabatanAll = async (id_client, id_cabang) => {
  let include = [
    {
      model: Unit,
      include: [
        {
          model: Cabang,
        },
      ],
    },
  ];

  let jabatan = await Jabatan.findAll({
    where: {
      id_client: id_client,
      "$unit.cabang.id_cabang$": id_cabang,
    },
    include: include,
    order: [["id_jabatan", "asc"]],
  });
  return jabatan;
};

const getJabatanById = async (jabatan_id) => {
  let include = [
    {
      model: Unit,
      include: [
        {
          model: Cabang,
        },
      ],
    },
  ];

  let jabatan = "";
  if (jabatan_id != "") {
    jabatan = await Jabatan.findOne({
      where: {
        id_jabatan: jabatan_id,
      },
      order: [["id_jabatan", "asc"]],
      include: include,
    });
  }

  return jabatan;
};

module.exports = {
  getJabatanId,
  getJabatanAll,
  getJabatan,
  Jabatan,
  getJabatanById,
};
