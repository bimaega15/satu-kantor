const { Op } = require("sequelize");
const {
  PegawaiKontrak,
  Pegawai,
  Unit,
  Cabang,
  Jabatan,
  PegawaiKomponenGaji,
  PegawaiJabatan,
  PegawaiUnit,
  KomponenGaji,
  Client,
} = require("../model/index");
const moment = require("moment");

const getPegawaiKontrak = async (
  limit = null,
  offset = null,
  search = null,
  filter = {},
  id_client = null
) => {
  let whereObj = {};
  whereObj.queryLike = null;
  whereObj.filter = null;

  // filter
  if (filter.jenis_kontrak != "" && filter.jenis_kontrak != null) {
    whereObj.filter = {
      ...whereObj.filter,
      jenis_kontrak: filter.jenis_kontrak,
    };
  }
  if (filter.no_pegawai != "" && filter.no_pegawai != null) {
    whereObj.filter = {
      ...whereObj.filter,
      "$pegawai.no_pegawai$": {
        [Op.iLike]: "%" + filter.no_pegawai + "%",
      },
    };
  }
  if (filter.nama_lengkap != "" && filter.nama_lengkap != null) {
    whereObj.filter = {
      ...whereObj.filter,
      "$pegawai.nama_lengkap$": {
        [Op.iLike]: "%" + filter.nama_lengkap + "%",
      },
    };
  }
  if (filter.id_unit != "" && filter.id_unit != null) {
    whereObj.filter = {
      ...whereObj.filter,
      id_unit: filter.id_unit,
    };
  }
  if (filter.id_jabatan != "" && filter.id_jabatan != null) {
    whereObj.filter = {
      ...whereObj.filter,
      id_jabatan: filter.id_jabatan,
    };
  }
  if (filter.tanggal_mulai != "" && filter.tanggal_mulai != null) {
    whereObj.filter = {
      ...whereObj.filter,
      tanggal_mulai: moment(filter.tanggal_mulai, "DD-MM-YYYY").format(
        "YYYY-MM-DD"
      ),
    };
  }
  if (filter.tanggal_selesai != "" && filter.tanggal_selesai != null) {
    whereObj.filter = {
      ...whereObj.filter,
      tanggal_selesai: moment(filter.tanggal_selesai, "DD-MM-YYYY").format(
        "YYYY-MM-DD"
      ),
    };
  }
  if (filter.is_aktif != "" && filter.is_aktif != null) {
    whereObj.filter = {
      ...whereObj.filter,
      is_aktif: filter.is_aktif,
    };
  }

  if (whereObj.filter != null) {
    whereObj.filter = {
      [Op.and]: whereObj.filter,
    };
  }

  let pegawaiKontrak = "";
  // condition
  if (search != null && search != "") {
    whereObj.queryLike = {
      [Op.or]: {
        jenis_kontrak: {
          [Op.iLike]: "%" + search + "%",
        },
        "$pegawai.no_pegawai$": {
          [Op.iLike]: "%" + search + "%",
        },
        "$pegawai.nama_lengkap$": {
          [Op.iLike]: "%" + search + "%",
        },
        "$unit.nama_unit$": {
          [Op.iLike]: "%" + search + "%",
        },
        "$jabatan.nama_jabatan$": {
          [Op.iLike]: "%" + search + "%",
        },
      },
    };
  }

  // execute
  let pushWhere = [];
  pushWhere.push({
    "$pegawai.client.id_client$": id_client,
  });
  if (search != null && search != "") {
    pushWhere.push(whereObj.queryLike);
  }

  if (whereObj.filter != null) {
    pushWhere.push(whereObj.filter);
  }

  let getInclude = [
    {
      model: Pegawai,
      include: [
        {
          model: Client,
          required: true,
        },
      ],
    },
    {
      model: Unit,
    },
    {
      model: Jabatan,
    },
  ];

  pegawaiKontrak = await PegawaiKontrak.findAll({
    where: pushWhere,
    order: [["id_pegawai_kontrak", "asc"]],
    limit: limit,
    offset: offset,
    include: getInclude,
  });

  if (limit == null && offset == null) {
    pegawaiKontrak = await PegawaiKontrak.findAll({
      where: pushWhere,
      order: [["id_pegawai_kontrak", "asc"]],
      include: getInclude,
    });
  }

  return pegawaiKontrak;
};

const getPegawaiKontrakAll = async () => {
  let pegawaiKontrak = await PegawaiKontrak.findAll();
  return pegawaiKontrak;
};

const getPegawaiKontrakId = async (
  id_pegawai_kontrak = null,
  id_pegawai = null,
  is_aktif = null
) => {
  let whereObject = {};
  whereObject.filter = null;

  // execute
  if (id_pegawai != null) {
    whereObject.filter = {
      ...whereObject.filter,
      id_pegawai: id_pegawai,
    };
  }

  if (is_aktif != null) {
    whereObject.filter = {
      ...whereObject.filter,
      is_aktif: is_aktif,
    };
  }

  let pushWhere = [];
  pushWhere.push({
    id_pegawai_kontrak: id_pegawai_kontrak,
    is_aktif: 1,
  });
  if (whereObject.filter != null) {
    pushWhere.push(whereObject.filter);
  }

  let getInclude = [
    {
      model: Pegawai,
    },
    {
      model: Cabang,
    },
    {
      model: Jabatan,
      include: [
        {
          model: PegawaiJabatan,
          where: {
            is_aktif: 1,
          },
        },
      ],
    },
    {
      model: Unit,
      include: [
        {
          model: PegawaiUnit,
          where: {
            is_aktif: 1,
          },
        },
      ],
    },
    {
      model: PegawaiKomponenGaji,
      include: [
        {
          model: KomponenGaji,
        },
      ],
    },
  ];

  let pegawaiKontrak = null;
  if (pushWhere.length > 0) {
    pegawaiKontrak = await PegawaiKontrak.findOne({
      where: pushWhere,
      include: getInclude,
    });
  }
  return pegawaiKontrak;
};

const createPegawaiKontrak = async (data = {}, transaction = {}) => {
  let insert = await PegawaiKontrak.create(data, transaction);
  return insert;
};

const updatePegawaiKontrak = async (
  data = {},
  id_pegawai_kontrak,
  transaction = {}
) => {
  let update = await PegawaiKontrak.update(data, {
    where: {
      id_pegawai_kontrak: id_pegawai_kontrak,
    },
    ...transaction,
  });
  return update;
};

const deletePegawaiKontrak = async (
  id_pegawai_kontrak = null,
  transaction = {}
) => {
  let pegawaiKontrak = await PegawaiKontrak.destroy({
    where: {
      id_pegawai_kontrak: id_pegawai_kontrak,
    },
    ...transaction,
  });
  return pegawaiKontrak;
};

const checkPegawai = async (id_pegawai = null, id_pegawai_kontrak = null) => {
  let pegawaiKontrak = null;
  if (id_pegawai != null && id_pegawai_kontrak == null) {
    pegawaiKontrak = await PegawaiKontrak.findAll({
      attributes: ["id_pegawai", "id_pegawai_kontrak"],
      where: {
        id_pegawai: id_pegawai,
        is_aktif: 1,
      },
    });
  }
  if (id_pegawai != null && id_pegawai_kontrak != null) {
    pegawaiKontrak = await PegawaiKontrak.findAll({
      attributes: ["id_pegawai", "id_pegawai_kontrak"],
      where: {
        id_pegawai: id_pegawai,
        id_pegawai_kontrak: {
          [Op.not]: id_pegawai_kontrak,
        },
        is_aktif: 1,
      },
    });
  }

  return pegawaiKontrak;
};

module.exports = {
  getPegawaiKontrakAll,
  getPegawaiKontrak,
  getPegawaiKontrakId,
  createPegawaiKontrak,
  updatePegawaiKontrak,
  deletePegawaiKontrak,
  checkPegawai,
  PegawaiKontrak,
};
