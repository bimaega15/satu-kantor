const { Op } = require("sequelize");
const {
  PengajuanLembur,
  PengajuanLemburList,
  Cabang,
  JenisLembur,
  Pegawai,
  PegawaiUnit,
  PegawaiJabatan,
  Unit,
  Jabatan,
} = require("../model/index");
const moment = require("moment");

const getPengajuanLembur = async (
  limit = null,
  offset = null,
  search = null,
  filter = {},
  id_client = null
) => {
  let whereObj = {};
  whereObj.queryLike = null;
  whereObj.filter = null;

  // filter
  if (filter.id_cabang != "" && filter.id_cabang != null) {
    whereObj.filter = {
      ...whereObj.filter,
      id_cabang: filter.id_cabang,
    };
  }
  if (filter.id_jenis_lembur != "" && filter.id_jenis_lembur != null) {
    whereObj.filter = {
      ...whereObj.filter,
      id_jenis_lembur: filter.id_jenis_lembur,
    };
  }
  if (filter.tanggal_lembur != "" && filter.tanggal_lembur != null) {
    whereObj.filter = {
      ...whereObj.filter,
      tanggal_lembur: filter.tanggal_lembur,
    };
  }
  if (filter.waktu_mulai != "" && filter.waktu_mulai != null) {
    whereObj.filter = {
      ...whereObj.filter,
      waktu_mulai: filter.waktu_mulai,
    };
  }
  if (filter.waktu_selesai != "" && filter.waktu_selesai != null) {
    whereObj.filter = {
      ...whereObj.filter,
      waktu_selesai: filter.waktu_selesai,
    };
  }

  if (whereObj.filter != null) {
    whereObj.filter = {
      [Op.and]: whereObj.filter,
    };
  }

  let pengajuanLembur = "";

  let pushIncludePegawai = {};
  if (filter.id_pegawai != "" && filter.id_pegawai != null) {
    pushIncludePegawai = {
      model: PengajuanLemburList,
      required: true,
      where: {
        id_pegawai: filter.id_pegawai,
      },
      include: [
        {
          model: Pegawai,
        },
      ],
    };
  } else {
    pushIncludePegawai = {
      model: PengajuanLemburList,
      required: true,
      include: [
        {
          model: Pegawai,
        },
      ],
    };
  }

  // execute
  let pushWhere = [];
  if (id_client != null) {
    pushWhere.push({
      id_client: id_client,
    });
  }

  if (search != null) {
    whereObj.queryLike = {
      [Op.or]: {},
    };
  }

  if (search != null) {
    pushWhere.push(whereObj.queryLike);
  }

  if (whereObj.filter != null) {
    pushWhere.push(whereObj.filter);
  }

  pengajuanLembur = await PengajuanLembur.findAll({
    where: pushWhere,
    include: [
      {
        model: Cabang,
      },
      {
        model: JenisLembur,
      },
      pushIncludePegawai,
    ],
    order: [["id_pengajuan_lembur", "asc"]],
    limit: limit,
    offset: offset,
  });

  if (limit == null && offset == null) {
    pengajuanLembur = await PengajuanLembur.findAll({
      where: pushWhere,
      include: [
        {
          model: Cabang,
        },
        {
          model: JenisLembur,
        },
        pushIncludePegawai,
      ],
      order: [["id_pengajuan_lembur", "asc"]],
    });
  }

  return pengajuanLembur;
};

const getPengajuanLemburAll = async () => {
  let pengajuanLembur = await PengajuanLembur.findAll();
  return pengajuanLembur;
};

const getPengajuanLemburId = async (id_pengajuan_lembur = null) => {
  let pengajuanLembur = null;
  let getInclude = [
    {
      model: Cabang,
    },
    {
      model: JenisLembur,
    },
    {
      model: PengajuanLemburList,
      required: true,
      include: [
        {
          model: Pegawai,
          include: [
            {
              model: PegawaiUnit,
              where: {
                is_aktif: 1,
              },
              include: [
                {
                  model: Unit,
                },
              ],
            },
            {
              model: PegawaiJabatan,
              where: {
                is_aktif: 1,
              },
              include: [
                {
                  model: Jabatan,
                },
              ],
            },
          ],
        },
      ],
    },
  ];
  if (id_pengajuan_lembur != null) {
    pengajuanLembur = await PengajuanLembur.findOne({
      where: {
        id_pengajuan_lembur: id_pengajuan_lembur,
      },
      include: getInclude,
      order: [["id_pengajuan_lembur", "asc"]],
    });
  }
  return pengajuanLembur;
};

const createPengajuanLembur = async (data = {}, transaction = {}) => {
  let insert = await PengajuanLembur.create(data, transaction);
  return insert;
};

const updatePengajuanLembur = async (
  data = {},
  id_pengajuan_lembur,
  transaction = {}
) => {
  let update = await PengajuanLembur.update(data, {
    where: {
      id_pengajuan_lembur: id_pengajuan_lembur,
    },
    ...transaction,
  });
  return update;
};

const deletePengajuanLembur = async (
  id_pengajuan_lembur = null,
  transaction = {}
) => {
  let pengajuanLembur = await PengajuanLembur.destroy({
    where: {
      id_pengajuan_lembur: id_pengajuan_lembur,
    },
    ...transaction,
  });
  return pengajuanLembur;
};

module.exports = {
  getPengajuanLemburAll,
  getPengajuanLembur,
  getPengajuanLemburId,
  createPengajuanLembur,
  updatePengajuanLembur,
  deletePengajuanLembur,
  PengajuanLembur,
};
