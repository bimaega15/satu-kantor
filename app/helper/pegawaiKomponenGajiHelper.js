const { Op } = require("sequelize");
const { PegawaiKomponenGaji, Unit } = require("../model/index");

const getAll = async () => {
  let pegawaiKomponenGaji = await PegawaiKomponenGaji.findAll({
    order: [["id_komponen_gaji", "asc"]],
  });
  return pegawaiKomponenGaji;
};

const getById = async (id_pegawai_komponen_gaji) => {
  let pegawaiKomponenGaji = await PegawaiKomponenGaji.findOne({
    where: {
      id_pegawai_komponen_gaji: id_pegawai_komponen_gaji,
    },
    order: [["id_komponen_gaji", "asc"]],
  });
  return pegawaiKomponenGaji;
};

const insertData = async (data) => {
  let pegawaiKomponenGaji = await PegawaiKomponenGaji.create(data);
  return pegawaiKomponenGaji;
};

const insertBatch = async (data, transaction = {}) => {
  let pegawaiKomponenGaji = await PegawaiKomponenGaji.bulkCreate(
    data,
    transaction
  );
  return pegawaiKomponenGaji;
};

const updateData = async (data, id_pegawai_komponen_gaji) => {
  let pegawaiKomponenGaji = await PegawaiKomponenGaji.update(data, {
    where: {
      id_pegawai_komponen_gaji: id_pegawai_komponen_gaji,
    },
  });
  return pegawaiKomponenGaji;
};

const deleteData = async (id_pegawai_komponen_gaji) => {
  let pegawaiKomponenGaji = await PegawaiKomponenGaji.destroy({
    where: {
      id_pegawai_komponen_gaji: id_pegawai_komponen_gaji,
    },
  });
  return pegawaiKomponenGaji;
};

module.exports = {
  PegawaiKomponenGaji,
  getAll,
  getById,
  insertData,
  updateData,
  deleteData,
  insertBatch,
};
