const { Op, Sequelize } = require("sequelize");
const { LokasiTugas, Client, Cabang } = require("../model/index");
const moment = require("moment");
const sequelize = require("../config/db");

const getLokasiTugas = async (
  limit = null,
  offset = null,
  search = null,
  filter = {}
) => {
  let whereObj = {};
  whereObj.queryLike = null;
  whereObj.filter = null;

  // filter
  if (filter.id_client != "" && filter.id_client != null) {
    whereObj.filter = {
      ...whereObj.filter,
      id_client: filter.id_client,
    };
  }
  if (filter.id_cabang != "" && filter.id_cabang != null) {
    whereObj.filter = {
      ...whereObj.filter,
      id_cabang: filter.id_cabang,
    };
  }
  if (filter.nama_lokasi != "" && filter.nama_lokasi != null) {
    whereObj.filter = {
      ...whereObj.filter,
      nama_lokasi: {
        [Op.iLike]: "%" + filter.nama_lokasi + "%",
      },
    };
  }
  if (filter.longitude != "" && filter.longitude != null) {
    whereObj.filter = {
      ...whereObj.filter,
      longitude: {
        [Op.iLike]: "%" + filter.longitude + "%",
      },
    };
  }
  if (filter.latitude != "" && filter.latitude != null) {
    whereObj.filter = {
      ...whereObj.filter,
      latitude: {
        [Op.iLike]: "%" + filter.latitude + "%",
      },
    };
  }
  if (filter.is_aktif != "" && filter.is_aktif != null) {
    whereObj.filter = {
      ...whereObj.filter,
      is_aktif: filter.is_aktif,
    };
  }

  let lokasiTugas = "";

  // execute
  let pushWhere = [];
  if (search != null) {
    whereObj.queryLike = {
      [Op.or]: {
        "$cabang.nama_cabang$": {
          [Op.iLike]: "%" + search + "%",
        },
        nama_lokasi: {
          [Op.iLike]: "%" + search + "%",
        },
        longitude: {
          [Op.iLike]: "%" + search + "%",
        },
        latitude: {
          [Op.iLike]: "%" + search + "%",
        },
      },
    };
  }

  if (search != null) {
    pushWhere.push(whereObj.queryLike);
  }

  if (whereObj.filter != null) {
    pushWhere.push(whereObj.filter);
  }

  let include = [
    {
      model: Client,
    },
    {
      model: Cabang,
    },
  ];
  lokasiTugas = await LokasiTugas.findAll({
    order: [["id_lokasi_tugas", "asc"]],
    where: pushWhere,
    include: include,
    limit: limit,
    offset: offset,
  });

  if (limit == null && offset == null) {
    lokasiTugas = await LokasiTugas.findAll({
      order: [["id_lokasi_tugas", "asc"]],
      where: pushWhere,
      include: include,
    });
  }

  return lokasiTugas;
};

const getLokasiTugasAll = async () => {
  let lokasiTugas = await LokasiTugas.findAll();
  return lokasiTugas;
};

const getLokasiTugasId = async (id_lokasi_tugas = null) => {
  let lokasiTugas = null;
  let include = [
    {
      model: Client,
    },
    {
      model: Cabang,
    },
  ];

  if (id_lokasi_tugas != null) {
    lokasiTugas = await LokasiTugas.findOne({
      where: {
        id_lokasi_tugas: id_lokasi_tugas,
      },
      include: include,
    });
  }
  return lokasiTugas;
};

const createLokasiTugas = async (data = {}, transaction = {}) => {
  let insert = await LokasiTugas.create(data, transaction);
  return insert;
};

const updateLokasiTugas = async (
  data = {},
  id_lokasi_tugas,
  transaction = {}
) => {
  let update = await LokasiTugas.update(data, {
    where: {
      id_lokasi_tugas: id_lokasi_tugas,
    },
    ...transaction,
  });
  return update;
};

const deleteLokasiTugas = async (id_lokasi_tugas = null, transaction = {}) => {
  let lokasiTugas = await LokasiTugas.destroy({
    where: {
      id_lokasi_tugas: id_lokasi_tugas,
    },
    ...transaction,
  });
  return lokasiTugas;
};

module.exports = {
  getLokasiTugasAll,
  getLokasiTugas,
  getLokasiTugasId,
  createLokasiTugas,
  updateLokasiTugas,
  deleteLokasiTugas,
  LokasiTugas,
};
