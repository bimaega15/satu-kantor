const { Op } = require("sequelize");
const {
  PegawaiJabatan,
  Pegawai,
  Jabatan,
  Cabang,
  Client,
} = require("../model/index");
const moment = require("moment");

const getJabatanPegawai = async (
  limit = null,
  offset = null,
  search = null,
  filter = {},
  id_client = null
) => {
  let whereObj = {};
  whereObj.queryLike = null;
  whereObj.filter = null;

  // filter
  if (filter.id_jabatan != "" && filter.id_jabatan != null) {
    whereObj.filter = {
      ...whereObj.filter,
      id_jabatan: filter.id_jabatan,
    };
  }
  if (filter.no_pegawai != "" && filter.no_pegawai != null) {
    whereObj.filter = {
      ...whereObj.filter,
      "$pegawai.no_pegawai$": {
        [Op.iLike]: "%" + filter.no_pegawai + "%",
      },
    };
  }
  if (filter.nama_lengkap != "" && filter.nama_lengkap != null) {
    whereObj.filter = {
      ...whereObj.filter,
      "$pegawai.nama_lengkap$": {
        [Op.iLike]: "%" + filter.nama_lengkap + "%",
      },
    };
  }
  if (filter.jenis_kelamin != "" && filter.jenis_kelamin != null) {
    whereObj.filter = {
      ...whereObj.filter,
      "$pegawai.jenis_kelamin$": filter.jenis_kelamin,
    };
  }
  if (filter.tanggal_mulai != "" && filter.tanggal_mulai != null) {
    whereObj.filter = {
      ...whereObj.filter,
      tanggal_mulai: moment(filter.tanggal_mulai, "DD-MM-YYYY").format(
        "YYYY-MM-DD"
      ),
    };
  }
  if (filter.tanggal_selesai != "" && filter.tanggal_selesai != null) {
    whereObj.filter = {
      ...whereObj.filter,
      tanggal_selesai: moment(filter.tanggal_selesai, "DD-MM-YYYY").format(
        "YYYY-MM-DD"
      ),
    };
  }
  if (filter.is_aktif != "" && filter.is_aktif != null) {
    whereObj.filter = {
      ...whereObj.filter,
      is_aktif: filter.is_aktif,
    };
  }
  if (whereObj.filter != null) {
    whereObj.filter = {
      [Op.and]: whereObj.filter,
    };
  }

  let pegawaiJabatan = "";
  // condition
  if (search != null && search != "") {
    whereObj.queryLike = {
      [Op.or]: {
        "$jabatan.nama_jabatan$": {
          [Op.iLike]: "%" + search + "%",
        },
        "$pegawai.no_pegawai$": {
          [Op.iLike]: "%" + search + "%",
        },
        "$pegawai.nama_lengkap$": {
          [Op.iLike]: "%" + search + "%",
        },
        "$pegawai.jenis_kelamin$": {
          [Op.iLike]: "%" + search + "%",
        },
      },
    };
  }

  // execute
  let pushWhere = [];
  pushWhere.push({
    "$pegawai.client.id_client$": id_client,
  });

  if (search != null && search != "") {
    pushWhere.push(whereObj.queryLike);
  }

  if (whereObj.filter != null) {
    pushWhere.push(whereObj.filter);
  }

  let include = [
    {
      model: Jabatan,
    },
    {
      model: Pegawai,
      include: [
        {
          model: Client,
          required: true,
        },
      ],
    },
  ];

  pegawaiJabatan = await PegawaiJabatan.findAll({
    order: [["id_pegawai_jabatan", "asc"]],
    where: pushWhere,
    include: include,
    limit: limit,
    offset: offset,
  });

  if (limit == null && offset == null) {
    pegawaiJabatan = await PegawaiJabatan.findAll({
      order: [["id_pegawai_jabatan", "asc"]],
      where: pushWhere,
      include: include,
    });
  }

  return pegawaiJabatan;
};

const getPegawaiJabatanAll = async () => {
  let pegawaiJabatan = await PegawaiJabatan.findAll();
  return pegawaiJabatan;
};

const getPegawaiJabatanId = async (id_pegawai_jabatan = null) => {
  let pegawaiJabatan = null;
  let include = [
    {
      model: Pegawai,
    },
    {
      model: Jabatan,
    },
    {
      model: Cabang,
    },
  ];

  if (id_pegawai_jabatan != null) {
    pegawaiJabatan = await PegawaiJabatan.findOne({
      where: {
        id_pegawai_jabatan: id_pegawai_jabatan,
      },
      include: include,
    });
  }
  return pegawaiJabatan;
};

const createPegawaiJabatan = async (data = {}) => {
  let insert = await PegawaiJabatan.create(data);
  return insert;
};

const updatePegawaiJabatan = async (data = {}, id_pegawai_jabatan) => {
  let update = await PegawaiJabatan.update(data, {
    where: {
      id_pegawai_jabatan: id_pegawai_jabatan,
    },
  });
  return update;
};

const deletePegawaiJabatan = async (id_pegawai_jabatan = null) => {
  let pegawaiJabatan = await PegawaiJabatan.destroy({
    where: {
      id_pegawai_jabatan: id_pegawai_jabatan,
    },
  });
  return pegawaiJabatan;
};

const checkPegawai = async (id_pegawai = null, id_pegawai_jabatan = null) => {
  let pegawaiJabatan = null;
  if (id_pegawai != null && id_pegawai_jabatan == null) {
    pegawaiJabatan = await PegawaiJabatan.findAll({
      attributes: ["id_pegawai", "id_pegawai_jabatan"],
      where: {
        id_pegawai: id_pegawai,
        is_aktif: 1,
      },
    });
  }
  if (id_pegawai != null && id_pegawai_jabatan != null) {
    pegawaiJabatan = await PegawaiJabatan.findAll({
      attributes: ["id_pegawai", "id_pegawai_jabatan"],
      where: {
        id_pegawai: id_pegawai,
        is_aktif: 1,
        id_pegawai_jabatan: {
          [Op.not]: id_pegawai_jabatan,
        },
      },
    });
  }

  return pegawaiJabatan;
};

module.exports = {
  getPegawaiJabatanAll,
  getJabatanPegawai,
  getPegawaiJabatanId,
  createPegawaiJabatan,
  updatePegawaiJabatan,
  deletePegawaiJabatan,
  checkPegawai,
  PegawaiJabatan,
};
