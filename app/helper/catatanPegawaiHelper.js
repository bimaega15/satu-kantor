const { Op } = require("sequelize");
const { PegawaiCatatan, Pegawai, Client } = require("../model/index");
const moment = require("moment");

const getCatatanPegawai = async (
  limit = null,
  offset = null,
  search = null,
  filter = {},
  id_client = null
) => {
  let whereObj = {};
  whereObj.queryLike = null;
  whereObj.filter = null;

  // filter
  if (filter.jenis != "" && filter.jenis != null) {
    whereObj.filter = {
      ...whereObj.filter,
      jenis: filter.jenis,
    };
  }
  if (filter.id_pegawai != "" && filter.id_pegawai != null) {
    whereObj.filter = {
      ...whereObj.filter,
      "$pegawai.nama_lengkap$": {
        [Op.iLike]: "%" + filter.id_pegawai + "%",
      },
    };
  }
  if (filter.catatan != "" && filter.catatan != null) {
    whereObj.filter = {
      ...whereObj.filter,
      catatan: {
        [Op.iLike]: "%" + filter.catatan + "%",
      },
    };
  }
  if (filter.waktu_efektif != "" && filter.waktu_efektif != null) {
    whereObj.filter = {
      ...whereObj.filter,
      waktu_efektif: moment(filter.waktu_efektif, "DD-MM-YYYY").format(
        "YYYY-MM-DD"
      ),
    };
  }

  if (whereObj.filter != null) {
    whereObj.filter = {
      [Op.and]: whereObj.filter,
    };
  }

  let catatanPegawai = "";
  // condition
  if (search != null && search != "") {
    whereObj.queryLike = {
      [Op.or]: {
        "$pegawai.nama_lengkap$": {
          [Op.iLike]: "%" + search + "%",
        },
        catatan: {
          [Op.iLike]: "%" + search + "%",
        },
      },
    };
  }

  // execute
  let pushWhere = [];
  pushWhere.push({
    "$pegawai.id_client$": id_client,
  });

  if (search != null && search != "") {
    pushWhere.push(whereObj.queryLike);
  }

  if (whereObj.filter != null) {
    pushWhere.push(whereObj.filter);
  }
  let include = [
    {
      model: Pegawai,
      include: [
        {
          model: Client,
          required: true,
        },
      ],
    },
  ];
  catatanPegawai = await PegawaiCatatan.findAll({
    where: pushWhere,
    order: [["id_pegawai_catatan", "asc"]],
    include: include,
    limit: limit,
    offset: offset,
  });

  if (limit == null && offset == null) {
    catatanPegawai = await PegawaiCatatan.findAll({
      order: [["id_pegawai_catatan", "asc"]],
      where: pushWhere,
      include: include,
    });
  }

  return catatanPegawai;
};

const getCatatanPegawaiAll = async () => {
  let pegawaiCatatan = await PegawaiCatatan.findAll();
  return pegawaiCatatan;
};

const getCatatanPegawaiId = async (id_pegawai_catatan = null) => {
  let pegawaiCatatan = null;
  if (id_pegawai_catatan != null) {
    pegawaiCatatan = await PegawaiCatatan.findOne({
      where: {
        id_pegawai_catatan: id_pegawai_catatan,
      },
      include: [
        {
          model: Pegawai,
        },
      ],
    });
  }
  return pegawaiCatatan;
};

const createCatatanPegawai = async (data = {}, transaction = {}) => {
  let insert = await PegawaiCatatan.create(data, transaction);
  return insert;
};

const updateCatatanPegawai = async (
  data = {},
  id_pegawai_catatan,
  transaction = {}
) => {
  let update = await PegawaiCatatan.update(data, {
    where: {
      id_pegawai_catatan: id_pegawai_catatan,
    },
    ...transaction,
  });
  return update;
};

const deleteCatatanPegawai = async (
  id_pegawai_catatan = null,
  transaction = {}
) => {
  let pegawaiCatatan = await PegawaiCatatan.destroy({
    where: {
      id_pegawai_catatan: id_pegawai_catatan,
    },
    ...transaction,
  });
  return pegawaiCatatan;
};

module.exports = {
  getCatatanPegawaiAll,
  getCatatanPegawai,
  getCatatanPegawaiId,
  createCatatanPegawai,
  updateCatatanPegawai,
  deleteCatatanPegawai,
  PegawaiCatatan,
};
