const { Op } = require("sequelize");
const { Penugasan, PenugasanFile } = require("../model/index");

const getPenugasanFile = async (
  limit = null,
  offset = null,
  search = null,
  filter = {}
) => {
  let whereObj = {};
  whereObj.queryLike = null;
  whereObj.filter = null;

  // filter
  if (filter.id_penugasan != "" && filter.id_penugasan != null) {
    whereObj.filter = {
      ...whereObj.filter,
      id_penugasan: filter.id_penugasan,
    };
  }
  if (filter.deskripsi != "" && filter.deskripsi != null) {
    whereObj.filter = {
      ...whereObj.filter,
      deskripsi: {
        [Op.iLike]: "%" + filter.deskripsi + "%",
      },
    };
  }

  let penugasan_file = "";

  // execute
  let pushWhere = [];
  if (search != null) {
    whereObj.queryLike = {
      [Op.or]: {
        deskripsi: {
          [Op.iLike]: "%" + search + "%",
        },
      },
    };
  }

  if (search != null) {
    pushWhere.push(whereObj.queryLike);
  }

  if (whereObj.filter != null) {
    pushWhere.push(whereObj.filter);
  }

  let getInclude = [
    {
      model: Penugasan,
    },
  ];

  penugasan_file = await PenugasanFile.findAll({
    order: [["id_penugasan_file", "asc"]],
    where: pushWhere,
    include: getInclude,
    limit: limit,
    offset: offset,
  });

  if (limit == null && offset == null) {
    penugasan_file = await PenugasanFile.findAll({
      order: [["id_penugasan_file", "asc"]],
      where: pushWhere,
      include: getInclude,
    });
  }

  return penugasan_file;
};

const getPenugasanFileAll = async () => {
  let penugasan_file = await PenugasanFile.findAll();
  return penugasan_file;
};

const getPenugasanFileId = async (id_penugasan_file = null) => {
  let penugasan_file = null;
  let getInclude = [
    {
      model: Penugasan,
      as: "penugasan_file",
    },
  ];
  if (id_penugasan_file != null) {
    penugasan_file = await PenugasanFile.findOne({
      where: {
        id_penugasan_file: id_penugasan_file,
      },
      include: getInclude,
    });
  }
  return penugasan_file;
};

const createPenugasanFile = async (data = {}) => {
  let insert = await PenugasanFile.create(data);
  return insert;
};

const updatePenugasanFile = async (
  data = {},
  id_penugasan_file,
  transaction = {}
) => {
  let update = await PenugasanFile.update(data, {
    where: {
      id_penugasan_file: id_penugasan_file,
    },
    ...transaction,
  });
  return update;
};

const deletePenugasanFile = async (id_penugasan_file = null) => {
  let penugasan_file = await PenugasanFile.destroy({
    where: {
      id_penugasan_file: id_penugasan_file,
    },
  });
  return penugasan_file;
};

module.exports = {
  getPenugasanFile,
  getPenugasanFileAll,
  getPenugasanFileId,
  createPenugasanFile,
  updatePenugasanFile,
  deletePenugasanFile,
  Penugasan,
};
