const {
  Rainburstment,
  RainburstmentLists,
  Pegawai,
} = require("../model/index");
const { Op } = require("sequelize");

const getRainburstment = async (
  limit = null,
  offset = null,
  search = null,
  filter = {}
) => {
  let whereObj = {};
  whereObj.queryLike = null;
  whereObj.filter = null;

  // filter
  if (filter.tanggal != "" && filter.tanggal != null) {
    whereObj.filter = {
      ...whereObj.filter,
      tanggal: filter.tanggal,
    };
  }
  if (filter.process != "" && filter.process != null) {
    whereObj.filter = {
      ...whereObj.filter,
      process: filter.process,
    };
  }

  if (filter.id_pegawai != "" && filter.id_pegawai != null) {
    whereObj.filter = {
      ...whereObj.filter,
      id_pegawai: filter.id_pegawai,
    };
  }
  if (filter.id_rainburstment != "" && filter.id_rainburstment != null) {
    whereObj.filter = {
      ...whereObj.filter,
      id_rainburstment: filter.id_rainburstment,
    };
  }

  if (filter.nama_id_pegawai != "" && filter.nama_id_pegawai != null) {
    whereObj.filter = {
      ...whereObj.filter,
      "$pegawai.nama_lengkap$": {
        [Op.iLike]: "%" + filter.nama_id_pegawai + "%",
      },
    };
  }

  let rainburstment = "";
  // condition
  if (search != null && search != "") {
    whereObj.queryLike = {
      [Op.or]: {
        "$pegawai.nama_lengkap$": {
          [Op.iLike]: "%" + filter.nama_id_pegawai + "%",
        },
      },
    };
  }

  // execute
  let pushWhere = [];
  if (search != null && search != "") {
    pushWhere.push(whereObj.queryLike);
  }
  if (whereObj.filter != null) {
    pushWhere.push(whereObj.filter);
  }

  let getInclude = [
    {
      model: Pegawai,
    },
    {
      model: RainburstmentLists,
      required: true,
    },
  ];

  rainburstment = await Rainburstment.findAll({
    where: pushWhere,
    include: getInclude,
    order: [["id_rainburstment", "asc"]],
    limit: limit,
    offset: offset,
  });

  if (limit == null && offset == null) {
    rainburstment = await Rainburstment.findAll({
      where: pushWhere,
      include: getInclude,
      order: [["id_rainburstment", "asc"]],
    });
  }

  return rainburstment;
};

const getRainbursetMentById = async (id_rainburstment = null) => {
  let rainburstment = "";
  let getInclude = [
    {
      model: Pegawai,
    },
    {
      model: RainburstmentLists,
      required: true,
    },
  ];
  rainburstment = await Rainburstment.findOne({
    where: {
      id_rainburstment: id_rainburstment,
    },
    include: getInclude,
    order: [["id_rainburstment", "asc"]],
  });

  return rainburstment;
};

const createRainburstMent = async (data = {}, transaction = {}) => {
  let insert = await Rainburstment.create(data, transaction);
  return insert;
};

const updateRainburstMent = async (
  data = {},
  id_rainburstment,
  transaction = {}
) => {
  let update = await Rainburstment.update(data, {
    where: {
      id_rainburstment: id_rainburstment,
    },
    ...transaction,
  });
  return update;
};

const deleteRainburstMent = async (
  id_rainburstment = null,
  transaction = {}
) => {
  let rainburstment = await Rainburstment.destroy({
    where: {
      id_rainburstment: id_rainburstment,
    },
    ...transaction,
  });
  return rainburstment;
};

module.exports = {
  getRainburstment,
  getRainbursetMentById,
  createRainburstMent,
  updateRainburstMent,
  deleteRainburstMent,
};
