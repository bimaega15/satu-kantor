const { Op } = require("sequelize");
const sequelize = require("../config/db");
const { Rekening } = require("../model/index");

const getRekening = async (pegawai_id = null) => {
  let rekening = "";
  if (pegawai_id != null) {
    rekening = await Rekening.findAll({
      where: {
        pegawai_id: pegawai_id,
      },
      order: [["pegawai_id", "asc"]],
    });
  } else {
    rekening = await Rekening.findAll({
      order: [["pegawai_id", "asc"]],
    });
  }

  return rekening;
};

const insertRekening = async (data = [], transaction = {}) => {
  let rekening = "";
  rekening = Rekening.create(data, transaction);
  return rekening;
};

const updateRekening = async (
  data = [],
  pegawai_id = null,
  transaction = {}
) => {
  let rekening = "";
  rekening = Rekening.update(data, {
    where: {
      pegawai_id: pegawai_id,
    },
    ...transaction,
  });
  return rekening;
};

module.exports = {
  getRekening,
  insertRekening,
  updateRekening,
  Rekening,
};
