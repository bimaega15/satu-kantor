const { Op, Sequelize } = require("sequelize");
const {
  Penugasan,
  Cabang,
  Client,
  Pegawai,
  Tugas,
  LokasiTugas,
  PenugasanFile,
  PenugasanStatus,
} = require("../model/index");
const moment = require("moment");
const sequelize = require("../config/db");

const getPenugasan = async (
  limit = null,
  offset = null,
  search = null,
  filter = {}
) => {
  let whereObj = {};
  whereObj.queryLike = null;
  whereObj.filter = null;

  // filter
  if (filter.id_client != "" && filter.id_client != null) {
    whereObj.filter = {
      ...whereObj.filter,
      id_client: filter.id_client,
    };
  }
  if (filter.id_cabang != "" && filter.id_cabang != null) {
    whereObj.filter = {
      ...whereObj.filter,
      id_cabang: filter.id_cabang,
    };
  }
  if (filter.id_pegawai != "" && filter.id_pegawai != null) {
    whereObj.filter = {
      ...whereObj.filter,
      id_pegawai: filter.id_pegawai,
    };
  }
  if (filter.tanggal_penugasan != "" && filter.tanggal_penugasan != null) {
    whereObj.filter = {
      ...whereObj.filter,
      tanggal_penugasan: filter.tanggal_penugasan,
    };
  }
  if (filter.jenis_penugasan != "" && filter.jenis_penugasan != null) {
    whereObj.filter = {
      ...whereObj.filter,
      jenis_penugasan: {
        [Op.iLike]: "%" + filter.jenis_penugasan + "%",
      },
    };
  }
  if (filter.id_tugas != "" && filter.id_tugas != null) {
    whereObj.filter = {
      ...whereObj.filter,
      id_tugas: filter.id_tugas,
    };
  }
  if (filter.id_lokasi_tugas != "" && filter.id_lokasi_tugas != null) {
    whereObj.filter = {
      ...whereObj.filter,
      id_lokasi_tugas: filter.id_lokasi_tugas,
    };
  }
  if (filter.is_aktif != "" && filter.is_aktif != null) {
    whereObj.filter = {
      ...whereObj.filter,
      is_aktif: filter.is_aktif,
    };
  }
  if (filter.is_aktif != "" && filter.is_aktif != null) {
    whereObj.filter = {
      ...whereObj.filter,
      is_aktif: filter.is_aktif,
    };
  }
  if (filter.qr_code != "" && filter.qr_code != null) {
    whereObj.filter = {
      ...whereObj.filter,
      "$lokasi_tugas.qr_code$": filter.qr_code,
    };
  }

  let penugasan = "";

  // execute
  let pushWhere = [];
  if (search != null) {
    whereObj.queryLike = {
      [Op.or]: {
        "$tugas.nama_tugas$": {
          [Op.iLike]: "%" + search + "%",
        },
        "$lokasi_tugas.nama_lokasi$": {
          [Op.iLike]: "%" + search + "%",
        },
        "$lokasi_tugas.qr_code$": {
          [Op.iLike]: "%" + search + "%",
        },
        "$cabang.nama_cabang$": {
          [Op.iLike]: "%" + search + "%",
        },
        "$pegawai.nama_lengkap$": {
          [Op.iLike]: "%" + search + "%",
        },
        jenis_penugasan: {
          [Op.iLike]: "%" + search + "%",
        },
      },
    };
  }

  if (search != null) {
    pushWhere.push(whereObj.queryLike);
  }

  if (whereObj.filter != null) {
    pushWhere.push(whereObj.filter);
  }

  let include = [
    {
      model: Cabang,
    },
    {
      model: Client,
    },
    {
      model: Pegawai,
    },
    {
      model: Tugas,
      as: "tugas",
    },
    {
      model: LokasiTugas,
      as: "lokasi_tugas",
    },
  ];

  penugasan = await Penugasan.findAll({
    order: [["id_penugasan", "asc"]],
    include: include,
    where: pushWhere,
    limit: limit,
    offset: offset,
  });

  if (limit == null && offset == null) {
    penugasan = await Penugasan.findAll({
      order: [["id_penugasan", "asc"]],
      include: include,
      where: pushWhere,
    });
  }

  return penugasan;
};

const getPenugasanAll = async () => {
  let penugasan = await Penugasan.findAll();
  return penugasan;
};

const getPenugasanId = async (id_penugasan = null) => {
  let penugasan = null;
  let include = [
    {
      model: Cabang,
    },
    {
      model: Client,
    },
    {
      model: Pegawai,
    },
    {
      model: Tugas,
      as: "tugas",
    },
    {
      model: LokasiTugas,
      as: "lokasi_tugas",
    },
  ];
  if (id_penugasan != null) {
    penugasan = await Penugasan.findOne({
      where: {
        id_penugasan: id_penugasan,
      },
      include: include,
    });
  }
  return penugasan;
};

const createPenugasan = async (data = {}, transaction = {}) => {
  let insert = await Penugasan.create(data, transaction);
  return insert;
};

const updatePenugasan = async (data = {}, id_penugasan, transaction = {}) => {
  let update = await Penugasan.update(data, {
    where: {
      id_penugasan: id_penugasan,
    },
    ...transaction,
  });
  return update;
};

const deletePenugasan = async (id_penugasan = null, transaction = {}) => {
  let penugasan = await Penugasan.destroy({
    where: {
      id_penugasan: id_penugasan,
    },
    ...transaction,
  });
  return penugasan;
};

module.exports = {
  getPenugasanAll,
  getPenugasan,
  getPenugasanId,
  createPenugasan,
  updatePenugasan,
  deletePenugasan,
  Penugasan,
};
