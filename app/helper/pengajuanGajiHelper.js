const { Op, Sequelize } = require("sequelize");
const { PengajuanGaji, Cabang, PengajuanGajiList } = require("../model/index");
const moment = require("moment");
const sequelize = require("../config/db");

const getPengajuanGaji = async (
  limit = null,
  offset = null,
  search = null,
  filter = {}
) => {
  let whereObj = {};
  whereObj.queryLike = null;
  whereObj.filter = null;

  // filter
  if (filter.id_client != "" && filter.id_client != null) {
    whereObj.filter = {
      ...whereObj.filter,
      id_client: filter.id_client,
    };
  }
  if (filter.id_cabang != "" && filter.id_cabang != null) {
    whereObj.filter = {
      ...whereObj.filter,
      id_cabang: filter.id_cabang,
    };
  }
  if (filter.periode_pengajuan != "" && filter.periode_pengajuan != null) {
    whereObj.filter = {
      ...whereObj.filter,
      periode_pengajuan: moment(filter.periode_pengajuan, "DD-MM-YYYY").format(
        "YYYY-MM-DD"
      ),
    };
  }
  if (filter.periode_awal != "" && filter.periode_awal != null) {
    whereObj.filter = {
      ...whereObj.filter,
      periode_awal: moment(filter.periode_awal, "DD-MM-YYYY").format(
        "YYYY-MM-DD"
      ),
    };
  }
  if (filter.periode_akhir != "" && filter.periode_akhir != null) {
    whereObj.filter = {
      ...whereObj.filter,
      periode_akhir: moment(filter.periode_akhir, "DD-MM-YYYY").format(
        "YYYY-MM-DD"
      ),
    };
  }
  if (filter.status_pengajuan != "" && filter.status_pengajuan != null) {
    whereObj.filter = {
      ...whereObj.filter,
      status_pengajuan: filter.status_pengajuan,
    };
  }

  if (whereObj.filter != null) {
    whereObj.filter = {
      [Op.and]: whereObj.filter,
    };
  }

  let pengajuanGaji = "";

  // execute
  let pushWhere = [];
  if (search != null) {
    whereObj.queryLike = {
      [Op.or]: {
        "$cabang.nama_cabang$": {
          [Op.iLike]: "%" + search + "%",
        },
      },
    };
  }

  if (search != null) {
    pushWhere.push(whereObj.queryLike);
  }

  if (whereObj.filter != null) {
    pushWhere.push(whereObj.filter);
  }

  let getInclude = [
    {
      model: Cabang,
    },
    {
      model: PengajuanGajiList,
    },
  ];

  pengajuanGaji = await PengajuanGaji.findAll({
    order: [["id_pengajuan_gaji", "asc"]],
    where: pushWhere,
    include: getInclude,
    limit: limit,
    offset: offset,
  });

  if (limit == null && offset == null) {
    pengajuanGaji = await PengajuanGaji.findAll({
      order: [["id_pengajuan_gaji", "asc"]],
      where: pushWhere,
      include: getInclude,
    });
  }

  return pengajuanGaji;
};

const getPengajuanGajiAll = async () => {
  let pengajuanGaji = await PengajuanGaji.findAll();
  return pengajuanGaji;
};

const getPengajuanGajiId = async (id_pengajuan_gaji = null) => {
  let pengajuanGaji = null;
  if (id_pengajuan_gaji != null) {
    pengajuanGaji = await PengajuanGaji.findOne({
      where: {
        id_pengajuan_gaji: id_pengajuan_gaji,
      },
    });
  }
  return pengajuanGaji;
};

const createPengajuanGaji = async (data = {}, transaction = {}) => {
  let insert = await PengajuanGaji.create(data, transaction);
  return insert;
};

const updatePengajuanGaji = async (
  data = {},
  id_pengajuan_gaji,
  transaction = {}
) => {
  let update = await PengajuanGaji.update(data, {
    where: {
      id_pengajuan_gaji: id_pengajuan_gaji,
    },
    ...transaction,
  });
  return update;
};

const deletePengajuanGaji = async (
  id_pengajuan_gaji = null,
  transaction = {}
) => {
  let pengajuanGaji = await PengajuanGaji.destroy({
    where: {
      id_pengajuan_gaji: id_pengajuan_gaji,
    },
    ...transaction,
  });
  return pengajuanGaji;
};

module.exports = {
  getPengajuanGajiAll,
  getPengajuanGaji,
  getPengajuanGajiId,
  createPengajuanGaji,
  updatePengajuanGaji,
  deletePengajuanGaji,
  PengajuanGaji,
};
