const { Op } = require("sequelize");
const { Users, UserMapping } = require("../model/index");

const getUsers = async (
  skip = null,
  offset = null,
  search = null,
  limit = null
) => {
  let client = "";
  if (search != null && search != "" && limit != "" && limit != null) {
    client = await Users.findAll({
      attributes: ["id", "username", "email", "pin", "status", "auth_key"],
      where: {
        [Op.or]: {
          username: {
            [Op.iLike]: "%" + search + "%",
          },
          email: {
            [Op.iLike]: "%" + search + "%",
          },
          pin: {
            [Op.iLike]: "%" + search + "%",
          },
        },
      },
      order: [["id", "asc"]],
      limit: limit,
    });
  }

  if (skip != null && offset != null) {
    client = await Users.findAll({
      attributes: ["id", "username", "email", "pin", "status", "auth_key"],
      order: [["id", "asc"]],
      limit: skip,
      offset: offset,
    });
  }

  if (limit == null && search != null) {
    client = await Users.findAll({
      attributes: ["id", "username", "email", "pin", "status", "auth_key"],
      where: {
        [Op.or]: {
          username: {
            [Op.iLike]: "%" + search + "%",
          },
          email: {
            [Op.iLike]: "%" + search + "%",
          },
          pin: {
            [Op.iLike]: "%" + search + "%",
          },
        },
      },
      order: [["id", "asc"]],
    });
  }
  return client;
};

const checkIdentitas = async (identitas, page, value, id = null) => {
  let check = "";
  if (page == "add") {
    if (identitas == "username") {
      check = await Users.count({
        where: {
          username: value,
        },
      });
    }
  } else {
    if (identitas == "username") {
      check = await Users.count({
        where: {
          username: value,
          id: {
            [Op.not]: id,
          },
        },
      });
    }
  }
  return check;
};

const getUsersById = async (
  id_user = null,
  username = null,
  password = null,
  email = null
) => {
  let users = "";
  let getInclude = {
    model: UserMapping,
    attributes: ["jenis_mapping", "id_user", "id_mapping", "id_user_mapping"],
  };
  if (id_user != null) {
    users = await Users.findOne({
      where: {
        id: id_user,
      },
    });
  }
  if (username != null) {
    users = await Users.findOne({
      where: {
        username: username,
      },
      include: getInclude,
    });
  }
  if ((username != null) & (password != null)) {
    users = await Users.findOne({
      where: {
        username: username,
        password_hash: password,
      },
    });
  }
  if (email != null) {
    users = await Users.findOne({
      where: {
        email: email,
      },
      include: getInclude,
    });
  }
  return users;
};

const getUserEmail = async (email = null, pin = null) => {
  let users = "";
  let getInclude = {
    model: UserMapping,
    attributes: ["jenis_mapping", "id_user", "id_mapping", "id_user_mapping"],
  };
  if (email != null && pin == null) {
    users = await Users.findOne({
      where: {
        email: email,
      },
      include: getInclude,
    });
  }
  if (email != null && pin != null) {
    users = await Users.findOne({
      where: {
        email: email,
        pin: pin,
      },
      include: getInclude,
    });
  }
  return users;
};

const getUserAccount = async (username = null, password = null) => {
  let users = "";
  let getInclude = {
    model: UserMapping,
    attributes: ["jenis_mapping", "id_user", "id_mapping", "id_user_mapping"],
  };
  if (username != null && password == null) {
    users = await Users.findOne({
      where: {
        username: username,
      },
      include: getInclude,
    });
  }

  if (username != null && password != null) {
    users = await Users.findOne({
      where: {
        username: username,
        password_hash: password,
      },
      include: getInclude,
    });
  }
  return users;
};

module.exports = {
  getUsers,
  checkIdentitas,
  getUsersById,
  getUserEmail,
  getUserAccount,
  Users,
};
