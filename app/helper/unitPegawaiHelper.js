const { Op } = require("sequelize");
const {
  PegawaiUnit,
  Pegawai,
  Unit,
  Cabang,
  Client,
} = require("../model/index");
const moment = require("moment");

const getUnitPegawai = async (
  limit = null,
  offset = null,
  search = null,
  filter = {},
  id_client = null
) => {
  let whereObj = {};
  whereObj.queryLike = null;
  whereObj.filter = null;

  // filter
  if (filter.id_unit != "" && filter.id_unit != null) {
    whereObj.filter = {
      ...whereObj.filter,
      id_unit: filter.id_unit,
    };
  }
  if (filter.no_pegawai != "" && filter.no_pegawai != null) {
    whereObj.filter = {
      ...whereObj.filter,
      "$pegawai.no_pegawai$": {
        [Op.iLike]: "%" + filter.no_pegawai + "%",
      },
    };
  }
  if (filter.nama_lengkap != "" && filter.nama_lengkap != null) {
    whereObj.filter = {
      ...whereObj.filter,
      "$pegawai.nama_lengkap$": {
        [Op.iLike]: "%" + filter.nama_lengkap + "%",
      },
    };
  }
  if (filter.jenis_kelamin != "" && filter.jenis_kelamin != null) {
    whereObj.filter = {
      ...whereObj.filter,
      "$pegawai.jenis_kelamin$": filter.jenis_kelamin,
    };
  }
  if (filter.tanggal_mulai != "" && filter.tanggal_mulai != null) {
    whereObj.filter = {
      ...whereObj.filter,
      tanggal_mulai: moment(filter.tanggal_mulai, "DD-MM-YYYY").format(
        "YYYY-MM-DD"
      ),
    };
  }
  if (filter.tanggal_selesai != "" && filter.tanggal_selesai != null) {
    whereObj.filter = {
      ...whereObj.filter,
      tanggal_selesai: moment(filter.tanggal_selesai, "DD-MM-YYYY").format(
        "YYYY-MM-DD"
      ),
    };
  }
  if (filter.is_aktif != "" && filter.is_aktif != null) {
    whereObj.filter = {
      ...whereObj.filter,
      is_aktif: filter.is_aktif,
    };
  }
  if (whereObj.filter != null) {
    whereObj.filter = {
      [Op.and]: whereObj.filter,
    };
  }

  let pegawaiUnit = "";
  // condition
  if (search != null && search != "") {
    whereObj.queryLike = {
      [Op.or]: {
        "$unit.nama_unit$": {
          [Op.iLike]: "%" + search + "%",
        },
        "$pegawai.no_pegawai$": {
          [Op.iLike]: "%" + search + "%",
        },
        "$pegawai.nama_lengkap$": {
          [Op.iLike]: "%" + search + "%",
        },
        "$pegawai.jenis_kelamin$": {
          [Op.iLike]: "%" + search + "%",
        },
      },
    };
  }

  // execute
  let pushWhere = [];
  pushWhere.push({
    "$pegawai.client.id_client$": id_client,
  });
  if (search != null && search != "") {
    pushWhere.push(whereObj.queryLike);
  }

  if (whereObj.filter != null) {
    pushWhere.push(whereObj.filter);
  }

  let getInclude = [
    {
      model: Unit,
    },
    {
      model: Pegawai,
      include: [
        {
          model: Client,
          required: true,
        },
      ],
    },
  ];

  pegawaiUnit = await PegawaiUnit.findAll({
    order: [["id_pegawai_unit", "asc"]],
    where: pushWhere,
    include: getInclude,
    limit: limit,
    offset: offset,
  });

  if (limit == null && offset == null) {
    pegawaiUnit = await PegawaiUnit.findAll({
      order: [["id_pegawai_unit", "asc"]],
      where: pushWhere,
      include: getInclude,
    });
  }

  return pegawaiUnit;
};

const getPegawaiUnitAll = async () => {
  let pegawaiUnit = await PegawaiUnit.findAll();
  return pegawaiUnit;
};

const getPegawaiUnitId = async (id_pegawai_unit = null) => {
  let pegawaiUnit = null;
  let getInclude = [
    {
      model: Pegawai,
    },
    {
      model: Unit,
    },
    {
      model: Cabang,
    },
  ];
  if (id_pegawai_unit != null) {
    pegawaiUnit = await PegawaiUnit.findOne({
      where: {
        id_pegawai_unit: id_pegawai_unit,
      },
      include: getInclude,
    });
  }
  return pegawaiUnit;
};

const getPegawaiUnitPegawaiId = async (pegawai_id = null) => {
  let pegawaiUnit = null;
  if (pegawai_id != null) {
    pegawaiUnit = await PegawaiUnit.findOne({
      where: {
        id_pegawai: pegawai_id,
        is_aktif: 1,
      },
    });
  }
  return pegawaiUnit;
};

const checkPegawai = async (id_pegawai = null, id_pegawai_unit = null) => {
  let pegawaiUnit = null;
  if (id_pegawai != null && id_pegawai_unit == null) {
    pegawaiUnit = await PegawaiUnit.findAll({
      attributes: ["id_pegawai", "id_pegawai_unit"],
      where: {
        id_pegawai: id_pegawai,
        is_aktif: 1,
      },
    });
  }
  if (id_pegawai != null && id_pegawai_unit != null) {
    pegawaiUnit = await PegawaiUnit.findAll({
      attributes: ["id_pegawai", "id_pegawai_unit"],
      where: {
        id_pegawai: id_pegawai,
        is_aktif: 1,
        id_pegawai_unit: {
          [Op.not]: id_pegawai_unit,
        },
      },
    });
  }

  return pegawaiUnit;
};

module.exports = {
  getPegawaiUnitAll,
  getUnitPegawai,
  getPegawaiUnitId,
  checkPegawai,
  getPegawaiUnitPegawaiId,
  PegawaiUnit,
};
