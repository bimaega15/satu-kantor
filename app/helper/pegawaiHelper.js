const { Op } = require("sequelize");
const { Pegawai, Rekening, Izin } = require("../model/index");

const getPegawai = async (
  limit = null,
  offset = null,
  search = null,
  client_id = null,
  filter = {}
) => {
  let whereObj = {};
  whereObj.queryLike = null;
  whereObj.filter = null;

  // filter
  if (filter.no_identitas != "" && filter.no_identitas != null) {
    whereObj.filter = {
      ...whereObj.filter,
      no_identitas: {
        [Op.iLike]: "%" + filter.no_identitas + "%",
      },
    };
  }
  if (filter.nama_lengkap != "" && filter.nama_lengkap != null) {
    whereObj.filter = {
      ...whereObj.filter,
      nama_lengkap: {
        [Op.iLike]: "%" + filter.nama_lengkap + "%",
      },
    };
  }
  if (filter.jenis_kelamin != "" && filter.jenis_kelamin != null) {
    whereObj.filter = {
      ...whereObj.filter,
      jenis_kelamin: filter.jenis_kelamin,
    };
  }
  if (filter.email != "" && filter.email != null) {
    whereObj.filter = {
      ...whereObj.filter,
      email: {
        [Op.iLike]: "%" + filter.email + "%",
      },
    };
  }
  if (filter.no_pegawai != "" && filter.no_pegawai != null) {
    whereObj.filter = {
      ...whereObj.filter,
      no_pegawai: {
        [Op.iLike]: "%" + filter.no_pegawai + "%",
      },
    };
  }

  if (whereObj.filter != null) {
    whereObj.filter = {
      [Op.and]: whereObj.filter,
    };
  }

  let pegawai = "";
  // condition
  if (search != null && search != "") {
    whereObj.queryLike = {
      [Op.or]: {
        no_identitas: {
          [Op.iLike]: "%" + search + "%",
        },
        nama_lengkap: {
          [Op.iLike]: "%" + search + "%",
        },
        jenis_kelamin: {
          [Op.iLike]: "%" + search + "%",
        },
        tempat_lahir: {
          [Op.iLike]: "%" + search + "%",
        },

        no_kontak1: {
          [Op.iLike]: "%" + search + "%",
        },
        email: {
          [Op.iLike]: "%" + search + "%",
        },
        no_pegawai: {
          [Op.iLike]: "%" + search + "%",
        },
      },
    };
  }

  // execute
  let pushWhere = [];
  pushWhere.push({
    id_client: client_id,
  });

  if (search != null && search != "") {
    pushWhere.push(whereObj.queryLike);
  }

  if (whereObj.filter != null) {
    pushWhere.push(whereObj.filter);
  }

  let include = [
    {
      model: Rekening,
    },
    {
      model: Izin,
    },
  ];

  pegawai = await Pegawai.findAll({
    order: [["id_pegawai", "asc"]],
    where: pushWhere,
    include: include,
    limit: limit,
    offset: offset,
  });

  if (limit == null && offset == null) {
    pegawai = await Pegawai.findAll({
      order: [["id_pegawai", "asc"]],
      where: pushWhere,
      include: include,
    });
  }

  return pegawai;
};

const checkIdentitas = async (identitas, page, value, id_pegawai = null) => {
  let check = "";
  if (page == "add") {
    if (identitas == "no_identitas") {
      check = await Pegawai.count({
        where: {
          no_identitas: value,
        },
      });
    }
    if (identitas == "no_pegawai") {
      check = await Pegawai.count({
        where: {
          no_pegawai: value,
        },
      });
    }
  } else {
    if (identitas == "no_identitas") {
      check = await Pegawai.count({
        where: {
          no_identitas: value,
          id_pegawai: {
            [Op.not]: id_pegawai,
          },
        },
      });
    }
    if (identitas == "no_pegawai") {
      check = await Pegawai.count({
        where: {
          no_pegawai: value,
          id_pegawai: {
            [Op.not]: id_pegawai,
          },
        },
      });
    }
  }
  return check;
};

const getPegawaiNotId = async (pegawai_id = []) => {
  let pegawai = "";
  let include = [
    {
      model: Rekening,
    },
    {
      model: Izin,
    },
  ];

  if (pegawai_id.length > 0) {
    pegawai = await Pegawai.findAll({
      attributes: [
        "id_pegawai",
        "id_client",
        "no_identitas",
        "nama_lengkap",
        "jenis_kelamin",
        "tempat_lahir",
        "tanggal_lahir",
        "no_kontak1",
        "email",
        "no_pegawai",
        "gambar",
      ],
      where: {
        id_pegawai: {
          [Op.notIn]: pegawai_id,
        },
      },
      include: include,
      order: [["id_pegawai", "asc"]],
    });
  } else {
    pegawai = Pegawai.findAll({
      order: [["id_pegawai", "asc"]],
    });
  }
  return pegawai;
};

const getPegawaiById = async (pegawai_id = null) => {
  let pegawai = "";
  let include = [
    {
      model: Rekening,
    },
    {
      model: Izin,
    },
  ];

  pegawai = await Pegawai.findOne({
    attributes: [
      "id_pegawai",
      "id_client",
      "no_identitas",
      "nama_lengkap",
      "jenis_kelamin",
      "tempat_lahir",
      "tanggal_lahir",
      "no_kontak1",
      "email",
      "no_pegawai",
      "gambar",
    ],
    where: {
      id_pegawai: pegawai_id,
    },
    include: include,
    order: [["id_pegawai", "asc"]],
  });
  return pegawai;
};
module.exports = {
  getPegawai,
  checkIdentitas,
  getPegawaiNotId,
  Pegawai,
  getPegawaiById,
};
