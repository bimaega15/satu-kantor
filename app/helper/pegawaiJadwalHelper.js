const {
  Pegawai,
  Jadwal,
  JenisAbsensi,
  PegawaiJadwal,
  Client,
} = require("../model/index");
const { Op } = require("sequelize");
const moment = require("moment");

const getPegawaiJadwal = async (
  limit = null,
  offset = null,
  search = null,
  filter = {},
  id_client = null,
  sortBy = []
) => {
  let whereObj = {};
  whereObj.queryLike = null;
  whereObj.filter = null;

  // filter
  if (filter.nama_lengkap != "" && filter.nama_lengkap != null) {
    whereObj.filter = {
      ...whereObj.filter,
      "$pegawai.nama_lengkap$": {
        [Op.iLike]: "%" + filter.nama_lengkap + "%",
      },
    };
  }
  if (filter.jadwal_id != "" && filter.jadwal_id != null) {
    whereObj.filter = {
      ...whereObj.filter,
      "$jadwal.id_jadwal$": filter.jadwal_id,
    };
  }
  if (filter.tanggal != "" && filter.tanggal != null) {
    whereObj.filter = {
      ...whereObj.filter,
      tanggal: moment(filter.tanggal, "DD-MM-YYYY").format("YYYY-MM-DD"),
    };
  }

  if (filter.is_kerja != "" && filter.is_kerja != null) {
    whereObj.filter = {
      ...whereObj.filter,
      is_kerja: filter.is_kerja,
    };
  }
  if (
    filter.is_request_ganti_jadwal != "" &&
    filter.is_request_ganti_jadwal != null
  ) {
    whereObj.filter = {
      ...whereObj.filter,
      is_request_ganti_jadwal: filter.is_request_ganti_jadwal,
    };
  }
  if (filter.is_setuju != "" && filter.is_setuju != null) {
    whereObj.filter = {
      ...whereObj.filter,
      is_setuju: filter.is_setuju,
    };
  }
  if (filter.is_home_care != "" && filter.is_home_care != null) {
    whereObj.filter = {
      ...whereObj.filter,
      is_home_care: filter.is_home_care,
    };
  }
  if (filter.is_ganti_jadwal != "" && filter.is_ganti_jadwal != null) {
    whereObj.filter = {
      ...whereObj.filter,
      is_ganti_jadwal: filter.is_ganti_jadwal,
    };
  }
  if (filter.id_pegawai != "" && filter.id_pegawai != null) {
    whereObj.filter = {
      ...whereObj.filter,
      id_pegawai: filter.id_pegawai,
    };
  }

  if (whereObj.filter != null) {
    whereObj.filter = {
      [Op.and]: whereObj.filter,
    };
  }

  let varPegawaiJadwal = "";

  // condition
  if (search != null && search != "") {
    whereObj.queryLike = {
      [Op.or]: {
        "$pegawai.nama_lengkap$": {
          [Op.iLike]: "%" + search + "%",
        },
        "$jadwal.jenis$": {
          [Op.iLike]: "%" + search + "%",
        },
        "$jenis_absensi.jenis_absensi$": {
          [Op.iLike]: "%" + search + "%",
        },
      },
    };
  }

  // execute
  let pushWhere = [];
  pushWhere.push({
    "$pegawai.id_client$": id_client,
  });

  if (sortBy.length == 0) {
    sortBy = ["id_pegawai_jadwal", "asc"];
  }

  if (search != null && search != "") {
    pushWhere.push(whereObj.queryLike);
  }

  if (whereObj.filter != null) {
    pushWhere.push(whereObj.filter);
  }

  let include = [
    {
      model: Pegawai,
      include: [
        {
          model: Client,
          required: true,
        },
      ],
    },
    {
      model: Jadwal,
    },
    {
      model: JenisAbsensi,
    },
  ];

  varPegawaiJadwal = await PegawaiJadwal.findAll({
    where: pushWhere,
    include: include,
    order: [sortBy],
    limit: limit,
    offset: offset,
  });

  if (limit == null && offset == null) {
    varPegawaiJadwal = await PegawaiJadwal.findAll({
      where: pushWhere,
      include: include,
      order: [sortBy],
    });
  }

  return varPegawaiJadwal;
};

const getPegawaiIdJadwal = async (
  limit = null,
  offset = null,
  search = null,
  pegawai_id = null
) => {
  let whereObject = {};
  whereObject.queryLike = null;
  whereObject.filter = null;

  let varPegawaiJadwal = "";
  let include = [
    {
      model: Pegawai,
      include: [
        {
          model: Client,
          required: true,
        },
      ],
    },
    {
      model: Jadwal,
    },
    {
      model: JenisAbsensi,
    },
  ];

  if (search != null && search != "") {
    whereObject.queryLike = {
      id_pegawai: pegawai_id,
      [Op.or]: {
        "$pegawai.nama_lengkap$": {
          [Op.iLike]: "%" + search + "%",
        },
        "$jadwal.jenis$": {
          [Op.iLike]: "%" + search + "%",
        },
        "$jenis_absensi.jenis_absensi$": {
          [Op.iLike]: "%" + search + "%",
        },
      },
    };
  }

  // execute
  let pushWhere = [];
  if (pegawai_id != null) {
    whereObject.filter = {
      id_pegawai: pegawai_id,
    };
  }

  if (whereObject.filter != null) {
    pushWhere.push(whereObject.filter);
  }

  if (search != null) {
    pushWhere.push(whereObject.queryLike);
  }

  varPegawaiJadwal = await PegawaiJadwal.findAll({
    where: pushWhere,
    include: include,
    order: [["tanggal", "desc"]],
    limit: limit,
    offset: offset,
  });

  return varPegawaiJadwal;
};

const getPegawaiByIdJadwal = async (
  id_pegawai = null,
  tanggalSekarang = null
) => {
  let include = [
    {
      model: Pegawai,
      include: [
        {
          model: Client,
          required: true,
        },
      ],
    },
    {
      model: Jadwal,
    },
    {
      model: JenisAbsensi,
    },
  ];

  let whereObject = {};

  if (id_pegawai != null) {
    whereObject = {
      ...whereObject,
      "$pegawai.id_pegawai$": id_pegawai,
    };
  }
  if (tanggalSekarang != null) {
    whereObject = {
      ...whereObject,
      tanggal: tanggalSekarang,
    };
  }

  let varPegawaiJadwal = "";
  varPegawaiJadwal = await PegawaiJadwal.findOne({
    where: whereObject,
    include: include,
    order: [["id_pegawai_jadwal", "asc"]],
  });

  return varPegawaiJadwal;
};

const getPegawaiRangeTanggal = async (
  id_pegawai = null,
  fromTanggal = null,
  toTanggal = null,
  limit = null,
  offset = null
) => {
  let include = [
    {
      model: Pegawai,
      include: [
        {
          model: Client,
          required: true,
        },
      ],
    },
    {
      model: Jadwal,
    },
    {
      model: JenisAbsensi,
    },
  ];

  let whereObj = {};
  whereObj.filter = null;

  // execute
  let varPegawaiJadwal = null;
  let pushWhere = [];
  let wherePegawai = {};
  if (id_pegawai != null) {
    wherePegawai = {
      ...wherePegawai,
      "$pegawai.id_pegawai$": id_pegawai,
    };
  }

  if (fromTanggal != null && toTanggal != null) {
    wherePegawai = {
      ...wherePegawai,
      tanggal: {
        [Op.between]: [fromTanggal, toTanggal],
      },
    };
  }

  if (wherePegawai != null) {
    pushWhere.push(wherePegawai);
  }

  varPegawaiJadwal = await PegawaiJadwal.findAll({
    where: pushWhere,
    include: include,
    order: [["tanggal", "desc"]],
    limit: limit,
    offset: offset,
  });
  return varPegawaiJadwal;
};

const insertBatch = async (data, transaction = {}) => {
  let pegawaiKomponenGaji = await PegawaiJadwal.bulkCreate(data, transaction);
  return pegawaiKomponenGaji;
};

const getIdPegawaiJadwal = async (id_pegawai_jadwal) => {
  let include = [
    {
      model: Pegawai,
      include: [
        {
          model: Client,
          required: true,
        },
      ],
    },
    {
      model: Jadwal,
    },
    {
      model: JenisAbsensi,
    },
  ];
  let result = await PegawaiJadwal.findAll({
    where: {
      id_pegawai_jadwal: id_pegawai_jadwal,
    },
    include: include,
  });

  return result;
};

module.exports = {
  getPegawaiJadwal,
  getPegawaiByIdJadwal,
  PegawaiJadwal,
  getPegawaiRangeTanggal,
  insertBatch,
  getPegawaiIdJadwal,
  getIdPegawaiJadwal,
};
