const {
  Pegawai,
  Jadwal,
  JenisAbsensi,
  PegawaiJadwal,
  Client,
  Cabang,
  PegawaiUnit,
} = require("../model/index");
const { Op } = require("sequelize");
const moment = require("moment");
const sequelize = require("../config/db");

const getLaporanPegawaiJadwal = async (
  limit = null,
  offset = null,
  search = null,
  filter = {},
  id_client = null
) => {
  let whereObj = {};
  whereObj.queryLike = null;
  whereObj.filter = null;

  // filter
  if (filter.id_pegawai != "" && filter.id_pegawai != null) {
    whereObj.filter = {
      ...whereObj.filter,
      id_pegawai: filter.id_pegawai,
    };
  }
  if (filter.tahun != "" && filter.tahun != null) {
    whereObj.filter = {
      ...whereObj.filter,
      [Op.and]: sequelize.fn('EXTRACT(YEAR from "tanggal") =', filter.tahun),
    };
  }
  if (filter.bulan != "" && filter.bulan != null) {
    whereObj.filter = {
      ...whereObj.filter,
      [Op.and]: sequelize.fn('EXTRACT(MONTH from "tanggal") =', filter.bulan),
    };
  }

  if (filter.tanggal != "" && filter.tanggal != null) {
    whereObj.filter = {
      ...whereObj.filter,
      tanggal: moment(filter.tanggal, "DD-MM-YYYY").format("YYYY-MM-DD"),
    };
  }

  if (filter.nama_lengkap != "" && filter.nama_lengkap != null) {
    whereObj.filter = {
      ...whereObj.filter,
      "$pegawai.nama_lengkap$": {
        [Op.iLike]: "%" + filter.nama_lengkap + "%",
      },
    };
  }
  if (filter.nama_cabang != "" && filter.nama_cabang != null) {
    whereObj.filter = {
      ...whereObj.filter,
      "$pegawai.pegawai_unit.cabang.nama_cabang$": {
        [Op.iLike]: "%" + filter.nama_cabang + "%",
      },
    };
  }
  if (filter.is_hadir != "" && filter.is_hadir != null) {
    whereObj.filter = {
      ...whereObj.filter,
      is_hadir: filter.is_hadir == 0 ? null : 1,
    };
  }
  if (filter.is_kerja != "" && filter.is_kerja != null) {
    whereObj.filter = {
      ...whereObj.filter,
      is_kerja: filter.is_kerja,
    };
  }

  if (whereObj.filter != null) {
    whereObj.filter = {
      [Op.and]: whereObj.filter,
    };
  }

  let varPegawaiJadwal = "";

  // condition
  if (search != null && search != "") {
    whereObj.queryLike = {
      [Op.or]: {
        "$pegawai.nama_lengkap$": {
          [Op.iLike]: "%" + search + "%",
        },
        "$pegawai.pegawai_unit.cabang.nama_cabang$": {
          [Op.iLike]: "%" + search + "%",
        },
      },
    };
  }

  // execute
  let pushWhere = [];
  pushWhere.push({
    [Op.or]: [
      { "$pegawai.id_client$": id_client },
      { "$pegawai.pegawai_unit.is_aktif$": 1 },
    ],
  });

  if (search != null && search != "") {
    pushWhere.push(whereObj.queryLike);
  }

  if (whereObj.filter != null) {
    pushWhere.push(whereObj.filter);
  }

  let include = [
    {
      model: Pegawai,
      include: [
        {
          model: PegawaiUnit,
          include: [
            {
              model: Cabang,
            },
          ],
        },
      ],
    },
    {
      model: Jadwal,
    },
    {
      model: JenisAbsensi,
    },
  ];

  varPegawaiJadwal = await PegawaiJadwal.findAll({
    where: pushWhere,
    include: include,
    order: [["id_pegawai_jadwal", "asc"]],
    limit: limit,
    offset: offset,
  });

  if (limit == null && offset == null) {
    varPegawaiJadwal = await PegawaiJadwal.findAll({
      where: pushWhere,
      include: include,
      order: [["id_pegawai_jadwal", "asc"]],
    });
  }

  return varPegawaiJadwal;
};

module.exports = {
  getLaporanPegawaiJadwal,
  PegawaiJadwal,
};
