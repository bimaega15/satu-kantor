const { Sequelize, DataTypes, Deferrable } = require("sequelize");
const sequilize = require("../config/db");
const { Pegawai, Rainburstment } = require("./index");
const moment = require("moment");

const RainburstmentLists = sequilize.define(
  "rainburstment_lists",
  {
    id_rainburstment_lists: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    id_rainburstment: {
      type: DataTypes.INTEGER,
      references: {
        model: Rainburstment,
        key: "id_rainburstment",
        deferrable: Deferrable.INITIALLY_IMMEDIATE,
      },
    },
    nama_pembelian: {
      type: DataTypes.STRING,
    },
    keterangan_list: {
      type: DataTypes.TEXT,
      allowNull: true,
      defaultValue: null,
    },
    harga: {
      type: DataTypes.FLOAT,
    },
    jumlah: {
      type: DataTypes.INTEGER,
    },
    time_create: {
      type: "TIMESTAMP",
      defaultValue: Sequelize.literal("CURRENT_TIMESTAMP"),
      allowNull: true,
      get() {
        let time_create = this.getDataValue("time_create");
        let varMoment = time_create;
        if (varMoment != null) {
          varMoment = moment(this.getDataValue("time_create")).format(
            "YYYY-MM-DD HH:mm:ss"
          );
        }
        return varMoment;
      },
    },
    time_update: {
      type: "TIMESTAMP",
      defaultValue: Sequelize.literal("CURRENT_TIMESTAMP"),
      allowNull: true,
      get() {
        let time_update = this.getDataValue("time_update");
        let varMoment = time_update;
        if (varMoment != null) {
          varMoment = moment(this.getDataValue("time_update")).format(
            "YYYY-MM-DD HH:mm:ss"
          );
        }
        return varMoment;
      },
    },
    user_create: {
      type: DataTypes.INTEGER,
    },
    user_update: {
      type: DataTypes.INTEGER,
    },
  },
  {
    freezeTableName: true,
    timestamps: false,
    tableName: "rainburstment_lists",
  }
);

module.exports = { RainburstmentLists };
