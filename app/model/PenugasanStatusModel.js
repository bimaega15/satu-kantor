const { Sequelize, DataTypes, Deferrable } = require("sequelize");
const sequilize = require("../config/db");
const moment = require("moment");
const { Penugasan } = require("./index");

const PenugasanStatus = sequilize.define(
  "id_penugasan_status",
  {
    id_penugasan_status: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    id_penugasan: {
      type: DataTypes.INTEGER,
      references: {
        model: Penugasan,
        key: "id_penugasan",
        deferrable: Deferrable.INITIALLY_IMMEDIATE,
      },
    },
    waktu_status: {
      type: "TIMESTAMP",
      defaultValue: Sequelize.literal("CURRENT_TIMESTAMP"),
      allowNull: true,
      get() {
        let waktu_status = this.getDataValue("waktu_status");
        let varMoment = waktu_status;
        if (varMoment != null) {
          varMoment = moment(this.getDataValue("waktu_status")).format(
            "YYYY-MM-DD HH:mm:ss"
          );
        }
        return varMoment;
      },
    },
    status: {
      type: DataTypes.STRING,
    },
    keterangan: {
      type: DataTypes.TEXT,
    },
    user_create: {
      type: DataTypes.INTEGER,
    },
    user_update: {
      type: DataTypes.INTEGER,
    },
    time_create: {
      type: "TIMESTAMP",
      defaultValue: Sequelize.literal("CURRENT_TIMESTAMP"),
      allowNull: true,
      get() {
        let time_create = this.getDataValue("time_create");
        let varMoment = time_create;
        if (varMoment != null) {
          varMoment = moment(this.getDataValue("time_create")).format(
            "YYYY-MM-DD HH:mm:ss"
          );
        }
        return varMoment;
      },
    },
    time_update: {
      type: "TIMESTAMP",
      defaultValue: Sequelize.literal("CURRENT_TIMESTAMP"),
      allowNull: true,
      get() {
        let time_update = this.getDataValue("time_update");
        let varMoment = time_update;
        if (varMoment != null) {
          varMoment = moment(this.getDataValue("time_update")).format(
            "YYYY-MM-DD HH:mm:ss"
          );
        }
        return varMoment;
      },
    },
  },
  {
    freezeTableName: true,
    timestamps: false,
    tableName: "penugasan_status",
  }
);

module.exports = { PenugasanStatus };
