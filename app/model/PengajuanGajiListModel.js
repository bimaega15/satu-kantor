const { Sequelize, DataTypes, Deferrable } = require("sequelize");
const sequelize = require("../config/db");
const moment = require("moment");
const { PengajuanGaji, Pegawai, PegawaiKontrak } = require("../model");

const PengajuanGajiList = sequelize.define(
  "pengajuan_gaji_list",
  {
    id_pengajuan_gaji_list: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    id_pengajuan_gaji: {
      type: DataTypes.INTEGER,
      references: {
        model: PengajuanGaji,
        key: "id_pengajuan_gaji",
        deferrable: Deferrable.INITIALLY_IMMEDIATE,
      },
    },
    id_pegawai: {
      type: DataTypes.INTEGER,
      references: {
        model: Pegawai,
        key: "id_pegawai",
        deferrable: Deferrable.INITIALLY_IMMEDIATE,
      },
    },
    id_pegawai_kontrak: {
      type: DataTypes.INTEGER,
      references: {
        model: PegawaiKontrak,
        key: "id_pegawai_kontrak",
        deferrable: Deferrable.INITIALLY_IMMEDIATE,
      },
    },
    nominal_total: {
      type: DataTypes.DOUBLE,
    },
    nominal_perusahaan: {
      type: DataTypes.DOUBLE,
    },
    is_valid: {
      type: DataTypes.INTEGER,
    },
  },
  {
    timestamps: false,
    tableName: "pengajuan_gaji_list",
  }
);

module.exports = { PengajuanGajiList };
