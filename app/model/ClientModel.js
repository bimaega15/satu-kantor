const { Sequelize, DataTypes, Op } = require("sequelize");
const sequelize = require("../config/db");
const moment = require("moment");

const Client = sequelize.define(
  "client",
  {
    id_client: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    nama_client: {
      type: DataTypes.STRING,
    },
    alamat: {
      type: DataTypes.STRING,
    },
    kontak: {
      type: DataTypes.STRING,
    },
    is_aktif: {
      type: DataTypes.INTEGER,
    },
    user_create: {
      type: DataTypes.INTEGER,
    },
    user_update: {
      type: DataTypes.INTEGER,
    },
    time_create: {
      type: "TIMESTAMP",
      defaultValue: Sequelize.literal("CURRENT_TIMESTAMP"),
      allowNull: true,
      get() {
        let varMoment = this.getDataValue("time_create");
        if (varMoment != null) {
          varMoment = moment(this.getDataValue("time_create")).format(
            "YYYY-MM-DD HH:mm:ss"
          );
        }
        return varMoment;
      },
    },
    time_update: {
      type: "TIMESTAMP",
      defaultValue: Sequelize.literal("CURRENT_TIMESTAMP"),
      allowNull: true,
      get() {
        let varMoment = this.getDataValue("time_update");
        if (varMoment != null) {
          varMoment = moment(this.getDataValue("time_update")).format(
            "YYYY-MM-DD HH:mm:ss"
          );
        }
        return varMoment;
      },
    },
  },
  {
    timestamps: false,
    tableName: "client",
  }
);

module.exports = { Client };
