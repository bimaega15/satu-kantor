const { DataTypes, Sequelize } = require("sequelize");
const sequelize = require("../config/db");
const moment = require("moment");

const Users = sequelize.define(
  "user",
  {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    username: {
      type: DataTypes.STRING,
    },
    auth_key: {
      type: DataTypes.STRING,
    },
    password_hash: {
      type: DataTypes.STRING,
    },
    password_reset_token: {
      type: DataTypes.STRING,
    },
    pin: {
      type: DataTypes.STRING,
    },
    email: {
      type: DataTypes.STRING,
    },
    status: {
      type: DataTypes.INTEGER,
    },
    created_at: {
      type: "TIMESTAMP",
      defaultValue: Sequelize.literal("CURRENT_TIMESTAMP"),
      allowNull: true,
      get() {
        let varMoment = this.getDataValue("created_at");
        if (varMoment != null) {
          varMoment = moment(this.getDataValue("created_at")).format(
            "YYYY-MM-DD HH:mm:ss"
          );
        }
        return varMoment;
      },
    },
    updated_at: {
      type: "TIMESTAMP",
      defaultValue: Sequelize.literal("CURRENT_TIMESTAMP"),
      allowNull: true,
      get() {
        let varMoment = this.getDataValue("updated_at");
        if (varMoment != null) {
          varMoment = moment(this.getDataValue("updated_at")).format(
            "YYYY-MM-DD HH:mm:ss"
          );
        }
        return varMoment;
      },
    },
  },
  {
    timestamps: false,
    tableName: "user",
  }
);

module.exports = { Users };
