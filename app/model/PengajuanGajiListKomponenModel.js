const { DataTypes, Deferrable } = require("sequelize");
const sequelize = require("../config/db");
const { PegawaiKomponenGaji, KomponenGaji } = require(".");

const PengajuanGajiListKomponen = sequelize.define(
  "pengajuan_gaji_list_komponen",
  {
    id_pengajuan_gaji_list: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    id_pegawai_komponen_gaji: {
      type: DataTypes.INTEGER,
      references: {
        model: PegawaiKomponenGaji,
        key: "id_pegawai_komponen_gaji",
        deferrable: Deferrable.INITIALLY_IMMEDIATE,
      },
    },
    id_komponen_gaji: {
      type: DataTypes.INTEGER,
      references: {
        model: KomponenGaji,
        key: "id_pegawai",
        deferrable: Deferrable.INITIALLY_IMMEDIATE,
      },
    },
    id_piutang: {
      type: DataTypes.INTEGER,
      references: {
        model: KomponenGaji,
        key: "id_piutang",
        deferrable: Deferrable.INITIALLY_IMMEDIATE,
      },
    },
    nominal: {
      type: DataTypes.DOUBLE,
    },
    jenis_komponen: {
      type: DataTypes.STRING,
    },
    komponen: {
      type: DataTypes.STRING,
    },
  },
  {
    timestamps: false,
    tableName: "pengajuan_gaji_list_komponen",
  }
);

module.exports = { PengajuanGajiListKomponen };
