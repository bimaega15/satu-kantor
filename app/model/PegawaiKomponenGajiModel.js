const { DataTypes, Deferrable } = require("sequelize");
const sequelize = require("../config/db");
const { PegawaiKontrak, KomponenGaji } = require("./index");

const PegawaiKomponenGaji = sequelize.define(
  "pegawai_komponen_gaji",
  {
    id_pegawai_komponen_gaji: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    id_pegawai_kontrak: {
      type: DataTypes.INTEGER,
      references: {
        model: PegawaiKontrak,
        key: "id_pegawai_kontrak",
        deferrable: Deferrable.INITIALLY_IMMEDIATE,
      },
    },
    id_komponen_gaji: {
      type: DataTypes.INTEGER,
      references: {
        model: KomponenGaji,
        key: "id_komponen_gaji",
        deferrable: Deferrable.INITIALLY_IMMEDIATE,
      },
    },
    nominal: {
      type: DataTypes.DOUBLE,
    },
  },
  {
    timestamps: false,
    tableName: "pegawai_komponen_gaji",
  }
);

module.exports = { PegawaiKomponenGaji };
