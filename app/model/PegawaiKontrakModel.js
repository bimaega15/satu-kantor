const { Sequelize, DataTypes, Op, Deferrable } = require("sequelize");
const sequelize = require("../config/db");
const { Pegawai, Unit, Cabang, Jabatan } = require("./index");
const moment = require("moment");

const PegawaiKontrak = sequelize.define(
  "pegawai_kontrak",
  {
    id_pegawai_kontrak: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    id_pegawai: {
      type: DataTypes.INTEGER,
      references: {
        model: Pegawai,
        key: "id_pegawai",
        deferrable: Deferrable.INITIALLY_IMMEDIATE,
      },
    },
    id_cabang: {
      type: DataTypes.INTEGER,
      references: {
        model: Cabang,
        key: "id_cabang",
        deferrable: Deferrable.INITIALLY_IMMEDIATE,
      },
    },
    id_unit: {
      type: DataTypes.INTEGER,
      references: {
        model: Unit,
        key: "id_unit",
        deferrable: Deferrable.INITIALLY_IMMEDIATE,
      },
    },
    id_jabatan: {
      type: DataTypes.INTEGER,
      references: {
        model: Jabatan,
        key: "id_jabatan",
        deferrable: Deferrable.INITIALLY_IMMEDIATE,
      },
    },
    jenis_kontrak: {
      type: DataTypes.STRING,
    },
    tanggal_mulai: {
      type: DataTypes.DATEONLY,
      allowNull: true,
    },
    tanggal_selesai: {
      type: DataTypes.DATEONLY,
      allowNull: true,
    },
    berkas: {
      type: DataTypes.STRING,
    },
    is_aktif: {
      type: DataTypes.INTEGER,
    },
    user_create: {
      type: DataTypes.INTEGER,
    },
    user_update: {
      type: DataTypes.INTEGER,
    },
    time_create: {
      type: "TIMESTAMP",
      defaultValue: Sequelize.literal("CURRENT_TIMESTAMP"),
      allowNull: true,
      get() {
        return moment(this.getDataValue("time_create")).format(
          "YYYY-MM-DD HH:mm:ss"
        );
      },
    },
    time_update: {
      type: "TIMESTAMP",
      defaultValue: Sequelize.literal("CURRENT_TIMESTAMP"),
      allowNull: true,
      get() {
        return moment(this.getDataValue("time_update")).format(
          "YYYY-MM-DD HH:mm:ss"
        );
      },
    },
  },
  {
    timestamps: false,
    tableName: "pegawai_kontrak",
  }
);

module.exports = { PegawaiKontrak };
