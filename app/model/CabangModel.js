const { Sequelize, DataTypes, Deferrable } = require("sequelize");
const sequilize = require("../config/db");
const { Client } = require("./index");
const moment = require("moment");

const Cabang = sequilize.define(
  "cabang",
  {
    id_cabang: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    nama_cabang: {
      type: DataTypes.STRING,
    },
    alamat: {
      type: DataTypes.TEXT,
    },
    kontak: {
      type: DataTypes.STRING,
    },
    is_aktif: {
      type: DataTypes.INTEGER,
    },
    user_create: {
      type: DataTypes.INTEGER,
    },
    user_update: {
      type: DataTypes.INTEGER,
    },
    time_create: {
      type: "TIMESTAMP",
      defaultValue: Sequelize.literal("CURRENT_TIMESTAMP"),
      allowNull: true,
      get() {
        let time_create = this.getDataValue("time_create");
        let varMoment = time_create;
        if (varMoment != null) {
          varMoment = moment(this.getDataValue("time_create")).format(
            "YYYY-MM-DD HH:mm:ss"
          );
        }
        return varMoment;
      },
    },
    time_update: {
      type: "TIMESTAMP",
      defaultValue: Sequelize.literal("CURRENT_TIMESTAMP"),
      allowNull: true,
      get() {
        let time_update = this.getDataValue("time_update");
        let varMoment = time_update;
        if (varMoment != null) {
          varMoment = moment(this.getDataValue("time_update")).format(
            "YYYY-MM-DD HH:mm:ss"
          );
        }
        return varMoment;
      },
    },
    id_client: {
      type: DataTypes.INTEGER,
      references: {
        model: Client,
        key: "id_client",
        deferrable: Deferrable.INITIALLY_IMMEDIATE,
      },
    },
    longitude: {
      type: DataTypes.STRING,
    },
    latitude: {
      type: DataTypes.STRING,
    },
  },
  {
    freezeTableName: true,
    timestamps: false,
    tableName: "cabang",
  }
);

module.exports = { Cabang };
