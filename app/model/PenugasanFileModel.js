const { Sequelize, DataTypes, Deferrable } = require("sequelize");
const sequilize = require("../config/db");
const moment = require("moment");
const { Penugasan } = require("./index");

const PenugasanFile = sequilize.define(
  "id_penugasan_file",
  {
    id_penugasan_file: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    id_penugasan: {
      type: DataTypes.INTEGER,
      references: {
        model: Penugasan,
        key: "id_penugasan",
        deferrable: Deferrable.INITIALLY_IMMEDIATE,
      },
    },
    nama_file: {
      type: DataTypes.STRING,
    },
    deskripsi: {
      type: DataTypes.TEXT,
    },
    user_create: {
      type: DataTypes.INTEGER,
    },
    user_update: {
      type: DataTypes.INTEGER,
    },
    time_create: {
      type: "TIMESTAMP",
      defaultValue: Sequelize.literal("CURRENT_TIMESTAMP"),
      allowNull: true,
      get() {
        let time_create = this.getDataValue("time_create");
        let varMoment = time_create;
        if (varMoment != null) {
          varMoment = moment(this.getDataValue("time_create")).format(
            "YYYY-MM-DD HH:mm:ss"
          );
        }
        return varMoment;
      },
    },
    time_update: {
      type: "TIMESTAMP",
      defaultValue: Sequelize.literal("CURRENT_TIMESTAMP"),
      allowNull: true,
      get() {
        let time_update = this.getDataValue("time_update");
        let varMoment = time_update;
        if (varMoment != null) {
          varMoment = moment(this.getDataValue("time_update")).format(
            "YYYY-MM-DD HH:mm:ss"
          );
        }
        return varMoment;
      },
    },
  },
  {
    freezeTableName: true,
    timestamps: false,
    tableName: "penugasan_file",
  }
);

module.exports = { PenugasanFile };
