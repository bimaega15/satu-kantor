const { DataTypes, Deferrable } = require("sequelize");
const sequelize = require("../config/db");
const { Pegawai } = require("./index");

const Izin = sequelize.define(
  "izin",
  {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    str_izin: {
      type: DataTypes.STRING,
      allowNull: true,
    },
    jenis_izin: {
      type: DataTypes.STRING,
      allowNull: true,
    },
    nomor_izin: {
      type: DataTypes.STRING,
      allowNull: true,
    },
    tanggal_kadaluarsa_izin: {
      type: DataTypes.DATEONLY,
      allowNull: true,
    },
    pegawai_id: {
      type: DataTypes.INTEGER,
      references: {
        model: Pegawai,
        key: "id_pegawai",
        deferrable: Deferrable.INITIALLY_IMMEDIATE,
      },
    },
  },
  {
    timestamps: false,
    tableName: "izin",
  }
);

module.exports = { Izin };
