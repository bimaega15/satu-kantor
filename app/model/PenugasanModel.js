const { Sequelize, DataTypes, Deferrable } = require("sequelize");
const sequilize = require("../config/db");
const { Client, Cabang, Pegawai, LokasiTugas, Tugas } = require("./index");
const moment = require("moment");

const Penugasan = sequilize.define(
  "id_penugasan",
  {
    id_penugasan: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    id_client: {
      type: DataTypes.INTEGER,
      references: {
        model: Client,
        key: "id_client",
        deferrable: Deferrable.INITIALLY_IMMEDIATE,
      },
    },
    id_cabang: {
      type: DataTypes.INTEGER,
      references: {
        model: Cabang,
        key: "id_cabang",
        deferrable: Deferrable.INITIALLY_IMMEDIATE,
      },
    },
    id_pegawai: {
      type: DataTypes.INTEGER,
      references: {
        model: Pegawai,
        key: "id_pegawai",
        deferrable: Deferrable.INITIALLY_IMMEDIATE,
      },
    },
    tanggal_penugasan: {
      type: DataTypes.DATEONLY,
    },
    jenis_penugasan: {
      type: DataTypes.STRING,
    },
    id_tugas: {
      type: DataTypes.INTEGER,
      references: {
        model: Tugas,
        key: "id_tugas",
        deferrable: Deferrable.INITIALLY_IMMEDIATE,
      },
    },
    id_lokasi_tugas: {
      type: DataTypes.INTEGER,
      references: {
        model: LokasiTugas,
        key: "id_lokasi_tugas",
        deferrable: Deferrable.INITIALLY_IMMEDIATE,
      },
    },
    nama_tugas: {
      type: DataTypes.STRING,
    },
    waktu_mulai: {
      type: DataTypes.TIME,
    },
    waktu_selesai: {
      type: DataTypes.TIME,
    },
    is_aktif: {
      type: DataTypes.BOOLEAN,
    },
    user_create: {
      type: DataTypes.INTEGER,
    },
    user_update: {
      type: DataTypes.INTEGER,
    },
    time_create: {
      type: "TIMESTAMP",
      defaultValue: Sequelize.literal("CURRENT_TIMESTAMP"),
      allowNull: true,
      get() {
        let time_create = this.getDataValue("time_create");
        let varMoment = time_create;
        if (varMoment != null) {
          varMoment = moment(this.getDataValue("time_create")).format(
            "YYYY-MM-DD HH:mm:ss"
          );
        }
        return varMoment;
      },
    },
    time_update: {
      type: "TIMESTAMP",
      defaultValue: Sequelize.literal("CURRENT_TIMESTAMP"),
      allowNull: true,
      get() {
        let time_update = this.getDataValue("time_update");
        let varMoment = time_update;
        if (varMoment != null) {
          varMoment = moment(this.getDataValue("time_update")).format(
            "YYYY-MM-DD HH:mm:ss"
          );
        }
        return varMoment;
      },
    },
  },
  {
    freezeTableName: true,
    timestamps: false,
    tableName: "penugasan",
  }
);

module.exports = { Penugasan };
