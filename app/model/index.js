const { Cabang } = require("./CabangModel");
const { Client } = require("./ClientModel");
const { Jabatan } = require("./JabatanModel");
const { Jadwal } = require("./JadwalModel");
const { JenisAbsensi } = require("./JenisAbsensiModel");
const { JenisCuti } = require("./JenisCutiModel");
const { JenisLembur } = require("./jenisLemburModel");
const { KomponenGaji } = require("./KomponenGajiModel");
const { Pegawai } = require("./PegawaiModel");
const { Unit } = require("./UnitModel");
const { UserMapping } = require("./UsersMappingModel");
const { Users } = require("./UsersModel");
const { PegawaiJadwal } = require("./PegawaiJadwalModel");
const { Langganan } = require("./LanggananModel");
const { PegawaiJabatan } = require("./PegawaiJabatanModel");
const { PegawaiUnit } = require("./PegawaiUnitModel");
const { PegawaiKontrak } = require("./PegawaiKontrakModel");
const { PegawaiCatatan } = require("./PegawaiCatatanModel");
const { PegawaiSaldoCuti } = require("./PegawaiSaldoCutiModel");
const { Rekening } = require("./RekeningModel");
const { Izin } = require("./IzinModel");
const { PegawaiKomponenGaji } = require("./PegawaiKomponenGajiModel");
const { PengajuanGaji } = require("./PengajuanGajiModel");
const { PengajuanGajiList } = require("./PengajuanGajiListModel");
const {
  PengajuanGajiListKomponen,
} = require("./PengajuanGajiListKomponenModel");
const { PengajuanCuti } = require("./PengajuanCutiModel");
const { PengajuanLembur } = require("./PengajuanLemburModel");
const { PengajuanLemburList } = require("./PengajuanLemburListModel");
const { Rainburstment } = require("./RainburstmentModel");
const { RainburstmentLists } = require("./RainburstmentListsModel");
const { Tugas } = require("./TugasModel");
const { LokasiTugas } = require("./LokasiTugasModel");
const { Penugasan } = require("./PenugasanModel");
const { PenugasanFile } = require("./PenugasanFileModel");
const { PenugasanStatus } = require("./PenugasanStatusModel");

Cabang.hasMany(Unit, {
  foreignKey: "id_cabang",
});
Unit.belongsTo(Cabang, {
  foreignKey: "id_cabang",
});

Unit.hasMany(Jabatan, {
  foreignKey: "id_unit",
});
Jabatan.belongsTo(Unit, {
  foreignKey: "id_unit",
});

Client.hasMany(Langganan, {
  foreignKey: "id_client",
});
Langganan.belongsTo(Client, {
  foreignKey: "id_client",
});

Pegawai.hasMany(PegawaiJadwal, {
  foreignKey: "id_pegawai",
});
PegawaiJadwal.belongsTo(Pegawai, {
  foreignKey: "id_pegawai",
});

Jadwal.hasMany(PegawaiJadwal, {
  foreignKey: "id_jadwal",
});
PegawaiJadwal.belongsTo(Jadwal, {
  foreignKey: "id_jadwal",
});

JenisAbsensi.hasMany(PegawaiJadwal, {
  foreignKey: "id_jenis_absensi",
});
PegawaiJadwal.belongsTo(JenisAbsensi, {
  foreignKey: "id_jenis_absensi",
});

Pegawai.hasOne(UserMapping, {
  foreignKey: "id_mapping",
});
UserMapping.belongsTo(Pegawai, {
  foreignKey: "id_mapping",
});

Client.hasOne(UserMapping, {
  foreignKey: "id_mapping",
});
UserMapping.belongsTo(Client, {
  foreignKey: "id_mapping",
});

Users.hasOne(UserMapping, {
  foreignKey: "id_user",
});
UserMapping.belongsTo(Users, {
  foreignKey: "id_user",
});

Pegawai.hasOne(PegawaiJabatan, {
  foreignKey: "id_pegawai",
});
PegawaiJabatan.belongsTo(Pegawai, {
  foreignKey: "id_pegawai",
});

Jabatan.hasMany(PegawaiJabatan, {
  foreignKey: "id_jabatan",
});
PegawaiJabatan.belongsTo(Jabatan, {
  foreignKey: "id_jabatan",
});

Cabang.hasMany(PegawaiJabatan, {
  foreignKey: "id_cabang",
});
PegawaiJabatan.belongsTo(Cabang, {
  foreignKey: "id_cabang",
});

Pegawai.hasOne(PegawaiUnit, {
  foreignKey: "id_pegawai",
});
PegawaiUnit.belongsTo(Pegawai, {
  foreignKey: "id_pegawai",
});

Unit.hasMany(PegawaiUnit, {
  foreignKey: "id_unit",
});
PegawaiUnit.belongsTo(Unit, {
  foreignKey: "id_unit",
});

Cabang.hasMany(PegawaiUnit, {
  foreignKey: "id_cabang",
});
PegawaiUnit.belongsTo(Cabang, {
  foreignKey: "id_cabang",
});

Pegawai.hasOne(PegawaiKontrak, {
  foreignKey: "id_pegawai",
});
PegawaiKontrak.belongsTo(Pegawai, {
  foreignKey: "id_pegawai",
});

Unit.hasMany(PegawaiKontrak, {
  foreignKey: "id_unit",
});
PegawaiKontrak.belongsTo(Unit, {
  foreignKey: "id_unit",
});

Cabang.hasMany(PegawaiKontrak, {
  foreignKey: "id_cabang",
});
PegawaiKontrak.belongsTo(Cabang, {
  foreignKey: "id_cabang",
});
Cabang.hasMany(PengajuanGaji, {
  foreignKey: "id_cabang",
});
PengajuanGaji.belongsTo(Cabang, {
  foreignKey: "id_cabang",
});

Jabatan.hasMany(PegawaiKontrak, {
  foreignKey: "id_jabatan",
});
PegawaiKontrak.belongsTo(Jabatan, {
  foreignKey: "id_jabatan",
});

Pegawai.hasMany(PegawaiCatatan, {
  foreignKey: "id_pegawai",
});
PegawaiCatatan.belongsTo(Pegawai, {
  foreignKey: "id_pegawai",
});

JenisCuti.hasMany(PegawaiSaldoCuti, {
  foreignKey: "id_jenis_cuti",
});
PegawaiSaldoCuti.belongsTo(JenisCuti, {
  foreignKey: "id_jenis_cuti",
});
Pegawai.hasMany(PegawaiSaldoCuti, {
  foreignKey: "id_pegawai",
});
PegawaiSaldoCuti.belongsTo(Pegawai, {
  foreignKey: "id_pegawai",
});

Pegawai.hasOne(Rekening, {
  foreignKey: "pegawai_id",
});
Rekening.belongsTo(Pegawai, {
  foreignKey: "pegawai_id",
});

Pegawai.hasOne(Izin, {
  foreignKey: "pegawai_id",
});
Izin.belongsTo(Pegawai, {
  foreignKey: "pegawai_id",
});
PegawaiKontrak.hasMany(PegawaiKomponenGaji, {
  foreignKey: "id_pegawai_kontrak",
});
PegawaiKomponenGaji.belongsTo(PegawaiKontrak, {
  foreignKey: "id_pegawai_kontrak",
});
KomponenGaji.hasMany(PegawaiKomponenGaji, {
  foreignKey: "id_komponen_gaji",
});
PegawaiKomponenGaji.belongsTo(KomponenGaji, {
  foreignKey: "id_komponen_gaji",
});
PengajuanGaji.hasMany(PengajuanGajiList, {
  foreignKey: "id_pengajuan_gaji",
});
PengajuanGajiList.belongsTo(PengajuanGaji, {
  foreignKey: "id_pengajuan_gaji",
});
Pegawai.hasMany(PengajuanGajiList, {
  foreignKey: "id_pegawai",
});
PengajuanGajiList.belongsTo(Pegawai, {
  foreignKey: "id_pegawai",
});
PegawaiKontrak.hasMany(PengajuanGajiList, {
  foreignKey: "id_pegawai_kontrak",
});
PengajuanGajiList.belongsTo(PegawaiKontrak, {
  foreignKey: "id_pegawai_kontrak",
});
PengajuanGajiList.hasOne(PengajuanGajiListKomponen, {
  foreignKey: "id_pengajuan_gaji_list",
});
PengajuanGajiListKomponen.belongsTo(PengajuanGajiList, {
  foreignKey: "id_pengajuan_gaji_list",
});
PegawaiKomponenGaji.hasOne(PengajuanGajiListKomponen, {
  foreignKey: "id_pegawai_komponen_gaji",
});
PengajuanGajiListKomponen.belongsTo(PegawaiKomponenGaji, {
  foreignKey: "id_pegawai_komponen_gaji",
});
KomponenGaji.hasMany(PengajuanGajiListKomponen, {
  foreignKey: "id_komponen_gaji",
});
PengajuanGajiListKomponen.belongsTo(KomponenGaji, {
  foreignKey: "id_komponen_gaji",
});
Pegawai.hasMany(PengajuanCuti, {
  foreignKey: "id_pegawai",
});
PengajuanCuti.belongsTo(Pegawai, {
  foreignKey: "id_pegawai",
});
JenisCuti.hasMany(PengajuanCuti, {
  foreignKey: "id_jenis_cuti",
});
PengajuanCuti.belongsTo(JenisCuti, {
  foreignKey: "id_jenis_cuti",
});
Client.hasMany(Pegawai, {
  foreignKey: "id_client",
});
Pegawai.belongsTo(Client, {
  foreignKey: "id_client",
});
Client.hasMany(Cabang, {
  foreignKey: "id_client",
});
Cabang.hasOne(Client, {
  foreignKey: "id_client",
});

Client.hasMany(PengajuanLembur, {
  foreignKey: "id_client",
});
PengajuanLembur.belongsTo(Client, {
  foreignKey: "id_client",
});
Cabang.hasMany(PengajuanLembur, {
  foreignKey: "id_cabang",
});
PengajuanLembur.belongsTo(Cabang, {
  foreignKey: "id_cabang",
});
JenisLembur.hasMany(PengajuanLembur, {
  foreignKey: "id_jenis_lembur",
});
PengajuanLembur.belongsTo(JenisLembur, {
  foreignKey: "id_jenis_lembur",
});
PengajuanLembur.hasMany(PengajuanLemburList, {
  foreignKey: "id_pengajuan_lembur",
});
PengajuanLemburList.belongsTo(PengajuanLembur, {
  foreignKey: "id_pengajuan_lembur",
});
Pegawai.hasMany(PengajuanLemburList, {
  foreignKey: "id_pegawai",
});
PengajuanLemburList.belongsTo(Pegawai, {
  foreignKey: "id_pegawai",
});

Pegawai.hasMany(Rainburstment, {
  foreignKey: "id_pegawai",
});
Rainburstment.belongsTo(Pegawai, {
  foreignKey: "id_pegawai",
});

Rainburstment.hasMany(RainburstmentLists, {
  foreignKey: "id_rainburstment",
});
RainburstmentLists.belongsTo(Rainburstment, {
  foreignKey: "id_rainburstment",
});

Client.hasMany(Tugas, {
  foreignKey: "id_client",
});
Tugas.belongsTo(Client, {
  foreignKey: "id_client",
});
Cabang.hasMany(Tugas, {
  foreignKey: "id_cabang",
});
Tugas.belongsTo(Cabang, {
  foreignKey: "id_cabang",
});

Client.hasMany(LokasiTugas, {
  foreignKey: "id_client",
});
LokasiTugas.belongsTo(Client, {
  foreignKey: "id_client",
});
Cabang.hasMany(LokasiTugas, {
  foreignKey: "id_cabang",
});
LokasiTugas.belongsTo(Cabang, {
  foreignKey: "id_cabang",
});

Client.hasMany(Penugasan, {
  foreignKey: "id_client",
});
Penugasan.belongsTo(Client, {
  foreignKey: "id_client",
});
Cabang.hasMany(Penugasan, {
  foreignKey: "id_cabang",
});
Penugasan.belongsTo(Cabang, {
  foreignKey: "id_cabang",
});
Pegawai.hasMany(Penugasan, {
  foreignKey: "id_pegawai",
});
Penugasan.belongsTo(Pegawai, {
  foreignKey: "id_pegawai",
});
Tugas.hasMany(Penugasan, {
  foreignKey: "id_tugas",
  as: "tugas",
});
Penugasan.belongsTo(Tugas, {
  foreignKey: "id_tugas",
  as: "tugas",
});
LokasiTugas.hasMany(Penugasan, {
  foreignKey: "id_lokasi_tugas",
  as: "lokasi_tugas",
});
Penugasan.belongsTo(LokasiTugas, {
  foreignKey: "id_lokasi_tugas",
  as: "lokasi_tugas",
});
Penugasan.hasMany(PenugasanFile, {
  foreignKey: "id_penugasan",
  as: "penugasan_file",
});
PenugasanFile.belongsTo(Penugasan, {
  foreignKey: "id_penugasan",
  as: "penugasan_file",
});
Penugasan.hasMany(PenugasanStatus, {
  foreignKey: "id_penugasan",
  as: "penugasan_status",
});
PenugasanStatus.belongsTo(Penugasan, {
  foreignKey: "id_penugasan",
  as: "penugasan_status",
});

module.exports = {
  Cabang,
  Client,
  Jabatan,
  Jadwal,
  JenisAbsensi,
  JenisCuti,
  JenisLembur,
  KomponenGaji,
  Rekening,
  Izin,
  Pegawai,
  Unit,
  UserMapping,
  Users,
  PegawaiJadwal,
  Langganan,
  PegawaiJabatan,
  PegawaiUnit,
  PegawaiKontrak,
  PegawaiCatatan,
  PegawaiSaldoCuti,
  PegawaiKomponenGaji,
  PengajuanGaji,
  PengajuanGajiList,
  PengajuanGajiListKomponen,
  PengajuanCuti,
  PengajuanLembur,
  PengajuanLemburList,
  Rainburstment,
  RainburstmentLists,
  Tugas,
  LokasiTugas,
  Penugasan,
  PenugasanFile,
  PenugasanStatus,
};
