const { validationResult } = require("express-validator");
const moment = require("moment");
const sequelize = require("../../config/db");
const {
  userMappingHelper,
  pegawaiJadwalHelper,
  jenisLemburHelper,
  pagination,
  jadwalHelper,
} = require("../../helper/index");
const { getPegawaiIdJadwal } = require("../../helper/pegawaiJadwalHelper");

const index = async (req, res) => {
  // const t = await sequelize.transaction();

  try {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({
        status: 400,
        message: "Invalid form validation",
        result: errors.array(),
      });
    }

    const { radius } = req.body;
    if (radius) {
      let user = req.user.data;
      let getPegawai = await Pegawai.findOne({
        where: {
          id_client: user.id_mapping,
        },
      });
      req.query.client_id = getPegawai.id_client;
      const getMapping = await userMappingHelper.getJoinDataMapping(
        user.jenis_mapping,
        null,
        user.id_user_mapping
      );
      const tanggal_sekarang = moment().format("YYYY-MM-DD");
      let jadwal = await pegawaiJadwalHelper.getPegawaiByIdJadwal(
        getMapping.pegawai.id_pegawai,
        tanggal_sekarang
      );

      let id_pegawai_jadwal = jadwal.id_pegawai_jadwal;
      jadwal = jadwal.jadwal;
      let jadwalWaktuMasuk =
        moment().format("YYYY-MM-DD") + " " + jadwal.waktu_masuk;
      jadwalWaktuMasuk = moment(jadwalWaktuMasuk)
        .add(10, "minutes")
        .format("YYYY-MM-DD HH:mm:ss");
      let jadwal_masuk = moment.duration(moment(jadwalWaktuMasuk)).asSeconds();

      let is_hadir = 1;
      let waktu_sekarang = moment().format("YYYY-MM-DD HH:mm:ss");
      let waktu_masuk = moment.duration(moment(waktu_sekarang)).asSeconds();
      let duration = waktu_masuk - jadwal_masuk;

      let is_terlambat = "";
      let total_jam_terlambat = null;
      if (duration <= 0) {
        is_terlambat = 0;
      } else {
        is_terlambat = 1;
        total_jam_terlambat = moment
          .utc(moment.duration(duration, "seconds").asMilliseconds())
          .format("HH:mm:ss");
      }
      waktu_masuk = moment().format("YYYY-MM-DD HH:mm:ss");
      let output = {};
      output.is_hadir = is_hadir;
      output.waktu_masuk = waktu_masuk;
      output.is_terlambat = is_terlambat;
      output.total_jam_terlambat = total_jam_terlambat;

      let checkIn = await pegawaiJadwalHelper.PegawaiJadwal.update(output, {
        where: {
          id_pegawai_jadwal: id_pegawai_jadwal,
        },
        // transaction: t,
      });
      // await t.commit();

      if (checkIn) {
        let result = await pegawaiJadwalHelper.getPegawaiByIdJadwal(
          getMapping.pegawai.id_pegawai,
          tanggal_sekarang
        );
        return res.status(200).json({
          status: 200,
          message: "Berhasil check in",
          result: result,
        });
      } else {
        return res.status(400).json({
          status: 400,
          message: "Gagal check in",
        });
      }
    } else {
      return res.status(400).json({
        status: 400,
        message: "Anda berada diluar zona absen",
      });
    }
  } catch (error) {
    // await t.rollback();
    return res.status(400).json({
      status: 400,
      message: "Terjadi kesalahan data",
      result: error.message,
    });
  }
};

const checkOut = async (req, res) => {
  // const t = await sequelize.transaction();

  try {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({
        status: 400,
        message: "Invalid form validation",
        result: errors.array(),
      });
    }

    const { radius } = req.body;
    if (radius) {
      // ambil user data
      let user = req.user.data;
      let getPegawai = await Pegawai.findOne({
        where: {
          id_client: user.id_mapping,
        },
      });
      req.query.client_id = getPegawai.id_client;
      const getMapping = await userMappingHelper.getJoinDataMapping(
        user.jenis_mapping,
        null,
        user.id_user_mapping
      );
      // jenis lembur
      const jenis_lembur = await jenisLemburHelper.getJenisLemburByClientId(
        getMapping.pegawai.id_client
      );

      // pegawai jadwal
      let tanggal_sekarang = moment().format("YYYY-MM-DD");
      let jadwal = await pegawaiJadwalHelper.getPegawaiByIdJadwal(
        getMapping.pegawai.id_pegawai,
        tanggal_sekarang
      );

      if (jadwal == null) {
        tanggal_sekarang = moment().subtract(1, "days").format("YYYY-MM-DD");
        jadwal = await pegawaiJadwalHelper.getPegawaiByIdJadwal(
          getMapping.pegawai.id_pegawai,
          tanggal_sekarang
        );
        if (jadwal == null) {
          return res.status(400).json({
            status: 400,
            message: "Terjadi kesalahan data checkout",
          });
        }
      }

      // waktu masuk pegawai
      let waktu_masuk = jadwal.waktu_masuk;

      // jam keluar
      let id_pegawai_jadwal = jadwal.id_pegawai_jadwal;
      jadwal = jadwal.jadwal;
      let waktu_jadwal_keluar = tanggal_sekarang + " " + jadwal.waktu_keluar;
      let jadwal_keluar = moment
        .duration(moment(waktu_jadwal_keluar))
        .asSeconds();

      // waktu saat ini
      let waktu_keluar_now = moment().format("YYYY-MM-DD HH:mm:ss");
      let waktu_keluar = moment.duration(moment(waktu_keluar_now)).asSeconds();

      // durasi waktu keluar dengan waktu saat ini
      let duration = jadwal_keluar - waktu_keluar;
      let is_pulang_cepat = null;
      let total_jam_pulang_cepat = null;
      let total_jam_kerja_real = null;
      let total_jam_lembur = null;
      let is_lembur = null;

      // check apakah pulang cepat
      waktu_masuk = moment.duration(moment(waktu_masuk)).asSeconds();
      if (duration <= 0) {
        is_pulang_cepat = 0;
      } else {
        is_pulang_cepat = 1;
        total_jam_pulang_cepat = moment
          .utc(moment.duration(duration, "seconds").asMilliseconds())
          .format("HH:mm:ss");
      }

      // durasi total kerja real
      let waktu_pulang = waktu_keluar_now;
      let db_waktu_pulang = waktu_pulang;

      waktu_pulang = moment.duration(moment(waktu_pulang)).asSeconds();
      duration = waktu_pulang - waktu_masuk;
      total_jam_kerja_real = moment
        .utc(moment.duration(duration, "seconds").asMilliseconds())
        .format("HH:mm:ss");

      let output = {};
      output.waktu_pulang = db_waktu_pulang;
      output.is_pulang_cepat = is_pulang_cepat;
      output.is_lembur = is_lembur;
      output.total_jam_kerja_real = total_jam_kerja_real;
      output.total_jam_pulang_cepat = total_jam_pulang_cepat;
      output.total_jam_lembur = total_jam_lembur;

      let checkOut = await pegawaiJadwalHelper.PegawaiJadwal.update(output, {
        where: {
          id_pegawai_jadwal: id_pegawai_jadwal,
        },
        // transaction: t,
      });

      // await t.commit();
      if (checkOut) {
        let result = await pegawaiJadwalHelper.getPegawaiByIdJadwal(
          getMapping.pegawai.id_pegawai,
          tanggal_sekarang
        );
        return res.status(200).json({
          status: 200,
          message: "Berhasil check out",
          result: result,
        });
      } else {
        return res.status(400).json({
          status: 400,
          message: "Gagal check out",
        });
      }
    } else {
      return res.status(400).json({
        status: 400,
        message: "Anda berada diluar zona absen",
      });
    }
  } catch (error) {
    // await t.rollback();
    return res.status(400).json({
      status: 400,
      message: "Terjadi kesalahan data",
      result: error.message,
    });
  }
};

const history = async (req, res) => {
  try {
    // ambil user data
    const {
      semingguTerakhir,
      sebulanTerakhir,
      setahunTerakhir,
      dariTanggal,
      sampaiTanggal,
    } = req.query;

    let user = req.user.data;
    let getPegawai = await Pegawai.findOne({
      where: {
        id_client: user.id_mapping,
      },
    });
    req.query.client_id = getPegawai.id_client;

    let passFromTanggal = null;
    let passToTanggal = null;

    let currentTanggal = moment().format("YYYY-MM-DD");
    passToTanggal = currentTanggal;
    let tanggalBanding = null;
    if (semingguTerakhir) {
      tanggalBanding = moment().subtract(1, "weeks").format("YYYY-MM-DD");
      passFromTanggal = tanggalBanding;
    }
    if (sebulanTerakhir) {
      tanggalBanding = moment().subtract(1, "months").format("YYYY-MM-DD");
      passFromTanggal = tanggalBanding;
    }
    if (setahunTerakhir) {
      tanggalBanding = moment().subtract(1, "years").format("YYYY-MM-DD");
      passFromTanggal = tanggalBanding;
    }

    if (dariTanggal && sampaiTanggal) {
      let dariTanggalPass = moment(dariTanggal, "DD-MM-YYYY").format(
        "YYYY-MM-DD"
      );
      let sampaiTanggalPass = moment(sampaiTanggal, "DD-MM-YYYY").format(
        "YYYY-MM-DD"
      );
      passFromTanggal = dariTanggalPass;
      passToTanggal = sampaiTanggalPass;
    }

    // page
    const page =
      req.query.page == null || req.query.page == "" ? 1 : req.query.page;
    const limit =
      req.query.limit == null || req.query.limit == "" ? 10 : req.query.limit;
    const search = req.query.search;

    const halamanAkhir = page * limit;
    const halmaanAwal = halamanAkhir - limit;
    const offset = halamanAkhir;
    const skip = halmaanAwal;

    let pegawaiJadwal = await pegawaiJadwalHelper.getPegawaiRangeTanggal(
      user.id_mapping,
      passFromTanggal,
      passToTanggal,
      limit,
      skip
    );
    let pegawai_id = user.id_mapping;
    let model = await pegawaiJadwalHelper.PegawaiJadwal.count({
      where: {
        id_pegawai: pegawai_id,
      },
    });

    // pagination
    const getPagination = pagination(page, model, limit);

    let keterangan = {
      from: skip + 1,
      to: offset,
      total: model,
    };

    let output = {
      data: pegawaiJadwal,
      pagination: getPagination,
      keterangan: keterangan,
    };

    if (output) {
      return res.status(200).json({
        status: 200,
        message: "Berhasil mengambil history absense",
        result: output,
      });
    } else {
      return res.status(400).json({
        status: 400,
        message: "Gagal mengambil data",
      });
    }
  } catch (error) {
    return res.status(400).json({
      status: 400,
      message: "Terjadi kesalahan data",
      result: error.message,
    });
  }
};

const alasanTerlambat = async (req, res) => {
  // const t = await sequelize.transaction();

  try {
    // ambil user data
    const { alasan_terlambat } = req.body;
    let user = req.user.data;
    let getPegawai = await Pegawai.findOne({
      where: {
        id_client: user.id_mapping,
      },
    });
    req.query.client_id = getPegawai.id_client;
    const getMapping = await userMappingHelper.getJoinDataMapping(
      user.jenis_mapping,
      null,
      user.id_user_mapping
    );

    const tanggal_sekarang = moment().format("YYYY-MM-DD");
    let pegawaiJadwal = await pegawaiJadwalHelper.getPegawaiByIdJadwal(
      getMapping.pegawai.id_pegawai,
      tanggal_sekarang
    );
    if (pegawaiJadwal.is_terlambat == 1) {
      let id_pegawai_jadwal = pegawaiJadwal.id_pegawai_jadwal;
      let output = {};
      output.alasan_terlambat = alasan_terlambat;
      let update = await pegawaiJadwalHelper.PegawaiJadwal.update(output, {
        where: {
          id_pegawai_jadwal: id_pegawai_jadwal,
        },
        // transaction: t,
      });
      // await t.commit();
      if (update) {
        pegawaiJadwal = await pegawaiJadwalHelper.getPegawaiByIdJadwal(
          getMapping.pegawai.id_pegawai,
          tanggal_sekarang
        );
        return res.status(200).json({
          status: 200,
          message: "Pesan berhasil dikirim",
          result: pegawaiJadwal,
        });
      } else {
        return res.status(200).json({
          status: 200,
          message: "Pegawai ini tidak terlambat",
        });
      }
    } else {
      return res.status(400).json({
        status: 400,
        message: "Gagal membuat alasan terlambat",
      });
    }
  } catch (error) {
    // await t.rollback();
    return res.status(400).json({
      status: 400,
      message: "Terjadi kesalahan data",
      result: error.message,
    });
  }
};

const alasanPulangCepat = async (req, res) => {
  // const t = await sequelize.transaction();

  try {
    // ambil user data
    const { alasan_pulang_cepat } = req.body;
    let user = req.user.data;
    let getPegawai = await Pegawai.findOne({
      where: {
        id_client: user.id_mapping,
      },
    });
    req.query.client_id = getPegawai.id_client;
    const getMapping = await userMappingHelper.getJoinDataMapping(
      user.jenis_mapping,
      null,
      user.id_user_mapping
    );

    let tanggal_sekarang = moment().format("YYYY-MM-DD");
    let pegawaiJadwal = await pegawaiJadwalHelper.getPegawaiByIdJadwal(
      getMapping.pegawai.id_pegawai,
      tanggal_sekarang
    );

    if (pegawaiJadwal.waktu_masuk == null) {
      pegawaiJadwal = await pegawaiJadwalHelper.getPegawaiByIdJadwal(
        getMapping.pegawai.id_pegawai,
        tanggal_sekarang
      );
    }

    if (pegawaiJadwal.is_pulang_cepat == 1) {
      let id_pegawai_jadwal = pegawaiJadwal.id_pegawai_jadwal;
      let output = {};
      output.alasan_pulang_cepat = alasan_pulang_cepat;
      let update = await pegawaiJadwalHelper.PegawaiJadwal.update(output, {
        where: {
          id_pegawai_jadwal: id_pegawai_jadwal,
        },
        // transaction: t,
      });
      // await t.commit();
      if (update) {
        pegawaiJadwal = await pegawaiJadwalHelper.getPegawaiByIdJadwal(
          getMapping.pegawai.id_pegawai,
          tanggal_sekarang
        );
        return res.status(200).json({
          status: 200,
          message: "Pesan berhasil dikirim",
          result: pegawaiJadwal,
        });
      } else {
        return res.status(400).json({
          status: 400,
          message: "Gagal update alasan terlambat",
        });
      }
    } else {
      return res.status(400).json({
        status: 400,
        message: "Gagal membuat alasan pulang cepat",
      });
    }
  } catch (error) {
    // await t.rollback();
    return res.status(400).json({
      status: 400,
      message: "Terjadi kesalahan data",
      result: error.message,
    });
  }
};

const jadwal = async (req, res) => {
  try {
    let user = req.user.data;
    let getPegawai = await Pegawai.findOne({
      where: {
        id_client: user.id_mapping,
      },
    });
    req.query.client_id = getPegawai.id_client;

    // page
    const page =
      req.query.page == null || req.query.page == "" ? 1 : req.query.page;
    const limit =
      req.query.limit == null || req.query.limit == "" ? 10 : req.query.limit;
    const search = req.query.search;

    const halamanAkhir = page * limit;
    const halmaanAwal = halamanAkhir - limit;
    const offset = halamanAkhir;
    const skip = halmaanAwal;

    let id_pegawai = user.id_mapping;
    const pegawai_id = req.query.pegawai_id;
    if (pegawai_id != null) {
      id_pegawai = pegawai_id;
    }
    let pegawaiJadwal = await getPegawaiIdJadwal(limit, skip, null, id_pegawai);
    let model = await pegawaiJadwalHelper.PegawaiJadwal.count({
      where: {
        id_pegawai: id_pegawai,
      },
    });
    if (search != null && search != "") {
      pegawaiJadwal = await getPegawaiIdJadwal(limit, skip, search, id_pegawai);
      let getModel = await getPegawaiIdJadwal(null, null, search, id_pegawai);
      model = getModel.length;
    }

    // pagination
    const getPagination = pagination(page, model, limit);

    let keterangan = {
      from: skip + 1,
      to: offset,
      total: model,
    };

    let output = {
      data: pegawaiJadwal,
      pagination: getPagination,
      keterangan: keterangan,
    };
    if (output) {
      return res.status(200).json({
        status: 200,
        message: "Berhasil mengambil data jadwal pegawai",
        output: output,
      });
    } else {
      return res.status(400).json({
        status: 400,
        message: "Gagal ambil data",
      });
    }
  } catch (error) {
    return res.status(400).json({
      status: 400,
      message: "Terjadi kesalahan data",
      result: error.message,
    });
  }
};

module.exports = {
  index,
  checkOut,
  history,
  alasanTerlambat,
  alasanPulangCepat,
  jadwal,
};
