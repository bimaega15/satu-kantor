const moment = require("moment");
const {
  Pegawai,
  PengajuanLembur,
  PengajuanLemburList,
  Client,
  JenisLembur,
  Cabang,
  PegawaiKontrak,
  PegawaiUnit,
  PegawaiJadwal,
} = require("../../model");
const {
  pegawaiJadwalHelper,
  pegawaiJadwalHelper: {
    getPegawaiJadwal,
    getPegawaiIdJadwal,
    getIdPegawaiJadwal,
  },
  pagination,
} = require("../../helper/index");
const { validationResult, check } = require("express-validator");
const mv = require("mv");
const sequelize = require("../../config/db");

const index = async (req, res) => {
  let user = req.user.data;
  let getPegawai = await Pegawai.findOne({
    where: {
      id_client: user.id_mapping,
    },
  });
  req.query.client_id = getPegawai.id_client;
  try {
    // page
    const page =
      req.query.page == null || req.query.page == "" ? 1 : req.query.page;
    const limit =
      req.query.limit == null || req.query.limit == "" ? 10 : req.query.limit;
    const search = req.query.search;

    const halamanAkhir = page * limit;
    const halmaanAwal = halamanAkhir - limit;
    const offset = halamanAkhir;
    const skip = halmaanAwal;

    let id_pegawai = user.id_mapping;
    let getPegawai = await Pegawai.findOne({
      where: {
        id_pegawai: id_pegawai,
      },
    });
    let id_client = getPegawai.id_client;

    let getFilter = {};
    getFilter.id_pegawai = user.id_mapping;
    getFilter = { ...getFilter, ...req.query };

    let sortBy = [];
    sortBy = ["tanggal", "desc"];

    delete getFilter.page;
    delete getFilter.limit;
    delete getFilter.search;

    let pegawaiJadwal = await getPegawaiJadwal(
      limit,
      skip,
      null,
      getFilter,
      id_client,
      sortBy
    );

    let model = await pegawaiJadwalHelper.PegawaiJadwal.count({
      where: {
        id_pegawai: id_pegawai,
        "$pegawai.client.id_client$": user.id_mapping,
      },
      include: [
        {
          model: Pegawai,
          include: [
            {
              model: Client,
              required: true,
            },
          ],
        },
      ],
    });

    if (getFilter != null) {
      let getModel = await getPegawaiJadwal(
        null,
        null,
        null,
        getFilter,
        id_client,
        sortBy
      );
      model = getModel.length;
    }

    if (search != null && search != "") {
      pegawaiJadwal = await getPegawaiJadwal(
        limit,
        skip,
        search,
        getFilter,
        id_client,
        sortBy
      );

      let getModel = await getPegawaiJadwal(
        null,
        null,
        search,
        getFilter,
        id_client,
        sortBy
      );
      model = getModel.length;
    }

    // pagination
    const getPagination = pagination(page, model, limit);

    let keterangan = {
      from: skip + 1,
      to: offset,
      total: model,
    };

    let output = {
      data: pegawaiJadwal,
      pagination: getPagination,
      keterangan: keterangan,
    };
    return res.status(200).json({
      status: 200,
      message: "Berhasil tangkap data",
      output: output,
    });
  } catch (error) {
    return res.status(400).json({
      status: 400,
      message: "Terjadi kesalahan data",
      result: error.message,
    });
  }
};

const edit = async (req, res) => {
  try {
    const id_pegawai_jadwal = req.params.id_pegawai_jadwal;
    const getPegawaiJadwal = await pegawaiJadwalHelper.getIdPegawaiJadwal(
      id_pegawai_jadwal
    );

    if (getPegawaiJadwal) {
      return res.status(200).json({
        status: 200,
        message: "Berhasil mengambil data pegawai jadwal",
        result: getPegawaiJadwal,
      });
    } else {
      return res.status(400).json({
        status: 400,
        message: "Gagal mengambil data pegawai jadwal",
      });
    }
  } catch (error) {
    return res.status(400).json({
      status: 400,
      message: "Terjadi kesalahan data",
      result: error.message,
    });
  }
};

const deleteData = async (req, res) => {
  // const t = await sequelize.transaction();

  try {
    const id_pegawai_jadwal = req.params.id_pegawai_jadwal;
    let checkPengajuan = await PegawaiJadwal.count({
      where: {
        id_pegawai_jadwal: id_pegawai_jadwal,
        is_request_ganti_jadwal: 1,
      },
    });
    if (checkPengajuan == 0) {
      return res.status(400).json({
        status: 400,
        message: "Belum ada pengajuan ganti jadwal",
      });
    }

    checkPengajuan = await PegawaiJadwal.count({
      where: {
        id_pegawai_jadwal: id_pegawai_jadwal,
        is_request_ganti_jadwal: 1,
        is_setuju: 1,
      },
    });
    if (checkPengajuan > 0) {
      return res.status(400).json({
        status: 400,
        message:
          "Maaf anda tidak dapat batalkan pengajuan ganti jadwal, karena atasan sudah approve penggantian jadwal anda",
      });
    }

    const batalPengajuan = {
      is_request_ganti_jadwal: null,
      keterangan_pegawai: null,
      is_setuju: null,
      keterangan_atasan: null,
    };
    const execBatalPengajuan = await PegawaiJadwal.update(batalPengajuan, {
      where: {
        id_pegawai_jadwal: id_pegawai_jadwal,
      },
      // transaction: t,
    });
    // await t.commit();
    if (execBatalPengajuan) {
      return res.status(200).json({
        status: 200,
        message: "Berhasil batalkan pengajuan ganti jadwal",
      });
    } else {
      return res.status(400).json({
        status: 400,
        message: "Gagal batalkan pengajuan ganti jadwal",
      });
    }
  } catch (error) {
    // await t.rollback();
    return res.status(400).json({
      status: 400,
      message: "Terjadi kesalahan data",
      result: error.message,
    });
  }
};

const update = async (req, res) => {
  let user = req.user.data;
  let getPegawai = await Pegawai.findOne({
    where: {
      id_client: user.id_mapping,
    },
  });
  req.query.client_id = getPegawai.id_client;
  // const t = await sequelize.transaction();

  try {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({
        status: 400,
        message: "Invalid form validation",
        result: errors.array(),
      });
    } else {
      const response = req.body;
      let output = {};
      output.keterangan_pegawai = response.keterangan_pegawai;
      output.is_request_ganti_jadwal = 1;

      const { id_pegawai_jadwal } = req.params;
      let updateGantiJadwal = await PegawaiJadwal.update(output, {
        where: {
          id_pegawai_jadwal: id_pegawai_jadwal,
        },
        // transaction: t,
      });

      // await t.commit();
      if (updateGantiJadwal) {
        return res.status(400).json({
          status: 400,
          message: "Berhasil melakukan pengajuan ganti jadwal",
        });
      } else {
        return res.status(400).json({
          status: 400,
          message: "Gagal pengajuan ganti jadwal",
        });
      }
    }
  } catch (error) {
    // await t.rollback();
    return res.status(400).json({
      status: 400,
      message: "Terjadi kesalahan data",
      result: error.message,
    });
  }
};

module.exports = {
  index,
  edit,
  deleteData,
  update,
};
