const moment = require("moment");
const { Op } = require("sequelize");
const {
  userMappingHelper,
  pegawaiJadwalHelper,
  pegawaiHelper,
  pagination,
} = require("../../helper/index");
const {
  Pegawai,
  PegawaiUnit,
  Unit,
  PegawaiJabatan,
  Jabatan,
} = require("../../model");

const index = async (req, res) => {
  try {
    let user = req.user.data;
    let getPegawai = await Pegawai.findOne({
      where: {
        id_client: user.id_mapping,
      },
    });
    req.query.client_id = getPegawai.id_client;
    const getMapping = await userMappingHelper.getJoinDataMapping(
      user.jenis_mapping,
      null,
      user.id_user_mapping
    );

    let id_client = getMapping.client.id_client;
    // page
    const page =
      req.query.page == null || req.query.page == "" ? 1 : req.query.page;
    const limit =
      req.query.limit == null || req.query.limit == "" ? 10 : req.query.limit;
    const search = req.query.search;

    const halamanAkhir = page * limit;
    const halmaanAwal = halamanAkhir - limit;
    const offset = halamanAkhir;
    const skip = halmaanAwal;

    let pegawai = await pegawaiHelper.getPegawai(
      limit,
      skip,
      null,
      null,
      id_client
    );

    let model = await pegawaiHelper.Pegawai.count();
    if (search != null && search != "") {
      pegawai = await pegawaiHelper.getPegawai(
        null,
        null,
        search,
        limit,
        id_client
      );
      let getModel = await pegawaiHelper.getPegawai(
        null,
        null,
        search,
        null,
        id_client
      );
      model = getModel.length;
    }

    // pagination
    const getPagination = pagination(page, model, limit);

    let keterangan = {
      from: skip + 1,
      to: offset,
      total: model,
    };

    let output = {
      data: pegawai,
      pagination: getPagination,
      keterangan: keterangan,
    };
    if (output) {
      return res.status(200).json({
        status: 200,
        message: "Berhasil mengambil data",
        result: output,
      });
    } else {
      return res.status(400).json({
        status: 400,
        message: "Gagal mengambil data",
        result: output,
      });
    }
  } catch (error) {
    return res.status(400).json({
      status: 400,
      message: "Terjadi kesalahan data",
      result: error.message,
    });
  }
};

const pegawaiHistory = async (req, res) => {
  try {
    // ambil user data
    const {
      semingguTerakhir,
      sebulanTerakhir,
      setahunTerakhir,
      dariTanggal,
      sampaiTanggal,
      pegawai_id,
    } = req.query;

    let passFromTanggal = null;
    let passToTanggal = null;

    let currentTanggal = moment().format("YYYY-MM-DD");
    passToTanggal = currentTanggal;
    let tanggalBanding = null;
    if (semingguTerakhir) {
      tanggalBanding = moment().subtract(1, "weeks").format("YYYY-MM-DD");
      passFromTanggal = tanggalBanding;
    }
    if (sebulanTerakhir) {
      tanggalBanding = moment().subtract(1, "months").format("YYYY-MM-DD");
      passFromTanggal = tanggalBanding;
    }
    if (setahunTerakhir) {
      tanggalBanding = moment().subtract(1, "years").format("YYYY-MM-DD");
      passFromTanggal = tanggalBanding;
    }

    if (dariTanggal && sampaiTanggal) {
      let dariTanggalPass = moment(dariTanggal, "DD-MM-YYYY").format(
        "YYYY-MM-DD"
      );
      let sampaiTanggalPass = moment(sampaiTanggal, "DD-MM-YYYY").format(
        "YYYY-MM-DD"
      );
      passFromTanggal = dariTanggalPass;
      passToTanggal = sampaiTanggalPass;
    }

    // page
    const page =
      req.query.page == null || req.query.page == "" ? 1 : req.query.page;
    const limit =
      req.query.limit == null || req.query.limit == "" ? 10 : req.query.limit;
    const search = req.query.search;

    const halamanAkhir = page * limit;
    const halmaanAwal = halamanAkhir - limit;
    const offset = halamanAkhir;
    const skip = halmaanAwal;

    let pegawaiJadwal = await pegawaiJadwalHelper.getPegawaiRangeTanggal(
      pegawai_id,
      passFromTanggal,
      passToTanggal,
      limit,
      skip
    );
    let model = await pegawaiJadwalHelper.PegawaiJadwal.count({
      where: {
        id_pegawai: pegawai_id,
      },
    });

    // pagination
    const getPagination = pagination(page, model, limit);

    let keterangan = {
      from: skip + 1,
      to: offset,
      total: model,
    };

    let output = {
      data: pegawaiJadwal,
      pagination: getPagination,
      keterangan: keterangan,
    };

    if (output) {
      return res.status(200).json({
        status: 200,
        message: "Berhasil mengambil data jadwal pegawai",
        result: output,
      });
    } else {
      return res.status(400).json({
        status: 400,
        message: "Gagal mengambil data",
      });
    }
  } catch (error) {
    return res.status(400).json({
      status: 400,
      message: "Terjadi kesalahan data",
      result: error.message,
    });
  }
};

const membawahiUnit = async (req, res) => {
  try {
    let user = req.user.data;
    let getPegawai = await Pegawai.findOne({
      where: {
        id_client: user.id_mapping,
      },
    });
    req.query.client_id = getPegawai.id_client;
    let pegawai = await Pegawai.findOne({
      where: {
        id_client: user.id_mapping,
        "$pegawai_unit.is_aktif$": 1,
      },
      include: [
        {
          model: PegawaiUnit,
          include: [
            {
              model: Unit,
            },
          ],
        },
      ],
    });
    let membawahiUnit = pegawai.pegawai_unit.membawahi_unit;
    membawahiUnit = membawahiUnit.split(",");
    let dataUnit = null;
    if (membawahiUnit[0] != "") {
      dataUnit = await Unit.findAll({
        attributes: ["id_unit", "nama_unit"],
        order: [["id_unit", "asc"]],
        where: {
          id_unit: {
            [Op.in]: membawahiUnit,
          },
          "$pegawai_unit.is_aktif$": 1,
        },
        include: [
          {
            model: PegawaiUnit,
            include: [
              {
                model: Pegawai,
              },
            ],
          },
        ],
      });
    }
    let output = {};
    output.pegawaiUnit = pegawai;
    output.membawahiUnit = dataUnit;
    if (output != null) {
      return res.status(200).json({
        status: 200,
        message: "Berhasil mengambil data pegawai membawahi unit",
        result: output,
      });
    } else {
      return res.status(400).json({
        status: 400,
        message: "Pegawai tidak ada membawahi unit apapun",
      });
    }
  } catch (error) {
    return res.status(400).json({
      status: 400,
      message: "Terjadi kesalahan data",
      result: error.message,
    });
  }
};

const membawahiJabatan = async (req, res) => {
  try {
    let user = req.user.data;
    let getPegawai = await Pegawai.findOne({
      where: {
        id_client: user.id_mapping,
      },
    });
    req.query.client_id = getPegawai.id_client;
    let pegawai = await Pegawai.findOne({
      where: {
        id_client: user.id_mapping,
        "$pegawai_jabatan.is_aktif$": 1,
      },
      include: [
        {
          model: PegawaiJabatan,
          include: [
            {
              model: Jabatan,
            },
          ],
        },
      ],
    });
    let membawahiJabatan = pegawai.pegawai_jabatan.membawahi_jabatan;
    membawahiJabatan = membawahiJabatan.split(",");

    let dataJabatan = null;
    if (membawahiJabatan[0] != "") {
      dataJabatan = await Jabatan.findAll({
        attributes: ["id_jabatan", "nama_jabatan"],
        order: [["id_jabatan", "asc"]],
        where: {
          id_jabatan: {
            [Op.in]: membawahiJabatan,
          },
          "$pegawai_jabatan.is_aktif$": 1,
        },
        include: [
          {
            model: PegawaiJabatan,
            include: [
              {
                model: Pegawai,
              },
            ],
          },
        ],
      });
    }
    let output = {};
    output.pegawaiJabatan = pegawai;
    output.membawahiJabatan = dataJabatan;
    if (output != null) {
      return res.status(200).json({
        status: 200,
        message: "Berhasil mengambil data pegawai membawahi unit",
        result: output,
      });
    } else {
      return res.status(400).json({
        status: 400,
        message: "Pegawai tidak ada membawahi unit apapun",
      });
    }
  } catch (error) {
    return res.status(400).json({
      status: 400,
      message: "Terjadi kesalahan data",
      result: error.message,
    });
  }
};

module.exports = {
  index,
  pegawaiHistory,
  membawahiUnit,
  membawahiJabatan,
};
