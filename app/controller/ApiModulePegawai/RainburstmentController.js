const moment = require("moment");
const { Pegawai, RainburstmentLists, Rainburstment } = require("../../model");
const {
  rainburstmentHelper,
  rainburstmentHelper: { getRainburstment, getRainbursetMentById },
  pagination,
} = require("../../helper/index");
const { validationResult, check } = require("express-validator");
const mv = require("mv");
const fs = require("fs");
const sequelize = require("../../config/db");

const index = async (req, res) => {
  let user = req.user.data;
  let getPegawai = await Pegawai.findOne({
    where: {
      id_client: user.id_mapping,
    },
  });
  req.query.client_id = getPegawai.id_client;
  try {
    // page
    const page =
      req.query.page == null || req.query.page == "" ? 1 : req.query.page;
    const limit =
      req.query.limit == null || req.query.limit == "" ? 10 : req.query.limit;
    const search = req.query.search;

    const halamanAkhir = page * limit;
    const halmaanAwal = halamanAkhir - limit;
    const offset = halamanAkhir;
    const skip = halmaanAwal;

    let id_pegawai = user.id_mapping;
    let getPegawai = await Pegawai.findOne({
      where: {
        id_pegawai: id_pegawai,
      },
    });

    let getFilter = {};
    getFilter.id_pegawai = user.id_mapping;
    getFilter = { ...getFilter, ...req.query };

    delete getFilter.page;
    delete getFilter.limit;
    delete getFilter.search;

    let model = await Rainburstment.findAll({
      where: {
        id_pegawai: getPegawai.id_pegawai,
      },
    });
    model = model.length;
    let pengajuanLembur = await getRainburstment(limit, skip, null, getFilter);

    if (getFilter != null) {
      let getModel = await getRainburstment(null, null, null, getFilter);
      model = getModel.length;
    }
    if (search != null && search != "") {
      pengajuanLembur = await getRainburstment(limit, skip, search, getFilter);
      let getModel = await getRainburstment(null, null, search, getFilter);
      model = getModel.length;
    }

    // pagination
    const getPagination = pagination(page, model, limit);

    let keterangan = {
      from: skip + 1,
      to: offset,
      total: model,
    };

    let output = {
      data: pengajuanLembur,
      pagination: getPagination,
      keterangan: keterangan,
    };

    return res.status(200).json({
      status: 200,
      message: "Berhasil tangkap data",
      result: output,
    });
  } catch (error) {
    return res.status(400).json({
      status: 400,
      message: "Terjadi kesalahan data",
      result: error.message,
    });
  }
};

const store = async (req, res) => {
  let user = req.user.data;
  let getPegawai = await Pegawai.findOne({
    where: {
      id_client: user.id_mapping,
    },
  });
  req.query.client_id = getPegawai.id_client;
  // const t = await sequelize.transaction();

  try {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({
        status: 400,
        message: "Invalid form validation",
        result: errors.array(),
      });
    } else {
      const response = req.body;
      let struk_pembelian = await uploadStrukPembelian(
        response.struk_pembelian
      );

      let dateTime = moment().format("YYYY-MM-DD HH:mm:ss");

      let output = {};
      output.id_pegawai = user.id_mapping;
      output.tanggal = moment(response.tanggal, "DD-MM-YYYY").format(
        "YYYY-MM-DD"
      );
      output.user_create = user.id;
      output.user_update = user.id;
      output.time_create = dateTime;
      output.time_update = dateTime;
      output.keterangan = response.keterangan;
      output.process = "menunggu";
      output.struk_pembelian = struk_pembelian;

      let insertRainburstMent = await rainburstmentHelper.createRainburstMent(
        output,
        {
          // transaction: t,
        }
      );
      let id_rainburstment = insertRainburstMent.id_rainburstment;

      let outputDetail = [];
      let namaPembelian = response.nama_pembelian.split(",");
      let keteranganList = response.keterangan_list.split(",");
      let harga = response.harga.split(",");
      let jumlah = response.jumlah.split(",");

      namaPembelian.map((v, i) => {
        outputDetail.push({
          nama_pembelian: v,
          keterangan_list: keteranganList[i],
          harga: harga[i].split(".").join(""),
          jumlah: jumlah[i],
          id_rainburstment: id_rainburstment,
          time_create: dateTime,
          time_update: dateTime,
          user_create: user.id,
          user_update: user.id,
        });
      });

      let insertRainburstmentList = await RainburstmentLists.bulkCreate(
        outputDetail,
        {
          // transaction: t,
        }
      );

      // await t.commit();

      if (insertRainburstMent || insertRainburstmentList) {
        return res.json({
          status: 200,
          message: "Berhasil insert data",
        });
      } else {
        return res.json({
          status: 400,
          message: "Gagal insert data",
        });
      }
    }
  } catch (error) {
    // await t.rollback();
    return res.status(400).json({
      status: 400,
      message: "Terjadi kesalahan data",
      result: error.message,
    });
  }
};

const edit = async (req, res) => {
  try {
    const id_rainburstment = req.params.id_rainburstment;
    const getRainburstment = await rainburstmentHelper.getRainbursetMentById(
      id_rainburstment
    );

    if (getRainburstment) {
      return res.status(200).json({
        status: 200,
        message: "Berhasil mengambil data rainburstment",
        result: getRainburstment,
      });
    } else {
      return res.status(400).json({
        status: 400,
        message: "Gagal mengambil data rainburstment",
      });
    }
  } catch (error) {
    return res.status(400).json({
      status: 400,
      message: "Terjadi kesalahan data",
      result: error.message,
    });
  }
};

const deleteData = async (req, res) => {
  // const t = await sequelize.transaction();

  try {
    const id_rainburstment = req.params.id_rainburstment;
    await deleteStrukPembelian(id_rainburstment);
    const getRainburstment = await rainburstmentHelper.deleteRainburstMent(
      id_rainburstment,
      {
        // transaction: t,
      }
    );

    // await t.commit();
    if (getRainburstment) {
      return res.status(200).json({
        status: 200,
        message: "Berhasil menghapus data rainburstment",
      });
    } else {
      return res.status(400).json({
        status: 400,
        message: "Gagal menghapus data rainburstment",
      });
    }
  } catch (error) {
    // await t.rollback();
    return res.status(400).json({
      status: 400,
      message: "Terjadi kesalahan data",
      result: error.message,
    });
  }
};

const update = async (req, res) => {
  let user = req.user.data;
  let getPegawai = await Pegawai.findOne({
    where: {
      id_client: user.id_mapping,
    },
  });
  req.query.client_id = getPegawai.id_client;
  // const t = await sequelize.transaction();

  try {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({
        status: 400,
        message: "Invalid form validation",
        result: errors.array(),
      });
    } else {
      const response = req.body;
      let struk_pembelian = await uploadStrukPembelian(
        response.struk_pembelian
      );

      let dateTime = moment().format("YYYY-MM-DD HH:mm:ss");

      const id_rainburstment = req.params.id_rainburstment;

      let checkRainburstment = await rainburstmentHelper.getRainbursetMentById(
        id_rainburstment
      );

      if (checkRainburstment.process.trim() != "menunggu") {
        return res.status(400).json({
          status: 400,
          message:
            "Maaf anda sudah tidak dapat mengubah rainburstment, karena sudah diproses",
        });
      }

      let output = {};
      output.id_pegawai = user.id_mapping;
      output.tanggal = moment(response.tanggal, "DD-MM-YYYY").format(
        "YYYY-MM-DD"
      );
      output.user_create = user.id;
      output.user_update = user.id;
      output.time_create = dateTime;
      output.time_update = dateTime;
      output.keterangan = response.keterangan;
      output.process = "menunggu";
      output.struk_pembelian = struk_pembelian;

      let updateRainburstment = await rainburstmentHelper.updateRainburstMent(
        output,
        id_rainburstment,
        {
          // transaction: t,
        }
      );

      // check rainburstment lists
      let checkRainburstMentLists = await RainburstmentLists.count({
        where: {
          id_rainburstment: id_rainburstment,
        },
      });

      if (checkRainburstMentLists > 0) {
        await RainburstmentLists.destroy({
          where: {
            id_rainburstment: id_rainburstment,
          },
          // transaction: t,
        });
      }

      let outputDetail = [];
      let namaPembelian = response.nama_pembelian.split(",");
      let keteranganList = response.keterangan_list.split(",");
      let harga = response.harga.split(",");
      let jumlah = response.jumlah.split(",");

      namaPembelian.map((v, i) => {
        outputDetail.push({
          nama_pembelian: v,
          keterangan_list: keteranganList[i],
          harga: harga[i].split(".").join(""),
          jumlah: jumlah[i],
          id_rainburstment: id_rainburstment,
          time_create: dateTime,
          time_update: dateTime,
          user_create: user.id,
          user_update: user.id,
        });
      });

      let updateRainburstMentLists = await RainburstmentLists.bulkCreate(
        outputDetail,
        {
          // transaction: t,
        }
      );
      // await t.commit();

      if (updateRainburstment || updateRainburstMentLists) {
        return res.json({
          status: 200,
          message: "Berhasil update data",
        });
      } else {
        return res.json({
          status: 400,
          message: "Gagal update data",
        });
      }
    }
  } catch (error) {
    // await t.rollback();
    return res.status(400).json({
      status: 400,
      message: "Terjadi kesalahan data",
      result: error.message,
    });
  }
};

const uploadStrukPembelian = async (
  reponseStrukPembelian,
  id_rainburstment = null
) => {
  if (reponseStrukPembelian.struk_pembelian != null) {
    const { struk_pembelian } = reponseStrukPembelian;

    let pathOld = struk_pembelian.path;
    let name = struk_pembelian.name;
    let size = struk_pembelian.size;

    name =
      moment
        .duration(moment(moment().format("YYYY-MM-DD HH:mm:ss")))
        .asSeconds() +
      "_" +
      name.split(" ").join("-");
    pathNew = "public/image/strukPembelian/" + name;

    if (size > 0) {
      if (pathOld != null && pathNew != null) {
        if (id_rainburstment != null) {
          await deleteStrukPembelian(id_rainburstment);
        }

        mv(pathOld, pathNew, function (err) {
          if (err) {
            return err;
          }
        });

        const fileName = pathNew.split("/");
        const fileDb = fileName[fileName.length - 1];
        return fileDb;
      }
    }
  }

  if (id_rainburstment != null) {
    let getRainburstment = await getRainbursetMentById(id_rainburstment);
    if (
      getRainburstment.struk_pembelian != "default.png" &&
      getRainburstment.struk_pembelian != null
    ) {
      return getRainburstment.struk_pembelian;
    }
  }
  return "default.png";
};

const deleteStrukPembelian = async (id_rainburstment = null) => {
  try {
    if (id_rainburstment != null) {
      let getRainburstment = await getRainbursetMentById(id_rainburstment);
      if (
        getRainburstment.struk_pembelian != "default.png" &&
        getRainburstment.struk_pembelian != null
      ) {
        let unlink =
          "public/image/strukPembelian/" + getRainburstment.struk_pembelian;
        if (fs.existsSync(unlink)) {
          fs.unlinkSync(unlink);
        }
      }
    }
  } catch (error) {
    return res.status(400).json({
      status: 400,
      message: "Terjadi kesalahan data",
      result: error.message,
    });
  }
};

module.exports = {
  index,
  store,
  edit,
  deleteData,
  update,
};
