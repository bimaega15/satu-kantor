const moment = require("moment");
const {
  Pegawai,
  PengajuanLembur,
  PengajuanLemburList,
  Client,
  JenisLembur,
  Cabang,
  PegawaiKontrak,
  PegawaiUnit,
} = require("../../model");
const {
  pengajuanLemburHelper,
  pengajuanLemburHelper: { getPengajuanLembur, getPengajuanLemburId },
  pagination,
} = require("../../helper/index");
const { validationResult } = require("express-validator");
const mv = require("mv");
const sequelize = require("../../config/db");

const index = async (req, res) => {
  let user = req.user.data;
  let getPegawai = await Pegawai.findOne({
    where: {
      id_client: user.id_mapping,
    },
  });
  req.query.client_id = getPegawai.id_client;
  try {
    // page
    const page =
      req.query.page == null || req.query.page == "" ? 1 : req.query.page;
    const limit =
      req.query.limit == null || req.query.limit == "" ? 10 : req.query.limit;
    const search = req.query.search;

    const halamanAkhir = page * limit;
    const halmaanAwal = halamanAkhir - limit;
    const offset = halamanAkhir;
    const skip = halmaanAwal;

    let id_pegawai = user.id_mapping;
    let getPegawai = await Pegawai.findOne({
      where: {
        id_pegawai: id_pegawai,
      },
    });
    let id_client = getPegawai.id_client;

    let getFilter = {};
    getFilter.id_pegawai = user.id_mapping;
    getFilter = { ...getFilter, ...req.query };

    delete getFilter.page;
    delete getFilter.limit;
    delete getFilter.search;

    let model = await PengajuanLembur.findAll({
      include: {
        model: PengajuanLemburList,
        required: true,
        where: {
          id_pegawai: id_pegawai,
        },
      },
    });
    model = model.length;

    let pengajuanLembur = await getPengajuanLembur(
      limit,
      skip,
      null,
      getFilter,
      id_client
    );

    if (getFilter != null) {
      let getModel = await getPengajuanLembur(
        null,
        null,
        null,
        getFilter,
        id_client
      );
      model = getModel.length;
    }
    if (search != null && search != "") {
      pengajuanLembur = await getPengajuanLembur(
        limit,
        skip,
        search,
        getFilter,
        id_client
      );
      let getModel = await getPengajuanLembur(
        null,
        null,
        search,
        getFilter,
        id_client
      );
      model = getModel.length;
    }

    // pagination
    const getPagination = pagination(page, model, limit);

    let keterangan = {
      from: skip + 1,
      to: offset,
      total: model,
    };

    let output = {
      data: pengajuanLembur,
      pagination: getPagination,
      keterangan: keterangan,
    };

    return res.status(200).json({
      status: 200,
      message: "Berhasil tangkap data",
      result: output,
    });
  } catch (error) {
    return res.status(400).json({
      status: 400,
      message: "Terjadi kesalahan data",
      result: error.message,
    });
  }
};

const store = async (req, res) => {
  let user = req.user.data;
  let getPegawai = await Pegawai.findOne({
    where: {
      id_client: user.id_mapping,
    },
  });
  req.query.client_id = getPegawai.id_client;
  // const t = await sequelize.transaction();

  try {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({
        status: 400,
        message: "Invalid form validation",
        result: errors.array(),
      });
    } else {
      const response = req.body;
      let dateTime = moment().format("YYYY-MM-DD HH:mm:ss");

      let getPegawai = await Pegawai.findOne({
        include: [
          {
            model: PegawaiUnit,
            required: true,
            where: {
              is_aktif: 1,
            },
          },
        ],
        where: {
          id_client: user.id_mapping,
        },
      });

      let output = {};
      output.tanggal_lembur = moment(
        response.tanggal_lembur,
        "DD-MM-YYYY"
      ).format("YYYY-MM-DD");
      output.waktu_mulai = response.waktu_mulai;
      output.waktu_selesai = response.waktu_selesai;
      output.id_jenis_lembur = response.id_jenis_lembur;
      output.id_client = getPegawai.id_client;
      output.id_cabang = getPegawai.pegawai_unit.id_cabang;
      output.user_create = user.id;
      output.user_update = user.id;
      output.time_create = dateTime;
      output.time_update = dateTime;

      let insertPengajuanLembur =
        await pengajuanLemburHelper.createPengajuanLembur(output, {
          // transaction: t,
        });

      // pengajuan lembur list
      let arrayIdPegawai = [];
      arrayIdPegawai.push(user.id_mapping);

      let pushPengajuanLemburList = [];
      arrayIdPegawai.map((v, i) => {
        pushPengajuanLemburList.push({
          id_pengajuan_lembur: insertPengajuanLembur.id_pengajuan_lembur,
          id_pegawai: v,
          user_create: user.id_mapping,
          user_update: user.id_mapping,
          time_create: dateTime,
          time_update: dateTime,
        });
      });

      let insertPengajuanLemburList = await PengajuanLemburList.bulkCreate(
        pushPengajuanLemburList,
        {
          // transaction: t,
        }
      );

      // await t.commit();

      if (insertPengajuanLembur || insertPengajuanLemburList) {
        return res.json({
          status: 200,
          message: "Berhasil insert data",
        });
      } else {
        return res.json({
          status: 400,
          message: "Gagal insert data",
        });
      }
    }
  } catch (error) {
    // await t.rollback();
    return res.status(400).json({
      status: 400,
      message: "Terjadi kesalahan data",
      result: error.message,
    });
  }
};

const edit = async (req, res) => {
  try {
    const id_pengajuan_lembur = req.params.id_pengajuan_lembur;
    const getPengajuanLembur = await pengajuanLemburHelper.getPengajuanLemburId(
      id_pengajuan_lembur
    );

    if (getPengajuanLembur) {
      return res.status(200).json({
        status: 200,
        message: "Berhasil mengambil data pengajuan lembur",
        result: getPengajuanLembur,
      });
    } else {
      return res.status(400).json({
        status: 400,
        message: "Gagal mengambil data pengajuan lembur",
      });
    }
  } catch (error) {
    return res.status(400).json({
      status: 400,
      message: "Terjadi kesalahan data",
      result: error.message,
    });
  }
};

const deleteData = async (req, res) => {
  // const t = await sequelize.transaction();

  try {
    const id_pengajuan_lembur = req.params.id_pengajuan_lembur;
    const getPengajuanLembur =
      await pengajuanLemburHelper.deletePengajuanLembur(id_pengajuan_lembur, {
        // transaction: t,
      });

    // await t.commit();
    if (getPengajuanLembur) {
      return res.status(200).json({
        status: 200,
        message: "Berhasil menghapus data pengajuan lembur",
      });
    } else {
      return res.status(400).json({
        status: 400,
        message: "Gagal menghapus data pengajuan lembur",
      });
    }
  } catch (error) {
    // await t.rollback();
    return res.status(400).json({
      status: 400,
      message: "Terjadi kesalahan data",
      result: error.message,
    });
  }
};

const jenisLembur = async (req, res) => {
  const user = req.user.data;

  try {
    let getPegawai = await Pegawai.findOne({
      where: {
        id_client: user.id_mapping,
      },
    });
    let id_client = getPegawai.id_client;
    let getJenisLembur = await JenisLembur.findAll({
      where: {
        is_aktif: 1,
        id_client: id_client,
      },
    });
    if (getJenisLembur) {
      return res.status(200).json({
        status: 200,
        message: "Berhasil ambil data jenis lembur",
        result: getJenisLembur,
      });
    } else {
      return res.status(400).json({
        status: 400,
        message: "Gagal ambil data jenis lembur",
      });
    }
  } catch (error) {
    return res.status(400).json({
      status: 400,
      message: "Terjadi kesalahan data",
      result: error.message,
    });
  }
};

const update = async (req, res) => {
  let user = req.user.data;
  let getPegawai = await Pegawai.findOne({
    where: {
      id_client: user.id_mapping,
    },
  });
  req.query.client_id = getPegawai.id_client;
  // const t = await sequelize.transaction();

  try {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({
        status: 400,
        message: "Invalid form validation",
        result: errors.array(),
      });
    } else {
      const response = req.body;
      let dateTime = moment().format("YYYY-MM-DD HH:mm:ss");

      let getPegawai = await Pegawai.findOne({
        include: [
          {
            model: PegawaiUnit,
            required: true,
            where: {
              is_aktif: 1,
            },
          },
        ],
        where: {
          id_client: user.id_mapping,
        },
      });

      let id_pengajuan_lembur = req.params.id_pengajuan_lembur;

      let output = {};
      output.tanggal_lembur = moment(
        response.tanggal_lembur,
        "DD-MM-YYYY"
      ).format("YYYY-MM-DD");
      output.waktu_mulai = response.waktu_mulai;
      output.waktu_selesai = response.waktu_selesai;
      output.id_jenis_lembur = response.id_jenis_lembur;
      output.id_client = getPegawai.id_client;
      output.id_cabang = getPegawai.pegawai_unit.id_cabang;
      output.user_update = user.id;
      output.time_update = dateTime;

      let updatePengajuanLembur =
        await pengajuanLemburHelper.updatePengajuanLembur(
          output,
          id_pengajuan_lembur,
          {
            // transaction: t,
          }
        );

      // pengajuan lembur list
      // check pengajuan lembur list
      let checkPengajuanLemburList = 0;
      checkPengajuanLemburList = await PengajuanLemburList.count({
        where: {
          id_pengajuan_lembur: id_pengajuan_lembur,
        },
      });
      if (checkPengajuanLemburList > 0) {
        await PengajuanLemburList.destroy({
          where: {
            id_pengajuan_lembur: id_pengajuan_lembur,
          },
          // transaction: t,
        });
      }

      let arrayIdPegawai = [];
      arrayIdPegawai.push(user.id_mapping);

      let pushPengajuanLemburList = [];
      arrayIdPegawai.map((v, i) => {
        pushPengajuanLemburList.push({
          id_pengajuan_lembur: id_pengajuan_lembur,
          id_pegawai: v,
          user_create: user.id_mapping,
          user_update: user.id_mapping,
          time_create: dateTime,
          time_update: dateTime,
        });
      });

      let updatePengajuanLemburList = await PengajuanLemburList.bulkCreate(
        pushPengajuanLemburList,
        {
          // transaction: t,
        }
      );
      // await t.commit();

      if (updatePengajuanLembur || updatePengajuanLemburList) {
        return res.json({
          status: 200,
          message: "Berhasil update data",
        });
      } else {
        return res.json({
          status: 400,
          message: "Gagal update data",
        });
      }
    }
  } catch (error) {
    // await t.rollback();
    return res.status(400).json({
      status: 400,
      message: "Terjadi kesalahan data",
      result: error.message,
    });
  }
};

module.exports = {
  index,
  store,
  edit,
  deleteData,
  update,
  jenisLembur,
};
