const { validationResult } = require("express-validator");
const {
  penugasanHelper,
  penugasanHelper: { getPenugasanId },
  pagination,
  penugasanFileHelper,
  penugasanFileHelper: { updatePenugasanFile },
  penugasanStatusHelper: { updatePenugasanStatus },
} = require("../../helper/index");
const moment = require("moment");
const {
  Tugas,
  PenugasanFile,
  PenugasanStatus,
  Cabang,
  Pegawai,
  LokasiTugas,
} = require("../../model");
const sequelize = require("../../config/db");
const mv = require("mv");
const fs = require("fs");

const index = async (req, res) => {
  try {
    let user = req.user.data;
    let getPegawai = await Pegawai.findOne({
      where: {
        id_client: user.id_mapping,
      },
    });
    req.query.client_id = getPegawai.id_client;

    // page
    const page =
      req.query.page == null || req.query.page == "" ? 1 : req.query.page;
    const limit =
      req.query.limit == null || req.query.limit == "" ? 10 : req.query.limit;
    const search = req.query.search;

    const halamanAkhir = page * limit;
    const halamanAwal = halamanAkhir - limit;
    const offset = halamanAkhir;
    const skip = halamanAwal;

    let getFilter = {};
    getFilter.id_pegawai = user.id_mapping;
    getFilter.is_aktif = true;
    getFilter = { ...getFilter, ...req.query };

    delete getFilter.page;
    delete getFilter.limit;
    delete getFilter.search;

    let penugasan = await penugasanHelper.getPenugasan(
      limit,
      skip,
      null,
      getFilter
    );
    let model = await penugasanHelper.Penugasan.count({
      where: {
        id_client: user.id_mapping,
      },
    });
    if (getFilter != null) {
      let getModel = await penugasanHelper.getPenugasan(
        null,
        null,
        null,
        getFilter
      );
      model = getModel.length;
    }

    if (search != null && search != "") {
      penugasan = await penugasanHelper.getPenugasan(
        limit,
        skip,
        search,
        getFilter
      );
      let getModel = await penugasanHelper.getPenugasan(
        null,
        null,
        search,
        getFilter
      );
      model = getModel.length;
    }

    // pagination
    const getPagination = pagination(page, model, limit);

    let keterangan = {
      from: skip + 1,
      to: offset,
      total: model,
    };

    let output = {
      data: penugasan,
      pagination: getPagination,
      keterangan: keterangan,
    };
    return res.status(200).json({
      status: 200,
      message: "Berhasil tangkap data",
      result: output,
    });
  } catch (error) {
    return res.status(400).json({
      status: 400,
      message: "Terjadi kesalahan data",
      result: error.message,
    });
  }
};

const update = async (req, res) => {
  let user = req.user.data;
  let getPegawai = await Pegawai.findOne({
    where: {
      id_client: user.id_mapping,
    },
  });
  req.query.client_id = getPegawai.id_client;
  // const t = await sequelize.transaction();

  try {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({
        status: 400,
        message: "Invalid form validation",
        result: errors.array(),
      });
    } else {
      const response = req.body;

      let dateTime = moment().format("YYYY-MM-DD HH:mm:ss");
      let dataPenugasanFile = {
        nama_file: await uploadNamaFile(
          response.nama_file,
          response.id_penugasan_file
        ),
        user_update: user.id_mapping,
        time_update: dateTime,
      };

      let varUpdatePenugasanFile = await updatePenugasanFile(
        dataPenugasanFile,
        response.id_penugasan_file,
        {
          transaction: t,
        }
      );

      let dataPenugasanStatus = {
        status: response.status,
        keterangan: response.keterangan,
        user_update: user.id_mapping,
        time_update: dateTime,
      };
      let varUpdatePenugasanStatus = await updatePenugasanStatus(
        dataPenugasanStatus,
        response.id_penugasan_status,
        {
          // transaction: t,
        }
      );
      // await t.commit();
      if (varUpdatePenugasanFile || varUpdatePenugasanStatus) {
        return res.json({
          status: 200,
          message: "Berhasil update data",
        });
      } else {
        return res.json({
          status: 400,
          message: "Gagal update data",
        });
      }
    }
  } catch (error) {
    // await t.rollback();
    return res.status(400).json({
      status: 400,
      message: "Terjadi kesalahan data",
      result: error.message,
    });
  }
};

const edit = async (req, res) => {
  try {
    const id_penugasan = req.params.id_penugasan;
    const getPenugasan = await penugasanHelper.getPenugasanId(id_penugasan);
    if (getPenugasan) {
      return res.status(200).json({
        status: 200,
        message: "Berhasil mengambil data penugasan",
        result: getPenugasan,
      });
    } else {
      return res.status(400).json({
        status: 400,
        message: "Gagal mengambil data penugasan",
      });
    }
  } catch (error) {
    return res.status(400).json({
      status: 400,
      message: "Terjadi kesalahan data",
      result: error.message,
    });
  }
};

const uploadNamaFile = async (
  responseNamaFile,
  id_penugasan_file = null,
  multiple = false
) => {
  if (multiple == true) {
    let getNamaFile = {};
    getNamaFile.nama_file = responseNamaFile;
    responseNamaFile = getNamaFile;
  }

  if (responseNamaFile.nama_file != null) {
    const { nama_file } = responseNamaFile;

    let pathOld = nama_file.path;
    let name = nama_file.name;
    let size = nama_file.size;

    name =
      moment
        .duration(moment(moment().format("YYYY-MM-DD HH:mm:ss")))
        .asSeconds() +
      "_" +
      name.split(" ").join("-");
    pathNew = "public/image/filePenugasan/" + name;
    if (size > 0) {
      if (pathOld != null && pathNew != null) {
        if (id_penugasan_file != null) {
          await deleteNamaFile(id_penugasan_file);
        }

        mv(pathOld, pathNew, function (err) {
          if (err) {
            return err;
          }
        });

        const fileName = pathNew.split("/");
        const fileDb = fileName[fileName.length - 1];
        return fileDb;
      }
    }
  }

  if (id_penugasan_file != null) {
    let getPenugasan = await getPenugasanId(id_penugasan_file);
    if (
      getPenugasan.nama_file != "default.png" &&
      getPenugasan.nama_file != null
    ) {
      return getPenugasan.nama_file;
    }
  }
  return "default.png";
};

const deleteNamaFile = async (id_penugasan_file = null) => {
  if (id_penugasan_file != null) {
    let getPenugasan = await penugasanFileHelper.getPenugasanFileId(
      id_penugasan_file
    );
    if (
      getPenugasan.nama_file != "default.png" &&
      getPenugasan.nama_file != null
    ) {
      let unlink = "public/image/filePenugasan/" + getPenugasan.nama_file;
      if (fs.existsSync(unlink)) {
        fs.unlinkSync(unlink);
      }
    }
  }
};

module.exports = {
  index,
  edit,
  update,
};
