const { validationResult } = require("express-validator");
const usersModel = require("../../helper/userHelper");
const bcrypt = require("bcrypt");
const sequelize = require("../../config/db");
const saltRounds = 10;

const index = async (req, res) => {
  try {
    let user = req.user.data;
    let getPegawai = await Pegawai.findOne({
      where: {
        id_client: user.id_mapping,
      },
    });
    req.query.client_id = getPegawai.id_client;
    let getUserById = await usersModel.getUsersById(user.id);
    if (getUserById) {
      return res.status(200).json({
        status: 200,
        message: "Berhasil tangkap data",
        result: getUserById,
      });
    } else {
      return res.status(400).json({
        status: 400,
        message: "Gagal mengambil data",
      });
    }
  } catch (error) {
    return res.status(400).json({
      status: 400,
      message: "Terjadi kesalahan data",
      result: error.message,
    });
  }
};

const store = async (req, res) => {
  // const t = await sequelize.transaction();

  try {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({
        status: 400,
        message: "Invalid form validation",
        result: errors.array(),
      });
    } else {
      const { body } = req;
      let user = req.user.data;
      let getPegawai = await Pegawai.findOne({
        where: {
          id_client: user.id_mapping,
        },
      });
      req.query.client_id = getPegawai.id_client;
      let password_db = body.password_hash_old;

      let password = body.password_hash;
      if (password != null) {
        password_db = bcrypt.hashSync(password, saltRounds);
      }
      let update = await usersModel.Users.update(
        {
          username: body.username,
          password_hash: password_db,
          email: body.email,
        },
        {
          where: {
            id: user.id,
          },
          // transaction: t,
        }
      );

      // await t.commit();

      if (update) {
        return res.json({
          status: 200,
          message: "Berhasil update data",
        });
      } else {
        return res.json({
          status: 400,
          message: "Gagal update data",
        });
      }
    }
  } catch (error) {
    // await t.rollback();
    return res.status(400).json({
      status: 400,
      message: "Terjadi kesalahan data",
      result: error.message,
    });
  }
};

module.exports = {
  index,
  store,
};
