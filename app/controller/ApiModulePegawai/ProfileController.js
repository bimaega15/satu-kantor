const { validationResult } = require("express-validator");
var moment = require("moment");
const {
  pegawaiHelper,
  userMappingHelper,
  pegawaiHelper: { getPegawaiById },
} = require("../../helper/index");
const fs = require("fs");
const mv = require("mv");
const {
  Rekening,
  Izin,
  PegawaiJabatan,
  Jabatan,
  Cabang,
  PegawaiUnit,
  Unit,
  PegawaiKontrak,
  PegawaiKomponenGaji,
  KomponenGaji,
} = require("../../model");
const sequelize = require("../../config/db");

const index = async (req, res) => {
  try {
    const data = req.user.data;

    // get data maping
    const getPegawai = await pegawaiHelper.Pegawai.findOne({
      where: {
        id_pegawai: data.id_mapping,
        "$pegawai_unit.is_aktif$": 1,
        "$pegawai_jabatan.is_aktif$": 1,
        "$pegawai_kontrak.is_aktif$": 1,
      },
      include: [
        {
          model: Rekening,
          required: true,
        },
        {
          model: Izin,
          required: true,
        },
        {
          model: PegawaiJabatan,
          required: true,
          include: [
            {
              model: Jabatan,
              required: true,
            },
            {
              model: Cabang,
              required: true,
            },
          ],
        },
        {
          model: PegawaiUnit,
          required: true,
          include: [
            {
              model: Unit,
              required: true,
            },
          ],
        },
        {
          model: PegawaiKontrak,
          required: true,
          include: [
            {
              model: PegawaiKomponenGaji,
              include: [
                {
                  model: KomponenGaji,
                },
              ],
            },
          ],
        },
      ],
    });

    // get data jadwal
    return res.status(200).json({
      status: 200,
      message: "Berhasil mengambil data profile",
      result: getPegawai,
    });
  } catch (error) {
    return res.status(400).json({
      status: 400,
      message: "Terjadi kesalahan data",
      result: error.message,
    });
  }
};

const store = async (req, res) => {
  const data = req.user.data;
  // const t = await sequelize.transaction();

  try {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({
        status: 400,
        message: "Invalid form validation",
        result: errors.array(),
      });
    }

    // get data maping
    const { body } = req;

    let dateTime = moment().format("YYYY-MM-DD HH:mm:ss");
    let id_pegawai = data.id_mapping;
    let gambar_db = await uploadGambar(body.gambar, data.id_mapping);

    let update = await pegawaiHelper.Pegawai.update(
      {
        jenis_identitas: body.jenis_identitas,
        no_identitas: body.no_identitas,
        nama_lengkap: body.nama_lengkap,
        jenis_kelamin: body.jenis_kelamin,
        tanggal_lahir: moment(body.tanggal_lahir, "DD-MM-YYYY").format(
          "YYYY-MM-DD"
        ),
        tempat_lahir: body.tempat_lahir,
        status_perkawinan: body.status_perkawinan,
        agama: body.agama,
        pendidikan: body.pendidikan,
        alamat_domisili: body.alamat_domisili,
        alamat_ktp: body.alamat_ktp,
        no_kontak1: body.no_kontak1,
        no_kontak2: body.no_kontak2,
        email: body.email,
        no_pegawai: body.no_pegawai,
        user_update: data.id,
        time_update: dateTime,
        gambar: gambar_db,
      },
      {
        where: {
          id_pegawai: id_pegawai,
        },
        // transaction: t,
      }
    );

    // await t.commit();

    if (update) {
      return res.json({
        status: 200,
        message: "Berhasil update data",
      });
    } else {
      return res.json({
        status: 400,
        message: "Gagal update data",
      });
    }
  } catch (error) {
    // await t.rollback();
    return res.status(400).json({
      status: 400,
      message: "Terjadi kesalahan data",
      result: error.message,
    });
  }
};

const changePhotoProfile = async (req, res) => {
  // const t = await sequelize.transaction();

  try {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({
        status: 400,
        message: "Invalid form validation",
        result: errors.array(),
      });
    }

    const data = req.user.data;
    // get data maping
    const getMapping = await userMappingHelper.getJoinDataMapping(
      data.jenis_mapping,
      null,
      data.id_user_mapping
    );

    const { body } = req;

    let id_pegawai = getMapping.pegawai.id_pegawai;
    let gambar_db = await uploadGambar(body.gambar, id_pegawai);

    let update = await pegawaiHelper.Pegawai.update(
      {
        gambar: gambar_db,
      },
      {
        where: {
          id_pegawai: id_pegawai,
        },
        // transaction: t,
      }
    );
    // await t.commit();

    if (update) {
      return res.json({
        status: 200,
        message: "Berhasil ganti photo",
      });
    } else {
      return res.json({
        status: 400,
        message: "Gagal ganti photo",
      });
    }
  } catch (error) {
    // await t.rollback();
    return res.status(400).json({
      status: 400,
      message: "Terjadi kesalahan data",
      result: error.message,
    });
  }
};

const uploadGambar = async (responseGambar, id_pegawai = null) => {
  if (responseGambar.gambar != null) {
    const { gambar } = responseGambar;

    let pathOld = gambar.path;
    let name = gambar.name;
    let size = gambar.size;

    name =
      moment
        .duration(moment(moment().format("YYYY-MM-DD HH:mm:ss")))
        .asSeconds() +
      "_" +
      name.split(" ").join("-");
    pathNew = "public/image/pegawai/" + name;
    if (size > 0) {
      if (pathOld != null && pathNew != null) {
        if (id_pegawai != null) {
          await deleteGambar(id_pegawai);
        }

        mv(pathOld, pathNew, function (err) {
          if (err) {
            return err;
          }
        });

        const fileName = pathNew.split("/");
        const fileDb = fileName[fileName.length - 1];
        return fileDb;
      }
    }
  }

  if (id_pegawai != null) {
    let getPegawai = await getPegawaiById(id_pegawai);
    if (getPegawai.gambar != "default.png" && getPegawai.gambar != null) {
      return getPegawai.gambar;
    }
  }
  return "default.png";
};

const deleteGambar = async (id_pegawai = null) => {
  try {
    if (id_pegawai != null) {
      let getPegawai = await getPegawaiById(id_pegawai);
      if (getPegawai.gambar != "default.png" && getPegawai.gambar != null) {
        let unlink = "public/image/pegawai/" + getPegawai.gambar;
        if (fs.existsSync(unlink)) {
          fs.unlinkSync(unlink);
        }
      }
    }
  } catch (error) {
    return res.status(400).json({
      status: 400,
      message: "Terjadi kesalahan data",
      result: error.message,
    });
  }
};

module.exports = {
  index,
  store,
  changePhotoProfile,
};
