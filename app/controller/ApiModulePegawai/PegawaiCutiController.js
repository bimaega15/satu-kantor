const {
  pagination,
  pegawaiSaldoCutiHelper,
  pegawaiSaldoCutiHelper: { getPegawaiSaldoCuti },
  pengajuanCutiHelper: { getPengajuanCutiId, getPengajuanCuti },
  pengajuanCutiHelper,
} = require("../../helper/index");
const { validationResult } = require("express-validator");
const moment = require("moment");
const { Pegawai, Client, JenisCuti } = require("../../model");

const fs = require("fs");
const mv = require("mv");
const { Op } = require("sequelize");
const sequelize = require("../../config/db");

const index = async (req, res) => {
  let user = req.user.data;
  let getPegawai = await Pegawai.findOne({
    where: {
      id_client: user.id_mapping,
    },
  });
  req.query.client_id = getPegawai.id_client;
  try {
    // page
    const page =
      req.query.page == null || req.query.page == "" ? 1 : req.query.page;
    const limit =
      req.query.limit == null || req.query.limit == "" ? 10 : req.query.limit;
    const search = req.query.search;

    const halamanAkhir = page * limit;
    const halmaanAwal = halamanAkhir - limit;
    const offset = halamanAkhir;
    const skip = halmaanAwal;

    let id_pegawai = user.id_mapping;
    let getPegawai = await Pegawai.findOne({
      where: {
        id_pegawai: id_pegawai,
      },
    });
    let id_client = getPegawai.id_client;

    let getFilter = {};
    getFilter.id_pegawai_id = id_pegawai;
    getFilter = { ...getFilter, ...req.query };

    delete getFilter.page;
    delete getFilter.limit;
    delete getFilter.search;

    let pengajuanCuti = await getPengajuanCuti(
      limit,
      skip,
      null,
      getFilter,
      id_client
    );
    let model = await pengajuanCutiHelper.PengajuanCuti.count({
      where: {
        "$pegawai.client.id_client$": id_client,
        "$pegawai.id_pegawai$": id_pegawai,
      },
      include: [
        {
          model: Pegawai,
          include: [
            {
              model: Client,
              required: true,
            },
          ],
        },
      ],
    });

    if (getFilter != null) {
      let getModel = await getPengajuanCuti(null, null, null, getFilter);
      model = getModel.length;
    }
    if (search != null && search != "") {
      pengajuanCuti = await getPengajuanCuti(limit, skip, search, getFilter);
      let getModel = await getPengajuanCuti(null, null, search, getFilter);
      model = getModel.length;
    }

    // pagination
    const getPagination = pagination(page, model, limit);

    let keterangan = {
      from: skip + 1,
      to: offset,
      total: model,
    };

    let output = {
      data: pengajuanCuti,
      pagination: getPagination,
      keterangan: keterangan,
    };
    return res.status(200).json({
      status: 200,
      message: "Berhasil tangkap data",
      output: output,
    });
  } catch (error) {
    return res.status(400).json({
      status: 400,
      message: "Terjadi kesalahan",
      result: error.message,
    });
  }
};

const store = async (req, res) => {
  // const t = await sequelize.transaction();

  try {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({
        status: 400,
        message: "Invalid form validation",
        result: errors.array(),
      });
    }

    const data = req.user.data;
    const {
      id_jenis_cuti,
      tanggal_awal_cuti,
      tanggal_akhir_cuti,
      catatan,
      berkas,
    } = req.body;
    let getJenisCuti = await JenisCuti.findOne({
      where: {
        id_jenis_cuti: id_jenis_cuti,
      },
    });
    let tanggalAwalCuti = moment(tanggal_awal_cuti, "DD-MM-YYYY").format(
      "YYYY-MM-DD"
    );
    let tanggalAkhirCuti = moment(tanggal_akhir_cuti, "DD-MM-YYYY").format(
      "YYYY-MM-DD"
    );

    let tbJenisCuti = await JenisCuti.findOne({
      where: {
        id_jenis_cuti: id_jenis_cuti,
      },
    });
    let saldoJenisCuti = tbJenisCuti.saldo;
    let rangeBetweenDate = getRange(tanggalAwalCuti, tanggalAkhirCuti, "days");
    let rangeBetweenDateLength = rangeBetweenDate.length;

    if (rangeBetweenDateLength > saldoJenisCuti) {
      return res.status(400).json({
        status: 400,
        message: "Saldo jenis cuti pegawai tidak cukup",
        result: {
          jenis_cuti: {
            saldo: saldoJenisCuti,
          },
          pengajuan_cuti: {
            saldo: rangeBetweenDateLength,
          },
        },
      });
    }

    let getBerkas = await Berkas(berkas);
    let dateTime = moment().format("YYYY-MM-DD HH:mm:ss");
    let submit = {};
    submit.id_jenis_cuti = id_jenis_cuti;
    submit.id_pegawai = data.id_mapping;
    submit.tanggal_awal_cuti = tanggalAwalCuti;
    submit.tanggal_akhir_cuti = tanggalAkhirCuti;
    submit.catatan = catatan;
    submit.waktu_pengajuan = dateTime;
    submit.berkas = getBerkas;
    submit.user_create = data.id;
    submit.user_update = data.id;
    submit.time_create = dateTime;
    submit.time_update = dateTime;

    // insert pengajuan cuti
    let insertPengajuanCuti = await pengajuanCutiHelper.createPengajuanCuti(
      submit,
      {
        // transaction: t,
      }
    );
    // await t.commit();

    if (insertPengajuanCuti) {
      return res.status(200).json({
        status: 200,
        message: "Berhasil insert data",
      });
    } else {
      return res.status(200).json({
        status: 200,
        message: "Gagal insert data",
      });
    }
  } catch (error) {
    // await t.rollback();
    return res.status(400).json({
      status: 400,
      message: "Terjadi kesalahan data",
      result: error.message,
    });
  }
};

const saldoCuti = async (req, res) => {
  let user = req.user.data;
  let getPegawai = await Pegawai.findOne({
    where: {
      id_client: user.id_mapping,
    },
  });
  req.query.client_id = getPegawai.id_client;
  try {
    // page
    const page =
      req.query.page == null || req.query.page == "" ? 1 : req.query.page;
    const limit =
      req.query.limit == null || req.query.limit == "" ? 10 : req.query.limit;
    const search = req.query.search;

    const halamanAkhir = page * limit;
    const halamanAwal = halamanAkhir - limit;
    const offset = halamanAkhir;
    const skip = halamanAwal;

    let id_pegawai = user.id_mapping;
    let getPegawai = await Pegawai.findOne({
      where: {
        id_pegawai: id_pegawai,
      },
    });
    let id_client = getPegawai.id_client;

    let getFilter = {};
    getFilter.id_pegawai = id_pegawai;
    getFilter.is_aktif = 1;
    getFilter.saldo = {
      [Op.gt]: 0,
    };

    let varPegawaiSaldoCuti = await getPegawaiSaldoCuti(
      limit,
      skip,
      null,
      getFilter,
      id_client
    );

    let model = await pegawaiSaldoCutiHelper.PegawaiSaldoCuti.count({
      where: {
        "$pegawai.client.id_client$": id_client,
        id_pegawai: getFilter.id_pegawai,
      },

      include: [
        {
          model: Pegawai,
          include: [
            {
              model: Client,
              required: true,
            },
          ],
        },
      ],
    });
    if (getFilter != null) {
      let getModel = await getPegawaiSaldoCuti(
        null,
        null,
        null,
        getFilter,
        id_client
      );
      model = getModel.length;
    }
    if (search != null && search != "") {
      varPegawaiSaldoCuti = await getPegawaiSaldoCuti(
        limit,
        skip,
        search,
        getFilter,
        id_client
      );
      let getModel = await getPegawaiSaldoCuti(
        null,
        null,
        search,
        getFilter,
        id_client
      );
      model = getModel.length;
    }

    // pagination
    const getPagination = pagination(page, model, limit);

    let keterangan = {
      from: skip + 1,
      to: offset,
      total: model,
    };

    let output = {
      data: varPegawaiSaldoCuti,
      pagination: getPagination,
      keterangan: keterangan,
    };

    return res.status(200).json({
      status: 200,
      message: "Berhasil tangkap data",
      output: output,
    });
  } catch (error) {
    return res.status(400).json({
      status: 400,
      message: "Terjadi kesalahan data",
      result: error.message,
    });
  }
};

const uploadBerkas = async (responseBerkas, id_pengajuan_cuti = null) => {
  if (responseBerkas.berkas != null) {
    const { berkas } = responseBerkas;

    let pathOld = berkas.path;
    let name = berkas.name;
    let size = berkas.size;

    name =
      moment
        .duration(moment(moment().format("YYYY-MM-DD HH:mm:ss")))
        .asSeconds() +
      "_" +
      name.split(" ").join("-");
    pathNew = "public/image/pengajuanCuti/" + name;
    if (size > 0) {
      if (pathOld != null && pathNew != null) {
        if (id_pengajuan_cuti != null) {
          await deleteBerkas(id_pengajuan_cuti);
        }

        mv(pathOld, pathNew, function (err) {
          if (err) {
            return err;
          }
        });

        const fileName = pathNew.split("/");
        const fileDb = fileName[fileName.length - 1];
        return fileDb;
      }
    }
  }

  if (id_pengajuan_cuti != null) {
    let getPengajuanCuti = await getPengajuanCutiId(id_pengajuan_cuti);
    if (
      getPengajuanCuti.berkas != "default.png" &&
      getPengajuanCuti.berkas != null
    ) {
      return getPengajuanCuti.berkas;
    }
  }
  return "default.png";
};

const deleteBerkas = async (id_pengajuan_cuti = null) => {
  try {
    if (id_pengajuan_cuti != null) {
      let getPengajuanCuti = await getPengajuanCutiId(id_pengajuan_cuti);
      if (
        getPengajuanCuti.berkas != "default.png" &&
        getPengajuanCuti.berkas != null
      ) {
        let unlink = "public/image/pengajuanCuti/" + getPengajuanCuti.berkas;
        if (fs.existsSync(unlink)) {
          fs.unlinkSync(unlink);
        }
      }
    }
  } catch (error) {
    return res.status(400).json({
      status: 400,
      message: "Terjadi kesalahan data",
      result: error.message,
    });
  }
};

const update = async (req, res) => {
  // const t = await sequelize.transaction();

  try {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({
        status: 400,
        message: "Invalid form validation",
        result: errors.array(),
      });
    }

    const data = req.user.data;
    const {
      id_jenis_cuti,
      tanggal_awal_cuti,
      tanggal_akhir_cuti,
      catatan,
      berkas,
    } = req.body;
    let getJenisCuti = await JenisCuti.findOne({
      where: {
        id_jenis_cuti: id_jenis_cuti,
      },
    });
    let tanggalAwalCuti = moment(tanggal_awal_cuti, "DD-MM-YYYY").format(
      "YYYY-MM-DD"
    );
    let tanggalAkhirCuti = moment(tanggal_akhir_cuti, "DD-MM-YYYY").format(
      "YYYY-MM-DD"
    );
    let tbJenisCuti = await JenisCuti.findOne({
      where: {
        id_jenis_cuti: id_jenis_cuti,
      },
    });
    let saldoJenisCuti = tbJenisCuti.saldo;
    let rangeBetweenDate = getRange(tanggalAwalCuti, tanggalAkhirCuti, "days");
    let rangeBetweenDateLength = rangeBetweenDate.length;
    if (rangeBetweenDateLength > saldoJenisCuti) {
      return res.status(400).json({
        status: 400,
        message: "Saldo jenis cuti pegawai tidak cukup",
        result: {
          jenis_cuti: {
            saldo: saldoJenisCuti,
          },
          pengajuan_cuti: {
            saldo: rangeBetweenDateLength,
          },
        },
      });
    }

    // let tanggalAkhirCuti = moment(tanggalAwalCuti)
    //   .add(getJenisCuti.saldo, "days")
    //   .format("YYYY-MM-DD");

    let id_pengajuan_cuti = req.params.id_pengajuan_cuti;
    let getBerkas = await uploadBerkas(berkas, id_pengajuan_cuti);

    let dateTime = moment().format("YYYY-MM-DD HH:mm:ss");
    let submit = {};
    submit.id_jenis_cuti = id_jenis_cuti;
    submit.id_pegawai = data.id_mapping;
    submit.tanggal_awal_cuti = tanggalAwalCuti;
    submit.tanggal_akhir_cuti = tanggalAkhirCuti;
    submit.catatan = catatan;
    submit.waktu_pengajuan = dateTime;
    submit.berkas = getBerkas;
    submit.user_update = data.id;
    submit.time_update = dateTime;

    // update pengajuan cuti
    let updatePengajuanCuti = await pengajuanCutiHelper.updatePengajuanCuti(
      submit,
      id_pengajuan_cuti,
      {
        // transaction: t,
      }
    );
    // await t.commit();
    if (updatePengajuanCuti) {
      return res.status(200).json({
        status: 200,
        message: "Berhasil update data",
      });
    } else {
      return res.status(200).json({
        status: 200,
        message: "Gagal update data",
      });
    }
  } catch (error) {
    // await t.rollback();
    return res.status(400).json({
      status: 400,
      message: "Terjadi kesalahan data",
      result: error.message,
    });
  }
};

const edit = async (req, res) => {
  try {
    const id_pengajuan_cuti = req.params.id_pengajuan_cuti;
    const getPengajuanCuti = await pengajuanCutiHelper.getPengajuanCutiId(
      id_pengajuan_cuti
    );

    if (getPengajuanCuti) {
      return res.status(200).json({
        status: 200,
        message: "Berhasil mengambil data pengajuan cuti",
        result: getPengajuanCuti,
      });
    } else {
      return res.status(400).json({
        status: 400,
        message: "Gagal mengambil data pengajuan cuti",
      });
    }
  } catch (error) {
    return res.status(400).json({
      status: 400,
      message: "Terjadi kesalahan data",
      result: error.message,
    });
  }
};

const deleteData = async (req, res) => {
  // const t = await sequelize.transaction();

  try {
    const id_pengajuan_cuti = req.params.id_pengajuan_cuti;
    await deleteBerkas(id_pengajuan_cuti);
    const getPengajuanCuti = await pengajuanCutiHelper.deletePengajuanCuti(
      id_pengajuan_cuti,
      {
        // transaction: t,
      }
    );

    // await t.commit();
    if (getPengajuanCuti) {
      return res.status(200).json({
        status: 200,
        message: "Berhasil menghapus data pengajuan cuti",
      });
    } else {
      return res.status(400).json({
        status: 400,
        message: "Gagal menghapus data pengajuan cuti",
      });
    }
  } catch (error) {
    // await t.rollback();
    return res.status(400).json({
      status: 400,
      message: "Terjadi kesalahan data",
      result: error.message,
    });
  }
};

const getRange = (startDate, endDate, type) => {
  let fromDate = moment(startDate);
  let toDate = moment(endDate);
  let diff = toDate.diff(fromDate, type);
  let range = [];
  for (let i = 0; i < diff; i++) {
    range.push(moment(startDate).add(i, type));
  }
  return range;
};

module.exports = {
  saldoCuti,
  store,
  index,
  edit,
  update,
  deleteData,
};
