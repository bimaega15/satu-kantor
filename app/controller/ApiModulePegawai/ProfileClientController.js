const { validationResult } = require("express-validator");
const { clientHelper, userMappingHelper } = require("../../helper/index");
const moment = require("moment");
const sequelize = require("../../config/db");

const store = async (req, res) => {
  // const t = await sequelize.transaction();

  try {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({
        status: 400,
        message: "Invalid form validation",
        result: errors.array(),
      });
    }

    const data = req.user.data;
    // get data maping
    const getMapping = await userMappingHelper.getJoinDataMapping(
      data.jenis_mapping,
      null,
      data.id_user_mapping
    );
    const { body } = req;
    const id_client = getMapping.client.id_client;
    let time_update = moment().format("YYYY-MM-DD HH:mm:ss");
    let update = await clientHelper.Client.update(
      {
        nama_client: body.nama_client,
        alamat: body.alamat,
        kontak: body.kontak,
        time_update: time_update,
      },
      {
        where: {
          id_client: id_client,
        },
        // transaction: t,
      }
    );

    // await t.commit();
    if (update) {
      return res.json({
        status: 200,
        message: "Berhasil update data",
      });
    } else {
      return res.json({
        status: 400,
        message: "Gagal update data",
      });
    }
  } catch (error) {
    // await t.rollback();
    return res.status(400).json({
      status: 400,
      message: "Terjadi kesalahan data",
      result: error.message,
    });
  }
};

module.exports = {
  store,
};
