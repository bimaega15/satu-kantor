const { validationResult } = require("express-validator");
const {
  penugasanHelper,
  penugasanHelper: { getPenugasanId },
  pagination,
  penugasanFileHelper,
  penugasanStatusHelper,
} = require("../../helper/index");
const moment = require("moment");
const {
  Tugas,
  PenugasanFile,
  PenugasanStatus,
  Cabang,
  Pegawai,
  LokasiTugas,
} = require("../../model");
const sequelize = require("../../config/db");
const mv = require("mv");

const index = async (req, res) => {
  try {
    let user = req.user.data;
    let getPegawai = await Pegawai.findOne({
      where: {
        id_client: user.id_mapping,
      },
    });
    req.query.client_id = getPegawai.id_client;

    if (req.xhr) {
      // page
      const page =
        req.query.page == null || req.query.page == "" ? 1 : req.query.page;
      const limit =
        req.query.limit == null || req.query.limit == "" ? 10 : req.query.limit;
      const search = req.query.search;

      const halamanAkhir = page * limit;
      const halamanAwal = halamanAkhir - limit;
      const offset = halamanAkhir;
      const skip = halamanAwal;

      let getFilter = {
        id_client: user.id_mapping,
      };
      getFilter = { ...getFilter, ...req.query.filter };
      let penugasan = await penugasanHelper.getPenugasan(
        limit,
        skip,
        null,
        getFilter
      );
      let model = await penugasanHelper.Penugasan.count({
        where: {
          id_client: user.id_mapping,
        },
      });
      if (getFilter != null) {
        let getModel = await penugasanHelper.getPenugasan(
          null,
          null,
          null,
          getFilter
        );
        model = getModel.length;
      }

      if (search != null && search != "") {
        penugasan = await penugasanHelper.getPenugasan(
          limit,
          skip,
          search,
          getFilter
        );
        let getModel = await penugasanHelper.getPenugasan(
          null,
          null,
          search,
          getFilter
        );
        model = getModel.length;
      }

      // pagination
      const getPagination = pagination(page, model, limit);

      let keterangan = {
        from: skip + 1,
        to: offset,
        total: model,
      };

      let output = {
        data: penugasan,
        pagination: getPagination,
        keterangan: keterangan,
      };
      return res.status(200).json({
        status: 200,
        message: "Berhasil tangkap data",
        output: output,
      });
    }

    // breadcrumb
    let breadcrumb = [];
    breadcrumb.push({
      label: "Dashboard",
      url: "/admin/dashboard",
    });
    breadcrumb.push({
      label: "Penugasan",
      url: "/pegawai/penugasan",
      isActive: "active",
    });

    let cabang = await Cabang.findAll({
      where: {
        id_client: user.id_mapping,
      },
    });
    let pegawai = await Pegawai.findAll({
      where: {
        id_client: user.id_mapping,
      },
    });
    let tugas = await Tugas.findAll({
      where: {
        id_client: user.id_mapping,
      },
    });
    let lokasiTugas = await LokasiTugas.findAll({
      where: {
        id_client: user.id_mapping,
      },
    });

    res.render("./modulPegawai/penugasan/index", {
      title: "Penugasan",
      breadcrumb: breadcrumb,
      currentUrl: req.originalUrl,
      cabang: cabang,
      pegawai: pegawai,
      tugas: tugas,
      lokasiTugas: lokasiTugas,
    });
  } catch (error) {
    return res.status(400).json({
      status: 400,
      message: "Terjadi kesalahan data",
      result: error.message,
    });
  }
};

const store = async (req, res) => {
  let user = req.user.data;
  let getPegawai = await Pegawai.findOne({
    where: {
      id_client: user.id_mapping,
    },
  });
  req.query.client_id = getPegawai.id_client;
  // const t = await sequelize.transaction();

  try {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({
        status: 400,
        message: "Invalid form validation",
        result: errors.array(),
      });
    } else {
      const response = req.body;

      if (response.page == "add") {
        let dateTime = moment().format("YYYY-MM-DD HH:mm:ss");
        let getTugas = await Tugas.findOne({
          where: {
            id_tugas: response.id_tugas,
          },
        });
        let insert = await penugasanHelper.createPenugasan(
          {
            id_client: user.id_mapping,
            id_cabang: response.id_cabang,
            id_pegawai: response.id_pegawai,
            tanggal_penugasan: moment(
              response.tanggal_penugasan,
              "DD-MM-YYYY"
            ).format("YYYY-MM-DD"),
            jenis_penugasan: response.jenis_penugasan,
            id_tugas: response.id_tugas,
            id_lokasi_tugas: response.id_lokasi_tugas,
            nama_tugas: getTugas.nama_tugas,
            waktu_mulai: response.waktu_mulai,
            waktu_selesai: response.waktu_selesai,
            is_aktif: response.is_aktif,
            user_create: user.id,
            user_update: user.id,
            time_create: dateTime,
            time_update: dateTime,
          },
          {
            // transaction: t,
          }
        );
        let id_penugasan = insert.id_penugasan;

        // penugasan file
        let status = response.status;
        status = status.split(",");
        let pushPenugasanFile = [];
        let pushPenugasanStatus = [];

        let getDeskripsi = response.deskripsi;
        getDeskripsi = getDeskripsi.split(",");

        let getKeterangan = response.keterangan;
        getKeterangan = getKeterangan.split(",");

        let getNamaFile = response.nama_file;
        for (let i = 0; i < status.length; i++) {
          let vStatus = status[i];

          let nama_file = getNamaFile.nama_file;
          if (nama_file != undefined) {
            nama_file = await uploadNamaFile(nama_file[i], null, true);
          } else {
            nama_file = "default.png";
          }

          pushPenugasanFile.push({
            id_penugasan: id_penugasan,
            nama_file: nama_file,
            deskripsi: getDeskripsi[i],
            user_create: user.id,
            user_update: user.id,
            time_create: dateTime,
            time_update: dateTime,
          });

          pushPenugasanStatus.push({
            id_penugasan: id_penugasan,
            waktu_status: dateTime,
            status: vStatus,
            keterangan: getKeterangan[i],
            user_create: user.id,
            user_update: user.id,
            time_create: dateTime,
            time_update: dateTime,
          });
        }

        let insertPenugasanStatus = await PenugasanStatus.bulkCreate(
          pushPenugasanStatus,
          {
            // transaction: t,
          }
        );

        let insertPenugasanFile = await PenugasanFile.bulkCreate(
          pushPenugasanFile,
          {
            // transaction: t,
          }
        );

        // await t.commit();

        if (insert || insertPenugasanFile || insertPenugasanStatus) {
          return res.status(200).json({
            status: 200,
            message: "Berhasil insert data",
            result: response,
          });
        } else {
          return res.status(400).json({
            status: 400,
            message: "Gagal insert data",
          });
        }
      } else {
        let dateTime = moment().format("YYYY-MM-DD HH:mm:ss");
        let id_penugasan = response.id_penugasan;
        let getTugas = await Tugas.findOne({
          where: {
            id_tugas: response.id_tugas,
          },
        });
        let update = await penugasanHelper.updatePenugasan(
          {
            id_client: user.id_mapping,
            id_cabang: response.id_cabang,
            id_pegawai: response.id_pegawai,
            tanggal_penugasan: moment(
              response.tanggal_penugasan,
              "DD-MM-YYYY"
            ).format("YYYY-MM-DD"),
            jenis_penugasan: response.jenis_penugasan,
            id_tugas: response.id_tugas,
            id_lokasi_tugas: response.id_lokasi_tugas,
            nama_tugas: getTugas.nama_tugas,
            waktu_mulai: response.waktu_mulai,
            waktu_selesai: response.waktu_selesai,
            is_aktif: response.is_aktif,
            user_update: user.id,
            time_update: dateTime,
          },
          id_penugasan,
          {
            // transaction: t,
          }
        );

        // penugasan file
        let checkPenugasanFile = await PenugasanFile.findAll({
          where: {
            id_penugasan: id_penugasan,
          },
        });
        let timeCreatePenugasanFile = checkPenugasanFile[0].time_create;
        checkPenugasanFile.map(async (v, i) => {
          await deleteNamaFile(v.id_penugasan_file);
        });
        let userCreatePenugasanFile = checkPenugasanFile[0].user_create;

        let checkPenugasanStatus = await PenugasanStatus.findAll({
          where: {
            id_penugasan: id_penugasan,
          },
        });
        let timeCreatePenugasanStatus = checkPenugasanStatus[0].time_create;
        let userCreatePenugasanStatus = checkPenugasanStatus[0].user_create;

        if (checkPenugasanFile.length > 0) {
          await PenugasanFile.destroy({
            where: {
              id_penugasan: id_penugasan,
            },
            // transaction: t,
          });
        }
        if (checkPenugasanStatus.length > 0) {
          await PenugasanStatus.destroy({
            where: {
              id_penugasan: id_penugasan,
            },
            // transaction: t,
          });
        }

        let deskripsi = response.deskripsi;
        let pushPenugasanFile = [];
        let pushPenugasanStatus = [];

        deskripsi.map(async (v, i) => {
          let nama_file = "";
          if (nama_file != undefined) {
            nama_file = await uploadNamaFile(response.nama_file[i]);
          } else {
            nama_file = "default.png";
          }

          pushPenugasanFile.push({
            id_penugasan: id_penugasan,
            nama_file: nama_file,
            deskripsi: response.deskripsi[i],
            user_create: userCreatePenugasanFile,
            user_update: user.id,
            time_create: timeCreatePenugasanFile,
            time_update: dateTime,
          });

          pushPenugasanStatus.push({
            id_penugasan: id_penugasan,
            waktu_status: dateTime,
            status: response.status[i],
            keterangan: response.keterangan[i],
            user_create: userCreatePenugasanStatus,
            user_update: user.id,
            time_create: timeCreatePenugasanStatus,
            time_update: dateTime,
          });
        });

        // penugasan file
        let updatePenugasanFile = await PenugasanFile.bulkCreate(
          pushPenugasanFile,
          {
            // transaction: t,
          }
        );
        let updatePenugasanStatus = await PenugasanStatus.bulkCreate(
          pushPenugasanStatus,
          {
            // transaction: t,
          }
        );

        // await t.commit();
        if (update || updatePenugasanFile || updatePenugasanStatus) {
          return res.json({
            status: 200,
            message: "Berhasil update data",
            result: response,
          });
        } else {
          return res.json({
            status: 400,
            message: "Gagal update data",
          });
        }
      }
    }
  } catch (error) {
    // await t.rollback();
    return res.status(400).json({
      status: 400,
      message: "Terjadi kesalahan data",
      result: error.message,
    });
  }
};

const edit = async (req, res) => {
  try {
    const id_penugasan = req.params.id_penugasan;
    const getPenugasan = await penugasanHelper.getPenugasanId(id_penugasan);
    if (getPenugasan) {
      return res.status(200).json({
        status: 200,
        message: "Berhasil mengambil data penugasan",
        result: getPenugasan,
      });
    } else {
      return res.status(400).json({
        status: 400,
        message: "Gagal mengambil data penugasan",
      });
    }
  } catch (error) {
    return res.status(400).json({
      status: 400,
      message: "Terjadi kesalahan data",
      result: error.message,
    });
  }
};

const deleteData = async (req, res) => {
  // const t = await sequelize.transaction();

  try {
    const id_penugasan = req.params.id_penugasan;
    let getPenugasanFile = await PenugasanFile.findAll({
      where: {
        id_penugasan: id_penugasan,
      },
    });
    getPenugasanFile.map(async (v, i) => {
      await deleteNamaFile(v.id_penugasan_file);
    });

    const getPenugasan = await penugasanHelper.deletePenugasan(id_penugasan, {
      // transaction: t,
    });
    // await t.commit();
    if (getPenugasan) {
      return res.status(200).json({
        status: 200,
        message: "Berhasil menghapus data penugasan",
        result: getPenugasan,
      });
    } else {
      return res.status(400).json({
        status: 400,
        message: "Gagal menghapus data penugasan",
      });
    }
  } catch (error) {
    // await t.rollback();
    return res.status(400).json({
      status: 400,
      message: "Terjadi kesalahan data",
      result: error.message,
    });
  }
};

const uploadNamaFile = async (
  responseNamaFile,
  id_penugasan_file = null,
  multiple = false
) => {
  if (multiple == true) {
    let getNamaFile = {};
    getNamaFile.nama_file = responseNamaFile;
    responseNamaFile = getNamaFile;
  }

  if (responseNamaFile.nama_file != null) {
    const { nama_file } = responseNamaFile;

    let pathOld = nama_file.path;
    let name = nama_file.name;
    let size = nama_file.size;

    name =
      moment
        .duration(moment(moment().format("YYYY-MM-DD HH:mm:ss")))
        .asSeconds() +
      "_" +
      name.split(" ").join("-");
    pathNew = "public/image/filePenugasan/" + name;
    if (size > 0) {
      if (pathOld != null && pathNew != null) {
        if (id_penugasan_file != null) {
          await deleteNamaFile(id_penugasan_file);
        }

        mv(pathOld, pathNew, function (err) {
          if (err) {
            return err;
          }
        });

        const fileName = pathNew.split("/");
        const fileDb = fileName[fileName.length - 1];
        return fileDb;
      }
    }
  }

  if (id_penugasan_file != null) {
    let getPenugasan = await getPenugasanId(id_penugasan_file);
    if (
      getPenugasan.nama_file != "default.png" &&
      getPenugasan.nama_file != null
    ) {
      return getPenugasan.nama_file;
    }
  }
  return "default.png";
};

const deleteNamaFile = async (id_penugasan_file = null) => {
  try {
    if (id_penugasan_file != null) {
      let getPenugasan = await penugasanFileHelper.getPenugasanFileId(
        id_penugasan_file
      );
      if (
        getPenugasan.nama_file != "default.png" &&
        getPenugasan.nama_file != null
      ) {
        let unlink = "public/image/filePenugasan/" + getPenugasan.nama_file;
        if (fs.existsSync(unlink)) {
          fs.unlinkSync(unlink);
        }
      }
    }
  } catch (error) {
    return res.status(400).json({
      status: 400,
      message: "Terjadi kesalahan data",
      result: error.message,
    });
  }
};

module.exports = {
  index,
  store,
  edit,
  deleteData,
};
