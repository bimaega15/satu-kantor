const { validationResult } = require("express-validator");
const {
  komponenGajiHelper,
  pagination,
  userMappingHelper,
} = require("../../helper/index");
const moment = require("moment");
const readXlsxFile = require("read-excel-file/node");
const { KomponenGaji, Pegawai } = require("../../model");
const sequelize = require("../../config/db");

const index = async (req, res) => {
  let user = req.user.data;
  let getPegawai = await Pegawai.findOne({
    where: {
      id_client: user.id_mapping,
    },
  });
  req.query.client_id = getPegawai.id_client;

  try {
    if (req.xhr) {
      let id_client = req.query.client_id;
      if (user.jenis_mapping == "client") {
        id_client = user.id_mapping;
      }
      // page
      const page =
        req.query.page == null || req.query.page == "" ? 1 : req.query.page;
      const limit =
        req.query.limit == null || req.query.limit == "" ? 10 : req.query.limit;
      const search = req.query.search;

      const halamanAkhir = page * limit;
      const halmaanAwal = halamanAkhir - limit;
      const offset = halamanAkhir;
      const skip = halmaanAwal;

      let getFilter = req.query.filter;
      let komponenGaji = await komponenGajiHelper.getKomponenGaji(
        limit,
        skip,
        null,
        id_client,
        getFilter
      );
      let model = await komponenGajiHelper.KomponenGaji.count({
        where: {
          id_client: user.id_mapping,
        },
      });
      if (getFilter != null) {
        let getModel = await komponenGajiHelper.getKomponenGaji(
          null,
          null,
          null,
          id_client,
          getFilter
        );
        model = getModel.length;
      }

      if (search != null && search != "") {
        komponenGaji = await komponenGajiHelper.getKomponenGaji(
          null,
          null,
          search,
          id_client,
          getFilter
        );
        let getModel = await komponenGajiHelper.getKomponenGaji(
          null,
          null,
          search,
          id_client,
          getFilter
        );
        model = getModel.length;
      }

      // pagination
      const getPagination = pagination(page, model, limit);

      let keterangan = {
        from: skip + 1,
        to: offset,
        total: model,
      };

      let output = {
        data: komponenGaji,
        pagination: getPagination,
        keterangan: keterangan,
      };
      return res.status(200).json({
        status: 200,
        message: "Berhasil tangkap data",
        output: output,
      });
    }
    let id_client = req.query.client_id;
    if (user.jenis_mapping == "client") {
      id_client = user.id_mapping;
    }

    // breadcrumb
    let breadcrumb = [];
    if (user.jenis_mapping == "admin") {
      breadcrumb.push({ label: "Home", url: "/admin/dashboard", isActive: "" });
      breadcrumb.push({
        label: "Client",
        url: "/admin/client",
      });
      breadcrumb.push({
        label: "Dashboard Client",
        url: "/admin/dashboardClient?client_id=" + id_client,
      });
      breadcrumb.push({
        label: "Komponen Gaji",
        url: "/admin/komponenGaji?client_id=" + id_client,
        isActive: "active",
      });
    } else {
      breadcrumb.push({ label: "Home", url: "/admin/dashboard", isActive: "" });
      breadcrumb.push({
        label: "Komponen Gaji",
        url: "/admin/komponenGaji",
        isActive: "active",
      });
    }

    res.render("./moduleMaster/komponenGaji/index", {
      title: "Komponen Gaji",
      breadcrumb: breadcrumb,
      currentUrl: req.originalUrl,
      id_client: id_client,
      client_id: id_client,
    });
  } catch (error) {
    return res.status(400).json({
      status: 400,
      message: "Gagal menghapus data komponenGaji",
      result: error.message,
    });
  }
};

const store = async (req, res) => {
  let user = req.user.data;
  let getPegawai = await Pegawai.findOne({
    where: {
      id_client: user.id_mapping,
    },
  });
  req.query.client_id = getPegawai.id_client;
  // const t = await sequelize.transaction();

  try {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({
        status: 400,
        message: "Invalid form validation",
        result: errors.array(),
      });
    } else {
      const response = req.body;
      if (response.page == "add") {
        const is_aktif = response.is_aktif == 0 ? 0 : 1;

        let dateTime = moment().format("YYYY-MM-DD HH:mm:ss");
        let data = {
          id_client: response.id_client,
          komponen_gaji: response.komponen_gaji,
          jenis: response.jenis,
          kelompok_komponen: response.kelompok_komponen,
          komponen: response.komponen,
          is_aktif: is_aktif,
          user_create: user.id,
          user_update: user.id,
          time_create: dateTime,
          time_update: dateTime,
        };
        let insert = await komponenGajiHelper.KomponenGaji.create(data, {
          // transaction: t,
        });

        // await t.commit();

        if (insert) {
          return res.status(200).json({
            status: 200,
            message: "Berhasil insert data",
            result: response,
          });
        } else {
          return res.status(400).json({
            status: 400,
            message: "Gagal insert data",
          });
        }
      } else {
        let dateTime = moment().format("YYYY-MM-DD HH:mm:ss");
        let id_komponen_gaji = response.id_komponen_gaji;
        const is_aktif = response.is_aktif == 0 ? 0 : 1;

        let data = {
          id_client: response.id_client,
          komponen_gaji: response.komponen_gaji,
          jenis: response.jenis,
          kelompok_komponen: response.kelompok_komponen,
          komponen: response.komponen,
          is_aktif: is_aktif,
          user_update: user.id,
          time_update: dateTime,
        };
        let update = await komponenGajiHelper.KomponenGaji.update(data, {
          where: {
            id_komponen_gaji: id_komponen_gaji,
          },
          // transaction: t,
        });

        // await t.commit();

        if (update) {
          return res.json({
            status: 200,
            message: "Berhasil update data",
            result: response,
          });
        } else {
          return res.json({
            status: 400,
            message: "Gagal update data",
          });
        }
      }
    }
  } catch (error) {
    // await t.rollback();
    return res.status(400).json({
      status: 400,
      message: "Gagal insert data",
      result: error,
    });
  }
};

const edit = async (req, res) => {
  try {
    const id_komponen_gaji = req.params.id_komponen_gaji;
    const getKomponenGaji = await komponenGajiHelper.KomponenGaji.findOne({
      where: { id_komponen_gaji: id_komponen_gaji },
    });
    if (getKomponenGaji) {
      return res.status(200).json({
        status: 200,
        message: "Berhasil mengambil data komponenGaji",
        result: getKomponenGaji,
      });
    } else {
      return res.status(400).json({
        status: 400,
        message: "Gagal mengambil data komponenGaji",
      });
    }
  } catch (error) {
    return res.status(400).json({
      status: 400,
      message: "Gagal menghapus data komponenGaji",
      result: error.message,
    });
  }
};

const deleteData = async (req, res) => {
  // const t = await sequelize.transaction();

  try {
    const id_komponen_gaji = req.params.id_komponen_gaji;
    const getKomponenGaji = await komponenGajiHelper.KomponenGaji.destroy({
      where: {
        id_komponen_gaji: id_komponen_gaji,
      },
      // transaction: t,
    });

    // await t.commit();
    if (getKomponenGaji) {
      return res.status(200).json({
        status: 200,
        message: "Berhasil menghapus data komponenGaji",
        result: getKomponenGaji,
      });
    } else {
      return res.status(400).json({
        status: 400,
        message: "Gagal menghapus data komponenGaji",
      });
    }
  } catch (error) {
    // await t.rollback();
    return res.status(400).json({
      status: 400,
      message: "Gagal menghapus data komponenGaji",
      result: error.message,
    });
  }
};

const importData = async (req, res) => {
  let user = req.user.data;
  let getPegawai = await Pegawai.findOne({
    where: {
      id_client: user.id_mapping,
    },
  });
  req.query.client_id = getPegawai.id_client;
  // const t = await sequelize.transaction();
  try {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({
        status: 400,
        message: "Invalid form validation",
        result: errors.array(),
      });
    }

    const { importData } = req.body.importData;

    readXlsxFile(importData.path).then(async (rows) => {
      let pushData = [];
      let dateTime = moment().format("YYYY-MM-DD HH:mm:ss");
      rows.map((v, i) => {
        if (i > 0) {
          if (v[1] != null) {
            pushData.push({
              id_client: user.id_mapping,
              komponen_gaji: v[1],
              jenis: v[2],
              kelompok_komponen: v[3],
              komponen: v[4],
              is_aktif: 1,
              user_create: user.id,
              user_update: user.id,
              time_create: dateTime,
              time_update: dateTime,
            });
          }
        }
      });

      let importKomponenGaji = await KomponenGaji.bulkCreate(pushData, {
        // transaction: t,
      });
      // await t.commit();
      if (importKomponenGaji) {
        return res.status(200).json({
          status: 200,
          message: "Berhasil import " + pushData.length + " data cabang",
          result: req.body,
        });
      } else {
        return res.status(400).json({
          status: 400,
          message: "Gagal import " + pushData.length + " data cabang",
        });
      }
    });
  } catch (error) {
    // await t.rollback();
    return res.status(400).json({
      status: 400,
      message: "Terjadi kesalahan data",
      result: error.message,
    });
  }
};

module.exports = {
  index,
  store,
  edit,
  deleteData,
  importData,
};
