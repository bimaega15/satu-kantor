const { validationResult } = require("express-validator");
const { lokasiTugasHelper, pagination } = require("../../helper/index");
const moment = require("moment");
const { Cabang, LokasiTugas } = require("../../model");
const QRCode = require("qrcode");
const sequelize = require("../../config/db");

const index = async (req, res) => {
  try {
    let user = req.user.data;
    let getPegawai = await Pegawai.findOne({
      where: {
        id_client: user.id_mapping,
      },
    });
    req.query.client_id = getPegawai.id_client;

    if (req.xhr) {
      // page
      const page =
        req.query.page == null || req.query.page == "" ? 1 : req.query.page;
      const limit =
        req.query.limit == null || req.query.limit == "" ? 10 : req.query.limit;
      const search = req.query.search;

      const halamanAkhir = page * limit;
      const halamanAwal = halamanAkhir - limit;
      const offset = halamanAkhir;
      const skip = halamanAwal;

      let getFilter = {
        id_client: user.id_mapping,
      };
      getFilter = { ...getFilter, ...req.query.filter };
      let lokasiTugas = await lokasiTugasHelper.getLokasiTugas(
        limit,
        skip,
        null,
        getFilter
      );
      let model = await lokasiTugasHelper.LokasiTugas.count({
        where: {
          id_client: user.id_mapping,
        },
      });
      if (getFilter != null) {
        let getModel = await lokasiTugasHelper.getLokasiTugas(
          null,
          null,
          null,
          getFilter
        );
        model = getModel.length;
      }

      if (search != null && search != "") {
        lokasiTugas = await lokasiTugasHelper.getLokasiTugas(
          limit,
          skip,
          search,
          getFilter
        );
        let getModel = await lokasiTugasHelper.getLokasiTugas(
          null,
          null,
          search,
          getFilter
        );
        model = getModel.length;
      }

      // pagination
      const getPagination = pagination(page, model, limit);

      let keterangan = {
        from: skip + 1,
        to: offset,
        total: model,
      };

      let output = {
        data: lokasiTugas,
        pagination: getPagination,
        keterangan: keterangan,
      };

      return res.status(200).json({
        status: 200,
        message: "Berhasil tangkap data",
        output: output,
      });
    }

    // breadcrumb
    let breadcrumb = [];
    breadcrumb.push({
      label: "Dashboard",
      url: "/admin/dashboard",
    });
    breadcrumb.push({
      label: "Lokasi Tugas",
      url: "/admin/lokasiTugas",
      isActive: "active",
    });

    let cabang = await Cabang.findAll({
      where: {
        id_client: user.id_mapping,
      },
    });

    res.render("./moduleMaster/lokasiTugas/index", {
      title: "Lokasi Tugas",
      breadcrumb: breadcrumb,
      currentUrl: req.originalUrl,
      cabang: cabang,
    });
  } catch (error) {
    return res.status(400).json({
      status: 400,
      message: "Terjadi kesalahan data",
      result: error.message,
    });
  }
};

const store = async (req, res) => {
  let user = req.user.data;
  let getPegawai = await Pegawai.findOne({
    where: {
      id_client: user.id_mapping,
    },
  });
  req.query.client_id = getPegawai.id_client;
  // const t = await sequelize.transaction();

  try {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({
        status: 400,
        message: "Invalid form validation",
        result: errors.array(),
      });
    } else {
      const response = req.body;
      if (response.page == "add") {
        let dateTime = moment().format("YYYY-MM-DD HH:mm:ss");
        let time = moment().format("YYYYMMDDHHmmss");
        let random = Math.floor(Math.random() * 10000 + 1);
        let insert = await lokasiTugasHelper.createLokasiTugas(
          {
            id_client: user.id_mapping,
            id_cabang: response.id_cabang,
            nama_lokasi: response.nama_lokasi,
            longitude: response.longitude,
            latitude: response.latitude,
            qr_code: time + random,
            is_aktif: response.is_aktif,
            user_create: user.id,
            user_update: user.id,
            time_create: dateTime,
            time_update: dateTime,
          },
          {
            // transaction: t,
          }
        );
        // await t.commit();

        if (insert) {
          return res.status(200).json({
            status: 200,
            message: "Berhasil insert data",
            result: response,
          });
        } else {
          return res.status(400).json({
            status: 400,
            message: "Gagal insert data",
          });
        }
      } else {
        let dateTime = moment().format("YYYY-MM-DD HH:mm:ss");
        let id_lokasi_tugas = response.id_lokasi_tugas;
        let update = await lokasiTugasHelper.updateLokasiTugas(
          {
            id_client: user.id_mapping,
            id_cabang: response.id_cabang,
            nama_lokasi: response.nama_lokasi,
            longitude: response.longitude,
            latitude: response.latitude,
            is_aktif: response.is_aktif,
            user_update: user.id,
            time_update: dateTime,
          },
          id_lokasi_tugas,
          {
            // transaction: t,
          }
        );

        // await t.commit();

        if (update) {
          return res.json({
            status: 200,
            message: "Berhasil update data",
            result: response,
          });
        } else {
          return res.json({
            status: 400,
            message: "Gagal update data",
          });
        }
      }
    }
  } catch (error) {
    // await t.rollback();
    return res.status(400).json({
      status: 400,
      message: "Terjadi insert data",
      result: error.message,
    });
  }
};

const edit = async (req, res) => {
  try {
    const id_lokasi_tugas = req.params.id_lokasi_tugas;
    const getLokasiTugas = await lokasiTugasHelper.getLokasiTugasId(
      id_lokasi_tugas
    );
    if (getLokasiTugas) {
      return res.status(200).json({
        status: 200,
        message: "Berhasil mengambil data tugas",
        result: getLokasiTugas,
      });
    } else {
      return res.status(400).json({
        status: 400,
        message: "Gagal mengambil data tugas",
      });
    }
  } catch (error) {
    return res.status(400).json({
      status: 400,
      message: "Terjadi kesalahan data",
      result: error.message,
    });
  }
};

const deleteData = async (req, res) => {
  // const t = await sequelize.transaction();

  try {
    const id_lokasi_tugas = req.params.id_lokasi_tugas;
    const getLokasiTugas = await lokasiTugasHelper.deleteLokasiTugas(
      id_lokasi_tugas,
      {
        // transaction: t,
      }
    );

    // await t.commit();
    if (getLokasiTugas) {
      return res.status(200).json({
        status: 200,
        message: "Berhasil menghapus data tugas",
        result: getLokasiTugas,
      });
    } else {
      return res.status(400).json({
        status: 400,
        message: "Gagal menghapus data tugas",
      });
    }
  } catch (error) {
    // await t.rollback();
    return res.status(400).json({
      status: 400,
      message: "Terjadi kesalahan data",
      result: error.message,
    });
  }
};

const getCabang = async (req, res) => {
  try {
    const id_cabang = req.params.id_cabang;
    const getLokasiTugas = await LokasiTugas.findAll({
      where: {
        id_cabang: id_cabang,
        is_aktif: true,
      },
    });
    if (getLokasiTugas) {
      return res.status(200).json({
        status: 200,
        message: "Berhasil mengambil data lokasi tugas",
        result: getLokasiTugas,
      });
    } else {
      return res.status(400).json({
        status: 400,
        message: "Gagal mengambil data lokasi tugas",
      });
    }
  } catch (error) {
    return res.status(400).json({
      status: 400,
      message: "Terjadi kesalahan data",
      result: error.message,
    });
  }
};

module.exports = {
  index,
  store,
  edit,
  deleteData,
  getCabang,
};
