const { validationResult } = require("express-validator");
const {
  clientHelper,
  pagination,
  userHelper,
  userMappingHelper,
} = require("../../helper/index");
const moment = require("moment");
const bcrypt = require("bcrypt");
const sequelize = require("../../config/db");
const saltRounds = 10;

const index = async (req, res) => {
  try {
    if (req.xhr) {
      // page
      const page =
        req.query.page == null || req.query.page == "" ? 1 : req.query.page;
      const limit =
        req.query.limit == null || req.query.limit == "" ? 10 : req.query.limit;
      const search = req.query.search;

      const halamanAkhir = page * limit;
      const halmaanAwal = halamanAkhir - limit;
      const offset = halamanAkhir;
      const skip = halmaanAwal;

      let client = await clientHelper.getClient(limit, skip);
      let model = await clientHelper.Client.count();
      if (search != null && search != "") {
        client = await clientHelper.getClient(null, null, search, limit);
        let getModel = await clientHelper.getClient(null, null, search);
        model = getModel.length;
      }

      // pagination
      const getPagination = pagination(page, model, limit);

      let keterangan = {
        from: skip + 1,
        to: offset,
        total: model,
      };

      let output = {
        data: client,
        pagination: getPagination,
        keterangan: keterangan,
      };
      return res.status(200).json({
        status: 200,
        message: "Berhasil tangkap data",
        output: output,
      });
    }

    // breadcrumb
    let breadcrumb = [];
    breadcrumb.push({ label: "Home", url: "/admin/dashboard", isActive: "" });
    breadcrumb.push({
      label: "Client",
      url: "/admin/client",
      isActive: "active",
    });

    res.render("./moduleMaster/client/index", {
      title: "Client",
      breadcrumb: breadcrumb,
      currentUrl: req.originalUrl,
    });
  } catch (err) {
    console.log(err);
  }
};

const store = async (req, res) => {
  let user = req.user.data;
  let getPegawai = await Pegawai.findOne({
    where: {
      id_client: user.id_mapping,
    },
  });
  req.query.client_id = getPegawai.id_client;
  // const t = await sequelize.transaction();

  try {
    const errors = validationResult(req);

    if (!errors.isEmpty()) {
      return res.status(400).json({
        status: 400,
        message: "Invalid form validation",
        result: errors.array(),
      });
    } else {
      const response = req.body;
      if (response.page == "add") {
        let dateTime = moment().format("YYYY-MM-DD HH:mm:ss");

        let dataUser = {};
        dataUser.username = response.username;
        dataUser.password_hash = bcrypt.hashSync(
          response.password_hash,
          saltRounds
        );
        dataUser.email = response.email;
        dataUser.status = 1;
        dataUser.created_at = dateTime;
        dataUser.updated_at = dateTime;
        let insertUser = await userHelper.Users.create(dataUser);
        let id_user = insertUser.id;

        let insertClient = await clientHelper.Client.create({
          nama_client: response.nama_client,
          alamat: response.alamat,
          kontak: response.kontak,
          is_aktif: 1,
          user_create: user.id,
          user_update: user.id,
          time_create: dateTime,
          time_update: dateTime,
        });
        let id_client = insertClient.id_client;

        let dataMappingUser = {};
        dataMappingUser.jenis_mapping = "client";
        dataMappingUser.id_user = id_user;
        dataMappingUser.id_mapping = id_client;
        let insertMappingUser = await userMappingHelper.UserMapping.create(
          dataMappingUser,
          {
            // transaction: t,
          }
        );
        // await t.commit();

        if (insertUser || insertClient || insertMappingUser) {
          return res.status(200).json({
            status: 200,
            message: "Berhasil insert data",
            result: response,
          });
        } else {
          return res.status(400).json({
            status: 400,
            message: "Gagal insert data",
          });
        }
      } else {
        const getMapping = await userMappingHelper.UserMapping.findOne({
          where: {
            id_mapping: response.id_client,
          },
        });
        const getUsers = await userHelper.Users.findOne({
          where: {
            id: getMapping.id_user,
          },
        });
        let dateTime = moment().format("YYYY-MM-DD HH:mm:ss");

        let dataUser = {};
        dataUser.username = response.username;
        let password_db = response.password_hash_old;
        let password = response.password_hash;
        if (password != null) {
          password_db = bcrypt.hashSync(password, saltRounds);
        }
        dataUser.password_hash = password_db;
        dataUser.email = response.email;
        dataUser.status = 1;
        dataUser.user_update = user.id;
        dataUser.updated_at = dateTime;
        let updateUsers = await userHelper.Users.update(dataUser, {
          where: {
            id: getUsers.id,
          },
        });

        let updateClient = await clientHelper.Client.update(
          {
            nama_client: response.nama_client,
            alamat: response.alamat,
            kontak: response.kontak,
            is_aktif: 1,
            user_update: user.id,
            time_update: dateTime,
          },
          {
            where: {
              id_client: response.id_client,
            },
            // transaction: t,
          }
        );
        // await t.commit();

        if (updateUsers || updateClient) {
          return res.status(200).json({
            status: 200,
            message: "Berhasil update data",
            result: response,
          });
        } else {
          return res.status(400).json({
            status: 400,
            message: "Gagal update data",
          });
        }
      }
    }
  } catch (error) {
    // await t.rollback();
    return res.status(400).json({
      status: 400,
      message: "Terjadi kesalahan data",
      result: error.message,
    });
  }
};

const edit = async (req, res) => {
  try {
    const id_client = req.params.id_client;
    const getClient = await clientHelper.Client.findOne({
      where: { id_client: id_client },
    });
    const getMapping = await userMappingHelper.UserMapping.findOne({
      where: {
        id_mapping: id_client,
      },
    });
    const getUsers = await userHelper.Users.findOne({
      where: {
        id: getMapping.id_user,
      },
    });
    let output = {};
    output.client = getClient;
    output.mapping = getMapping;
    output.users = getUsers;
    if (output) {
      return res.status(200).json({
        status: 200,
        message: "Berhasil mengambil data client",
        result: output,
      });
    } else {
      return res.status(400).json({
        status: 400,
        message: "Gagal mengambil data client",
      });
    }
  } catch (error) {
    return res.status(400).json({
      status: 400,
      message: "Terjadi kesalahan data",
      result: error.message,
    });
  }
};

const detail = async (req, res) => {
  try {
    const id_client = req.params.id_client;
    const getClient = await clientHelper.Client.findOne({
      where: { id_client: id_client },
    });
    const getMapping = await userMappingHelper.UserMapping.findOne({
      where: {
        id_mapping: id_client,
      },
    });
    const getUsers = await userHelper.Users.findOne({
      where: {
        id: getMapping.id_user,
      },
    });
    let output = {};
    output.client = getClient;
    output.mapping = getMapping;
    output.users = getUsers;
    if (output) {
      return res.status(200).json({
        status: 200,
        message: "Berhasil mengambil data client",
        result: output,
      });
    } else {
      return res.status(400).json({
        status: 400,
        message: "Gagal mengambil data client",
      });
    }
  } catch (error) {
    return res.status(400).json({
      status: 400,
      message: "Terjadi kesalahan data",
      result: error.message,
    });
  }
};

const deleteData = async (req, res) => {
  // const t = await sequelize.transaction();

  try {
    const id_client = req.params.id_client;
    const getMapping = await userMappingHelper.UserMapping.findOne({
      where: {
        id_mapping: id_client,
      },
    });
    const getUsers = await userHelper.Users.findOne({
      where: {
        id: getMapping.id_user,
      },
    });
    const deleteMappingUser = await userMappingHelper.UserMapping.destroy({
      where: {
        id_user_mapping: getMapping.id_user_mapping,
      },
      // transaction: t,
    });
    const deleteUser = await userHelper.Users.destroy({
      where: {
        id: getUsers.id,
      },
      // transaction: t,
    });
    const deleteClient = await clientHelper.Client.destroy({
      where: {
        id_client: id_client,
      },
      // transaction: t,
    });

    // await t.commit();
    if (deleteClient || deleteUser || deleteMappingUser) {
      return res.status(200).json({
        status: 200,
        message: "Berhasil menghapus data client",
      });
    } else {
      return res.status(400).json({
        status: 400,
        message: "Gagal menghapus data client",
      });
    }
  } catch (error) {
    // await t.rollback();
    return res.status(400).json({
      status: 400,
      message: "Terjadi kesalahan data",
      result: error.message,
    });
  }
};

module.exports = {
  index,
  store,
  edit,
  deleteData,
  detail,
};
