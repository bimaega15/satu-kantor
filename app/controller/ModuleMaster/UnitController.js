const {
  unitHelper,
  unitHelper: { getUnitId, getUnitAll, getUnitById },
  userMappingHelper,
  pagination,
  cabangHelper,
} = require("../../helper/index");
let moment = require("moment");
const isset = require("isset-php");
const { validationResult } = require("express-validator");
const readXlsxFile = require("read-excel-file/node");
const { Cabang, Unit, Pegawai } = require("../../model");
const { Sequelize, Op } = require("sequelize");
const sequelize = require("../../config/db");

const index = async (req, res) => {
  try {
    let user = req.user.data;
    let getPegawai = await Pegawai.findOne({
      where: {
        id_client: user.id_mapping,
      },
    });
    req.query.client_id = getPegawai.id_client;

    if (req.xhr) {
      let id_client = req.query.client_id;
      if (user.jenis_mapping == "client") {
        id_client = user.id_mapping;
      }

      // page
      const page =
        req.query.page == null || req.query.page == "" ? 1 : req.query.page;
      const limit =
        req.query.limit == null || req.query.limit == "" ? 10 : req.query.limit;
      const search = req.query.search;

      const halamanAkhir = page * limit;
      const halmaanAwal = halamanAkhir - limit;
      const offset = halamanAkhir;
      const skip = halmaanAwal;

      let getFilter = req.query.filter;
      let unit = await unitHelper.getUnit(
        limit,
        skip,
        null,
        id_client,
        getFilter
      );
      let model = await unitHelper.Unit.count({
        where: {
          id_client: user.id_mapping,
        },
      });

      if (getFilter != null) {
        let getModel = await unitHelper.getUnit(
          null,
          null,
          null,
          id_client,
          getFilter
        );
        model = getModel.length;
      }
      if (search != null && search != "") {
        unit = await unitHelper.getUnit(
          limit,
          skip,
          search,
          id_client,
          getFilter
        );
        let getModel = await unitHelper.getUnit(
          null,
          null,
          search,
          id_client,
          getFilter
        );
        model = getModel.length;
      }

      // pagination
      const getPagination = pagination(page, model, limit);

      let keterangan = {
        from: skip + 1,
        to: offset,
        total: model,
      };

      let output = {
        data: unit,
        pagination: getPagination,
        keterangan: keterangan,
      };

      return res.status(200).json({
        status: 200,
        message: "Berhasil tangkap data",
        output: output,
      });
    }
    let id_client = req.query.client_id;
    if (user.jenis_mapping == "client") {
      id_client = user.id_mapping;
    }

    // breadcrumb
    let breadcrumb = [];
    if (user.jenis_mapping == "admin") {
      breadcrumb.push({ label: "Home", url: "/admin/dashboard", isActive: "" });
      breadcrumb.push({
        label: "Client",
        url: "/admin/client",
      });
      breadcrumb.push({
        label: "Dashboard Client",
        url: "/admin/dashboardClient?client_id=" + id_client,
      });
      breadcrumb.push({
        label: "Unit",
        url: "/admin/unit?client_id=" + id_client,
        isActive: "active",
      });
    } else {
      breadcrumb.push({ label: "Home", url: "/admin/dashboard", isActive: "" });
      breadcrumb.push({
        label: "Unit",
        url: "/admin/unit",
        isActive: "active",
      });
    }

    let cabang = await cabangHelper.Cabang.findAll({
      where: {
        id_client: user.id_mapping,
      },
    });

    res.render("./moduleMaster/unit/index", {
      title: "Unit",
      breadcrumb: breadcrumb,
      currentUrl: req.originalUrl,
      id_client: id_client,
      client_id: id_client,
      cabang: cabang,
    });
  } catch (error) {
    return res.status(400).json({
      status: 400,
      message: "Terjadi kesalahan data",
      result: error.message,
    });
  }
};

const store = async (req, res) => {
  // const t = await sequelize.transaction();
  let user = req.user.data;
  let getPegawai = await Pegawai.findOne({
    where: {
      id_client: user.id_mapping,
    },
  });
  req.query.client_id = getPegawai.id_client;

  try {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({
        status: 400,
        message: "Invalid form validation",
        result: errors.array(),
      });
    } else {
      const response = req.body;
      if (response.page == "add") {
        const is_aktif = response.is_aktif == 0 ? 0 : 1;
        const is_parent = response.membawahi_unit == "" ? 0 : 1;

        let dateTime = moment().format("YYYY-MM-DD HH:mm:ss");
        let data = {
          id_client: response.id_client,
          id_cabang: response.id_cabang,
          nama_unit: response.nama_unit,
          membawahi_unit: response.membawahi_unit,
          is_parent: is_parent,
          is_aktif: is_aktif,
          user_create: user.id,
          user_update: user.id,
          time_create: dateTime,
          time_update: dateTime,
        };


        let insert = await unitHelper.Unit.create(data, {
          // transaction: t,
        });
        // await t.commit();

        if (insert) {
          return res.status(200).json({
            status: 200,
            message: "Berhasil insert data",
            result: response,
          });
        } else {
          return res.status(400).json({
            status: 400,
            message: "Gagal insert data",
          });
        }
      } else {
        let dateTime = moment().format("YYYY-MM-DD HH:mm:ss");
        let id_unit = response.id_unit;
        const is_parent = response.membawahi_unit == "" ? 0 : 1;
        const is_aktif = response.is_aktif == 0 ? 0 : 1;

        let data = {
          id_client: response.id_client,
          id_cabang: response.id_cabang,
          nama_unit: response.nama_unit,
          membawahi_unit: response.membawahi_unit,
          is_parent: is_parent,
          is_aktif: is_aktif,
          user_update: user.id,
          time_update: dateTime,
        };
        let update = await unitHelper.Unit.update(data, {
          where: {
            id_unit: id_unit,
          },
          // transaction: t,
        });
        // await t.commit();

        if (update) {
          return res.json({
            status: 200,
            message: "Berhasil update data",
            result: response,
          });
        } else {
          return res.json({
            status: 400,
            message: "Gagal update data",
          });
        }
      }
    }
  } catch (error) {
    // await t.rollback();
    return res.status(400).json({
      status: 400,
      message: "Terjadi kesalahan data",
      result: error.message,
    });
  }
};

const edit = async (req, res) => {
  try {
    const id_unit = req.params.id_unit;
    const getUnit = await unitHelper.Unit.findOne({
      where: { id_unit: id_unit },
      order: [["id_unit", "asc"]],
    });
    if (getUnit) {
      return res.status(200).json({
        status: 200,
        message: "Berhasil mengambil data unit",
        result: getUnit,
      });
    } else {
      return res.status(400).json({
        status: 400,
        message: "Gagal mengambil data unit",
      });
    }
  } catch (error) {
    return res.status(400).json({
      status: 400,
      message: "Terjadi kesalahan data",
      result: error.message,
    });
  }
};

const deleteData = async (req, res) => {
  // const t = await sequelize.transaction();

  try {
    const id_unit = req.params.id_unit;
    const getUnit = await unitHelper.Unit.destroy({
      where: {
        id_unit: id_unit,
      },
      // transaction: t,
    });
    // await t.commit();

    if (getUnit) {
      return res.status(200).json({
        status: 200,
        message: "Berhasil menghapus data unit",
        result: getUnit,
      });
    } else {
      return res.status(400).json({
        status: 400,
        message: "Gagal menghapus data unit",
      });
    }
  } catch (error) {
    // await t.rollback();
    return res.status(400).json({
      status: 400,
      message: "Terjadi kesalahan data",
      result: error.message,
    });
  }
};

const detail = async (req, res) => {
  try {
    const id_unit = req.params.id_unit;
    let detail = await getUnitById(id_unit);

    let dataUnit = {};
    dataUnit.data = detail;
    let dataBawahiUnit = [];
    if (detail.membawahi_unit != null && detail.membawahi_unit != "") {
      let getMembawahiUnit = detail.membawahi_unit;
      getMembawahiUnit = getMembawahiUnit.split(",");

      dataBawahiUnit = [];
      if (getMembawahiUnit[0] != "") {
        getMembawahiUnit.forEach(function (unit_id) {
          dataBawahiUnit.push(getDataMembawahiUnit(unit_id));
        });
      }
    }

    Promise.all(dataBawahiUnit)
      .then((results) => {
        if (results) {
          return res.status(200).json({
            status: 200,
            message: "Berhasil mengambil detail unit",
            result: {
              detail: detail,
              membawahi: results,
            },
          });
        } else {
          return res.status(400).json({
            status: 400,
            message: "Gagal mengambil detail unit",
          });
        }
      })
      .catch((err) => {
        console.log("get error", err);
        return res.status(400).json({
          status: 400,
          message: "Terjadi kesalahan data",
        });
      });
  } catch (error) {
    return res.status(400).json({
      status: 400,
      message: "Terjadi kesalahan data",
      result: error.message,
    });
  }
};

const getDataMembawahiUnit = async (unit_id) => {
  let output = {};
  let getOneData = await getUnitById(unit_id);
  let membawahiUnitOneData = getOneData.membawahi_unit;
  if (membawahiUnitOneData != null && membawahiUnitOneData != "") {
    membawahiUnitOneData = membawahiUnitOneData.split(",");
    membawahiUnitOneData.map(async (v1, i1) => {
      if (isset(() => output[unit_id].hasOwnProperty(v1))) {
        output[unit_id] = [...output[unit_id], v1];
      } else {
        output[unit_id] = [v1];
      }
    });
  } else {
    output = { [unit_id]: getOneData.nama_unit };
  }
  return output;
};

const getFindInUnit = async (req, res) => {
  try {
    if (req.xhr) {
      const { unit_id } = req.query;
      const getUnit = await getUnitId(unit_id);
      if (getUnit) {
        return res.status(200).json({
          status: 200,
          message: "Berhasil mendapatkan data unit",
          result: getUnit,
        });
      } else {
        return res.status(400).json({
          status: 400,
          message: "Gagal mendapatkan data unit",
          result: getUnit,
        });
      }
    }
  } catch (error) {
    return res.status(400).json({
      status: 400,
      message: "Terjadi kesalahan data",
      result: error.message,
    });
  }
};

const getFindAllUnit = async (req, res) => {
  try {
    let user = req.user.data;
    let getPegawai = await Pegawai.findOne({
      where: {
        id_client: user.id_mapping,
      },
    });
    req.query.client_id = getPegawai.id_client;
    let id_cabang = req.query.id_cabang;
    if (req.xhr) {
      const getUnit = await getUnitAll(user.id_mapping, id_cabang);
      if (getUnit) {
        return res.status(200).json({
          status: 200,
          message: "Berhasil mendapatkan data unit",
          result: getUnit,
        });
      } else {
        return res.status(400).json({
          status: 400,
          message: "Gagal mendapatkan data unit",
          result: getUnit,
        });
      }
    }
  } catch (error) {
    return res.status(400).json({
      status: 400,
      message: "Terjadi kesalahan data",
      result: error.message,
    });
  }
};

const importData = async (req, res) => {
  // const t = await sequelize.transaction();
  try {
    let user = req.user.data;
    let getPegawai = await Pegawai.findOne({
      where: {
        id_client: user.id_mapping,
      },
    });
    req.query.client_id = getPegawai.id_client;
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({
        status: 400,
        message: "Invalid form validation",
        result: errors.array(),
      });
    }

    const { importData } = req.body.importData;

    readXlsxFile(importData.path).then(async (rows) => {
      let pushData = [];
      let dateTime = moment().format("YYYY-MM-DD HH:mm:ss");

      rows.map(async (v, i) => {
        if (i > 0) {
          if (v[1] != null) {
            pushData.push(pushImportData(v, user));
          }
        }
      });

      Promise.all(pushData).then(async (result) => {
        let dataDb = [];
        result.map((v, i) => {
          dataDb.push(...v);
        });

        let importUnit = await Unit.bulkCreate(dataDb, {
          // transaction: t,
        });
        // await t.commit();

        if (importUnit) {
          return res.status(200).json({
            status: 200,
            message: "Berhasil import " + dataDb.length + " data unit",
            result: req.body,
          });
        } else {
          return res.status(400).json({
            status: 400,
            message: "Gagal import " + dataDb.length + " data unit",
          });
        }
      });
    });
  } catch (error) {
    // await t.rollback();
    return res.status(400).json({
      status: 400,
      message: "Terjadi kesalahan data",
      result: error.message,
    });
  }
};

const pushImportData = async (v, user) => {
  let pushData = [];
  let dateTime = moment().format("YYYY-MM-DD HH:mm:ss");
  let dataCabang = await Cabang.findOne({
    where: {
      nama_cabang: {
        [Op.iLike]: "%" + v[1] + "%",
      },
      id_client: user.id_mapping,
    },
  });

  pushData.push({
    id_client: user.id_mapping,
    id_cabang: dataCabang.id_cabang,
    nama_unit: v[2],
    membawahi_unit: null,
    is_parent: 0,
    is_aktif: 1,
    user_create: user.id,
    user_update: user.id,
    time_create: dateTime,
    time_update: dateTime,
  });
  return pushData;
};
module.exports = {
  index,
  store,
  edit,
  deleteData,
  getFindInUnit,
  getFindAllUnit,
  detail,
  importData,
};
