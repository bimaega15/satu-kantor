const {
  jabatanHelper,
  jabatanHelper: { getJabatanId, getJabatanAll, getJabatanById },
  unitHelper,
  pagination,
  userMappingHelper,
} = require("../../helper/index");
const moment = require("moment");
const { validationResult } = require("express-validator");
const isset = require("isset-php");
const readXlsxFile = require("read-excel-file/node");
const { Unit, Jabatan, Cabang, Pegawai } = require("../../model");
const sequelize = require("../../config/db");
const { Op } = require("sequelize");

const index = async (req, res) => {
  try {
    let user = req.user.data;
    let getPegawai = await Pegawai.findOne({
      where: {
        id_client: user.id_mapping,
      },
    });
    req.query.client_id = getPegawai.id_client;

    if (req.xhr) {
      let id_client = req.query.client_id;
      if (user.jenis_mapping == "client") {
        id_client = user.id_mapping;
      }
      // page
      const page =
        req.query.page == null || req.query.page == "" ? 1 : req.query.page;
      const limit =
        req.query.limit == null || req.query.limit == "" ? 10 : req.query.limit;
      const search = req.query.search;

      const halamanAkhir = page * limit;
      const halmaanAwal = halamanAkhir - limit;
      const offset = halamanAkhir;
      const skip = halmaanAwal;

      let getFilter = req.query.filter;
      let jabatan = await jabatanHelper.getJabatan(
        limit,
        skip,
        null,
        id_client,
        getFilter
      );
      let model = await jabatanHelper.Jabatan.count({
        where: {
          id_client: user.id_mapping,
        },
      });
      if (getFilter != null) {
        let getModel = await jabatanHelper.getJabatan(
          null,
          null,
          null,
          id_client,
          getFilter
        );
        model = getModel.length;
      }
      if (search != null && search != "") {
        jabatan = await jabatanHelper.getJabatan(
          limit,
          skip,
          search,
          id_client,
          getFilter
        );
        let getModel = await jabatanHelper.getJabatan(
          null,
          null,
          search,
          id_client,
          getFilter
        );
        model = getModel.length;
      }

      // pagination
      const getPagination = pagination(page, model, limit);

      let keterangan = {
        from: skip + 1,
        to: offset,
        total: model,
      };

      let output = {
        data: jabatan,
        pagination: getPagination,
        keterangan: keterangan,
      };
      return res.status(200).json({
        status: 200,
        message: "Berhasil tangkap data",
        output: output,
      });
    }
    let id_client = req.query.client_id;
    if (user.jenis_mapping == "client") {
      id_client = user.id_mapping;
    }

    // breadcrumb
    let breadcrumb = [];
    if (user == "admin") {
      breadcrumb.push({ label: "Home", url: "/admin/dashboard", isActive: "" });
      breadcrumb.push({
        label: "Client",
        url: "/admin/client",
      });
      breadcrumb.push({
        label: "Dashboard Client",
        url: "/admin/dashboardClient?client_id=" + id_client,
      });
      breadcrumb.push({
        label: "Jabatan",
        url: "/admin/jabatan?client_id=" + id_client,
        isActive: "active",
      });
    } else {
      breadcrumb.push({ label: "Home", url: "/admin/dashboard", isActive: "" });
      breadcrumb.push({
        label: "Jabatan",
        url: "/admin/jabatan",
        isActive: "active",
      });
    }

    let cabang = await Cabang.findAll({
      where: {
        id_client: user.id_mapping,
      },
      order: [["id_cabang", "asc"]],
    });

    let unit = await unitHelper.Unit.findAll({
      where: {
        id_client: user.id_mapping,
      },
      order: [["id_unit", "asc"]],
    });

    res.render("./moduleMaster/jabatan/index", {
      title: "Jabatan",
      breadcrumb: breadcrumb,
      currentUrl: req.originalUrl,
      id_client: id_client,
      client_id: id_client,
      unit: unit,
      cabang: cabang,
    });
  } catch (err) {
    return res.status(400).json({
      status: 400,
      message: "Terjadi kesalahan data",
      result: err.message,
    });
  }
};

const store = async (req, res) => {
  let user = req.user.data;
  let getPegawai = await Pegawai.findOne({
    where: {
      id_client: user.id_mapping,
    },
  });
  req.query.client_id = getPegawai.id_client;
  // const t = await sequelize.transaction();

  try {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({
        status: 400,
        message: "Invalid form validation",
        result: errors.array(),
      });
    } else {
      const response = req.body;
      if (response.page == "add") {
        const is_parent = response.membawahi_jabatan == "" ? 0 : 1;
        const is_aktif = response.is_aktif == 0 ? 0 : 1;

        let dateTime = moment().format("YYYY-MM-DD HH:mm:ss");
        let data = {
          id_client: response.id_client,
          id_unit: response.id_unit,
          nama_jabatan: response.nama_jabatan,
          membawahi_jabatan: response.membawahi_jabatan,
          is_parent: is_parent,
          is_aktif: is_aktif,
          user_create: user.id,
          user_update: user.id,
          time_create: dateTime,
          time_update: dateTime,
        };
        let insert = await jabatanHelper.Jabatan.create(data, {
          // transaction: t,
        });

        // await t.commit();

        if (insert) {
          return res.status(200).json({
            status: 200,
            message: "Berhasil insert data",
            result: response,
          });
        } else {
          return res.status(400).json({
            status: 400,
            message: "Gagal insert data",
          });
        }
      } else {
        let dateTime = moment().format("YYYY-MM-DD HH:mm:ss");
        let id_jabatan = response.id_jabatan;
        const is_parent = response.membawahi_jabatan == "" ? 0 : 1;
        const is_aktif = response.is_aktif == 0 ? 0 : 1;

        let data = {
          id_client: response.id_client,
          id_unit: response.id_unit,
          nama_jabatan: response.nama_jabatan,
          membawahi_jabatan: response.membawahi_jabatan,
          is_parent: is_parent,
          is_aktif: is_aktif,
          user_update: user.id,
          time_update: dateTime,
        };
        let update = await jabatanHelper.Jabatan.update(data, {
          where: {
            id_jabatan: id_jabatan,
          },
          // transaction: t,
        });

        // await t.commit();

        if (update) {
          return res.json({
            status: 200,
            message: "Berhasil update data",
            result: response,
          });
        } else {
          return res.json({
            status: 400,
            message: "Gagal update data",
          });
        }
      }
    }
  } catch (error) {
    // await t.rollback();
    return res.status(400).json({
      status: 400,
      message: "Terjadi kesalahan data",
      result: error.message,
    });
  }
};

const edit = async (req, res) => {
  try {
    const id_jabatan = req.params.id_jabatan;
    const getJabatan = await jabatanHelper.Jabatan.findOne({
      where: { id_jabatan: id_jabatan },
      include: [
        {
          model: Unit,
          include: [
            {
              model: Cabang,
            },
          ],
        },
      ],
      order: [["id_jabatan", "asc"]],
    });
    if (getJabatan) {
      return res.status(200).json({
        status: 200,
        message: "Berhasil mengambil data jabatan",
        result: getJabatan,
      });
    } else {
      return res.status(400).json({
        status: 400,
        message: "Gagal mengambil data jabatan",
      });
    }
  } catch (error) {
    return res.status(400).json({
      status: 400,
      message: "Terjadi kesalahan data",
      result: error.message,
    });
  }
};

const deleteData = async (req, res) => {
  // const t = await sequelize.transaction();

  try {
    const id_jabatan = req.params.id_jabatan;
    const getJabatan = await jabatanHelper.Jabatan.destroy({
      where: {
        id_jabatan: id_jabatan,
      },
      // transaction: t,
    });

    // await t.commit();
    if (getJabatan) {
      return res.status(200).json({
        status: 200,
        message: "Berhasil menghapus data jabatan",
        result: getJabatan,
      });
    } else {
      return res.status(400).json({
        status: 400,
        message: "Gagal menghapus data jabatan",
      });
    }
  } catch (error) {
    // await t.rollback();
    return res.status(400).json({
      status: 400,
      message: "Terjadi kesalahan data",
      result: error.message,
    });
  }
};

const detail = async (req, res) => {
  try {
    const id_jabatan = req.params.id_jabatan;
    let detail = await getJabatanById(id_jabatan);
    let dataJabatan = {};
    dataJabatan.data = detail;
    let getMembawahiJabatan = detail.membawahi_jabatan;
    getMembawahiJabatan = getMembawahiJabatan.split(",");

    const dataBawahiJabatan = [];
    if (getMembawahiJabatan[0] != "") {
      getMembawahiJabatan.forEach(function (jabatan_id) {
        dataBawahiJabatan.push(getDataMembawahiJabatan(jabatan_id));
      });
    }

    Promise.all(dataBawahiJabatan)
      .then((results) => {
        if (results) {
          return res.status(200).json({
            status: 200,
            message: "Berhasil mengambil detail jabatan",
            result: {
              detail: detail,
              membawahi: results,
            },
          });
        } else {
          return res.status(400).json({
            status: 400,
            message: "Gagal mengambil detail jabatan",
          });
        }
      })
      .catch((err) => {
        return res.status(400).json({
          status: 400,
          message: "Terjadi kesalahan data",
        });
      });
  } catch (error) {
    return res.status(400).json({
      status: 400,
      message: "Terjadi kesalahan data",
      result: error.message,
    });
  }
};

const getDataMembawahiJabatan = async (jabatan_id) => {
  let output = {};
  let getOneData = await getJabatanById(jabatan_id);
  let membawahiJabatanOneData = getOneData.membawahi_jabatan;
  if (membawahiJabatanOneData != null && membawahiJabatanOneData != "") {
    membawahiJabatanOneData = membawahiJabatanOneData.split(",");
    membawahiJabatanOneData.map(async (v1, i1) => {
      if (isset(() => output[jabatan_id].hasOwnProperty(v1))) {
        output[jabatan_id] = [...output[jabatan_id], v1];
      } else {
        output[jabatan_id] = [v1];
      }
    });
  } else {
    output = { [jabatan_id]: getOneData.nama_jabatan };
  }

  return output;
};

const getFindInJabatan = async (req, res) => {
  try {
    if (req.xhr) {
      const { jabatan_id } = req.query;
      const getJabatan = await getJabatanId(jabatan_id);
      if (getJabatan) {
        return res.status(200).json({
          status: 200,
          message: "Berhasil mendapatkan data jabatan",
          result: getJabatan,
        });
      } else {
        return res.status(400).json({
          status: 400,
          message: "Gagal mendapatkan data jabatan",
          result: getJabatan,
        });
      }
    }
  } catch (error) {
    return res.status(400).json({
      status: 400,
      message: "Terjadi kesalahan data",
      result: error.message,
    });
  }
};

const getFindAllJabatan = async (req, res) => {
  try {
    let user = req.user.data;
    let getPegawai = await Pegawai.findOne({
      where: {
        id_client: user.id_mapping,
      },
    });
    req.query.client_id = getPegawai.id_client;
    let id_cabang = req.query.id_cabang;
    if (req.xhr) {
      const getJabatan = await getJabatanAll(user.id_mapping, id_cabang);
      if (getJabatan) {
        return res.status(200).json({
          status: 200,
          message: "Berhasil mendapatkan data jabatan",
          result: getJabatan,
        });
      } else {
        return res.status(400).json({
          status: 400,
          message: "Gagal mendapatkan data jabatan",
          result: getJabatan,
        });
      }
    }
  } catch (error) {
    return res.status(400).json({
      status: 400,
      message: "Terjadi kesalahan data",
      result: error.message,
    });
  }
};

const importData = async (req, res) => {
  // const t = await sequelize.transaction();

  try {
    let user = req.user.data;
    let getPegawai = await Pegawai.findOne({
      where: {
        id_client: user.id_mapping,
      },
    });
    req.query.client_id = getPegawai.id_client;
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({
        status: 400,
        message: "Invalid form validation",
        result: errors.array(),
      });
    }

    const { importData } = req.body.importData;

    readXlsxFile(importData.path).then(async (rows) => {
      let pushData = [];
      rows.map(async (v, i) => {
        if (i > 0) {
          if (v[1] != null) {
            pushData.push(pushImportData(v, user));
          }
        }
      });

      Promise.all(pushData).then(async (result) => {
        let dataDb = [];
        result.map((v, i) => {
          dataDb.push(...v);
        });

        let importJabatan = await Jabatan.bulkCreate(dataDb, {
          // transaction: t,
        });
        // await t.commit();

        if (importJabatan) {
          return res.status(200).json({
            status: 200,
            message: "Berhasil import " + dataDb.length + " data unit",
            result: req.body,
          });
        } else {
          return res.status(400).json({
            status: 400,
            message: "Gagal import " + dataDb.length + " data unit",
          });
        }
      });
    });
  } catch (error) {
    // await t.rollback();
    return res.status(400).json({
      status: 400,
      message: "Terjadi kesalahan data",
      result: error.message,
    });
  }
};

const pushImportData = async (v, user) => {
  let pushData = [];
  let dateTime = moment().format("YYYY-MM-DD HH:mm:ss");
  let dataCabang = await Cabang.findOne({
    where: {
      id_client: user.id_mapping,
    },
  });

  let dataUnit = await Unit.findOne({
    where: {
      nama_unit: {
        [Op.iLike]: "%" + v[1] + "%",
      },
      id_cabang: dataCabang.id_cabang,
      id_client: user.id_mapping,
    },
  });

  pushData.push({
    id_client: user.id_mapping,
    id_unit: dataUnit.id_unit,
    nama_jabatan: v[2],
    membawahi_jabatan: null,
    is_parent: 0,
    is_aktif: 1,
    user_create: user.id,
    user_update: user.id,
    time_create: dateTime,
    time_update: dateTime,
  });
  return pushData;
};
module.exports = {
  index,
  store,
  edit,
  deleteData,
  getFindInJabatan,
  getFindAllJabatan,
  detail,
  importData,
};
