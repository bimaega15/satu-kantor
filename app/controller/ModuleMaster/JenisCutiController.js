const {
  jenisCutiHelper,
  pagination,
  userMappingHelper,
} = require("../../helper/index");
const { validationResult } = require("express-validator");
const moment = require("moment");
const readXlsxFile = require("read-excel-file/node");
const { JenisCuti, Pegawai } = require("../../model");
const sequelize = require("../../config/db");

const index = async (req, res) => {
  let user = req.user.data;
  let getPegawai = await Pegawai.findOne({
    where: {
      id_client: user.id_mapping,
    },
  });
  req.query.client_id = getPegawai.id_client;

  try {
    if (req.xhr) {
      let id_client = req.query.client_id;
      if (user.jenis_mapping == "client") {
        id_client = user.id_mapping;
      }

      // page
      const page =
        req.query.page == null || req.query.page == "" ? 1 : req.query.page;
      const limit =
        req.query.limit == null || req.query.limit == "" ? 10 : req.query.limit;
      const search = req.query.search;

      const halamanAkhir = page * limit;
      const halmaanAwal = halamanAkhir - limit;
      const offset = halamanAkhir;
      const skip = halmaanAwal;

      let getFilter = req.query.filter;
      let jenisCuti = await jenisCutiHelper.getJenisCuti(
        limit,
        skip,
        null,
        id_client,
        getFilter
      );
      let model = await jenisCutiHelper.JenisCuti.count({
        where: {
          id_client: user.id_mapping,
        },
      });
      if (getFilter != null) {
        let getModel = await jenisCutiHelper.getJenisCuti(
          null,
          null,
          null,
          id_client,
          getFilter
        );
        model = getModel.length;
      }
      if (search != null && search != "") {
        jenisCuti = await jenisCutiHelper.getJenisCuti(
          limit,
          skip,
          search,
          id_client,
          getFilter
        );
        let getModel = await jenisCutiHelper.getJenisCuti(
          null,
          null,
          search,
          id_client,
          getFilter
        );
        model = getModel.length;
      }

      // pagination
      const getPagination = pagination(page, model, limit);

      let keterangan = {
        from: skip + 1,
        to: offset,
        total: model,
      };

      let output = {
        data: jenisCuti,
        pagination: getPagination,
        keterangan: keterangan,
      };
      return res.status(200).json({
        status: 200,
        message: "Berhasil tangkap data",
        output: output,
      });
    }
    let id_client = req.query.client_id;
    if (user.jenis_mapping == "client") {
      id_client = user.id_mapping;
    }

    // breadcrumb
    let breadcrumb = [];
    if (user.jenis_mapping == "admin") {
      breadcrumb.push({ label: "Home", url: "/admin/dashboard", isActive: "" });
      breadcrumb.push({
        label: "Client",
        url: "/admin/client",
      });
      breadcrumb.push({
        label: "Dashboard Client",
        url: "/admin/dashboardClient?client_id=" + id_client,
      });
      breadcrumb.push({
        label: "Jenis Cuti",
        url: "/admin/jenisCuti?client_id=" + id_client,
        isActive: "active",
      });
    } else {
      breadcrumb.push({ label: "Home", url: "/admin/dashboard", isActive: "" });
      breadcrumb.push({
        label: "Jenis Cuti",
        url: "/admin/jenisCuti",
        isActive: "active",
      });
    }

    res.render("./moduleMaster/jenisCuti/index", {
      title: "Jenis Cuti",
      breadcrumb: breadcrumb,
      currentUrl: req.originalUrl,
      id_client: id_client,
      client_id: id_client,
    });
  } catch (error) {
    return res.status(400).json({
      status: 400,
      message: "Terjadi kesalahan data",
      result: error.message,
    });
  }
};

const store = async (req, res) => {
  let user = req.user.data;
  let getPegawai = await Pegawai.findOne({
    where: {
      id_client: user.id_mapping,
    },
  });
  req.query.client_id = getPegawai.id_client;
  // const t = await sequelize.transaction();

  try {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({
        status: 400,
        message: "Invalid form validation",
        result: errors.array(),
      });
    } else {
      const response = req.body;
      if (response.page == "add") {
        let dateTime = moment().format("YYYY-MM-DD HH:mm:ss");
        const is_aktif = response.is_aktif == 0 ? 0 : 1;

        let data = {
          jenis_cuti: response.jenis_cuti,
          saldo: response.saldo,
          is_aktif: is_aktif,
          user_create: user.id,
          user_update: user.id,
          time_create: dateTime,
          time_update: dateTime,
          id_client: response.id_client,
        };
        let insert = await jenisCutiHelper.JenisCuti.create(data, {
          // transaction: t,
        });

        // await t.commit();

        if (insert) {
          return res.status(200).json({
            status: 200,
            message: "Berhasil insert data",
            result: response,
          });
        } else {
          return res.status(400).json({
            status: 400,
            message: "Gagal insert data",
          });
        }
      } else {
        let dateTime = moment().format("YYYY-MM-DD HH:mm:ss");
        const is_aktif = response.is_aktif == 0 ? 0 : 1;
        let id_jenis_cuti = response.id_jenis_cuti;
        let data = {
          jenis_cuti: response.jenis_cuti,
          saldo: response.saldo,
          is_aktif: is_aktif,
          user_update: user.id,
          time_update: dateTime,
          id_client: response.id_client,
        };
        let update = await jenisCutiHelper.JenisCuti.update(data, {
          where: {
            id_jenis_cuti: id_jenis_cuti,
          },
          // transaction: t,
        });
        // await t.commit();

        if (update) {
          return res.json({
            status: 200,
            message: "Berhasil update data",
            result: response,
          });
        } else {
          return res.json({
            status: 400,
            message: "Gagal update data",
          });
        }
      }
    }
  } catch (error) {
    // await t.rollback();
    return res.status(400).json({
      status: 400,
      message: "Terjadi kesalahan data",
      result: error.message,
    });
  }
};

const edit = async (req, res) => {
  try {
    const id_jenis_cuti = req.params.id_jenis_cuti;
    const getJenisCuti = await jenisCutiHelper.JenisCuti.findOne({
      where: { id_jenis_cuti: id_jenis_cuti },
    });
    if (getJenisCuti) {
      return res.status(200).json({
        status: 200,
        message: "Berhasil mengambil data jenis cuti",
        result: getJenisCuti,
      });
    } else {
      return res.status(400).json({
        status: 400,
        message: "Gagal mengambil data jenis cuti",
      });
    }
  } catch (error) {
    return res.status(400).json({
      status: 400,
      message: "Terjadi kesalahan data",
      result: error.message,
    });
  }
};

const deleteData = async (req, res) => {
  // const t = await sequelize.transaction();

  try {
    const id_jenis_cuti = req.params.id_jenis_cuti;
    const getJenisCuti = await jenisCutiHelper.JenisCuti.destroy({
      where: {
        id_jenis_cuti: id_jenis_cuti,
      },
      // transaction: t,
    });

    // await t.commit();
    if (getJenisCuti) {
      return res.status(200).json({
        status: 200,
        message: "Berhasil menghapus data jenis cuti",
        result: getJenisCuti,
      });
    } else {
      return res.status(400).json({
        status: 400,
        message: "Gagal menghapus data jenis cuti",
      });
    }
  } catch (error) {
    // await t.rollback();
    return res.status(400).json({
      status: 400,
      message: "Terjadi kesalahan data",
      result: error.message,
    });
  }
};

const importData = async (req, res) => {
  // const t = await sequelize.transaction();
  try {
    let user = req.user.data;
    let getPegawai = await Pegawai.findOne({
      where: {
        id_client: user.id_mapping,
      },
    });
    req.query.client_id = getPegawai.id_client;
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({
        status: 400,
        message: "Invalid form validation",
        result: errors.array(),
      });
    }

    const { importData } = req.body.importData;

    let dateTime = moment().format("YYYY-MM-DD HH:mm:ss");
    readXlsxFile(importData.path).then(async (rows) => {
      let pushData = [];
      rows.map((v, i) => {
        if (i > 0) {
          if (v[1] != null) {
            pushData.push({
              id_client: user.id_mapping,
              jenis_cuti: v[1],
              saldo: v[2],
              is_aktif: 1,
              user_create: user.id,
              user_update: user.id,
              time_create: dateTime,
              time_update: dateTime,
            });
          }
        }
      });

      let importJenisCuti = await JenisCuti.bulkCreate(pushData, {
        // transaction: t,
      });
      // await t.commit();
      if (importJenisCuti) {
        return res.status(200).json({
          status: 200,
          message: "Berhasil import " + pushData.length + " data cabang",
          result: req.body,
        });
      } else {
        return res.status(400).json({
          status: 400,
          message: "Gagal import " + pushData.length + " data cabang",
        });
      }
    });
  } catch (error) {
    // await t.rollback();
    return res.status(400).json({
      status: 400,
      message: "Terjadi kesalahan data",
      result: error.message,
    });
  }
};

module.exports = {
  index,
  store,
  edit,
  deleteData,
  importData,
};
