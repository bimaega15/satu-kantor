const { validationResult } = require("express-validator");
const {
  jenisLemburHelper,
  pagination,
  userMappingHelper,
} = require("../../helper/index");
const moment = require("moment");
const readXlsxFile = require("read-excel-file/node");
const { JenisLembur, Pegawai } = require("../../model");
const sequelize = require("../../config/db");

const index = async (req, res) => {
  let user = req.user.data;
  let getPegawai = await Pegawai.findOne({
    where: {
      id_client: user.id_mapping,
    },
  });
  req.query.client_id = getPegawai.id_client;
  try {
    if (req.xhr) {
      let id_client = req.query.client_id;
      if (user.jenis_mapping == "client") {
        id_client = user.id_mapping;
      }
      // page
      const page =
        req.query.page == null || req.query.page == "" ? 1 : req.query.page;
      const limit =
        req.query.limit == null || req.query.limit == "" ? 10 : req.query.limit;
      const search = req.query.search;

      const halamanAkhir = page * limit;
      const halmaanAwal = halamanAkhir - limit;
      const offset = halamanAkhir;
      const skip = halmaanAwal;

      let getFilter = req.query.filter;
      let jenisLembur = await jenisLemburHelper.getJenisLembur(
        limit,
        skip,
        null,
        id_client,
        getFilter
      );
      let model = await jenisLemburHelper.JenisLembur.count({
        where: {
          id_client: user.id_mapping,
        },
      });
      if (getFilter != null) {
        let getModel = await jenisLemburHelper.getJenisLembur(
          null,
          null,
          null,
          id_client,
          getFilter
        );
        model = getModel.length;
      }
      if (search != null && search != "") {
        jenisLembur = await jenisLemburHelper.getJenisLembur(
          limit,
          skip,
          search,
          id_client,
          getFilter
        );
        let getModel = await jenisLemburHelper.getJenisLembur(
          null,
          null,
          null,
          id_client,
          getFilter
        );
        model = getModel.length;
      }

      // pagination
      const getPagination = pagination(page, model, limit);

      let keterangan = {
        from: skip + 1,
        to: offset,
        total: model,
      };

      let output = {
        data: jenisLembur,
        pagination: getPagination,
        keterangan: keterangan,
      };
      return res.status(200).json({
        status: 200,
        message: "Berhasil tangkap data",
        output: output,
      });
    }
    let id_client = req.query.client_id;
    if (user.jenis_mapping == "client") {
      id_client = user.id_mapping;
    }

    // breadcrumb
    let breadcrumb = [];
    breadcrumb.push({ label: "Home", url: "/admin/dashboard", isActive: "" });
    breadcrumb.push({
      label: "Jenis Lembur",
      url: "/admin/jenisLembur",
      isActive: "active",
    });

    let jenisLembur = await JenisLembur.findAll({
      where: {
        id_client: user.id_mapping,
      },
    });
    let pegawai = await Pegawai.findAll({
      where: {
        id_client: user.id_mapping,
      },
    });

    res.render("./moduleMaster/jenisLembur/index", {
      title: "Jenis Lembur",
      breadcrumb: breadcrumb,
      currentUrl: req.originalUrl,
      id_client: id_client,
      client_id: id_client,
      jenisLembur: jenisLembur,
      pegawai: pegawai,
    });
  } catch (error) {
    return res.status(400).json({
      status: 400,
      message: "Terjadi kesalahan data",
      result: error.message,
    });
  }
};

const store = async (req, res) => {
  let user = req.user.data;
  let getPegawai = await Pegawai.findOne({
    where: {
      id_client: user.id_mapping,
    },
  });
  req.query.client_id = getPegawai.id_client;
  // const t = await sequelize.transaction();

  try {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({
        status: 400,
        message: "Invalid form validation",
        result: errors.array(),
      });
    } else {
      const response = req.body;
      if (response.page == "add") {
        let dateTime = moment().format("YYYY-MM-DD HH:mm:ss");
        const is_aktif = response.is_aktif == 0 ? 0 : 1;

        let data = {
          jenis_lembur: response.jenis_lembur,
          jenis_jadwal: response.jenis_jadwal,
          jenis_perhitungan: response.jenis_perhitungan,
          formula: response.formula,
          user_create: user.id,
          user_update: user.id,
          time_create: dateTime,
          time_update: dateTime,
          id_client: response.id_client,
          is_aktif: is_aktif,
        };
        let insert = await jenisLemburHelper.JenisLembur.create(data, {
          // transaction: t,
        });
        // await t.commit();

        if (insert) {
          return res.status(200).json({
            status: 200,
            message: "Berhasil insert data",
            result: response,
          });
        } else {
          return res.status(400).json({
            status: 400,
            message: "Gagal insert data",
          });
        }
      } else {
        let dateTime = moment().format("YYYY-MM-DD HH:mm:ss");
        let id_jenis_lembur = response.id_jenis_lembur;
        const is_aktif = response.is_aktif == 0 ? 0 : 1;

        let data = {
          jenis_lembur: response.jenis_lembur,
          jenis_jadwal: response.jenis_jadwal,
          jenis_perhitungan: response.jenis_perhitungan,
          formula: response.formula,
          user_update: user.id,
          time_update: dateTime,
          id_client: response.id_client,
          is_aktif: is_aktif,
        };
        let update = await jenisLemburHelper.JenisLembur.update(data, {
          where: {
            id_jenis_lembur: id_jenis_lembur,
          },
          // transaction: t,
        });
        // await t.commit();

        if (update) {
          return res.json({
            status: 200,
            message: "Berhasil update data",
            result: response,
          });
        } else {
          return res.json({
            status: 400,
            message: "Gagal update data",
          });
        }
      }
    }
  } catch (error) {
    // await t.rollback();
    return res.status(400).json({
      status: 400,
      message: "Terjadi kesalahan data",
      result: error.message,
    });
  }
};

const edit = async (req, res) => {
  try {
    const id_jenis_lembur = req.params.id_jenis_lembur;
    const getJenisLembur = await jenisLemburHelper.JenisLembur.findOne({
      where: { id_jenis_lembur: id_jenis_lembur },
    });
    if (getJenisLembur) {
      return res.status(200).json({
        status: 200,
        message: "Berhasil mengambil data jenisLembur",
        result: getJenisLembur,
      });
    } else {
      return res.status(400).json({
        status: 400,
        message: "Gagal mengambil data jenisLembur",
      });
    }
  } catch (error) {
    return res.status(400).json({
      status: 400,
      message: "Terjadi kesalahan data",
      result: error.message,
    });
  }
};

const deleteData = async (req, res) => {
  // const t = await sequelize.transaction();

  try {
    const id_jenis_lembur = req.params.id_jenis_lembur;
    const getJenisLembur = await jenisLemburHelper.JenisLembur.destroy({
      where: {
        id_jenis_lembur: id_jenis_lembur,
      },
      // transaction: t,
    });

    // await t.commit();
    if (getJenisLembur) {
      return res.status(200).json({
        status: 200,
        message: "Berhasil menghapus data jenisLembur",
        result: getJenisLembur,
      });
    } else {
      return res.status(400).json({
        status: 400,
        message: "Gagal menghapus data jenisLembur",
      });
    }
  } catch (error) {
    // await t.rollback();
    return res.status(400).json({
      status: 400,
      message: "Terjadi kesalahan data",
      result: error.message,
    });
  }
};

const importData = async (req, res) => {
  // const t = await sequelize.transaction();

  try {
    let user = req.user.data;
    let getPegawai = await Pegawai.findOne({
      where: {
        id_client: user.id_mapping,
      },
    });
    req.query.client_id = getPegawai.id_client;
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({
        status: 400,
        message: "Invalid form validation",
        result: errors.array(),
      });
    }

    const { importData } = req.body.importData;

    let dateTime = moment().format("YYYY-MM-DD HH:mm:ss");
    readXlsxFile(importData.path).then(async (rows) => {
      let pushData = [];
      let formula = [];
      let pushDb = [];
      let jenisPerhitungan = null;
      rows.map((v, i) => {
        if (i > 0) {
          if (v[1] != null) {
            jenisPerhitungan = v[2];
            pushData.push({
              [jenisPerhitungan]: {
                id_client: user.id_mapping,
                jenis_lembur: v[1],
                jenis_perhitungan: v[2],
                jenis_jadwal: v[3],
                formula: null,
                is_aktif: 1,
                user_create: user.id,
                user_update: user.id,
                time_create: dateTime,
                time_update: dateTime,
              },
            });
          }

          if (v[4] != null) {
            let textJenisLembur = null;
            let mergeJenisLembur = [];
            if (jenisPerhitungan.toLowerCase() == "jam") {
              textJenisLembur = v[4].split(" ");
              mergeJenisLembur[0] = textJenisLembur[2];
              mergeJenisLembur[1] = textJenisLembur[1];
              mergeJenisLembur = mergeJenisLembur.join("_").toLowerCase();
            }
            if (jenisPerhitungan.toLowerCase() == "kehadiran") {
              textJenisLembur = v[4].split(" ");
              mergeJenisLembur = textJenisLembur[1].toLowerCase();
            }

            formula.push({
              [jenisPerhitungan]: {
                [mergeJenisLembur]: v[5],
              },
            });
          }
        }
      });

      pushData.map((v, i) => {
        Object.keys(v).map((key, i) => {
          let pushFormula = [];
          formula.map((v2, i2) => {
            if (v2[key] != undefined) {
              pushFormula.push(v2[key]);
            }
          });
          let dataDb = { ...v[key], formula: JSON.stringify(pushFormula) };
          pushDb.push(dataDb);
        });
      });

      let importJenisLembur = await JenisLembur.bulkCreate(pushDb, {
        // transaction: t,
      });
      // await t.commit();

      if (importJenisLembur) {
        return res.status(200).json({
          status: 200,
          message: "Berhasil import " + pushDb.length + " data jenis lembur",
          result: req.body,
        });
      } else {
        return res.status(400).json({
          status: 400,
          message: "Gagal import " + pushDb.length + " data jenis lembur",
        });
      }
    });
  } catch (error) {
    // await t.rollback();
    return res.status(400).json({
      status: 400,
      message: "Terjadi kesalahan data",
      result: error.message,
    });
  }
};

module.exports = {
  index,
  store,
  edit,
  deleteData,
  importData,
};
