const {
  jadwalHelper,
  pagination,
  userMappingHelper,
} = require("../../helper/index");
const { validationResult } = require("express-validator");
const moment = require("moment");
const readXlsxFile = require("read-excel-file/node");
const { Jadwal, Pegawai } = require("../../model");
const sequelize = require("../../config/db");

const index = async (req, res) => {
  let user = req.user.data;
  let getPegawai = await Pegawai.findOne({
    where: {
      id_client: user.id_mapping,
    },
  });
  req.query.client_id = getPegawai.id_client;

  try {
    if (req.xhr) {
      let id_client = req.query.client_id;
      if (user.jenis_mapping == "client") {
        id_client = user.id_mapping;
      }
      // page
      const page =
        req.query.page == null || req.query.page == "" ? 1 : req.query.page;
      const limit =
        req.query.limit == null || req.query.limit == "" ? 10 : req.query.limit;
      const search = req.query.search;

      const halamanAkhir = page * limit;
      const halamanAwal = halamanAkhir - limit;
      const offset = halamanAkhir;
      const skip = halamanAwal;

      let getFilter = req.query.filter;
      let jadwal = await jadwalHelper.getJadwal(
        limit,
        skip,
        null,
        id_client,
        getFilter
      );
      let model = await jadwalHelper.Jadwal.count({
        where: {
          id_client: user.id_mapping,
        },
      });
      if (getFilter != null) {
        let getModel = await jadwalHelper.getJadwal(
          null,
          null,
          null,
          id_client,
          getFilter
        );
        model = getModel.length;
      }

      if (search != null && search != "") {
        jadwal = await jadwalHelper.getJadwal(
          limit,
          skip,
          limit,
          id_client,
          getFilter
        );
        let getModel = await jadwalHelper.getJadwal(
          null,
          null,
          search,
          id_client,
          getFilter
        );
        model = getModel.length;
      }

      // pagination
      const getPagination = pagination(page, model, limit);

      let keterangan = {
        from: skip + 1,
        to: offset,
        total: model,
      };

      let output = {
        data: jadwal,
        pagination: getPagination,
        keterangan: keterangan,
      };
      return res.status(200).json({
        status: 200,
        message: "Berhasil tangkap data",
        output: output,
      });
    }

    let id_client = req.query.client_id;
    if (user.jenis_mapping == "client") {
      id_client = user.id_mapping;
    }

    // breadcrumb
    let breadcrumb = [];
    if (user.jenis_mapping == "admin") {
      breadcrumb.push({ label: "Home", url: "/admin/dashboard", isActive: "" });
      breadcrumb.push({
        label: "Client",
        url: "/admin/client",
      });
      breadcrumb.push({
        label: "Dashboard Client",
        url: "/admin/dashboardClient?client_id=" + id_client,
      });
      breadcrumb.push({
        label: "Jadwal",
        url: "/admin/jadwal?client_id=" + id_client,
        isActive: "active",
      });
    } else {
      breadcrumb.push({ label: "Home", url: "/admin/dashboard", isActive: "" });
      breadcrumb.push({
        label: "Jadwal",
        url: "/admin/jadwal",
        isActive: "active",
      });
    }

    res.render("./moduleMaster/jadwal/index", {
      title: "Jadwal",
      breadcrumb: breadcrumb,
      currentUrl: req.originalUrl,
      id_client: id_client,
      client_id: id_client,
    });
  } catch (err) {
    return res.status(400).json({
      status: 400,
      message: "Terjadi kesalahan data",
      result: err.message,
    });
  }
};

const store = async (req, res) => {
  // const t = await sequelize.transaction();

  try {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({
        status: 400,
        message: "Invalid form validation",
        result: errors.array(),
      });
    } else {
      const response = req.body;
      if (response.page == "add") {
        const is_aktif = response.is_aktif == 0 ? 0 : 1;

        let data = {
          id_client: response.id_client,
          waktu_masuk: response.waktu_masuk,
          waktu_keluar: response.waktu_keluar,
          is_aktif: is_aktif,
          warna: response.warna,
          jenis: response.jenis,
        };
        let insert = await jadwalHelper.Jadwal.create(data, {
          // transaction: t,
        });
        // await t.commit();

        if (insert) {
          return res.status(200).json({
            status: 200,
            message: "Berhasil insert data",
            result: response,
          });
        } else {
          return res.status(400).json({
            status: 400,
            message: "Gagal insert data",
          });
        }
      } else {
        let id_jadwal = response.id_jadwal;
        const is_aktif = response.is_aktif == 0 ? 0 : 1;

        let data = {
          id_client: response.id_client,
          waktu_masuk: response.waktu_masuk,
          waktu_keluar: response.waktu_keluar,
          is_aktif: is_aktif,
          warna: response.warna,
          jenis: response.jenis.toLowerCase(),
        };
        let update = await jadwalHelper.Jadwal.update(data, {
          where: {
            id_jadwal: id_jadwal,
          },
          // transaction: t,
        });
        // await t.commit();

        if (update) {
          return res.json({
            status: 200,
            message: "Berhasil update data",
            result: response,
          });
        } else {
          return res.json({
            status: 400,
            message: "Gagal update data",
          });
        }
      }
    }
  } catch (error) {
    // await t.rollback();
    return res.status(400).json({
      status: 400,
      message: "Terjadi kesalahan data",
      result: error.message,
    });
  }
};

const edit = async (req, res) => {
  try {
    const id_jadwal = req.params.id_jadwal;
    const getJadwal = await jadwalHelper.Jadwal.findOne({
      where: { id_jadwal: id_jadwal },
    });
    if (getJadwal) {
      return res.status(200).json({
        status: 200,
        message: "Berhasil mengambil data jadwal",
        result: getJadwal,
      });
    } else {
      return res.status(400).json({
        status: 400,
        message: "Gagal mengambil data jadwal",
      });
    }
  } catch (error) {
    return res.status(400).json({
      status: 400,
      message: "Terjadi kesalahan data",
      result: error.message,
    });
  }
};

const deleteData = async (req, res) => {
  // const t = await sequelize.transaction();

  try {
    const id_jadwal = req.params.id_jadwal;
    const getJadwal = await jadwalHelper.Jadwal.destroy({
      where: {
        id_jadwal: id_jadwal,
      },
      // transaction: t,
    });

    // await t.commit();

    if (getJadwal) {
      return res.status(200).json({
        status: 200,
        message: "Berhasil menghapus data jadwal",
        result: getJadwal,
      });
    } else {
      return res.status(400).json({
        status: 400,
        message: "Gagal menghapus data jadwal",
      });
    }
  } catch (error) {
    // await t.rollback();
    return res.status(400).json({
      status: 400,
      message: "Terjadi kesalahan data",
      result: error.message,
    });
  }
};

const importData = async (req, res) => {
  // const t = await sequelize.transaction();

  try {
    let user = req.user.data;
    let getPegawai = await Pegawai.findOne({
      where: {
        id_client: user.id_mapping,
      },
    });
    req.query.client_id = getPegawai.id_client;
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({
        status: 400,
        message: "Invalid form validation",
        result: errors.array(),
      });
    }

    const { importData } = req.body.importData;

    readXlsxFile(importData.path).then(async (rows) => {
      let pushData = [];
      rows.map((v, i) => {
        if (i > 0) {
          if (v[1] != null) {
            pushData.push({
              id_client: user.id_mapping,
              waktu_masuk: v[1],
              waktu_keluar: v[2],
              jenis: v[3].toLowerCase(),
              warna: v[4],
              is_aktif: 1,
            });
          }
        }
      });

      let importJadwal = await Jadwal.bulkCreate(pushData, {
        // transaction: t,
      });
      // await t.commit();
      if (importJadwal) {
        return res.status(200).json({
          status: 200,
          message: "Berhasil import " + pushData.length + " data jadwal",
          result: req.body,
        });
      } else {
        return res.status(400).json({
          status: 400,
          message: "Gagal import " + pushData.length + " data jadwal",
        });
      }
    });
  } catch (error) {
    // await t.rollback();
    return res.status(400).json({
      status: 400,
      message: "Terjadi kesalahan data",
      result: error.message,
    });
  }
};

module.exports = {
  index,
  store,
  edit,
  deleteData,
  importData,
};
