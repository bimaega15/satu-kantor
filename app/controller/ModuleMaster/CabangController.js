const { validationResult } = require("express-validator");
const { cabangHelper, pagination } = require("../../helper/index");
const moment = require("moment");
const readXlsxFile = require("read-excel-file/node");
const { Cabang, Pegawai } = require("../../model");
const sequelize = require("../../config/db");

const index = async (req, res) => {
  try {
    let user = req.user.data;
    let getPegawai = await Pegawai.findOne({
      where: {
        id_client: user.id_mapping,
      },
    });
    req.query.client_id = getPegawai.id_client;

    if (req.xhr) {
      let id_client = req.query.client_id;
      if (user.jenis_mapping == "client") {
        id_client = user.id_mapping;
      }

      // page
      const page =
        req.query.page == null || req.query.page == "" ? 1 : req.query.page;
      const limit =
        req.query.limit == null || req.query.limit == "" ? 10 : req.query.limit;
      const search = req.query.search;

      const halamanAkhir = page * limit;
      const halamanAwal = halamanAkhir - limit;
      const offset = halamanAkhir;
      const skip = halamanAwal;

      let getFilter = req.query.filter;
      let cabang = await cabangHelper.getCabang(
        limit,
        skip,
        null,
        id_client,
        getFilter
      );
      let model = await cabangHelper.Cabang.count({
        where: {
          id_client: user.id_mapping,
        },
      });
      if (getFilter != null) {
        let getModel = await cabangHelper.getCabang(
          null,
          null,
          null,
          id_client,
          getFilter
        );
        model = getModel.length;
      }

      if (search != null && search != "") {
        cabang = await cabangHelper.getCabang(
          limit,
          skip,
          search,
          id_client,
          getFilter
        );
        let getModel = await cabangHelper.getCabang(
          null,
          null,
          search,
          id_client,
          getFilter
        );
        model = getModel.length;
      }

      // pagination
      const getPagination = pagination(page, model, limit);

      let keterangan = {
        from: skip + 1,
        to: offset,
        total: model,
      };

      let output = {
        data: cabang,
        pagination: getPagination,
        keterangan: keterangan,
      };
      return res.status(200).json({
        status: 200,
        message: "Berhasil tangkap data",
        output: output,
      });
    }
    let id_client = req.query.client_id;
    if (user.jenis_mapping == "client") {
      id_client = user.id_mapping;
    }

    // breadcrumb
    let breadcrumb = [];
    if (user.jenis_mapping == "admin") {
      breadcrumb.push({ label: "Home", url: "/admin/dashboard", isActive: "" });
      breadcrumb.push({
        label: "Client",
        url: "/admin/client",
      });
      breadcrumb.push({
        label: "Dashboard Client",
        url: "/admin/dashboardClient?client_id=" + id_client,
      });
      breadcrumb.push({
        label: "Cabang",
        url: "/admin/cabang?client_id=" + id_client,
        isActive: "active",
      });
    } else {
      breadcrumb.push({ label: "Home", url: "/admin/dashboard", isActive: "" });

      breadcrumb.push({
        label: "Cabang",
        url: "/admin/cabang",
        isActive: "active",
      });
    }

    res.render("./moduleMaster/cabang/index", {
      title: "Cabang",
      breadcrumb: breadcrumb,
      currentUrl: req.originalUrl,
      id_client: id_client,
      client_id: id_client,
    });
  } catch (err) {
    return res.status(400).json({
      status: 400,
      message: "Terjadi kesalahan data",
      result: err.message,
    });
  }
};

const store = async (req, res) => {
  let user = req.user.data;
  let getPegawai = await Pegawai.findOne({
    where: {
      id_client: user.id_mapping,
    },
  });
  req.query.client_id = getPegawai.id_client;
  // const t = await sequelize.transaction();

  try {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({
        status: 400,
        message: "Invalid form validation",
        result: errors.array(),
      });
    } else {
      const response = req.body;

      if (response.page == "add") {
        let dateTime = moment().format("YYYY-MM-DD HH:mm:ss");
        let data = {
          nama_cabang: response.nama_cabang,
          alamat: response.alamat,
          kontak: response.kontak,
          is_aktif: response.is_aktif,
          user_create: user.id,
          user_update: user.id,
          time_create: dateTime,
          time_update: dateTime,
          id_client: response.id_client,
          longitude: response.longitude,
          latitude: response.latitude,
        };
        let insert = await cabangHelper.Cabang.create(data, {
          // transaction: t,
        });
        // await t.commit();

        if (insert) {
          return res.status(200).json({
            status: 200,
            message: "Berhasil insert data",
            result: response,
          });
        } else {
          return res.status(400).json({
            status: 400,
            message: "Gagal insert data",
          });
        }
      } else {
        let dateTime = moment().format("YYYY-MM-DD HH:mm:ss");
        let id_cabang = response.id_cabang;
        let data = {
          nama_cabang: response.nama_cabang,
          alamat: response.alamat,
          kontak: response.kontak,
          is_aktif: response.is_aktif,
          user_update: user.id,
          time_update: dateTime,
          id_client: response.id_client,
          longitude: response.longitude,
          latitude: response.latitude,
        };
        let update = await cabangHelper.Cabang.update(data, {
          where: {
            id_cabang: id_cabang,
          },
          // transaction: t,
        });
        // await t.commit();

        if (update) {
          return res.status(200).json({
            status: 200,
            message: "Berhasil update data",
            result: response,
          });
        } else {
          return res.status(400).json({
            status: 400,
            message: "Gagal update data",
          });
        }
      }
    }
  } catch (error) {
    // await t.rollback();
    return res.status(400).json({
      status: 400,
      message: "Terjadi kesalahan data",
      result: error.message,
    });
  }
};

const edit = async (req, res) => {
  try {
    const id_cabang = req.params.id_cabang;
    const getCabang = await cabangHelper.Cabang.findOne({
      where: { id_cabang: id_cabang },
    });
    if (getCabang) {
      return res.status(200).json({
        status: 200,
        message: "Berhasil mengambil data cabang",
        result: getCabang,
      });
    } else {
      return res.status(400).json({
        status: 400,
        message: "Gagal mengambil data cabang",
      });
    }
  } catch (error) {
    return res.status(400).json({
      status: 400,
      message: "Terjadi kesalahan data",
      result: error.message,
    });
  }
};

const deleteData = async (req, res) => {
  // const t = await sequelize.transaction();

  try {
    const id_cabang = req.params.id_cabang;
    const getCabang = await cabangHelper.Cabang.destroy({
      where: {
        id_cabang: id_cabang,
      },
      // transaction: t,
    });

    // await t.commit();
    if (getCabang) {
      return res.status(200).json({
        status: 200,
        message: "Berhasil menghapus data cabang",
        result: getCabang,
      });
    } else {
      return res.status(400).json({
        status: 400,
        message: "Gagal menghapus data cabang",
      });
    }
  } catch (error) {
    // await t.rollback();
    return res.status(400).json({
      status: 400,
      message: "Terjadi kesalahan data",
      result: error.message,
    });
  }
};

const importData = async (req, res) => {
  // const t = await sequelize.transaction();
  try {
    let user = req.user.data;
    let getPegawai = await Pegawai.findOne({
      where: {
        id_client: user.id_mapping,
      },
    });
    req.query.client_id = getPegawai.id_client;
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({
        status: 400,
        message: "Invalid form validation",
        result: errors.array(),
      });
    }

    const { importData } = req.body.importData;

    readXlsxFile(importData.path).then(async (rows) => {
      let pushData = [];
      let dateTime = moment().format("YYYY-MM-DD HH:mm:ss");

      rows.map((v, i) => {
        if (i > 0) {
          if (v[1] != null) {
            pushData.push({
              id_client: user.id_mapping,
              nama_cabang: v[1],
              alamat: v[2],
              kontak: v[3],
              is_aktif: 1,
              user_create: user.id,
              user_update: user.id,
              time_create: dateTime,
              time_update: dateTime,
              longitude: v[4],
              latitude: v[5],
            });
          }
        }
      });

      let importCabang = await Cabang.bulkCreate(pushData, {
        // transaction: t,
      });
      // await t.commit();

      if (importCabang) {
        return res.status(200).json({
          status: 200,
          message: "Berhasil import " + pushData.length + " data cabang",
          result: req.body,
        });
      } else {
        return res.status(400).json({
          status: 400,
          message: "Gagal import " + pushData.length + " data cabang",
        });
      }
    });
  } catch (error) {
    // await t.rollback();
    return res.status(400).json({
      status: 400,
      message: "Terjadi kesalahan data",
      result: error.message,
    });
  }
};

module.exports = {
  index,
  store,
  edit,
  deleteData,
  importData,
};
