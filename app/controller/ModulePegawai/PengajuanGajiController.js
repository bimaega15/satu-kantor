const {
  pagination,
  pengajuanGajiHelper,
  pengajuanGajiHelper: { getPengajuanGaji },
  cabangHelper,
} = require("../../helper/index");
const { validationResult } = require("express-validator");
const moment = require("moment");
const {
  PegawaiUnit,
  Pegawai,
  PegawaiKontrak,
  PegawaiKomponenGaji,
  KomponenGaji,
  PengajuanGajiList,
  PengajuanGajiListKomponen,
  Client,
  Cabang,
  PengajuanGaji,
} = require("../../model");
const { Op, where } = require("sequelize");
const { underline } = require("colors");
const sequelize = require("../../config/db");

const index = async (req, res) => {
  let user = req.user.data;
  let getPegawai = await Pegawai.findOne({
    where: {
      id_client: user.id_mapping,
    },
  });
  req.query.client_id = getPegawai.id_client;

  try {
    if (req.xhr) {
      // page
      const page =
        req.query.page == null || req.query.page == "" ? 1 : req.query.page;
      const limit =
        req.query.limit == null || req.query.limit == "" ? 10 : req.query.limit;
      const search = req.query.search;

      const halamanAkhir = page * limit;
      const halmaanAwal = halamanAkhir - limit;
      const offset = halamanAkhir;
      const skip = halmaanAwal;

      let getFilter = {};
      getFilter.id_client = user.id_mapping;
      if (req.query.filter != null) {
        getFilter = {
          ...getFilter,
          ...req.query.filter,
        };
      }
      let pengajuanGaji = await getPengajuanGaji(limit, skip, null, getFilter);
      let model = await pengajuanGajiHelper.PengajuanGaji.count({
        where: {
          id_client: user.id_mapping,
        },
      });
      if (getFilter != null) {
        let getModel = await getPengajuanGaji(null, null, null, getFilter);
        model = getModel.length;
      }
      if (search != null && search != "") {
        pengajuanGaji = await getPengajuanGaji(limit, skip, search, getFilter);
        let getModel = await getPengajuanGaji(null, null, search, getFilter);
        model = getModel.length;
      }

      // pagination
      const getPagination = pagination(page, model, limit);

      let keterangan = {
        from: skip + 1,
        to: offset,
        total: model,
      };

      let output = {
        data: pengajuanGaji,
        pagination: getPagination,
        keterangan: keterangan,
      };
      return res.status(200).json({
        status: 200,
        message: "Berhasil tangkap data",
        output: output,
      });
    }

    // breadcrumb
    let breadcrumb = [];
    breadcrumb.push({ label: "Home", url: "/admin/dashboard", isActive: "" });
    breadcrumb.push({
      label: "Pengajuan gaji",
      url: "/admin/pengajuanGaji",
      isActive: "active",
    });

    let cabang = await cabangHelper.Cabang.findAll({
      where: {
        id_client: user.id_mapping,
      },
      include: [
        {
          model: PegawaiUnit,
          where: {
            is_aktif: 1,
          },
          required: true,
        },
      ],
    });

    res.render("./modulPegawai/pengajuanGaji/index", {
      cabang: cabang,
      title: "Pengajuan gaji",
      breadcrumb: breadcrumb,
      currentUrl: req.originalUrl,
    });
  } catch (error) {
    return res.status(400).json({
      status: 400,
      message: "Terjadi kesalahan data",
      result: error.message,
    });
  }
};

const store = async (req, res) => {
  let user = req.user.data;
  let getPegawai = await Pegawai.findOne({
    where: {
      id_client: user.id_mapping,
    },
  });
  req.query.client_id = getPegawai.id_client;
  // const t = await sequelize.transaction();

  try {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({
        status: 400,
        message: "Invalid form validation",
        result: errors.array(),
      });
    } else {
      const response = req.body;
      if (response.page == "add") {
        let output = {};
        output.id_client = user.id_mapping;
        output.id_cabang = response.id_cabang;
        output.periode_pengajuan = moment().format("YYYY-MM-DD HH:mm:ss");
        output.status_pengajuan = "permohonan";
        output.is_delete = 0;
        output.user_create = user.id;
        output.user_update = user.id;
        output.time_create = moment().format("YYYY-MM-DD HH:mm:ss");
        output.time_update = moment().format("YYYY-MM-DD HH:mm:ss");
        output.periode_awal = moment().format("YYYY-MM-DD HH:mm:ss");
        output.periode_akhir = moment().format("YYYY-MM-DD HH:mm:ss");
        let insertPengajuanGaji = await pengajuanGajiHelper.createPengajuanGaji(
          output,
          {
            // transaction: t,
          }
        );
        insertPengajuanGaji = insertPengajuanGaji.id_pengajuan_gaji;
        // let insertPengajuanGaji = 1;

        // pegawai unit cabang
        let pegawaiKontrakCabang = await PegawaiKontrak.findAll({
          where: {
            id_cabang: response.id_cabang,
            is_aktif: 1,
          },
          include: [
            {
              model: Pegawai,
              required: true,
            },
          ],
        });

        // pegawai id
        let pegawaiId = [];
        pegawaiKontrakCabang.map((v, i) => {
          pegawaiId.push(v.pegawai.id_pegawai);
        });

        // pegawai kontrak
        let pegawaiKontrak = [];
        pegawaiKontrak = await PegawaiKontrak.findAll({
          where: {
            id_pegawai: {
              [Op.in]: pegawaiId,
            },
            is_aktif: 1,
          },
          include: [
            {
              model: PegawaiKomponenGaji,
              include: [
                {
                  model: KomponenGaji,
                },
              ],
            },
          ],
        });

        //  nominal total pegawai kontrak
        let getDataPegawaiKontrak = {};
        pegawaiKontrak.map((v, i) => {
          let id_pegawai_kontrak = v.id_pegawai_kontrak;
          if (v.pegawai_komponen_gajis != null) {
            v.pegawai_komponen_gajis.map((v2, i2) => {
              let getKomponenGaji = v2.komponen_gaji;
              let jenisKomponen = getKomponenGaji.komponen;
              let komponenGaji = getKomponenGaji.komponen_gaji.toLowerCase();

              let idPiutang = null;
              let idKomponenGaji = getKomponenGaji.id_komponen_gaji;

              if (komponenGaji == "piutang karyawan") {
                idPiutang = getKomponenGaji.id_komponen_gaji;
              }

              if (getDataPegawaiKontrak[id_pegawai_kontrak] !== undefined) {
                if (
                  getDataPegawaiKontrak[id_pegawai_kontrak]
                    .hasOwnProperty(jenisKomponen)
                    .hasOwnProperty(v2.id_pegawai_komponen_gaji)
                ) {
                } else {
                  getDataPegawaiKontrak[id_pegawai_kontrak][jenisKomponen] = {
                    ...getDataPegawaiKontrak[id_pegawai_kontrak][jenisKomponen],
                    [v2.id_pegawai_komponen_gaji]: {
                      nominal: v2.nominal,
                      jenis_komponen: getKomponenGaji.jenis,
                      idPiutang: idPiutang,
                      idKomponenGaji: idKomponenGaji,
                    },
                  };
                }
              } else {
                getDataPegawaiKontrak[id_pegawai_kontrak] = {
                  [jenisKomponen]: {
                    [v2.id_pegawai_komponen_gaji]: {
                      nominal: v2.nominal,
                      jenis_komponen: getKomponenGaji.jenis,
                      idPiutang: idPiutang,
                      idKomponenGaji: idKomponenGaji,
                    },
                  },
                };
              }
            });
          }
        });

        // insert pegawai gaji list
        // getDataPegawaiKontrak
        let nominalPerusahaan = 0;
        let nominalTotal = 0;
        let pushPengajuanGajiListKomponen = {};
        let pushPengajuanGajiList = [];
        if (getDataPegawaiKontrak != null) {
          Object.keys(getDataPegawaiKontrak).map(
            async (id_pegawai_kontrak, i) => {
              // list komponen gaji
              nominalPerusahaan = 0;
              nominalTotal = 0;
              Object.keys(getDataPegawaiKontrak[id_pegawai_kontrak]).map(
                (v2, i2) => {
                  Object.keys(
                    getDataPegawaiKontrak[id_pegawai_kontrak][v2]
                  ).map((v3, i3) => {
                    let getData =
                      getDataPegawaiKontrak[id_pegawai_kontrak][v2][v3];
                    let jenisKomponen = getData.jenis_komponen;
                    let nominalData = parseFloat(getData.nominal);

                    if (v2 == "perusahaan") {
                      if (jenisKomponen == "+") {
                        nominalPerusahaan += nominalData;
                      } else {
                        nominalPerusahaan -= nominalData;
                      }
                    }

                    if (jenisKomponen == "+") {
                      nominalTotal += nominalData;
                    } else {
                      nominalTotal -= nominalData;
                    }
                  });
                }
              );

              // pengajuan gaji list
              let dataPegawaiGajiList = {};
              dataPegawaiGajiList.id_pengajuan_gaji = insertPengajuanGaji;
              dataPegawaiGajiList.id_pegawai = null;
              dataPegawaiGajiList.id_pegawai_kontrak = id_pegawai_kontrak;
              dataPegawaiGajiList.nominal_total = nominalTotal;
              dataPegawaiGajiList.nominal_perusahaan = nominalPerusahaan;
              dataPegawaiGajiList.is_valid = 1;
              pushPengajuanGajiList.push(dataPegawaiGajiList);

              // pengajuan gaji list komponen
              Object.keys(getDataPegawaiKontrak[id_pegawai_kontrak]).map(
                (v2, i2) => {
                  Object.keys(
                    getDataPegawaiKontrak[id_pegawai_kontrak][v2]
                  ).map(async (v3, i3) => {
                    let getData =
                      getDataPegawaiKontrak[id_pegawai_kontrak][v2][v3];
                    let jenisKomponen = getData.jenis_komponen;
                    let nominalData = getData.nominal;
                    let idPiutang = getData.idPiutang;
                    let idKomponenGaji = getData.idKomponenGaji;

                    // insert to database
                    let pengajuanGajiListKomponen = {};
                    pengajuanGajiListKomponen.id_pengajuan_gaji_list = null;
                    pengajuanGajiListKomponen.id_pegawai_komponen_gaji = v3;
                    pengajuanGajiListKomponen.id_komponen_gaji = idKomponenGaji;
                    pengajuanGajiListKomponen.id_piutang = idPiutang;
                    pengajuanGajiListKomponen.nominal = nominalData;
                    pengajuanGajiListKomponen.jenis_komponen = jenisKomponen;
                    pengajuanGajiListKomponen.komponen = v2;
                    if (
                      pushPengajuanGajiListKomponen.hasOwnProperty(
                        id_pegawai_kontrak
                      )
                    ) {
                      pushPengajuanGajiListKomponen[id_pegawai_kontrak] = [
                        ...pushPengajuanGajiListKomponen[id_pegawai_kontrak],
                        pengajuanGajiListKomponen,
                      ];
                    } else {
                      pushPengajuanGajiListKomponen[id_pegawai_kontrak] = [
                        pengajuanGajiListKomponen,
                      ];
                    }
                  });
                }
              );
            }
          );

          let arrPengajuanGajiListKomponen = [];

          pushPengajuanGajiList.map(async (v, i) => {
            arrPengajuanGajiListKomponen.push(
              pushArrPengajuanGajiList(v, pushPengajuanGajiListKomponen, t)
            );
          });

          Promise.all(arrPengajuanGajiListKomponen)
            .then(async (result) => {
              let insertManyKomponenGaji = [];
              result.map((v, i) => {
                insertManyKomponenGaji.push(...v);
              });

              let insertPengajuanGajiListKomponen =
                await PengajuanGajiListKomponen.bulkCreate(
                  insertManyKomponenGaji,
                  {
                    // transaction: t,
                  }
                );

              // await t.commit();

              if (
                insertPengajuanGaji ||
                result ||
                insertPengajuanGajiListKomponen
              ) {
                return res.status(200).json({
                  status: 200,
                  message: "Berhasil insert data",
                  result: response,
                });
              } else {
                return res.status(400).json({
                  status: 400,
                  message: "Gagal insert data",
                });
              }
            })
            .catch(async (error) => {
              // await t.rollback();
              return res.status(400).json({
                status: 400,
                message: "Terjadi kesalahan data",
                result: error.message,
              });
            });
        } else {
          // await t.commit();
          if (insertPengajuanGaji) {
            return res.status(200).json({
              status: 200,
              message: "Berhasil insert data",
              result: response,
            });
          } else {
            return res.status(400).json({
              status: 400,
              message: "Gagal insert data",
            });
          }
        }
      } else {
        let output = {};
        let id_pengajuan_gaji = response.id_pengajuan_gaji;
        output.id_client = user.id_mapping;
        output.id_cabang = response.id_cabang;
        output.periode_pengajuan = moment().format("YYYY-MM-DD HH:mm:ss");
        output.status_pengajuan = "permohonan";
        output.is_delete = 0;
        output.user_create = user.id;
        output.user_update = user.id;
        output.time_create = moment().format("YYYY-MM-DD HH:mm:ss");
        output.time_update = moment().format("YYYY-MM-DD HH:mm:ss");
        output.periode_awal = moment().format("YYYY-MM-DD HH:mm:ss");
        output.periode_akhir = moment().format("YYYY-MM-DD HH:mm:ss");

        // check cabang yang beda
        let checkCabang = await PengajuanGaji.findOne({
          where: {
            id_pengajuan_gaji: id_pengajuan_gaji,
            id_cabang: response.id_cabang,
          },
          include: [
            {
              model: Cabang,
            },
          ],
        });

        let deletePengajuanGajiList = false;
        if (checkCabang == null) {
          deletePengajuanGajiList = true;
        }

        let updatePengajuanGaji = await pengajuanGajiHelper.updatePengajuanGaji(
          output,
          id_pengajuan_gaji,
          {
            // transaction: t,
          }
        );
        updatePengajuanGaji = updatePengajuanGaji.id_pengajuan_gaji;

        // pegawai unit cabang
        let pegawaiKontrakCabang = await PegawaiKontrak.findAll({
          where: {
            id_cabang: response.id_cabang,
          },
          include: [
            {
              model: Pegawai,
              required: true,
            },
          ],
        });

        // pegawai id
        let pegawaiId = [];
        pegawaiKontrakCabang.map((v, i) => {
          pegawaiId.push(v.pegawai.id_pegawai);
        });

        // pegawai kontrak
        let pegawaiKontrak = [];
        pegawaiKontrak = await PegawaiKontrak.findAll({
          where: {
            id_pegawai: {
              [Op.in]: pegawaiId,
            },
            is_aktif: 1,
          },
          include: [
            {
              model: PegawaiKomponenGaji,
              include: [
                {
                  model: KomponenGaji,
                },
              ],
            },
          ],
        });

        //  nominal total pegawai kontrak
        let getDataPegawaiKontrak = {};
        pegawaiKontrak.map((v, i) => {
          let id_pegawai_kontrak = v.id_pegawai_kontrak;
          if (v.pegawai_komponen_gajis != null) {
            v.pegawai_komponen_gajis.map((v2, i2) => {
              let getKomponenGaji = v2.komponen_gaji;
              let jenisKomponen = getKomponenGaji.komponen;
              let komponenGaji = getKomponenGaji.komponen_gaji.toLowerCase();

              let idPiutang = null;
              let idKomponenGaji = getKomponenGaji.id_komponen_gaji;

              if (komponenGaji == "piutang karyawan") {
                idPiutang = getKomponenGaji.id_komponen_gaji;
              }

              if (getDataPegawaiKontrak[id_pegawai_kontrak] !== undefined) {
                if (
                  getDataPegawaiKontrak[id_pegawai_kontrak]
                    .hasOwnProperty(jenisKomponen)
                    .hasOwnProperty(v2.id_pegawai_komponen_gaji)
                ) {
                } else {
                  getDataPegawaiKontrak[id_pegawai_kontrak][jenisKomponen] = {
                    ...getDataPegawaiKontrak[id_pegawai_kontrak][jenisKomponen],
                    [v2.id_pegawai_komponen_gaji]: {
                      nominal: v2.nominal,
                      jenis_komponen: getKomponenGaji.jenis,
                      idPiutang: idPiutang,
                      idKomponenGaji: idKomponenGaji,
                    },
                  };
                }
              } else {
                getDataPegawaiKontrak[id_pegawai_kontrak] = {
                  [jenisKomponen]: {
                    [v2.id_pegawai_komponen_gaji]: {
                      nominal: v2.nominal,
                      jenis_komponen: getKomponenGaji.jenis,
                      idPiutang: idPiutang,
                      idKomponenGaji: idKomponenGaji,
                    },
                  },
                };
              }
            });
          }
        });

        // insert pegawai gaji list
        // getDataPegawaiKontrak
        let nominalPerusahaan = 0;
        let nominalTotal = 0;
        let pushPengajuanGajiListKomponen = {};
        let pushPengajuanGajiList = [];
        if (getDataPegawaiKontrak != null && deletePengajuanGajiList) {
          // delete pengajuan gaji list
          await PengajuanGajiList.destroy({
            where: {
              id_pengajuan_gaji: response.id_pengajuan_gaji,
            },
            // transaction: t,
          });

          Object.keys(getDataPegawaiKontrak).map(
            async (id_pegawai_kontrak, i) => {
              // list komponen gaji
              nominalPerusahaan = 0;
              nominalTotal = 0;
              Object.keys(getDataPegawaiKontrak[id_pegawai_kontrak]).map(
                (v2, i2) => {
                  Object.keys(
                    getDataPegawaiKontrak[id_pegawai_kontrak][v2]
                  ).map((v3, i3) => {
                    let getData =
                      getDataPegawaiKontrak[id_pegawai_kontrak][v2][v3];
                    let jenisKomponen = getData.jenis_komponen;
                    let nominalData = parseFloat(getData.nominal);

                    if (v2 == "perusahaan") {
                      if (jenisKomponen == "+") {
                        nominalPerusahaan += nominalData;
                      } else {
                        nominalPerusahaan -= nominalData;
                      }
                    }

                    if (jenisKomponen == "+") {
                      nominalTotal += nominalData;
                    } else {
                      nominalTotal -= nominalData;
                    }
                  });
                }
              );

              // pengajuan gaji list
              let dataPegawaiGajiList = {};
              dataPegawaiGajiList.id_pengajuan_gaji = id_pengajuan_gaji;
              dataPegawaiGajiList.id_pegawai = null;
              dataPegawaiGajiList.id_pegawai_kontrak = id_pegawai_kontrak;
              dataPegawaiGajiList.nominal_total = nominalTotal;
              dataPegawaiGajiList.nominal_perusahaan = nominalPerusahaan;
              dataPegawaiGajiList.is_valid = 1;
              pushPengajuanGajiList.push(dataPegawaiGajiList);

              // pengajuan gaji list komponen
              Object.keys(getDataPegawaiKontrak[id_pegawai_kontrak]).map(
                (v2, i2) => {
                  Object.keys(
                    getDataPegawaiKontrak[id_pegawai_kontrak][v2]
                  ).map(async (v3, i3) => {
                    let getData =
                      getDataPegawaiKontrak[id_pegawai_kontrak][v2][v3];
                    let jenisKomponen = getData.jenis_komponen;
                    let nominalData = getData.nominal;
                    let idPiutang = getData.idPiutang;
                    let idKomponenGaji = getData.idKomponenGaji;

                    // insert to database
                    let pengajuanGajiListKomponen = {};
                    pengajuanGajiListKomponen.id_pengajuan_gaji_list = null;
                    pengajuanGajiListKomponen.id_pegawai_komponen_gaji = v3;
                    pengajuanGajiListKomponen.id_komponen_gaji = idKomponenGaji;
                    pengajuanGajiListKomponen.id_piutang = idPiutang;
                    pengajuanGajiListKomponen.nominal = nominalData;
                    pengajuanGajiListKomponen.jenis_komponen = jenisKomponen;
                    pengajuanGajiListKomponen.komponen = v2;
                    if (
                      pushPengajuanGajiListKomponen.hasOwnProperty(
                        id_pegawai_kontrak
                      )
                    ) {
                      pushPengajuanGajiListKomponen[id_pegawai_kontrak] = [
                        ...pushPengajuanGajiListKomponen[id_pegawai_kontrak],
                        pengajuanGajiListKomponen,
                      ];
                    } else {
                      pushPengajuanGajiListKomponen[id_pegawai_kontrak] = [
                        pengajuanGajiListKomponen,
                      ];
                    }
                  });
                }
              );
            }
          );

          let arrPengajuanGajiListKomponen = [];

          pushPengajuanGajiList.map(async (v, i) => {
            arrPengajuanGajiListKomponen.push(
              pushArrPengajuanGajiList(v, pushPengajuanGajiListKomponen, t)
            );
          });

          Promise.all(arrPengajuanGajiListKomponen)
            .then(async (result) => {
              let updatePegawaiKomponenGaji = [];
              result.map((v, i) => {
                updatePegawaiKomponenGaji.push(...v);
              });

              let updatePengajuanGajiListKomponen =
                await PengajuanGajiListKomponen.bulkCreate(
                  updatePegawaiKomponenGaji,
                  {
                    // transaction: t,
                  }
                );

              // await t.commit();
              if (
                updatePengajuanGaji ||
                result ||
                updatePengajuanGajiListKomponen
              ) {
                return res.status(200).json({
                  status: 200,
                  message: "Berhasil insert data",
                  result: response,
                });
              } else {
                return res.status(400).json({
                  status: 400,
                  message: "Gagal insert data",
                });
              }
            })
            .catch(async (error) => {
              // await t.rollback();
              return res.status(400).json({
                status: 400,
                message: "Terjadi kesalahan data",
                result: error.message,
              });
            });
        } else {
          // await t.commit();
          if (updatePengajuanGaji) {
            return res.status(200).json({
              status: 200,
              message: "Berhasil insert data",
              result: response,
            });
          } else {
            return res.status(400).json({
              status: 400,
              message: "Gagal insert data",
            });
          }
        }
      }
    }
  } catch (error) {
    // await t.rollback();
    return res.status(400).json({
      status: 400,
      message: "Terjadi kesalahan data",
      result: error.message,
    });
  }
};

const pushArrPengajuanGajiList = async (
  v,
  pushPengajuanGajiListKomponen,
  t
) => {
  let arrPengajuanGajiListKomponen = [];
  let getPegawaiKontrak = await PegawaiKontrak.findOne({
    where: {
      id_pegawai_kontrak: v.id_pegawai_kontrak,
    },
  });

  let dataPengajuanGajiList = {
    ...v,
    id_pegawai: getPegawaiKontrak.id_pegawai,
  };
  // insert many pengajuan gaji list
  let insertPengajuanGajiList = await PengajuanGajiList.create(
    dataPengajuanGajiList,
    {
      // transaction: t,
    }
  );
  insertPengajuanGajiList = insertPengajuanGajiList.id_pengajuan_gaji_list;

  // insert pengajuan gaji list komponen
  pushPengajuanGajiListKomponen[v.id_pegawai_kontrak].map(async (v2, i2) => {
    let dataPengajuanGajiListKomponen = {
      ...v2,
      id_pengajuan_gaji_list: insertPengajuanGajiList,
    };

    arrPengajuanGajiListKomponen.push(dataPengajuanGajiListKomponen);
  });

  return arrPengajuanGajiListKomponen;
};

const edit = async (req, res) => {
  try {
    const id_pengajuan_gaji = req.params.id_pengajuan_gaji;
    const getPengajuanGaji = await pengajuanGajiHelper.getPengajuanGajiId(
      id_pengajuan_gaji
    );
    if (getPengajuanGaji) {
      return res.status(200).json({
        status: 200,
        message: "Berhasil mengambil data pegawai jabatan",
        result: getPengajuanGaji,
      });
    } else {
      return res.status(400).json({
        status: 400,
        message: "Gagal mengambil data pegawai jabatan",
      });
    }
  } catch (error) {
    return res.status(400).json({
      status: 400,
      message: "Terjadi kesalahan data",
      result: error.message,
    });
  }
};

const deleteData = async (req, res) => {
  // const t = await sequelize.transaction();

  try {
    const id_pengajuan_gaji = req.params.id_pengajuan_gaji;
    const getPengajuanGaji = await pengajuanGajiHelper.deletePengajuanGaji(
      id_pengajuan_gaji,
      {
        // transaction: t,
      }
    );

    // await t.commit();

    if (getPengajuanGaji) {
      return res.status(200).json({
        status: 200,
        message: "Berhasil menghapus data pengajuanGaji",
        result: getPengajuanGaji,
      });
    } else {
      return res.status(400).json({
        status: 400,
        message: "Gagal menghapus data pengajuanGaji",
      });
    }
  } catch (error) {
    // await t.rollback();
    return res.status(400).json({
      status: 400,
      message: "Terjadi kesalahan data",
      result: error.message,
    });
  }
};

module.exports = {
  index,
  store,
  edit,
  deleteData,
};
