const {
  pagination,
  catatanPegawaiHelper,
  catatanPegawaiHelper: { getCatatanPegawai },
  pegawaiHelper,
} = require("../../helper/index");
const { validationResult } = require("express-validator");
const moment = require("moment");
const { Pegawai, Client } = require("../../model");
const sequelize = require("../../config/db");

const index = async (req, res) => {
  let user = req.user.data;
  let getPegawai = await Pegawai.findOne({
    where: {
      id_client: user.id_mapping,
    },
  });
  req.query.client_id = getPegawai.id_client;
  try {
    if (req.xhr) {
      // page
      const page =
        req.query.page == null || req.query.page == "" ? 1 : req.query.page;
      const limit =
        req.query.limit == null || req.query.limit == "" ? 10 : req.query.limit;
      const search = req.query.search;

      const halamanAkhir = page * limit;
      const halmaanAwal = halamanAkhir - limit;
      const offset = halamanAkhir;
      const skip = halmaanAwal;

      let id_client = user.id_mapping;
      let getFilter = req.query.filter;
      let catatanPegawai = await getCatatanPegawai(
        limit,
        skip,
        null,
        getFilter,
        id_client
      );
      let model = await catatanPegawaiHelper.PegawaiCatatan.count({
        where: {
          "$pegawai.client.id_client$": user.id_mapping,
        },
        include: [
          {
            model: Pegawai,
            include: [
              {
                model: Client,
                required: true,
              },
            ],
          },
        ],
      });
      if (getFilter != null) {
        let getModel = await getCatatanPegawai(
          null,
          null,
          null,
          getFilter,
          id_client
        );
        model = getModel.length;
      }
      if (search != null && search != "") {
        catatanPegawai = await getCatatanPegawai(
          limit,
          skip,
          search,
          getFilter,
          id_client
        );
        let getModel = await getCatatanPegawai(
          null,
          null,
          search,
          getFilter,
          id_client
        );
        model = getModel.length;
      }

      // pagination
      const getPagination = pagination(page, model, limit);

      let keterangan = {
        from: skip + 1,
        to: offset,
        total: model,
      };

      let output = {
        data: catatanPegawai,
        pagination: getPagination,
        keterangan: keterangan,
      };
      return res.status(200).json({
        status: 200,
        message: "Berhasil tangkap data",
        output: output,
      });
    }

    // breadcrumb
    let breadcrumb = [];
    breadcrumb.push({ label: "Home", url: "/admin/dashboard", isActive: "" });
    breadcrumb.push({
      label: "Catatan pegawai",
      url: "/admin/catatanPegawai",
      isActive: "active",
    });

    let pegawai = await pegawaiHelper.Pegawai.findAll({
      where: {
        id_client: user.id_mapping,
      },
    });

    res.render("./modulPegawai/catatanPegawai/index", {
      pegawai: pegawai,
      title: "Catatan pegawai",
      breadcrumb: breadcrumb,
      currentUrl: req.originalUrl,
    });
  } catch (error) {
    return res.status(400).json({
      status: 400,
      message: "Terjadi kesalahan data",
      result: error.message,
    });
  }
};

const store = async (req, res) => {
  let user = req.user.data;
  let getPegawai = await Pegawai.findOne({
    where: {
      id_client: user.id_mapping,
    },
  });
  req.query.client_id = getPegawai.id_client;
  // const t = await sequelize.transaction();

  try {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({
        status: 400,
        message: "Invalid form validation",
        result: errors.array(),
      });
    } else {
      const response = req.body;

      if (response.page == "add") {
        let output = {};
        output.jenis = response.jenis;
        output.id_pegawai = response.id_pegawai;
        output.catatan = response.catatan;
        output.waktu_efektif = moment(
          response.waktu_efektif,
          "DD-MM-YYYY"
        ).format("YYYY-MM-DD");
        output.user_create = user.id;
        output.user_update = user.id;
        output.time_create = moment().format("YYYY-MM-DD HH:mm:ss");
        output.time_update = moment().format("YYYY-MM-DD HH:mm:ss");

        let insert = await catatanPegawaiHelper.createCatatanPegawai(output, {
          // transaction: t,
        });
        // await t.commit();

        if (insert) {
          return res.status(200).json({
            status: 200,
            message: "Berhasil insert data",
            result: response,
          });
        } else {
          return res.status(400).json({
            status: 400,
            message: "Gagal insert data",
          });
        }
      } else {
        let id_pegawai_catatan = response.id_pegawai_catatan;
        let output = {};
        output.jenis = response.jenis;
        output.id_pegawai = response.id_pegawai;
        output.catatan = response.catatan;
        output.waktu_efektif = moment(
          response.waktu_efektif,
          "DD-MM-YYYY"
        ).format("YYYY-MM-DD");
        output.user_update = user.id;
        output.time_update = moment().format("YYYY-MM-DD HH:mm:ss");

        let update = await catatanPegawaiHelper.updateCatatanPegawai(
          output,
          id_pegawai_catatan,
          {
            // transaction: t,
          }
        );
        // await t.commit();

        if (update) {
          return res.json({
            status: 200,
            message: "Berhasil update data",
            result: response,
          });
        } else {
          return res.json({
            status: 400,
            message: "Gagal update data",
          });
        }
      }
    }
  } catch (error) {
    // await t.rollback();
    return res.status(400).json({
      status: 400,
      message: "Terjadi kesalahan data",
      result: error.message,
    });
  }
};

const edit = async (req, res) => {
  try {
    const id_pegawai_catatan = req.params.id_pegawai_catatan;
    const getCatatanPegawai = await catatanPegawaiHelper.getCatatanPegawaiId(
      id_pegawai_catatan
    );
    if (getCatatanPegawai) {
      return res.status(200).json({
        status: 200,
        message: "Berhasil mengambil data pegawai jabatan",
        result: getCatatanPegawai,
      });
    } else {
      return res.status(400).json({
        status: 400,
        message: "Gagal mengambil data pegawai jabatan",
      });
    }
  } catch (error) {
    return res.status(400).json({
      status: 400,
      message: "Terjadi kesalahan data",
      result: error.message,
    });
  }
};

const deleteData = async (req, res) => {
  // const t = await sequelize.transaction();

  try {
    const id_pegawai_catatan = req.params.id_pegawai_catatan;
    const getCatatanPegawai = await catatanPegawaiHelper.deleteCatatanPegawai(
      id_pegawai_catatan,
      {
        // transaction: t,
      }
    );
    // await t.commit();

    if (getCatatanPegawai) {
      return res.status(200).json({
        status: 200,
        message: "Berhasil menghapus data catatanPegawai",
        result: getCatatanPegawai,
      });
    } else {
      return res.status(400).json({
        status: 400,
        message: "Gagal menghapus data catatanPegawai",
      });
    }
  } catch (error) {
    // await t.rollback();
    return res.status(400).json({
      status: 400,
      message: "Terjadi kesalahan data",
      result: error.message,
    });
  }
};

module.exports = {
  index,
  store,
  edit,
  deleteData,
};
