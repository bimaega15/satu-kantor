const {
  pagination,
  jabatanPegawaiHelper,
  jabatanPegawaiHelper: { getJabatanPegawai },
  jabatanHelper,
} = require("../../helper/index");
const { Pegawai, Client } = require("../../model");

const index = async (req, res) => {
  let user = req.user.data;
  let getPegawai = await Pegawai.findOne({
    where: {
      id_client: user.id_mapping,
    },
  });
  req.query.client_id = getPegawai.id_client;
  try {
    if (req.xhr) {
      // page
      const page =
        req.query.page == null || req.query.page == "" ? 1 : req.query.page;
      const limit =
        req.query.limit == null || req.query.limit == "" ? 10 : req.query.limit;
      const search = req.query.search;

      const halamanAkhir = page * limit;
      const halamanAwal = halamanAkhir - limit;
      const offset = halamanAkhir;
      const skip = halamanAwal;

      let id_client = user.id_mapping;
      let getFilter = req.query.filter;
      let pegawaiJabatan = await getJabatanPegawai(
        limit,
        skip,
        null,
        getFilter,
        id_client
      );
      let model = await jabatanPegawaiHelper.PegawaiJabatan.count({
        where: {
          "$pegawai.client.id_client$": user.id_mapping,
        },
        include: [
          {
            model: Pegawai,
            include: [
              {
                model: Client,
                required: true,
              },
            ],
          },
        ],
      });
      if (getFilter != null) {
        let getModel = await getJabatanPegawai(
          null,
          null,
          null,
          getFilter,
          id_client
        );
        model = getModel.length;
      }
      if (search != null && search != "") {
        pegawaiJabatan = await getJabatanPegawai(
          limit,
          skip,
          search,
          getFilter,
          id_client
        );
        let getModel = await getJabatanPegawai(
          null,
          null,
          search,
          getFilter,
          id_client
        );
        model = getModel.length;
      }

      // pagination
      const getPagination = pagination(page, model, limit);

      let keterangan = {
        from: skip + 1,
        to: offset,
        total: model,
      };

      let output = {
        data: pegawaiJabatan,
        pagination: getPagination,
        keterangan: keterangan,
      };
      return res.status(200).json({
        status: 200,
        message: "Berhasil tangkap data",
        output: output,
      });
    }

    // breadcrumb
    let breadcrumb = [];
    breadcrumb.push({ label: "Home", url: "/admin/dashboard", isActive: "" });
    breadcrumb.push({
      label: "Jabatan pegawai",
      url: "/pegawai/jabatanPegawai",
      isActive: "active",
    });

    let jabatan = await jabatanHelper.Jabatan.findAll({
      where: {
        id_client: user.id_mapping,
      },
      order: [["id_jabatan", "asc"]],
    });

    res.render("./modulPegawai/jabatanPegawai/index", {
      title: "Pegawai jabatan",
      breadcrumb: breadcrumb,
      currentUrl: req.originalUrl,
      jabatan: jabatan,
    });
  } catch (error) {
    return res.status(400).json({
      status: 400,
      message: "Terjadi kesalahan data",
      result: error.message,
    });
  }
};

module.exports = {
  index,
};
