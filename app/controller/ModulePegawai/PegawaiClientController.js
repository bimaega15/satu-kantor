const { clientHelper, pagination } = require("../../helper/index");

const index = async (req, res) => {
  try {
    if (req.xhr) {
      // page
      const page =
        req.query.page == null || req.query.page == "" ? 1 : req.query.page;
      const limit =
        req.query.limit == null || req.query.limit == "" ? 10 : req.query.limit;
      const search = req.query.search;

      const halamanAkhir = page * limit;
      const halmaanAwal = halamanAkhir - limit;
      const offset = halamanAkhir;
      const skip = halmaanAwal;

      let client = await clientHelper.getClient(limit, skip);
      let model = await clientHelper.Client.count();
      if (search != null && search != "") {
        client = await clientHelper.getClient(null, null, search, limit);
        let getModel = await clientHelper.getClient(null, null, search);
        model = getModel.length;
      }

      // pagination
      const getPagination = pagination(page, model, limit);

      let keterangan = {
        from: skip + 1,
        to: offset,
        total: model,
      };

      let output = {
        data: client,
        pagination: getPagination,
        keterangan: keterangan,
      };
      return res.status(200).json({
        status: 200,
        message: "Berhasil tangkap data",
        output: output,
      });
    }

    // breadcrumb
    let breadcrumb = [];
    breadcrumb.push({ label: "Home", url: "/admin/dashboard", isActive: "" });
    breadcrumb.push({
      label: "Pegawai client",
      url: "/admin/pegawaiClient",
      isActive: "active",
    });

    res.render("./modulPegawai/pegawaiClient/index", {
      title: "Client",
      breadcrumb: breadcrumb,
      currentUrl: req.originalUrl,
    });
  } catch (error) {
    return res.status(400).json({
      status: 400,
      message: "Terjadi kesalahan data",
      result: error.message,
    });
  }
};

module.exports = {
  index,
};
