const moment = require("moment");
const {
  Pegawai,
  JenisCuti,
  Client,
  PegawaiSaldoCuti,
  PegawaiJadwal,
  Jadwal,
} = require("../../model");
const {
  pengajuanCutiHelper,
  pengajuanCutiHelper: { getPengajuanCuti, getPengajuanCutiId },
  pagination,
} = require("../../helper/index");
const { validationResult } = require("express-validator");
const mv = require("mv");
const { Op } = require("sequelize");
const sequelize = require("../../config/db");
const fs = require("fs");

const index = async (req, res) => {
  let user = req.user.data;
  let getPegawai = await Pegawai.findOne({
    where: {
      id_client: user.id_mapping,
    },
  });
  req.query.client_id = getPegawai.id_client;
  try {
    if (req.xhr) {
      // page
      const page =
        req.query.page == null || req.query.page == "" ? 1 : req.query.page;
      const limit =
        req.query.limit == null || req.query.limit == "" ? 10 : req.query.limit;
      const search = req.query.search;

      const halamanAkhir = page * limit;
      const halmaanAwal = halamanAkhir - limit;
      const offset = halamanAkhir;
      const skip = halmaanAwal;

      let getFilter = req.query.filter;
      let pengajuanCuti = await getPengajuanCuti(
        limit,
        skip,
        null,
        getFilter,
        user.id_mapping
      );
      let model = await pengajuanCutiHelper.PengajuanCuti.count({
        where: {
          "$pegawai.client.id_client$": user.id_mapping,
        },
        include: [
          {
            model: Pegawai,
            include: [
              {
                model: Client,
                required: true,
              },
            ],
          },
        ],
      });

      if (getFilter != null) {
        let getModel = await getPengajuanCuti(null, null, null, getFilter);
        model = getModel.length;
      }
      if (search != null && search != "") {
        pengajuanCuti = await getPengajuanCuti(limit, skip, search, getFilter);
        let getModel = await getPengajuanCuti(null, null, search, getFilter);
        model = getModel.length;
      }

      // pagination
      const getPagination = pagination(page, model, limit);

      let keterangan = {
        from: skip + 1,
        to: offset,
        total: model,
      };

      let output = {
        data: pengajuanCuti,
        pagination: getPagination,
        keterangan: keterangan,
      };
      return res.status(200).json({
        status: 200,
        message: "Berhasil tangkap data",
        output: output,
      });
    }

    // breadcrumb
    let breadcrumb = [];
    breadcrumb.push({ label: "Home", url: "/admin/dashboard", isActive: "" });
    breadcrumb.push({
      label: "Pengajuan cuti",
      url: "/admin/pengajuanCuti",
      isActive: "active",
    });

    let pegawai = await Pegawai.findAll({
      where: {
        id_client: user.id_mapping,
      },
    });
    let jenisCuti = await JenisCuti.findAll({
      where: {
        id_client: user.id_mapping,
      },
    });

    res.render("./modulPegawai/pengajuanCuti/index", {
      title: "Pengajuan cuti",
      breadcrumb: breadcrumb,
      currentUrl: req.originalUrl,
      pegawai: pegawai,
      jenisCuti: jenisCuti,
    });
  } catch (error) {
    return res.status(400).json({
      status: 400,
      message: "Terjadi kesalahan data",
      result: error.message,
    });
  }
};

const store = async (req, res) => {
  let user = req.user.data;
  let getPegawai = await Pegawai.findOne({
    where: {
      id_client: user.id_mapping,
    },
  });
  req.query.client_id = getPegawai.id_client;
  // const t = await sequelize.transaction();

  try {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({
        status: 400,
        message: "Invalid form validation",
        result: errors.array(),
      });
    } else {
      const response = req.body;
      let dateTime = moment().format("YYYY-MM-DD HH:mm:ss");

      if (response.page == "add") {
        let berkas_db = await uploadGambar(response.berkas);

        let output = {};
        let getJenisCuti = await JenisCuti.findOne({
          where: {
            id_jenis_cuti: response.id_jenis_cuti,
          },
        });
        output.id_jenis_cuti = response.id_jenis_cuti;
        output.id_pegawai = response.id_pegawai;
        output.tanggal_awal_cuti = moment(
          response.tanggal_awal_cuti,
          "DD-MM-YYYY"
        ).format("YYYY-MM-DD");
        output.tanggal_akhir_cuti = moment(output.tanggal_awal_cuti)
          .add(getJenisCuti.saldo, "days")
          .format("YYYY-MM-DD");
        output.id_pegawai_pengganti =
          response.id_pegawai_pengganti == ""
            ? null
            : response.id_pegawai_pengganti;
        output.catatan = response.catatan;
        output.id_atasan = response.id_atasan == "" ? null : response.id_atasan;
        output.is_setuju = null;
        output.waktu_pengajuan = dateTime;
        output.waktu_setuju = null;
        output.berkas = berkas_db;
        output.user_create = user.id;
        output.user_update = user.id;
        output.time_create = dateTime;
        output.time_update = dateTime;

        let insertPengajuanCuti = await pengajuanCutiHelper.createPengajuanCuti(
          output,
          {
            // transaction: t,
          }
        );

        // await t.commit();
        if (insertPengajuanCuti) {
          return res.json({
            status: 200,
            message: "Berhasil insert data",
            result: response,
          });
        } else {
          return res.json({
            status: 400,
            message: "Gagal insert data",
          });
        }
      } else {
        let id_pengajuan_cuti = response.id_pengajuan_cuti;
        let berkas_db = await uploadGambar(response.berkas, id_pengajuan_cuti);
        let getJenisCuti = await JenisCuti.findOne({
          where: {
            id_jenis_cuti: response.id_jenis_cuti,
          },
        });

        let output = {};
        output.id_jenis_cuti = response.id_jenis_cuti;
        output.id_pegawai = response.id_pegawai;
        output.tanggal_awal_cuti = moment(
          response.tanggal_awal_cuti,
          "DD-MM-YYYY"
        ).format("YYYY-MM-DD");
        output.tanggal_akhir_cuti = moment(output.tanggal_awal_cuti)
          .add(getJenisCuti.saldo, "days")
          .format("YYYY-MM-DD");
        output.id_pegawai_pengganti =
          response.id_pegawai_pengganti == ""
            ? null
            : response.id_pegawai_pengganti;
        output.catatan = response.catatan;
        output.id_atasan = response.id_atasan == "" ? null : response.id_atasan;
        output.is_setuju = null;
        output.waktu_pengajuan = dateTime;
        output.waktu_setuju = null;
        output.berkas = berkas_db;
        output.user_update = user.id;
        output.time_update = dateTime;

        let update = await pengajuanCutiHelper.updatePengajuanCuti(
          output,
          id_pengajuan_cuti,
          {
            // transaction: t,
          }
        );

        // await t.commit();
        if (update) {
          return res.json({
            status: 200,
            message: "Berhasil update data",
            result: response,
          });
        } else {
          return res.json({
            status: 400,
            message: "Gagal update data",
          });
        }
      }
    }
  } catch (error) {
    // await t.rollback();
    return res.status(400).json({
      status: 400,
      message: "Terjadi kesalahan data",
      result: error.message,
    });
  }
};

const edit = async (req, res) => {
  try {
    const id_pengajuan_cuti = req.params.id_pengajuan_cuti;
    const getPengajuanCuti = await pengajuanCutiHelper.getPengajuanCutiId(
      id_pengajuan_cuti
    );

    if (getPengajuanCuti) {
      return res.status(200).json({
        status: 200,
        message: "Berhasil mengambil data pengajuan cuti",
        result: getPengajuanCuti,
      });
    } else {
      return res.status(400).json({
        status: 400,
        message: "Gagal mengambil data pengajuan cuti",
      });
    }
  } catch (error) {
    return res.status(400).json({
      status: 400,
      message: "Terjadi kesalahan data",
      result: error.message,
    });
  }
};

const deleteData = async (req, res) => {
  // const t = await sequelize.transaction();

  try {
    const id_pengajuan_cuti = req.params.id_pengajuan_cuti;
    await deleteGambar(id_pengajuan_cuti);
    const getPengajuanCuti = await pengajuanCutiHelper.deletePengajuanCuti(
      id_pengajuan_cuti,
      {
        // transaction: t,
      }
    );
    // await t.commit();

    if (getPengajuanCuti) {
      return res.status(200).json({
        status: 200,
        message: "Berhasil menghapus data pengajuanCuti",
        result: getPengajuanCuti,
      });
    } else {
      return res.status(400).json({
        status: 400,
        message: "Gagal menghapus data pengajuanCuti",
      });
    }
  } catch (error) {
    // await t.rollback();
    return res.status(400).json({
      status: 400,
      message: "Terjadi kesalahan data",
      result: error.message,
    });
  }
};

const checkPegawaiSaldoCuti = async (req, res) => {
  try {
    const { id_pegawai, id_jenis_cuti } = req.query;

    let getPegawaiSaldoCuti = await PegawaiSaldoCuti.findOne({
      where: {
        id_pegawai: id_pegawai,
        id_jenis_cuti: id_jenis_cuti,
        is_aktif: 1,
        saldo: {
          [Op.gt]: 0,
        },
      },
    });
    if (getPegawaiSaldoCuti) {
      return res.status(200).json({
        status: 200,
        message: "Berhasil ambil pegawai saldo cuti",
        result: getPegawaiSaldoCuti,
      });
    } else {
      return res.status(400).json({
        status: 400,
        message: "Gagal ambil pegawai saldo cuti",
      });
    }
  } catch (error) {
    return res.status(400).json({
      status: 400,
      message: "Terjadi kesalahan data",
      result: error.message,
    });
  }
};

const checkJadwalPegawai = async (req, res) => {
  try {
    const {
      id_pegawai,
      tanggal_awal_cuti,
      tanggal_akhir_cuti,
      id_pegawai_pengajuan,
    } = req.query;

    const checkPegawaiPenggantiJadwal = await PegawaiJadwal.findAll({
      where: {
        id_pegawai: id_pegawai,
        tanggal: {
          [Op.between]: [tanggal_awal_cuti, tanggal_akhir_cuti],
        },
      },
      include: [
        {
          model: Jadwal,
        },
      ],
    });

    const checkPegawaiPengajuanJadwal = await PegawaiJadwal.findAll({
      where: {
        id_pegawai: id_pegawai_pengajuan,
        tanggal: {
          [Op.between]: [tanggal_awal_cuti, tanggal_akhir_cuti],
        },
      },
      include: [
        {
          model: Jadwal,
        },
      ],
    });

    let pushCheckJadwal = [];
    if (
      checkPegawaiPengajuanJadwal.length > 0 &&
      checkPegawaiPenggantiJadwal.length > 0
    ) {
      for (let i = 0; i < checkPegawaiPenggantiJadwal.length; i++) {
        let getPegawaiPengajuanJadwal = checkPegawaiPengajuanJadwal[i];
        let getPegawaiPenggantiJadwal = checkPegawaiPenggantiJadwal[i];

        let jadwalPegawaiPengganti = getPegawaiPenggantiJadwal.jadwal;
        let jadwalPegawaiPengajuan = getPegawaiPengajuanJadwal.jadwal;

        if (jadwalPegawaiPengganti == null) {
          pushCheckJadwal.push({
            is_ganti_jadwal: 1,
            id_pegawai_pengganti: getPegawaiPenggantiJadwal.id_pegawai,
            is_setuju: 1,
            id_pegawai_jadwal: getPegawaiPengajuanJadwal.id_pegawai_jadwal,
            is_bentrok: 0,
            tanggal: getPegawaiPengajuanJadwal.tanggal,
            jadwal: jadwalPegawaiPengganti,
            waktu_masuk: "",
            waktu_keluar: "",
          });
        }

        if (jadwalPegawaiPengganti != null && jadwalPegawaiPengajuan != null) {
          if (
            jadwalPegawaiPengganti.id_jadwal == jadwalPegawaiPengajuan.id_jadwal
          ) {
            pushCheckJadwal.push({
              is_ganti_jadwal: 1,
              id_pegawai_pengganti: getPegawaiPenggantiJadwal.id_pegawai,
              is_setuju: 1,
              id_pegawai_jadwal: getPegawaiPengajuanJadwal.id_pegawai_jadwal,
              is_bentrok: 1,
              tanggal: getPegawaiPengajuanJadwal.tanggal,
              jadwal: jadwalPegawaiPengganti,
              waktu_masuk: getPegawaiPengajuanJadwal.waktu_masuk,
              waktu_keluar: getPegawaiPengajuanJadwal.waktu_keluar,
            });
          } else {
            pushCheckJadwal.push({
              is_ganti_jadwal: 1,
              id_pegawai_pengganti: getPegawaiPenggantiJadwal.id_pegawai,
              is_setuju: 1,
              id_pegawai_jadwal: getPegawaiPengajuanJadwal.id_pegawai_jadwal,
              is_bentrok: 0,
              tanggal: getPegawaiPengajuanJadwal.tanggal,
              jadwal: jadwalPegawaiPengganti,
              waktu_masuk: getPegawaiPengajuanJadwal.waktu_masuk,
              waktu_keluar: getPegawaiPengajuanJadwal.waktu_keluar,
            });
          }
        }
      }
    }

    if (pushCheckJadwal) {
      return res.status(200).json({
        status: 200,
        message: "Berhasil ambil data check jadwal",
        result: pushCheckJadwal,
      });
    } else {
      return res.status(400).json({
        status: 400,
        message: "Gagal ambil data check jadwal",
      });
    }
  } catch (error) {
    return res.status(400).json({
      status: 400,
      message: "Terjadi kesalahan data",
      result: error.message,
    });
  }
};

const uploadGambar = async (responseBerkas, id_pengajuan_cuti = null) => {
  if (responseBerkas.berkas != null) {
    const { berkas } = responseBerkas;

    let pathOld = berkas.path;
    let name = berkas.name;
    let size = berkas.size;

    name =
      moment
        .duration(moment(moment().format("YYYY-MM-DD HH:mm:ss")))
        .asSeconds() +
      "_" +
      name.split(" ").join("-");
    pathNew = "public/image/pengajuanCuti/" + name;
    if (size > 0) {
      if (pathOld != null && pathNew != null) {
        if (id_pengajuan_cuti != null) {
          await deleteGambar(id_pengajuan_cuti);
        }

        mv(pathOld, pathNew, function (err) {
          if (err) {
            return err;
          }
        });

        const fileName = pathNew.split("/");
        const fileDb = fileName[fileName.length - 1];
        return fileDb;
      }
    }
  }

  if (id_pengajuan_cuti != null) {
    let getPengajuanCuti = await getPengajuanCutiId(id_pengajuan_cuti);
    if (
      getPengajuanCuti.berkas != "default.png" &&
      getPengajuanCuti.berkas != null
    ) {
      return getPengajuanCuti.berkas;
    }
  }
  return "default.png";
};

const deleteGambar = async (id_pengajuan_cuti = null) => {
  if (id_pengajuan_cuti != null) {
    let getPengajuanCuti = await getPengajuanCutiId(id_pengajuan_cuti);
    if (
      getPengajuanCuti.berkas != "default.png" &&
      getPengajuanCuti.berkas != null
    ) {
      let unlink = "public/image/pengajuanCuti/" + getPengajuanCuti.berkas;
      if (fs.existsSync(unlink)) {
        fs.unlinkSync(unlink);
      }
    }
  }
};

module.exports = {
  index,
  store,
  edit,
  deleteData,
  checkPegawaiSaldoCuti,
  checkJadwalPegawai,
};
