const {
  pagination,
  pegawaiKontrakHelper,
  pegawaiKontrakHelper: { getPegawaiKontrak },
  pegawaiHelper,
  jabatanHelper,
  jabatanHelper: { getJabatanId },
  cabangHelper,
  unitHelper,
  komponenGajiHelper,
  unitPegawaiHelper,
  jabatanPegawaiHelper,
  pegawaiKomponenGajiHelper,
  pegawaiJadwalHelper,
} = require("../../helper/index");
const { validationResult } = require("express-validator");
const moment = require("moment");
const {
  Unit,
  Jabatan,
  PegawaiKomponenGaji,
  Pegawai,
  Client,
  Cabang,
  PegawaiJabatan,
  PegawaiUnit,
  PegawaiKontrak,
  KomponenGaji,
} = require("../../model");
const sequelize = require("../../config/db");
const readXlsxFile = require("read-excel-file/node");
const { Op } = require("sequelize");

const index = async (req, res) => {
  let user = req.user.data;
  let getPegawai = await Pegawai.findOne({
    where: {
      id_client: user.id_mapping,
    },
  });
  req.query.client_id = getPegawai.id_client;
  try {
    if (req.xhr) {
      // page
      const page =
        req.query.page == null || req.query.page == "" ? 1 : req.query.page;
      const limit =
        req.query.limit == null || req.query.limit == "" ? 10 : req.query.limit;
      const search = req.query.search;

      const halamanAkhir = page * limit;
      const halamanAwal = halamanAkhir - limit;
      const offset = halamanAkhir;
      const skip = halamanAwal;

      let id_client = user.id_mapping;
      let getFilter = req.query.filter;
      let pegawaiJabatan = await getPegawaiKontrak(
        limit,
        skip,
        null,
        getFilter,
        id_client
      );
      let model = await pegawaiKontrakHelper.PegawaiKontrak.count({
        where: {
          "$pegawai.client.id_client$": user.id_mapping,
        },
        include: [
          {
            model: Pegawai,
            include: [
              {
                model: Client,
                required: true,
              },
            ],
          },
        ],
      });
      if (getFilter != null) {
        let getModel = await getPegawaiKontrak(
          null,
          null,
          null,
          getFilter,
          id_client
        );
        model = getModel.length;
      }
      if (search != null && search != "") {
        pegawaiJabatan = await getPegawaiKontrak(
          limit,
          skip,
          search,
          getFilter,
          id_client
        );
        let getModel = await getPegawaiKontrak(
          null,
          null,
          search,
          getFilter,
          id_client
        );
        model = getModel.length;
      }

      // pagination
      const getPagination = pagination(page, model, limit);

      let keterangan = {
        from: skip + 1,
        to: offset,
        total: model,
      };

      let output = {
        data: pegawaiJabatan,
        pagination: getPagination,
        keterangan: keterangan,
      };
      return res.status(200).json({
        status: 200,
        message: "Berhasil tangkap data",
        output: output,
      });
    }

    // breadcrumb
    let breadcrumb = [];
    breadcrumb.push({ label: "Home", url: "/admin/dashboard", isActive: "" });
    breadcrumb.push({
      label: "Kontrak pegawai",
      url: "/pegawai/pegawaiKontrak",
      isActive: "active",
    });

    let id_client = null;
    if (user.jenis_mapping == "client") {
      id_client = user.id_mapping;
    }

    let pegawai = await pegawaiHelper.Pegawai.findAll({
      where: {
        id_client: id_client,
      },
    });
    let cabang = await cabangHelper.Cabang.findAll({
      where: {
        id_client: id_client,
      },
    });
    let unit = await unitHelper.Unit.findAll({
      where: {
        id_client: id_client,
      },
      order: [["id_unit", "asc"]],
    });
    let jabatan = await jabatanHelper.Jabatan.findAll({
      where: {
        id_client: id_client,
      },
    });

    let komponenGajiKaryawan = await komponenGajiHelper.KomponenGaji.findAll({
      where: {
        id_client: id_client,
        komponen: sequelize.where(
          sequelize.fn("lower", sequelize.col("komponen")),
          sequelize.fn("lower", "karyawan")
        ),
      },
    });
    let komponenGajiPerusahaan = await komponenGajiHelper.KomponenGaji.findAll({
      where: {
        id_client: id_client,
        komponen: sequelize.where(
          sequelize.fn("lower", sequelize.col("komponen")),
          sequelize.fn("lower", "perusahaan")
        ),
      },
    });
    let pegawaiKomponenGaji = await komponenGajiHelper.KomponenGaji.findAll({
      where: {
        id_client: id_client,
      },
    });

    res.render("./modulPegawai/pegawaiKontrak/index", {
      pegawai: pegawai,
      cabang: cabang,
      unit: unit,
      jabatan: jabatan,
      title: "Kontrak pegawai",
      breadcrumb: breadcrumb,
      currentUrl: req.originalUrl,
      komponenGajiKaryawan: komponenGajiKaryawan,
      komponenGajiPerusahaan: komponenGajiPerusahaan,
      id_client: id_client,
      pegawaiKomponenGaji: pegawaiKomponenGaji,
    });
  } catch (error) {
    return res.status(400).json({
      status: 400,
      message: "Terjadi kesalahan data",
      result: error.message,
    });
  }
};

const store = async (req, res) => {
  let user = req.user.data;
  let getPegawai = await Pegawai.findOne({
    where: {
      id_client: user.id_mapping,
    },
  });
  req.query.client_id = getPegawai.id_client;
  // const t = await sequelize.transaction();

  try {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({
        status: 400,
        message: "Invalid form validation",
        result: errors.array(),
      });
    } else {
      const response = req.body;
      let dateTime = moment().format("YYYY-MM-DD HH:mm:ss");
      if (response.page == "add") {
        let dataPegawaiUnit = {};
        // check pegawai unit
        await unitPegawaiHelper.PegawaiUnit.update(
          {
            is_aktif: 0,
          },
          {
            where: {
              id_pegawai: response.id_pegawai,
            },
          }
        );

        let getDataUnit = await unitHelper.getUnitById(response.id_unit);
        let membawahi_unit = getDataUnit.membawahi_unit;

        dataPegawaiUnit.id_pegawai = response.id_pegawai;
        dataPegawaiUnit.id_unit = response.id_unit;
        dataPegawaiUnit.id_cabang = response.id_cabang;
        dataPegawaiUnit.membawahi_unit = membawahi_unit;
        dataPegawaiUnit.tanggal_mulai = moment(
          response.tanggal_masuk,
          "DD-MM-YYYY"
        ).format("YYYY-MM-DD");
        dataPegawaiUnit.is_aktif = response.is_aktif;
        dataPegawaiUnit.user_create = user.id;
        dataPegawaiUnit.user_update = user.id;
        dataPegawaiUnit.time_create = dateTime;
        dataPegawaiUnit.time_update = dateTime;
        let insertPegawaiUnit = await unitPegawaiHelper.PegawaiUnit.create(
          dataPegawaiUnit,
          {
            // transaction: t,
          }
        );

        let dataPegawaiJabatan = {};
        await jabatanPegawaiHelper.PegawaiJabatan.update(
          {
            is_aktif: 0,
          },
          {
            where: {
              id_pegawai: response.id_pegawai,
            },
          }
        );
        let getDataJabatan = await jabatanHelper.getJabatanById(
          response.id_jabatan
        );
        let membawahi_jabatan = getDataJabatan.membawahi_jabatan;

        dataPegawaiJabatan.id_pegawai = response.id_pegawai;
        dataPegawaiJabatan.id_jabatan = response.id_jabatan;
        dataPegawaiJabatan.id_cabang = response.id_cabang;
        dataPegawaiJabatan.membawahi_jabatan = membawahi_jabatan;
        dataPegawaiJabatan.tanggal_mulai = moment(
          response.tanggal_masuk,
          "DD-MM-YYYY"
        ).format("YYYY-MM-DD");
        dataPegawaiJabatan.is_aktif = response.is_aktif;
        dataPegawaiJabatan.user_create = user.id;
        dataPegawaiJabatan.user_update = user.id;
        dataPegawaiJabatan.time_create = dateTime;
        dataPegawaiJabatan.time_update = dateTime;
        let insertPegawaiJabatan =
          await jabatanPegawaiHelper.PegawaiJabatan.create(dataPegawaiJabatan, {
            // transaction: t,
          });

        let dataPegawaiKontrak = {};
        await pegawaiKontrakHelper.PegawaiKontrak.update(
          {
            is_aktif: 0,
          },
          {
            where: {
              id_pegawai: response.id_pegawai,
            },
          }
        );
        let tanggal_selesai =
          response.tanggal_selesai != null && response.tanggal_selesai != ""
            ? moment(response.tanggal_selesai, "DD-MM-YYYY").format(
                "YYYY-MM-DD"
              )
            : null;

        dataPegawaiKontrak.id_pegawai = response.id_pegawai;
        dataPegawaiKontrak.id_cabang = response.id_cabang;
        dataPegawaiKontrak.id_unit = response.id_unit;
        dataPegawaiKontrak.id_jabatan = response.id_jabatan;
        dataPegawaiKontrak.jenis_kontrak = response.jenis_kontrak;
        dataPegawaiKontrak.tanggal_mulai = moment(
          response.tanggal_mulai,
          "DD-MM-YYYY"
        ).format("YYYY-MM-DD");
        dataPegawaiKontrak.tanggal_selesai = tanggal_selesai;
        dataPegawaiKontrak.is_aktif = response.is_aktif;
        dataPegawaiKontrak.user_create = user.id;
        dataPegawaiKontrak.user_update = user.id;
        dataPegawaiKontrak.time_create = dateTime;
        dataPegawaiKontrak.time_update = dateTime;
        let insertPegawaiKontrak =
          await pegawaiKontrakHelper.createPegawaiKontrak(dataPegawaiKontrak, {
            // transaction: t,
          });

        // pegawai komponen gaji
        let id_komponen_gaji = response.id_komponen_gaji;
        let insertPegawaiKomponenGaji = null;
        if (id_komponen_gaji != null && id_komponen_gaji != "") {
          id_komponen_gaji = id_komponen_gaji.split(",");
          let nominal = response.nominal;
          nominal = nominal.split(",");
          let dataKomponenGaji = [];
          id_komponen_gaji.map((v, i) => {
            dataKomponenGaji.push({
              id_pegawai_kontrak: insertPegawaiKontrak.id_pegawai_kontrak,
              id_komponen_gaji: v,
              nominal: nominal[i],
            });
          });

          insertPegawaiKomponenGaji =
            await pegawaiKomponenGajiHelper.insertBatch(dataKomponenGaji, {
              // transaction: t,
            });
        }

        // await t.commit();
        if (
          insertPegawaiJabatan ||
          insertPegawaiUnit ||
          insertPegawaiKontrak ||
          insertPegawaiKomponenGaji
        ) {
          return res.status(200).json({
            status: 200,
            message: "Berhasil insert data",
            result: response,
          });
        } else {
          return res.status(400).json({
            status: 400,
            message: "Gagal insert data",
          });
        }
      } else {
        let id_pegawai_kontrak = response.id_pegawai_kontrak;
        let id_pegawai = response.id_pegawai;
        let is_aktif = response.is_aktif;

        const getPegawaiKontrak =
          await pegawaiKontrakHelper.getPegawaiKontrakId(
            id_pegawai_kontrak,
            id_pegawai,
            is_aktif
          );

        let id_pegawai_unit =
          getPegawaiKontrak.unit.pegawai_units[0].id_pegawai_unit;
        let id_pegawai_jabatan =
          getPegawaiKontrak.jabatan.pegawai_jabatans[0].id_pegawai_jabatan;

        let dataPegawaiUnit = {};
        // check pegawai unit
        await unitPegawaiHelper.PegawaiUnit.update(
          {
            is_aktif: 0,
          },
          {
            where: {
              id_pegawai: response.id_pegawai,
            },
          }
        );

        let getDataUnit = await unitHelper.getUnitById(response.id_unit);
        let membawahi_unit = getDataUnit.membawahi_unit;

        dataPegawaiUnit.id_pegawai = response.id_pegawai;
        dataPegawaiUnit.id_unit = response.id_unit;
        dataPegawaiUnit.id_cabang = response.id_cabang;
        dataPegawaiUnit.membawahi_unit = membawahi_unit;
        dataPegawaiUnit.tanggal_mulai = moment(
          response.tanggal_masuk,
          "DD-MM-YYYY"
        ).format("YYYY-MM-DD");
        dataPegawaiUnit.is_aktif = response.is_aktif;
        dataPegawaiUnit.user_create = user.id;
        dataPegawaiUnit.user_update = user.id;
        dataPegawaiUnit.time_create = dateTime;
        dataPegawaiUnit.time_update = dateTime;

        let updatePegawaiUnit = await unitPegawaiHelper.PegawaiUnit.update(
          dataPegawaiUnit,
          {
            where: {
              id_pegawai_unit: id_pegawai_unit,
            },
            // transaction: t,
          }
        );

        let dataPegawaiJabatan = {};
        await jabatanPegawaiHelper.PegawaiJabatan.update(
          {
            is_aktif: 0,
          },
          {
            where: {
              id_pegawai: response.id_pegawai,
            },
          }
        );
        let getDataJabatan = await jabatanHelper.getJabatanById(
          response.id_jabatan
        );
        let membawahi_jabatan = getDataJabatan.membawahi_jabatan;

        dataPegawaiJabatan.id_pegawai = response.id_pegawai;
        dataPegawaiJabatan.id_jabatan = response.id_jabatan;
        dataPegawaiJabatan.id_cabang = response.id_cabang;
        dataPegawaiJabatan.membawahi_jabatan = membawahi_jabatan;
        dataPegawaiJabatan.tanggal_mulai = moment(
          response.tanggal_masuk,
          "DD-MM-YYYY"
        ).format("YYYY-MM-DD");
        dataPegawaiJabatan.is_aktif = response.is_aktif;
        dataPegawaiJabatan.user_create = user.id;
        dataPegawaiJabatan.user_update = user.id;
        dataPegawaiJabatan.time_create = dateTime;
        dataPegawaiJabatan.time_update = dateTime;
        let updatePegawaijabatan =
          await jabatanPegawaiHelper.PegawaiJabatan.update(dataPegawaiJabatan, {
            where: {
              id_pegawai_jabatan: id_pegawai_jabatan,
            },
            // transaction: t,
          });

        let dataPegawaiKontrak = {};
        await pegawaiKontrakHelper.PegawaiKontrak.update(
          {
            is_aktif: 0,
          },
          {
            where: {
              id_pegawai: response.id_pegawai,
            },
          }
        );
        let tanggal_selesai =
          response.tanggal_selesai != null && response.tanggal_selesai != ""
            ? moment(response.tanggal_selesai, "DD-MM-YYYY").format(
                "YYYY-MM-DD"
              )
            : null;
        dataPegawaiKontrak.id_pegawai = response.id_pegawai;
        dataPegawaiKontrak.id_cabang = response.id_cabang;
        dataPegawaiKontrak.id_unit = response.id_unit;
        dataPegawaiKontrak.id_jabatan = response.id_jabatan;
        dataPegawaiKontrak.jenis_kontrak = response.jenis_kontrak;
        dataPegawaiKontrak.tanggal_mulai = moment(
          response.tanggal_mulai,
          "DD-MM-YYYY"
        ).format("YYYY-MM-DD");
        dataPegawaiKontrak.tanggal_selesai = tanggal_selesai;
        dataPegawaiKontrak.is_aktif = response.is_aktif;
        dataPegawaiKontrak.user_create = user.id;
        dataPegawaiKontrak.user_update = user.id;
        dataPegawaiKontrak.time_create = dateTime;
        dataPegawaiKontrak.time_update = dateTime;
        let updatePegawaiKontrak =
          await pegawaiKontrakHelper.updatePegawaiKontrak(
            dataPegawaiKontrak,
            id_pegawai_kontrak,
            {
              // transaction: t,
            }
          );

        // pegawai komponen gaji
        const countCheck = await PegawaiKomponenGaji.findAll({
          where: {
            id_pegawai_kontrak: id_pegawai_kontrak,
          },
        });

        const lengthCheck = countCheck.length;
        if (lengthCheck > 0) {
          await PegawaiKomponenGaji.destroy({
            where: {
              id_pegawai_kontrak: id_pegawai_kontrak,
            },
            // transaction: t,
          });
        }

        let id_komponen_gaji = response.id_komponen_gaji;

        let updatePegawaiKomponenGaji = null;
        if (id_komponen_gaji != null && id_komponen_gaji != "") {
          id_komponen_gaji = id_komponen_gaji.split(",");
          let nominal = response.nominal;
          nominal = nominal.split(",");
          let dataKomponenGaji = [];
          id_komponen_gaji.map((v, i) => {
            dataKomponenGaji.push({
              id_pegawai_kontrak: id_pegawai_kontrak,
              id_komponen_gaji: v,
              nominal: nominal[i],
            });
          });

          updatePegawaiKomponenGaji =
            await pegawaiKomponenGajiHelper.insertBatch(dataKomponenGaji, {
              // transaction: t,
            });
        }

        // await t.commit();

        if (
          updatePegawaijabatan ||
          updatePegawaiUnit ||
          updatePegawaiKontrak ||
          updatePegawaiKomponenGaji
        ) {
          return res.status(200).json({
            status: 200,
            message: "Berhasil update data",
            result: response,
          });
        } else {
          return res.status(400).json({
            status: 400,
            message: "Gagal update data",
          });
        }
      }
    }
  } catch (error) {
    // await t.rollback();
    return res.status(400).json({
      status: 400,
      message: "Terjadi kesalahan data",
      result: error.message,
    });
  }
};

const edit = async (req, res) => {
  try {
    const id_pegawai_kontrak = req.params.id_pegawai_kontrak;

    const getPegawai = await pegawaiKontrakHelper.getPegawaiKontrakId(
      id_pegawai_kontrak
    );

    const id_pegawai = getPegawai.id_pegawai;
    const is_aktif = getPegawai.is_aktif;

    const getPegawaiKontrak = await pegawaiKontrakHelper.getPegawaiKontrakId(
      id_pegawai_kontrak,
      id_pegawai,
      is_aktif
    );
    if (getPegawaiKontrak) {
      return res.status(200).json({
        status: 200,
        message: "Berhasil mengambil data pegawai kontrak",
        result: getPegawaiKontrak,
      });
    } else {
      return res.status(400).json({
        status: 400,
        message: "Gagal mengambil data pegawai kontrak",
      });
    }
  } catch (error) {
    return res.status(400).json({
      status: 400,
      message: "Terjadi kesalahan data",
      result: error.message,
    });
  }
};

const detail = async (req, res) => {
  try {
    const id_pegawai_kontrak = req.params.id_pegawai_kontrak;
    const getPegawai = await pegawaiKontrakHelper.getPegawaiKontrakId(
      id_pegawai_kontrak
    );
    const id_pegawai = getPegawai.id_pegawai;
    const is_aktif = getPegawai.is_aktif;

    const getPegawaiKontrak = await pegawaiKontrakHelper.getPegawaiKontrakId(
      id_pegawai_kontrak,
      id_pegawai,
      is_aktif
    );
    if (getPegawaiKontrak) {
      return res.status(200).json({
        status: 200,
        message: "Berhasil mengambil data pegawai kontrak",
        result: getPegawaiKontrak,
      });
    } else {
      return res.status(400).json({
        status: 400,
        message: "Gagal mengambil data pegawai kontrak",
      });
    }
  } catch (error) {
    return res.status(400).json({
      status: 400,
      message: "Terjadi kesalahan data",
      result: error.message,
    });
  }
};

const unitPegawai = async (req, res) => {
  try {
    const id_unit = req.params.id_unit;
    const getUnit = await unitHelper.Unit.findOne({
      where: {
        id_unit: id_unit,
      },
      include: [
        {
          model: Jabatan,
          required: true,
        },
      ],
    });
    if (getUnit) {
      return res.status(200).json({
        status: 200,
        message: "Berhasil mengambil data pegawai",
        result: getUnit,
      });
    } else {
      return res.status(400).json({
        status: 400,
        message: "Gagal mengambil data pegawai",
      });
    }
  } catch (error) {
    return res.status(400).json({
      status: 400,
      message: "Terjadi kesalahan data",
      result: error.message,
    });
  }
};

const deleteData = async (req, res) => {
  // const t = await sequelize.transaction();

  try {
    const id_pegawai_kontrak = req.params.id_pegawai_kontrak;
    const getPegawai = await pegawaiKontrakHelper.getPegawaiKontrakId(
      id_pegawai_kontrak
    );

    const id_pegawai = getPegawai.id_pegawai;
    const is_aktif = getPegawai.is_aktif;
    const getPegawaiKontrak = await pegawaiKontrakHelper.getPegawaiKontrakId(
      id_pegawai_kontrak,
      id_pegawai,
      is_aktif
    );

    let id_pegawai_unit =
      getPegawaiKontrak.unit.pegawai_units[0].id_pegawai_unit;
    let id_pegawai_jabatan =
      getPegawaiKontrak.jabatan.pegawai_jabatans[0].id_pegawai_jabatan;

    const deletePegawaiUnit = await unitPegawaiHelper.PegawaiUnit.destroy({
      where: {
        id_pegawai_unit: id_pegawai_unit,
      },
      // transaction: t,
    });
    const deletePegawaiJabatan =
      await jabatanPegawaiHelper.PegawaiJabatan.destroy({
        where: {
          id_pegawai_jabatan: id_pegawai_jabatan,
        },
        // transaction: t,
      });
    const deletePegawaiKontrak =
      await pegawaiKontrakHelper.deletePegawaiKontrak(id_pegawai_kontrak, {
        // transaction: t,
      });

    // await t.commit();
    if (deletePegawaiKontrak || deletePegawaiJabatan || deletePegawaiUnit) {
      return res.status(200).json({
        status: 200,
        message: "Berhasil menghapus data kontrak pegawai",
        result: getPegawaiKontrak,
      });
    } else {
      return res.status(400).json({
        status: 400,
        message: "Gagal menghapus data kontrak pegawai",
      });
    }
  } catch (error) {
    // await t.rollback();
    return res.status(400).json({
      status: 400,
      message: "Terjadi kesalahan data",
      result: error.message,
    });
  }
};

const jabatan = async (req, res) => {
  try {
    const { id_jabatan, client_id } = req.query;
    let getJabatan = await jabatanHelper.Jabatan.findOne({
      where: {
        id_jabatan: id_jabatan,
        id_client: client_id,
      },
    });
    let jabatanPass = getJabatan.membawahi_jabatan;
    let selectJabatan = jabatanPass.split(",");
    return res.json({
      status: 200,
      message: "Berhasil mengambil data jabatan",
      result: selectJabatan,
    });
  } catch (error) {
    return res.status(400).json({
      status: 400,
      message: "Terjadi kesalahan data",
      result: error.message,
    });
  }
};

const getFindInJabatan = async (req, res) => {
  try {
    if (req.xhr) {
      const { jabatan_id } = req.query;
      const getJabatan = await getJabatanId(jabatan_id);
      if (getJabatan) {
        return res.status(200).json({
          status: 200,
          message: "Berhasil mendapatkan data jabatan",
          result: getJabatan,
        });
      } else {
        return res.status(400).json({
          status: 400,
          message: "Gagal mendapatkan data jabatan",
          result: getJabatan,
        });
      }
    }
  } catch (error) {
    return res.status(400).json({
      status: 400,
      message: "Terjadi kesalahan data",
      result: error.message,
    });
  }
};

const importData = async (req, res) => {
  // const t = await sequelize.transaction();

  try {
    let user = req.user.data;
    let getPegawai = await Pegawai.findOne({
      where: {
        id_client: user.id_mapping,
      },
    });
    req.query.client_id = getPegawai.id_client;
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({
        status: 400,
        message: "Invalid form validation",
        result: errors.array(),
      });
    }

    const { importData } = req.body.importData;

    readXlsxFile(importData.path).then(async (rows) => {
      let pushPegawai = [];
      let pushPegawaiDetail = [];
      let dataInduk = [];
      let getPegawai = null;
      rows.map(async (v, i) => {
        if (i > 1) {
          if (v[1] != null) {
            dataInduk.push(v);
            pushPegawai.push(updateImportData(v, user, t));
          }

          if (v[9] != null) {
            let getKomponenGaji = await KomponenGaji.findOne({
              where: {
                komponen_gaji: {
                  [Op.iLike]: "%" + v[9] + "%",
                },
                id_client: user.id_mapping,
              },
            });

            if (v[1] != null) {
              getPegawai = v[1];
            }

            pushPegawaiDetail.push({
              [getPegawai]: {
                id_pegawai_kontrak: null,
                id_komponen_gaji: getKomponenGaji.id_komponen_gaji,
                nominal: v[10],
              },
            });
          }
        }
      });

      Promise.all(pushPegawai).then(async (result) => {
        let pushPegawaiJabatan = [];
        let pushPegawaiUnit = [];
        let pushPegawaiKontrak = [];
        let dateTime = moment().format("YYYY-MM-DD HH:mm:ss");
        result.map((v, i) => {
          const { pegawai, unit, jabatan } = v;
          let getData = dataInduk[i];

          // pegawai jabatan
          let pegawaiJabatan = {
            id_pegawai: pegawai.id_pegawai,
            id_jabatan: jabatan.id_jabatan,
            id_cabang: unit.id_cabang,
            membawahi_jabatan: jabatan.membawahi_jabatan,
            tanggal_mulai: getData[7],
            tanggal_selesai: getData[8],
            is_aktif: 1,
            user_create: user.id,
            user_update: user.id,
            time_create: dateTime,
            time_update: dateTime,
          };
          pushPegawaiJabatan.push(pegawaiJabatan);

          // pegawai unit
          let pegawaiUnit = {
            id_pegawai: pegawai.id_pegawai,
            id_unit: unit.id_unit,
            id_cabang: unit.id_cabang,
            membawahi_unit: unit.membawahi_unit,
            tanggal_mulai: getData[7],
            tanggal_selesai: getData[8],
            is_aktif: 1,
            user_create: user.id,
            user_update: user.id,
            time_create: dateTime,
            time_update: dateTime,
          };
          pushPegawaiUnit.push(pegawaiUnit);

          // pegawai kontrak
          let pegawaiKontrak = {
            id_pegawai: pegawai.id_pegawai,
            id_cabang: unit.id_cabang,
            id_unit: unit.id_unit,
            id_jabatan: jabatan.id_jabatan,
            jenis_kontrak: getData[6].toLowerCase(),
            tanggal_mulai: getData[7],
            tanggal_selesai: getData[8],
            berkas: null,
            is_aktif: 1,
            user_create: user.id,
            user_update: user.id,
            time_create: dateTime,
            time_update: dateTime,
          };
          pushPegawaiKontrak.push(pegawaiKontrak);
        });

        // insert pegawai kontrak
        await PegawaiJabatan.bulkCreate(pushPegawaiJabatan, {
          // transaction: t,
        });
        await PegawaiUnit.bulkCreate(pushPegawaiUnit, {
          // transaction: t,
        });
        let insertPegawaiKontrak = await PegawaiKontrak.bulkCreate(
          pushPegawaiKontrak,
          {
            // transaction: t,
          }
        );

        let pushPegawaiKomponenGaji = [];
        pushPegawaiDetail.map((v, i) => {
          Object.keys(v).map(async (vKey, iKey) => {
            pushPegawaiKomponenGaji.push(
              arrPegawaiKomponenGaji(vKey, v, insertPegawaiKontrak, user)
            );
          });
        });

        Promise.all(pushPegawaiKomponenGaji).then(async (result) => {
          let pushDb = [];
          result.map((v, i) => {
            pushDb.push(...v);
          });
          let insertPegawaiKomponenGaji = await PegawaiKomponenGaji.bulkCreate(
            pushDb,
            {
              // transaction: t,
            }
          );

          // await t.commit();
          if (insertPegawaiKomponenGaji || insertPegawaiKontrak) {
            return res.status(200).json({
              status: 200,
              message:
                "Berhasil import " +
                insertPegawaiKontrak.length +
                "data kontrak pegawai",
              result: req.body,
            });
          } else {
            return res.status(400).json({
              status: 400,
              message:
                "Gagal import " +
                insertPegawaiKontrak.length +
                "data kontrak pegawai",
            });
          }
        });
      });
    });
  } catch (error) {
    // await t.rollback();
    return res.status(400).json({
      status: 400,
      message: "Terjadi kesalahan data",
      result: error.message,
    });
  }
};

const arrPegawaiKomponenGaji = async (vKey, v, insertPegawaiKontrak, user) => {
  let pushDb = [];
  let getPegawai = await Pegawai.findOne({
    where: {
      nama_lengkap: sequelize.where(
        sequelize.fn("lower", sequelize.col("nama_lengkap")),
        sequelize.fn("lower", vKey)
      ),
      id_client: user.id_mapping,
    },
  });

  insertPegawaiKontrak.map((vKontrak, iKontrak) => {
    if (vKontrak.id_pegawai == getPegawai.id_pegawai) {
      dataValue = {
        ...v[vKey],
        id_pegawai_kontrak: vKontrak.id_pegawai_kontrak,
      };
      pushDb.push(dataValue);
    }
  });
  return pushDb;
};

const updateImportData = async (v, user, t) => {
  let getPegawai = await Pegawai.findOne({
    where: {
      nama_lengkap: {
        [Op.iLike]: "%" + v[1] + "%",
      },
      id_client: user.id_mapping,
    },
  });
  let getCabang = await Cabang.findOne({
    where: {
      nama_cabang: {
        [Op.iLike]: "%" + v[3] + "%",
      },
    },
  });

  let getUnit = await Unit.findOne({
    where: {
      nama_unit: {
        [Op.iLike]: "%" + v[4] + "%",
      },
      id_client: user.id_mapping,
      id_cabang: getCabang.id_cabang,
    },
  });

  let getJabatan = await Jabatan.findOne({
    where: {
      nama_jabatan: {
        [Op.iLike]: "%" + v[5] + "%",
      },
      id_client: user.id_mapping,
      "$unit.cabang.id_cabang$": getCabang.id_cabang,
    },
    include: [
      {
        model: Unit,
        include: [
          {
            model: Cabang,
          },
        ],
      },
    ],
  });

  // update
  let timeUpdate = moment().format("YYYY-MM-DD HH:mm:ss");
  let updatePegawaiKontrak = await PegawaiKontrak.update(
    {
      is_aktif: 0,
      time_update: timeUpdate,
      user_update: user.id,
    },
    {
      where: {
        id_pegawai: getPegawai.id_pegawai,
      },
      // transaction: t,
    }
  );
  let updatePegawaiJabatan = await PegawaiJabatan.update(
    {
      is_aktif: 0,
      time_update: timeUpdate,
      user_update: user.id,
    },
    {
      where: {
        id_pegawai: getPegawai.id_pegawai,
      },
      // transaction: t,
    }
  );
  let updatePegawaiUnit = await PegawaiUnit.update(
    {
      is_aktif: 0,
      time_update: timeUpdate,
      user_update: user.id,
    },
    {
      where: {
        id_pegawai: getPegawai.id_pegawai,
      },
      // transaction: t,
    }
  );

  return {
    pegawai: getPegawai,
    unit: getUnit,
    jabatan: getJabatan,
  };
};

module.exports = {
  index,
  store,
  edit,
  deleteData,
  jabatan,
  getFindInJabatan,
  unitPegawai,
  detail,
  importData,
};
