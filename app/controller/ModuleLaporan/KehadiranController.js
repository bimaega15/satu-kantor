const {
  laporanPegawaiJadwalHelper,
  laporanPegawaiJadwalHelper: { getLaporanPegawaiJadwal },
  pagination,
  jadwalHelper,
  pegawaiHelper,
} = require("../../helper/index");
const {
  Jadwal,
  Pegawai,
  Client,
  PegawaiJadwal,
  PegawaiUnit,
  Cabang,
  JenisAbsensi,
} = require("../../model");
const moment = require("moment");

const index = async (req, res) => {
  try {
    let user = req.user.data;
    let getPegawai = await Pegawai.findOne({
      where: {
        id_client: user.id_mapping,
      },
    });
    req.query.client_id = getPegawai.id_client;

    if (req.xhr) {
      let id_client = req.query.client_id;
      if (user.jenis_mapping == "client") {
        id_client = user.id_mapping;
      }

      // page
      const page =
        req.query.page == null || req.query.page == "" ? 1 : req.query.page;
      const limit =
        req.query.limit == null || req.query.limit == "" ? 10 : req.query.limit;
      const search = req.query.search;

      const halamanAkhir = page * limit;
      const halamanAwal = halamanAkhir - limit;
      const offset = halamanAkhir;
      const skip = halamanAwal;

      let getFilter = req.query.filter;
      let jadwalPegawai =
        await laporanPegawaiJadwalHelper.getLaporanPegawaiJadwal(
          limit,
          skip,
          null,
          getFilter,
          id_client
        );
      let model = await laporanPegawaiJadwalHelper.PegawaiJadwal.count({
        where: {
          "$pegawai.id_client$": user.id_mapping,
        },
        include: [
          {
            model: Pegawai,
          },
        ],
      });
      if (getFilter != null) {
        let getModel = await laporanPegawaiJadwalHelper.getLaporanPegawaiJadwal(
          null,
          null,
          null,
          getFilter,
          id_client
        );
        model = getModel.length;
      }

      if (search != null && search != "") {
        jadwalPegawai =
          await laporanPegawaiJadwalHelper.getLaporanPegawaiJadwal(
            limit,
            skip,
            search,
            getFilter,
            id_client
          );
        let getModel = await laporanPegawaiJadwalHelper.getLaporanPegawaiJadwal(
          null,
          null,
          search,
          getFilter,
          id_client
        );
        model = getModel.length;
      }

      // pagination
      const getPagination = pagination(page, model, limit);

      let keterangan = {
        from: skip + 1,
        to: offset,
        total: model,
      };


      let output = {
        data: jadwalPegawai,
        pagination: getPagination,
        keterangan: keterangan,
      };

      return res.status(200).json({
        status: 200,
        message: "Berhasil tangkap data",
        output: output,
      });
    }
    let id_client = req.query.client_id;
    if (user.jenis_mapping == "client") {
      id_client = user.id_mapping;
    }

    // jadwal
    let jadwal = await jadwalHelper.Jadwal.findAll({
      where: {
        id_client: user.id_mapping,
      },
    });
    let pegawai = await pegawaiHelper.Pegawai.findAll({
      where: {
        id_client: user.id_mapping,
      },
    });
    // breadcrumb
    let breadcrumb = [];
    breadcrumb.push({ label: "Home", url: "/admin/dashboard", isActive: "" });
    breadcrumb.push({
      label: "Laporan kehadiran",
      url: "/laporan/kehadiran",
      isActive: "active",
    });

    let currentDate = moment().format("DD-MM-YYYY");
    res.render("./moduleLaporan/kehadiran/index", {
      title: "Laporan kehadiran",
      breadcrumb: breadcrumb,
      currentUrl: req.originalUrl,
      jadwal: jadwal,
      pegawai: pegawai,
      tahun: moment().format("YYYY"),
      currentDate: currentDate,
    });
  } catch (error) {
    return res.status(400).json({
      status: 400,
      message: "Terjadi kesalahan data",
      result: error.message,
    });
  }
};

const detail = async (req, res) => {
  try {
    let user = req.user.data;
    let getPegawai = await Pegawai.findOne({
      where: {
        id_client: user.id_mapping,
      },
    });
    req.query.client_id = getPegawai.id_client;
    let id_pegawai_jadwal = req.params.id_pegawai_jadwal;
    let data = await PegawaiJadwal.findOne({
      where: {
        id_pegawai_jadwal: id_pegawai_jadwal,
        "$pegawai.pegawai_unit.is_aktif$": 1,
      },
      include: [
        {
          model: Pegawai,
          include: [
            {
              model: PegawaiUnit,
              include: [
                {
                  model: Cabang,
                },
              ],
            },
          ],
        },
        {
          model: Jadwal,
        },
        {
          model: JenisAbsensi,
        },
      ],
    });
    if (data) {
      return res.status(200).json({
        status: 200,
        message: "Berhasil tangkap data",
        result: data,
      });
    } else {
      return res.status(400).json({
        status: 400,
        message: "Gagal tangkap data",
      });
    }
  } catch (error) {
    return res.status(400).json({
      status: 400,
      message: "Terjadi kesalahan data",
      result: error.message,
    });
  }
};

module.exports = {
  index,
  detail,
};
