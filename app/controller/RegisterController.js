const index = async (req, res) => {
  try {
    res.render("./register/index");
  } catch (error) {
    return res.status(400).json({
      status: 400,
      message: "Terjadi kesalahan data",
      result: error.message,
    });
  }
};

module.exports = {
  index,
};
