var createError = require("http-errors");
var express = require("express");
const expressLayouts = require("express-ejs-layouts");
var path = require("path");
var cookieParser = require("cookie-parser");
var cookieSession = require("cookie-session");
var logger = require("morgan");
const session = require("express-session");
const flash = require("connect-flash");

var userRoute = require("./app/routes/index");
var adminRoute = require("./app/routes/moduleMaster/index");
var pegawaiRoute = require("./app/routes/modulePegawai/index");
var apiRoute = require("./app/routes/apiModulePegawai/index");
var laporanRoute = require("./app/routes/moduleLaporan/index");
const { webProtect, userGuide } = require("./app/middleware");

var app = express();
TZ = "Asia/Jakarta";

// Set Templating Engine
app.use(expressLayouts);
app.set("layout", "./layouts/full-width");

// view engine setup
app.set("views", path.join(__dirname, "views"));
app.set("view engine", "ejs");
app.set("layout extractScripts", true);

app.use(logger("dev"));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(
  cookieSession({
    name: "g987hg9hui",
    keys: ["34h54hj45j4", "j45j75yr"],
  })
);

app.use(express.static(path.join(__dirname, "public")));
app.use("/node_modules", express.static(path.join(__dirname, "node_modules")));
app.use(
  "/bower_components",
  express.static(path.join(__dirname, "bower_components"))
);

const oneDay = 1000 * 60 * 60 * 24;
app.use(
  session({
    secret: "code-satu-kantor",
    saveUninitialized: true,
    cookie: { maxAge: oneDay },
    resave: false,
  })
);

app.use(flash());

app.use("/", userGuide, userRoute);
app.use("/admin", webProtect, adminRoute);
app.use("/pegawai", webProtect, pegawaiRoute);
app.use("/laporan", webProtect, laporanRoute);
app.use("/api", apiRoute);

app.use("/test", function (req, res) {
  const checkRentBus = () => {
    let families = 5;
    let carryMostPassengers = 4;
    let memberFamilies = "1 5";
    memberFamilies = memberFamilies.split(" ");

    let modulo = memberFamilies.length % 2;

    let pushOutput = [];
    let except = [];
    let indexElement = null;
    for (let i = 0; i < memberFamilies.length; i++) {
      const elementI = parseInt(memberFamilies[i]);
      if (!except.includes(parseInt(i))) {
        if (parseInt(elementI) == carryMostPassengers) {
          except.push(parseInt(i));
          pushOutput.push(parseInt(elementI));
        }
      }

      for (let j = 0; j < memberFamilies.length; j++) {
        if (!except.includes(parseInt(i))) {
          if (!except.includes(parseInt(j))) {
            let index1 = parseInt(elementI);
            let index2 = parseInt(memberFamilies[j]);

            let hitungIndex = null;

            if (i != j) {
              hitungIndex = parseInt(index1) + parseInt(index2);
              if (parseInt(hitungIndex) <= carryMostPassengers) {
                except.push(parseInt(i));
                except.push(parseInt(j));
                pushOutput.push(parseInt(hitungIndex));
              }
            }

            if (modulo > 0) {
              if (hitungIndex > carryMostPassengers) {
                except.push(parseInt(i));
                pushOutput.push(parseInt(index1));
              }
            }

            if (
              memberFamilies.length - 1 == i &&
              memberFamilies.length - 1 == j
            ) {
              if (parseInt(index2) <= carryMostPassengers) {
                except.push(parseInt(i));
                pushOutput.push(parseInt(index1));
              }
            }
          }
        }
      }

      if (memberFamilies.length - 1 == i) {
        if (except.length == 0) {
          for (let k = 0; k < memberFamilies.length; k++) {
            const elementK = memberFamilies[k];
            if (parseInt(elementK) <= carryMostPassengers) {
              except.push(parseInt(k));
              pushOutput.push(parseInt(elementK));
            }
          }
        }
      }
    }

    return pushOutput;
  };
  let rentBus = checkRentBus();
  console.log(rentBus);
  return rentBus;
});

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  next(createError(404));
});

// error handler
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get("env") === "development" ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render("error");

  console.log("error message:", err.message);
});

module.exports = app;
